/* 
 *   IRC - Internet Relay Chat, include/config.h
 *   Copyright (C) 1990 Jarkko Oikarinen
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __config_include__
#define __config_include__

/*
 * IMPORTANT NOTICE
 *
 * Under normal conditions, it is not necessary to edit this file. Use the
 * configure and config scripts in the root directory instead!
 *
 * Windows is not a normal condition, edit this file if you use it.
 *
 */

/*
 * DVS CONFIGURATION HAS MOVED TO ITS OWN FILE -- dvs.h
 */

#include "setup.h"
#include "options.h"

/*****************************************************************************/

/* Protocol support.
 * SERV_CAPAB is the message sent via PROTOCTL to other servers.
 * PROTOCTL_SUPPORTED is the message sent to users on connect.
 */

#define SERV_CAPAB		"NOQUIT TOKEN SERVHASH LEV=6"
#define PROTOCTL_SUPPORTED	"MODES=8 CHANTYPES=# PREFIX=(ohv)@%+ DVS NETWORK=DarkFire"

/*****************************************************************************/

/*
 * Settings from include/options.h
 *
 * You should not need to edit these options. Use the confnig script in the
 * root directory instead. Changes made to include/options.h will not be
 * saved, you may want to edit the .options file in the root directory instead
 * and rerun config.
 */

/* DPATH
 * The location (directory) of the server's configuration files. Often the root
 * directory or the installation or the account's home dir.
 */
#ifndef DPATH
#define DPATH			"/usr/local/lib/ircd"
#endif

/* SPATH
 * The locaiton (directory of the server executable.
 */
#ifndef SPATH
#define SPATH			"/usr/local/bin/ircd"
#endif

/* LISTEN_SIZE
 * Size of the listen backlog. Some machines can handle larger amounts than
 * others. It defaults to 5, but can be raised if you are sure of the its
 * capabilities. See LISTEN(2) for more info.
 */
#ifndef LISTEN_SIZE
#define LISTEN_SIZE		5
#endif

/* MAXSENDQLENGTH
 * Max amount of internal send buffering when data is stuck (bytes)
 */
#ifndef MAXSENDQLENGTH
#define MAXSENDQLENGTH		3000000
#endif

/* BUFFERPOOL
 * Maximum size of the total of all sendq's.
 */
#ifndef BUFFERPOOL
#define BUFFERPOOL		(9 * MAXSENDQLENGTH)
#endif

/* MAXCONNECTIONS
 * The maximum number of connections to be made from the server. The bare
 * minimum is 5. This should be kept as low as possible, higher numbers
 * decrease preformance. You do not want to set this too low, however.
 *
 * If you get an FD-related error during compile or on execution, change this
 * to 256.
 *
 * This should be a fairly high number on windows systems. A good number is
 * 16384.
 *
 * Note: the maximum number of clients defaults to MAXCONNECTIONS-4.
 */
#ifndef MAXCONNECTIONS
#define MAXCONNECTIONS		1024
#endif

/* NICKNAMEHISTORYLENGTH
 * Length of history for when a user signs off or changes their nick. This is
 * used for chasing, and should be set at a minimum of 100. Higher numbers are
 * recommended, such as 1000.
 * Note: the higher the number this is, the more memory IRCd will use
 */
#ifndef NICKNAMEHISTORYLENGTH
#define NICKNAMEHISTORYLENGTH	1000
#endif

/* SHOWOPERS
 * Allow users to do /stats o and see the IRC Operators.
 * #define SHOWOPERS
 */

/* If your server is acting as a Hub, enable this option. DarkFire servers
 * should check with the ServApp team. If you are a leaf, do not enable this
 * for preformance reasons.
 * #define HUB
 */

/* CRYPT_OPER_PASSWORD
 * CRYPT_LINK_PASSWORD
 * Encrypt passwords for operators and/or N:lines. C:lines may not be encrypted
 * Passwords may different between a C/N pair as long as the corresponding
 * difference is on the remote server(s)
 * #define CRYPT_OPER_PASSWORD
 * #define CRYPT_LINK_PASSWORD
 */

/* NO_REMOTE_KLINE
 * NO_REMOTE_REHASH
 * Disable operators with specific flags on other servers to be able to remotely
 * kline users on your server and/or rehash. By default, opers with appropriate
 * access can remote kline/rehash (changed in df-1.2.0)
 * #define NO_REMOTE_KLINE
 * #define NO_REMOTE_REHASH
 */

/* USE_KLINE_LOGGING
 * Save a log file of klines/zlines made on your server.
 * #define USE_KLINE_LOGGING
 */

/* SERVICES_NAME
 * The name of network services. Important for many access checks.
 */
#ifndef SERVICES_NAME
#define SERVICES_NAME		"services.darkfire.net"
#endif

/* KLINE_ADDRESS
 * Address to be displayed to users when they are banned from your server.
 * This should be a valid email address where users can find information.
 *
 * DarkFire servers should set this to kline@darkfire.net.
 */
#ifndef KLINE_ADDRESS
#define KLINE_ADDRESS		"clueless-admin@poorly.configured.server"
#endif

/* AKILL_ADDR
 * Address to be displayed to users (website) after they are Auto-killed. The
 * akill reason will also be displayed. DarkFire servers will set this to
 * http://help.darkfire.net so users can use the ticketing system.
 *
 * #define AKILL_ADDR		"http://www.my.net/akillinfo"
 */

/* OLD_ILINES
 * Forces IRCd to use the older I:line logic. See doc/Ilines for more info.
 * Strongly discouraged.
 *
 * #define OLD_ILINES 1
 */

/* ALLOW_SA_CMD_WITHSERV
 * Allows SA's with a special oper flag (o:line flag) to use commands like
 * /samode and /akill while services are online
 *
 * #undef ALLOW_SA_CMD_WITHSERV
 */

/* EXEMPT_OPERS_FROM_EXFLOOD
 * Exempt opers from being killed by Excess Flood
 *
 * #define EXEMPT_OPERS_FROM_EXFLOOD
 */

/* OPERS_CAN_FLOOD
 * Allow opers with super flag to bypass throttling when they hav ea special
 * usermode set.
 *
 * #define OPERS_CAN_FLOOD
 */

/* HELP_MESSAGE
 * Message sent to users when they do /help. DarkFire servers - don't change.
 */
#ifndef HELP_MESSAGE
#define HELP_MESSAGE	"Please join #darkfire for help. (Entre a #darkfire para ayuda.)"
#endif

/*****************************************************************************/

/*
 * Other definitions and settings
 *
 * Many of these are not explained and will not need to be changed. For more
 * information, use common sense.
 */

/* AKILLEXPIRE_LIMIT
 * Minimum amount of time to wait before next akill expire.
 */
#define	AKILLEXPIRE_LIMIT	60

/* AUTOCONNECT_LIMIT
 * Hard limit in seconds between autoconnect attempts
 */
#define	AUTOCONNECT_LIMIT	20

/* VALLOC
 * May cause problems on many systems, not required. Optional if you have
 * valloc(3)
 */
#undef	VALLOC

/* USE_VARARGS
 * If your system supports the vsprintf(), vprintf(), and vscanf() C calls,
 * you can define this instead of internal argument processing.
 */
#undef	USE_VARARGS

/* DEBUGMODE
 * Enable debugging code.
 */
#undef	DEBUGMODE

/* FORCE_CORE
 * Automatically "ulimit core", forcing the server to dump a core file whenver
 * it has a fatal error. Strongly recommended.
 */
#define FORCE_CORE

/* PATHS
 * Various paths, may be relative to DPATH
 */
#define	CPATH			"ircd.conf"	/* server configuration file */
#define	MPATH			"ircd.motd"	/* server MOTD file */
#define LPATH			"debug.log"	/* server DEBUGMODE file */
#define PPATH			"ircd.pid"	/* server pid file */

/* FNAME_OPERLOG
 * Keep a log file of every successful /oper. Either full path or relative to
 * to DPATH. Logging will not occur if the file does not exist or this is
 * undefined.
 * #define FNAME_OPERLOG "opers.log"
 */
#undef	FNAME_OPERLOG

/* FAILOPER_WARN
 * Send message to clients that their attempt has been logged after an
 * unsuccessful /oper attempt
 */
#define FAILOPER_WARN

/* CHROOTDIR
 * Define for added security. All files accessed most be in DPATH.
 * You may also want to define IRC_UID and IRC_GID
 * #define CHROOTDIR
 */

/* IRC_UID
 * IRC_GID
 * Define this if you a starting the server as root but want to run it as
 * a different user/group.
 */
#undef IRC_UID
#undef IRC_GID

/* CONNECTFREQUENCY
 * PINGFREQUENCY
 * MAXIMUM_LINKS 
 * Defaults for the first class (default)
 */
#define CONNECTFREQUENCY	600
#define PINGFREQUENCY		120
#define	MAXIMUM_LINKS		1

/* CLIENT_FLOOD
 * Set the number of bytes a user can send to the server without being
 * processed before disconnecting the client.
 */
#define CLIENT_FLOOD		8000

/* Network services names
 */
#define	NickServ		"NickServ"
#define	ChanServ		"ChanServ"
#define MemoServ		"MemoServ"
#define OperServ		"OperServ"

/* SHOWCONNECTINFO
 * Show clients the progress of their connection.
 */
#define	SHOWCONNECTINFO

/* Misc.
 * If you don't know what these are, don't touch them
 */
#undef	VMS			/* Should work for IRC client, not server */
#undef	MAIL50			/* If you're running VMS 5.0 */
#undef	PCS			/* PCS Cadmus MUNIX, use with BSD flag! */

/* MALLOC_FLAGS_EXTRA
 * Additional flags to give FreeBSD's malloc. Only change this if you know
 * what you're doing.
 */
#define	MALLOC_FLAGS_EXTRA	""

/* CMDLINE_CONFIG
 * Allows conf file to be specified on command line.
 * NOTE: defining CMDLINE_CONFIG and installing ircd SUID or SGID is a MAJOR
 *       security problem - they can use the "-f" option to read any files
 *       that the 'new' access lets them. Note also that defining this is
 *       a major security hole if your ircd goes down and some other user
 *       starts up the server with a new conf file that has some extra
 *       O-lines. So don't use this unless you're debugging.
 */
#undef	CMDLINE_CONFIG

/* M4_PREPROC
 * To use m4 as a preprocessor on the ircd.conf file, define M4_PREPROC.
 * The server will then call m4 each time it reads the ircd.conf file,
 * reading m4 output as the server's ircd.conf file.
 */
#undef	M4_PREPROC

/* USE_SYSLOG
 * If you wish to have the server send 'vital' messages about server
 * through syslog, define USE_SYSLOG. Only system errors and events critical
 * to the server are logged. It is not recommended that this option is used 
 * unless you tell the system administrator beforehand and obtain their 
 * permission to send messages to the system log files.
 */
#undef  USE_SYSLOG

#ifdef  USE_SYSLOG
/* Define or undef what should be logged */
#undef  SYSLOG_KILL     /* log all operator kills to syslog */
#undef  SYSLOG_SQUIT    /* log all remote squits for all servers to syslog */
#undef  SYSLOG_CONNECT  /* log remote connect messages for other all servs */
#undef  SYSLOG_USERS    /* send userlog stuff to syslog */
#undef  SYSLOG_OPER     /* log all users who successfully become an Op */

/* If you want to log to a different facility than DAEMON, change this */
#define LOG_FACILITY LOG_DAEMON
#endif

/* End USE_SYSLOG */

/*****************************************************************************/

/* 
 * Settings you should probably not change.
 *
 * Do not continue unless you are absolutely sure you know what you are doing!
 */

/* PORTNUM
 * Fallback port number. Must be greater than 1024 if not run as root. DarkFire
 * servers should use 5550.
 */
#define	PORTNUM			5550

/* TIMESEC
 * Interval to wait for connections to be received. If no received then check
 * pings and connections
 */
#define	TIMESEC			60

/* Quick Netsplit Fix
 * If a server has been connected for HANGONGOODLINK seconds and disconnects,
 * don't wait the full CONNECTFREQUENCY time to reconnect to this good server.
 * Hang only long enough to give the net time to sync (HANGONRETRYDELAY).
 */
#define	HANGONRETRYDELAY	20
#define HANGONGOODLINK		300

/* CONNECTTIMEOUT
 * Number of seconds to wait for connect(2) call to complete. Must be at least
 * 10. Connecting clients have CONNECTTIMEOUT - 10 seconds for its host to
 * respond to ident lookup and for a DNS answer to be retrieved.
 */
#define	CONNECTTIMEOUT		30

/* KILLCHASETIMELIMIT
 * Max time for nickname change to still cause kill by automatically switching
 * to new nick (seconds).
 */
#define	KILLCHASETIMELIMIT	90

/* MAXCHANNELSPERUSER
 * Default max channels value. Real value must be no greater than 16.
 */
#define	MAXCHANNELSPERUSER	10

/* AUTO_UMODES
 */
#define	AUTO_UMODES		UMODE_DISGUISE

/*****************************************************************************/

/*
 * End of configuration section.
 */

#define	MOTD DPATH"/"MPATH
#define MYNAME SPATH
#define CONFIGFILE DPATH"/"CPATH
#define IRCD_PIDFILE DPATH"/"PPATH

#ifdef  __osf__
#define OSF
/* OSF defines BSD to be its version of BSD */
#undef BSD
#include <sys/param.h>
#ifndef BSD
#define BSD
#endif
#endif

#ifdef _SEQUENT_                /* Dynix 1.4 or 2.0 Generic Define.. */
#undef BSD
#define SYSV                    /* Also #define SYSV */
#endif

#ifdef  ultrix
#define ULTRIX
#endif

#ifdef  __hpux
#define HPUX
#endif

#ifdef  sgi
#define SGI
#endif

#ifdef  CLIENT_COMPILE
#undef  NPATH           /* _dl */
#endif

#ifdef DEBUGMODE
extern  void    debug();
# define Debug(x) debug x
# define LOGFILE LPATH
#else
# define Debug(x) ;
# if VMS
#       define LOGFILE "NLA0:"
# else
#       define LOGFILE "/dev/null"
# endif
#endif

#ifndef ENABLE_SUMMON
#  undef LEAST_IDLE
#endif

#if defined(mips) || defined(PCS)
#undef SYSV
#endif

#ifdef MIPS
#undef BSD
#define BSD             1       /* mips only works in bsd43 environment */
#endif

#ifdef sequent                   /* Dynix (sequent OS) */
#define SEQ_NOFILE    128        /* set to your current kernel impl, */
#endif                           /* max number of socket connections */

#ifdef _SEQUENT_
#define DYNIXPTX
#endif

#ifdef  BSD_RELIABLE_SIGNALS
# if defined(SYSV_UNRELIABLE_SIGNALS) || defined(POSIX_SIGNALS)
error You stuffed up config.h signals #defines use only one.
# endif
#define HAVE_RELIABLE_SIGNALS
#endif

#ifdef  SYSV_UNRELIABLE_SIGNALS
# ifdef POSIX_SIGNALS
error You stuffed up config.h signals #defines use only one.
# endif
#undef  HAVE_RELIABLE_SIGNALS
#endif

#ifdef  POSIX_SIGNALS
#define HAVE_RELIABLE_SIGNALS
#endif

/*
 * safety margin so we can always have one spare fd, for motd/authd or
 * whatever else.  -4 allows "safety" margin of 1 and space reserved.
 * must be a negative number!
 *#define MODMAXCLIENTS      (-4)
 */

#ifdef HAVECURSES
# define DOCURSES
#else
# undef DOCURSES
#endif

#ifdef HAVETERMCAP
# define DOTERMCAP
#else
# undef DOTERMCAP
#endif

#ifndef UNIXPORT
#undef  UNIXPORTPATH
#endif

#if defined(CLIENT_FLOOD)
#  if   (CLIENT_FLOOD > 8000)
#    define CLIENT_FLOOD 8000
#  else
#    if (CLIENT_FLOOD < 512)
error CLIENT_FLOOD needs redefining.
#    endif
#  endif
#else
error CLIENT_FLOOD undefined
#endif
#if (NICKNAMEHISTORYLENGTH < 100)
#  define NICKNAMEHISTORYLENGTH 100
#endif

/*
 * Some ugliness for AIX platforms.
 */
#ifdef AIX
# include <sys/machine.h>
# if BYTE_ORDER == BIG_ENDIAN
#  define BIT_ZERO_ON_LEFT
# endif
# if BYTE_ORDER == LITTLE_ENDIAN
#  define BIT_ZERO_ON_RIGHT
# endif
/*
 * this one is used later in sys/types.h (or so i believe). -avalon
 */
# define BSD_INCLUDES
#endif

#endif /* __config_include__ */

