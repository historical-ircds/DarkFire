/************************************************************************
 *   IRC - Internet Relay Chat, include/common.h
 *   Copyright (C) 1990 Armin Gruner
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__common_include__
#define __common_include__

#include "setup.h"
#include <time.h>

#ifdef	HAVE_SYS_PARAM_H
#include <sys/param.h>
#endif

#ifndef PROTO
#if __STDC__
#	define PROTO(x)	x
#else
#	define PROTO(x)	()
#endif
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

#define FALSE (0)
#define TRUE  (!FALSE)

#ifndef UNSURE
#define UNSURE (2)
#endif

extern	char	match PROTO((u_char *, u_char *));
#define mycmp(a,b) \
 ( (toupper((a)[0])!=toupper((b)[0])) || smycmp((a)+1,(b)+1) )
extern  u_char  smycmp PROTO((u_char *, u_char *));
extern	u_char	myncmp PROTO((u_char *, u_char *, int));
extern	void	mystrupper PROTO((u_char *));
extern	char	*strsplit PROTO((char **, char));
extern	char	*strblanksplit PROTO((char **, char));
extern	char	*strput PROTO((char *, char *));
#ifndef HAVE_INET_ADDR
extern unsigned long inet_addr PROTO((char *));
#endif

#if !defined(HAVE_INET_NTOA) || !defined(HAVE_INET_NETOF) && !defined(SCOUNIX)
#include <netinet/in.h>
#endif

#ifndef SCOUNIX /* SCO Openserver has these, but the header files are broken */
#ifndef HAVE_INET_NTOA
extern char *inet_ntoa PROTO((struct in_addr));
#endif

#ifndef HAVE_INET_NETOF
extern int inet_netof PROTO((struct in_addr));
#endif
#endif /* SCOUNIX */

extern char *myctime PROTO((time_t));
extern char *strtoken PROTO((char **, char *, char *));

#ifndef MAX
#define MAX(a, b)	((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b)	((a) < (b) ? (a) : (b))
#endif

#define DupString(x,y) do{x=MyMalloc(strlen(y)+1);(void)strcpy(x,y);}while(0)

#ifndef SCOUNIX
extern u_char tolowertab[], touppertab[];
#else
extern unsigned char tolowertab[], touppertab[]; 
	/* Typedef for u_char exists and is wrong. */
#endif

#undef tolower
#define tolower(c) (tolowertab[(u_char)(c)])

#undef toupper
#define toupper(c) (touppertab[(u_char)(c)])

#undef isalpha
#undef isdigit
#undef isxdigit
#undef isalnum
#undef isprint
#undef isascii
#undef isgraph
#undef ispunct
#undef islower
#undef isupper
#undef isspace
#undef iscntrl

extern unsigned char char_atribs[];

#define PRINT 1
#define CNTRL 2
#define ALPHA 4
#define PUNCT 8
#define DIGIT 16
#define SPACE 32
#define ALLOW 64

#define KLINE_PERM 0
#define KLINE_TEMP 1
#define KLINE_AKILL 2

#define KLINE_RET_AKILL 3
#define KLINE_RET_PERM 2
#define KLINE_RET_DELOK 1
#define KLINE_DEL_ERR 0

#define	find_client(x) hash_find_client(x)
#define find_server(x) hash_find_serv(x)

#define	iscntrl(c) (char_atribs[(u_char)(c)]&CNTRL)
#define isallowed(c) (char_atribs[(u_char)(c)]&ALLOW)
#define isalpha(c) (char_atribs[(u_char)(c)]&ALPHA)
#define isspace(c) (char_atribs[(u_char)(c)]&SPACE)
#define islower(c) ((char_atribs[(u_char)(c)]&ALPHA) && ((u_char)(c) > 0x5f))
#define isupper(c) ((char_atribs[(u_char)(c)]&ALPHA) && ((u_char)(c) < 0x60))
#define isdigit(c) (char_atribs[(u_char)(c)]&DIGIT)
#define	isxdigit(c) (isdigit(c) || 'a' <= (c) && (c) <= 'f' || \
		     'A' <= (c) && (c) <= 'F')
#define isalnum(c) (char_atribs[(u_char)(c)]&(DIGIT|ALPHA))
#define isprint(c) (char_atribs[(u_char)(c)]&PRINT)
#define isascii(c) ((u_char)(c) <= 0x7f)
#define isgraph(c) ((char_atribs[(u_char)(c)]&PRINT) && ((u_char)(c) != 0x32))
#define ispunct(c) (!(char_atribs[(u_char)(c)]&(CNTRL|ALPHA|DIGIT)))

extern char *MyMalloc();
extern void flush_connections();
extern struct SLink *find_user_link(/* struct SLink *, struct Client * */);

#ifdef __FreeBSD__
extern char *malloc_options;
#endif

extern int lu_noninv, lu_inv, lu_serv, lu_oper, 
           lu_unknown, lu_channel, lu_lu, lu_lulocal, lu_lserv, 
           lu_clu, lu_mlu, lu_cglobalu, lu_mglobalu;
           
time_t	now;

#endif /* __common_include__ */
