/************************************************************************
 *   IRC - Internet Relay Chat, include/msg.h
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__msg_include__
#define __msg_include__

#include "struct.h"

#define	NO_TOKEN	" "
#define NOTOKCOMP	' '

/* Commands with tokens */
#define MSG_PRIVATE	"PRIVMSG"
#define TOK_PRIVATE	"!"		/* 33 */
#define MSG_WHO		"WHO"
#define TOK_WHO		"\""		/* 34 */
#define MSG_WHOIS	"WHOIS"
#define TOK_WHOIS	"#"		/* 35 */
#define MSG_WHOWAS	"WHOWAS"
#define TOK_WHOWAS	"$"		/* 36 */
// TOKEN % available (37) [USER]
#define MSG_NICK	"NICK"
#define TOK_NICK	"&"		/* 38 */
#define MSG_SERVER	"SERVER"
#define TOK_SERVER	"'"		/* 39 */
#define MSG_ALLOW	"ALLOW"
#define TOK_ALLOW	"("		/* 40 */
#define MSG_TOPIC	"TOPIC"
#define TOK_TOPIC	")"		/* 41 */
#define MSG_INVITE	"INVITE"
#define TOK_INVITE	"*"		/* 42 */
// TOKEN + available (43) [VERSION]
#define MSG_QUIT	"QUIT"
#define TOK_QUIT	","		/* 44 */
#define MSG_SQUIT	"SQUIT"
#define TOK_SQUIT	"-"		/* 45 */
#define MSG_KILL	"KILL"
#define TOK_KILL	"."		/* 46 */
// TOKEN / available (47) [INFO]
// TOKEN 0 available (48) [LINKS]
// TOKEN 1 available (49) [MAP]
#define MSG_STATS	"STATS"
#define TOK_STATS	"2"		/* 50 */
// TOKEN 3 available (51) [RKILL]
// TOKEN 4 available (52) [HELP]
#define MSG_ERROR	"ERROR"
#define TOK_ERROR	"5"		/* 53 */
#define MSG_AWAY	"AWAY"
#define TOK_AWAY	"6"		/* 54 */
#define MSG_CONNECT	"CONNECT"
#define TOK_CONNECT	"7"		/* 55 */
#define MSG_PING	"PING"
#define TOK_PING	"8"		/* 56 */
#define MSG_PONG	"PONG"
#define TOK_PONG	"9"		/* 57 */
// TOKEN : NOT available (58)
// TOKEN ; available (59) [OPER]
// TOKEN < available (60) [PASS]
#define MSG_WALLOPS	"WALLOPS"
#define TOK_WALLOPS	"="		/* 61 */
#define MSG_TIME	"TIME"
#define TOK_TIME	">"		/* 62 */
// TOKEN ? available (63) [NAMES]
#define MSG_ADMIN	"ADMIN"
#define TOK_ADMIN	"@"		/* 64 */
// TOKEN A available (65)
#define MSG_NOTICE	"NOTICE"
#define TOK_NOTICE	"B"		/* 66 */
#define MSG_JOIN	"JOIN"
#define TOK_JOIN	"C"		/* 67 */
#define MSG_PART	"PART"
#define TOK_PART	"D"		/* 68 */
#define MSG_LUSERS	"LUSERS"
#define TOK_LUSERS	"E"		/* 69 */
#define MSG_MOTD	"MOTD"
#define TOK_MOTD	"F"		/* 70 */
#define MSG_MODE	"MODE"
#define TOK_MODE	"G"		/* 71 */
#define MSG_KICK	"KICK"
#define TOK_KICK	"H"		/* 72 */
#define MSG_DVS		"DVS"
#define TOK_DVS		"I"		/* 73 */
#define MSG_USERHOST	"USERHOST"
#define TOK_USERHOST	"J"		/* 74 */
// TOKEN K available (75) [ISON]
// TOKEN L available (76)
#define MSG_HUSH	"HUSH"
#define	TOK_HUSH	"M"		/* 77 */
#define MSG_UNHUSH	"UNHUSH"
#define	TOK_UNHUSH	"N"		/* 78 */
#define	MSG_REHASH	"REHASH"
#define TOK_REHASH	"O"		/* 79 */
// TOKEN P available (80) [RESTART]
// TOKEN Q available (81) [CLOSE]
// TOKEN R available (82) [DIE]
// TOKEN S available (83) [HASH]
// TOKEN T available (84) [DNS]
// TOKEN U available (85) [SILENCE]
#define MSG_AKILL	"AKILL"
#define TOK_AKILL	"V"		/* 86 */
#define MSG_KLINE	"KLINE"
#define TOK_KLINE	"W"		/* 87 */
#define MSG_UNKLINE	"UNKLINE"
#define TOK_UNKLINE	"X"		/* 88 */
#define MSG_RAKILL	"RAKILL"
#define TOK_RAKILL	"Y"		/* 89 */
#define MSG_GNOTICE	"GNOTICE"
#define TOK_GNOTICE	"Z"		/* 90 */
#define MSG_GOPER	"GOPER"
#define TOK_GOPER	"["		/* 91 */
// TOKEN \ available (92)
#define MSG_GLOBOPS	"GLOBOPS"
#define TOK_GLOBOPS	"]"		/* 93 */
// TOKEN ^ available (94) [LOCOPS]
// TOKEN _ available (95) [PROTOCTL]
// TOKEN ` available (96) [WATCH]
// TOKEN a available (97)
#define MSG_TRACE	"TRACE"
#define TOK_TRACE	"b"		/* 98 */
#define MSG_SQLINE	"SQLINE"
#define TOK_SQLINE	"c"		/* 99 */
#define MSG_UNSQLINE	"UNSQLINE"
#define TOK_UNSQLINE	"d"		/* 100 */
#define MSG_SVSNICK	"SVSNICK"
#define TOK_SVSNICK	"e"		/* 101 */
#define MSG_SVSNOOP	"SVSNOOP"
#define TOK_SVSNOOP	"f"		/* 102 */
// TOKEN g available (103) [IDENTIFY]
#define MSG_SVSKILL	"SVSKILL"
#define TOK_SVSKILL	"h"		/* 104 */
// TOKEN i available (105) [NICKSERV]
// TOKEN j available (106) [CHANSERV]
// TOKEN k available (107) [OPERSERV]
// TOKEN l available (108) [MEMOSERV]
// TOKEN m available (109) [SERVICES]
#define MSG_SVSMODE	"SVSMODE"
#define TOK_SVSMODE	"n"		/* 110 */
// TOKEN o available (111) [SAMODE]
#define MSG_CHATOPS	"CHATOPS"
#define TOK_CHATOPS	"p"		/* 112 */
#define MSG_ZLINE    	"ZLINE"
#define TOK_ZLINE	"q"		/* 113 */
#define MSG_UNZLINE  	"UNZLINE"
#define TOK_UNZLINE	"r"		/* 114 */
// TOKEN s available (115)
// TOKEN t available (116)
#define	MSG_UNBANME	"UNBANME"
#define MSG_RMBAN	"RMBAN"
#define	TOK_UNBANME	"u"		/* 117 */
// TOKEN v available (118) [MKILL]
#define	MSG_FQUIT	"FQUIT"
#define	TOK_FQUIT	"w"		/* 119 */
// TOKEN x available (120) [QOTD]
// TOKEN y available (121) [SAJOIN]
#define MSG_CJOIN	"CJOIN"	
#define TOK_CJOIN	"z"		/* 122 */
// TOKEN { available (123) [CONTRIB]
// TOKEN | available (124) [SET]
// TOKEN } available (125) [WKILL]
// TOKEN ~ available (126) [LANG]

/* Commands without tokens (and their old tokens) */
#define MSG_USER	"USER"		/* % (37) */
#define MSG_LIST	"LIST"		/* ( (40) */
#define MSG_VERSION	"VERSION"	/* + (43) */
#define MSG_INFO	"INFO"		/* / (47) */
#define MSG_LINKS	"LINKS"		/* 0 (48) */
#define MSG_MAP		"MAP"		/* 1 (49) */
#define MSG_RKILL	"RKILL"		/* 3 (51) */
#define MSG_HELP	"HELP"		/* 4 (52) */
#define MSG_OPER	"OPER"		/* ; (59) */
#define MSG_PASS	"PASS"		/* < (60) */
#define MSG_NAMES	"NAMES"		/* ? (63) */
#define MSG_ISON	"ISON"		/* K (75) */
#define MSG_RESTART	"RESTART"	/* P (80) */
#define MSG_CLOSE	"CLOSE"		/* Q (81) */
#define MSG_DIE		"DIE"		/* R (82) */
#define MSG_HASH	"HASH"		/* S (83) */
#define MSG_DNS		"DNS"		/* T (84) */
#define MSG_SILENCE	"SILENCE"	/* U (85) */
#define MSG_LOCOPS	"LOCOPS"	/* ^ (94) */
#define MSG_PROTOCTL	"PROTOCTL"	/* _ (95) */
#define MSG_WATCH	"WATCH"		/* ` (96) */
#define MSG_IDENTIFY	"IDENTIFY"	/* g (103) */
#define MSG_NICKSERV	"NICKSERV"	/* i (105) */
#define MSG_NS		"NS"
#define MSG_CHANSERV	"CHANSERV"	/* j (106) */
#define MSG_CS		"CS"
#define MSG_OPERSERV	"OPERSERV"	/* k (107) */
#define MSG_OS		"OS"
#define MSG_MEMOSERV	"MEMOSERV"	/* l (108) */
#define MSG_MS		"MS"
#define MSG_SERVICES	"SERVICES"	/* m (109) */
#define MSG_SAMODE	"SAMODE"	/* o (111) */
#define MSG_MKILL	"MKILL"		/* v (118) */
#define MSG_QOTD	"QOTD"		/* x (120) */
#define MSG_SAJOIN	"SAJOIN"	/* y (121) */
#define MSG_CONTRIB	"CONTRIB"	/* { (123) */
#define MSG_SET		"SET"		/* | (124) */
#define MSG_WKILL	"WKILL"		/* } (125) */
#define MSG_LANG	"LANG"		/* ~ (126) */

extern int m_private(), m_topic(), m_sajoin(), m_cjoin(), m_join(), m_part(), m_mode(), m_svsmode();
extern int m_ping(), m_pong(), m_wallops(), m_kick(), m_svsnick();
extern int m_nick(), m_error(), m_notice(), m_samode(), m_svsnoop();
extern int m_invite(), m_quit(), m_kill(), m_svskill();
extern int m_akill(), m_kline(), m_unkline(), m_rakill(), m_sqline(), m_set();
extern int m_zline(), m_unzline();
extern int m_goper(), m_globops(), m_locops(), m_unsqline();
extern int m_chatops(), m_rmban();
extern int m_protoctl(), m_unbanme(), m_mkill(), m_fquit(), m_qotd(), m_wkill();
extern int m_motd(), m_who(), m_whois(), m_user(), m_list();
extern int m_server(), m_contrib(), m_info(), m_links(), m_summon(), m_stats();
extern int m_users(), m_version(), m_help(), m_map();
extern int m_nickserv(), m_operserv(), m_chanserv(), m_memoserv(), m_lang();
extern int m_services(), m_identify();
extern int m_squit(), m_away(), m_connect(), m_hush(), m_unhush();
extern int m_oper(), m_pass(), m_trace();
extern int m_time(), m_names(), m_admin();
extern int m_lusers(), m_umode(), m_close();
extern int m_motd(), m_whowas(), m_silence();
extern int m_userhost(), m_ison(), m_watch();
extern int m_servset(), m_servlist();
extern int m_rehash(), m_restart(), m_die(), m_dns(), m_hash();
extern int m_noshortn(),m_noshortc(),m_noshortm(),m_noshorto();
extern int m_dvs(), m_allow();

extern Msg ms_private, ms_notice, ms_mode, ms_ison, ms_sajoin, ms_cjoin, 
	ms_join, ms_pong, ms_nick,
	ms_part, ms_quit, ms_whois, ms_ping, ms_away, ms_topic, ms_invite,
	ms_kick, ms_hush, ms_unhush, ms_protoctl, ms_watch, ms_server,
	ms_squit, ms_who, ms_whowas, ms_list, ms_names, ms_userhost,
	ms_trace, ms_pass, ms_lusers, ms_time, ms_oper, ms_connect,
	ms_version, ms_stats, ms_links, ms_map, ms_admin, ms_svsmode,
	ms_samode, ms_svskill, ms_svsnick, ms_svsnoop, ms_chanserv, ms_cs,
	ms_nickserv, ms_ns, ms_operserv, ms_os, ms_memoserv, ms_ms,
	ms_services, ms_identify, ms_help, ms_motd, ms_close, ms_silence,
	ms_akill, ms_sqline, ms_unsqline, ms_kline, ms_unkline, ms_zline,
	ms_unzline, ms_rakill, ms_gnotice, ms_goper, ms_globops, ms_chatops,
	ms_locops, ms_unbanme, ms_rmban, ms_mkill, ms_fquit, ms_kill,
	ms_qotd, ms_hash, ms_dns, ms_rehash, ms_restart, ms_die, ms_rkill,
	ms_set, ms_wkill, ms_lang, ms_dvs, ms_allow;

#ifdef MSGTAB
struct Message *msgmap[256];

Msg ms_private   = { MSG_PRIVATE, m_private,  0, 2,       TOK_PRIVATE, 0L, MSGFL_DVS };
Msg ms_notice    = { MSG_NOTICE,  m_notice,   0, 2,       TOK_NOTICE,  0L, MSGFL_DVS };
Msg ms_mode      = { MSG_MODE,    m_mode,     0, 2,       TOK_MODE,    0L, MSGFL_DVS };
Msg ms_ison      = { MSG_ISON,    m_ison,     0, 1,       NO_TOKEN,    0L, 0 };
Msg ms_join      = { MSG_JOIN,    m_join,     0, MAXPARA, TOK_JOIN,    0L, MSGFL_DVS };
Msg ms_cjoin     = { MSG_CJOIN,   m_cjoin,    0, MAXPARA, TOK_CJOIN,   0L, 0 };
Msg ms_sajoin    = { MSG_SAJOIN,  m_sajoin,   0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_pong      = { MSG_PONG,    m_pong,     0, MAXPARA, TOK_PONG,    0L, MSGFL_DVS|MSGFL_HUSH };
Msg ms_nick      = { MSG_NICK,    m_nick,     0, MAXPARA, TOK_NICK,    0L, MSGFL_DVS };
Msg ms_part      = { MSG_PART,    m_part,     0, 2,       TOK_PART,    0L, MSGFL_HUSH };
Msg ms_quit      = { MSG_QUIT,    m_quit,     0, 1,       TOK_QUIT,    0L, MSGFL_DVS|MSGFL_HUSH };
Msg ms_whois     = { MSG_WHOIS,   m_whois,    0, MAXPARA, TOK_WHOIS,   0L, 0 };
Msg ms_ping      = { MSG_PING,    m_ping,     0, MAXPARA, TOK_PING,    0L, MSGFL_DVS|MSGFL_HUSH };
Msg ms_user      = { MSG_USER,    m_user,     0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_away      = { MSG_AWAY,    m_away,     0, 1,       TOK_AWAY,    0L, 0 };
Msg ms_topic     = { MSG_TOPIC,   m_topic,    0, MAXPARA, TOK_TOPIC,   0L, 0 };
Msg ms_invite    = { MSG_INVITE,  m_invite,   0, 2,       TOK_INVITE,  0L, 0 };
Msg ms_kick      = { MSG_KICK,    m_kick,     0, 3,       TOK_KICK,    0L, 0 };
Msg ms_wallops   = { MSG_WALLOPS, m_wallops,  0, 1,       TOK_WALLOPS, 0L, 0 };
Msg ms_error     = { MSG_ERROR,   m_error,    0, 1,       TOK_ERROR,   0L, 0 };
Msg ms_kill      = { MSG_KILL,    m_kill,     0, MAXPARA, TOK_KILL,    0L, 0 };
Msg ms_rkill     = { MSG_RKILL,   m_kill,     0, 2,       NO_TOKEN,    0L, 0 };
Msg ms_hush      = { MSG_HUSH,    m_hush,     0, 3,       TOK_HUSH,    0L, 0 };  
Msg ms_unhush    = { MSG_UNHUSH,  m_unhush,   0, MAXPARA, TOK_UNHUSH,  0L, MSGFL_HUSH };  
Msg ms_protoctl  = { MSG_PROTOCTL,m_protoctl, 0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_watch     = { MSG_WATCH,   m_watch,    0, 1,       NO_TOKEN,    0L, 0 }; 
Msg ms_server    = { MSG_SERVER,  m_server,   0, MAXPARA, TOK_SERVER,  0L, 0 };
Msg ms_squit     = { MSG_SQUIT,   m_squit,    0, MAXPARA, TOK_SQUIT,   0L, 0 };
Msg ms_who       = { MSG_WHO,     m_who,      0, MAXPARA, TOK_WHO,     0L, 0 };
Msg ms_whowas    = { MSG_WHOWAS,  m_whowas,   0, MAXPARA, TOK_WHOWAS,  0L, 0 };
Msg ms_list      = { MSG_LIST,    m_list,     0, MAXPARA, NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_names     = { MSG_NAMES,   m_names,    0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_userhost  = { MSG_USERHOST,m_userhost, 0, 1,       TOK_USERHOST,0L, 0 };
Msg ms_trace     = { MSG_TRACE,   m_trace,    0, MAXPARA, TOK_TRACE,   0L, 0 };
Msg ms_pass      = { MSG_PASS,    m_pass,     0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_lusers    = { MSG_LUSERS,  m_lusers,   0, MAXPARA, TOK_LUSERS,  0L, 0 };
Msg ms_time      = { MSG_TIME,    m_time,     0, MAXPARA, TOK_TIME,    0L, 0 };
Msg ms_oper      = { MSG_OPER,    m_oper,     0, MAXPARA, NO_TOKEN,    0L, MSGFL_DVS|MSGFL_HUSH };
Msg ms_connect   = { MSG_CONNECT, m_connect,  0, MAXPARA, TOK_CONNECT, 0L, 0 };
Msg ms_version   = { MSG_VERSION, m_version,  0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_stats     = { MSG_STATS,   m_stats,    0, MAXPARA, TOK_STATS,   0L, 0 };
Msg ms_links     = { MSG_LINKS,   m_links,    0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_map       = { MSG_MAP,     m_map,      0, MAXPARA, NO_TOKEN,    0L, 0 };  
Msg ms_admin     = { MSG_ADMIN,   m_admin,    0, MAXPARA, TOK_ADMIN,   0L, 0 };
Msg ms_svsmode   = { MSG_SVSMODE, m_svsmode,  0, MAXPARA, TOK_SVSMODE, 0L, 0 };
Msg ms_samode    = { MSG_SAMODE,  m_samode,   0, 2,       NO_TOKEN,    0L, 0 };
Msg ms_svskill   = { MSG_SVSKILL, m_svskill,  0, MAXPARA, TOK_SVSKILL, 0L, 0 };
Msg ms_svsnick   = { MSG_SVSNICK, m_svsnick,  0, MAXPARA, TOK_SVSNICK, 0L, 0 };
Msg ms_svsnoop   = { MSG_SVSNOOP, m_svsnoop,  0, MAXPARA, TOK_SVSNOOP, 0L, 0 };
Msg ms_chanserv  = { MSG_CHANSERV,m_chanserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_cs        = { MSG_CS,	  m_chanserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_nickserv  = { MSG_NICKSERV,m_nickserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_ns        = { MSG_NS,	  m_nickserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_operserv  = { MSG_OPERSERV,m_operserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_os        = { MSG_OS,      m_operserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_memoserv  = { MSG_MEMOSERV,m_memoserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_ms        = { MSG_MS,      m_memoserv, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_services  = { MSG_SERVICES,m_services, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_identify  = { MSG_IDENTIFY,m_identify, 0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_help      = { MSG_HELP,    m_help,     0, 1,       NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_info      = { MSG_INFO,    m_info,     0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_contrib   = { MSG_CONTRIB, m_contrib,  0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_motd      = { MSG_MOTD,    m_motd,     0, MAXPARA, TOK_MOTD,    0L, 0 };
Msg ms_close     = { MSG_CLOSE,   m_close,    0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_silence   = { MSG_SILENCE, m_silence,  0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_akill     = { MSG_AKILL,   m_akill,    0, MAXPARA, TOK_AKILL,   0L, 0 };
Msg ms_sqline    = { MSG_SQLINE,  m_sqline,   0, MAXPARA, TOK_SQLINE,  0L, 0 };
Msg ms_unsqline  = { MSG_UNSQLINE,m_unsqline, 0, MAXPARA, TOK_UNSQLINE,0L, 0 };
Msg ms_kline     = { MSG_KLINE,   m_kline,    0, 2,       TOK_KLINE,   0L, 0 };
Msg ms_unkline   = { MSG_UNKLINE, m_unkline,  0, MAXPARA, TOK_UNKLINE, 0L, 0 };
Msg ms_zline     = { MSG_ZLINE,   m_zline,    0, MAXPARA, TOK_ZLINE,   0L, 0 };
Msg ms_unzline   = { MSG_UNZLINE, m_unzline,  0, MAXPARA, TOK_UNZLINE, 0L, 0 };
Msg ms_rakill    = { MSG_RAKILL,  m_rakill,   0, MAXPARA, TOK_RAKILL,  0L, 0 };
Msg ms_gnotice   = { MSG_GNOTICE, m_globops,  0, 1,       TOK_GNOTICE, 0L, 0 };
Msg ms_goper     = { MSG_GOPER,   m_goper,    0, 1,       TOK_GOPER,   0L, 0 };
Msg ms_globops   = { MSG_GLOBOPS, m_globops,  0, 1,       TOK_GLOBOPS, 0L, 0 };
Msg ms_chatops   = { MSG_CHATOPS, m_chatops,  0, 1,       TOK_CHATOPS, 0L, 0 };
Msg ms_locops    = { MSG_LOCOPS,  m_locops,   0, 1,       NO_TOKEN,    0L, 0 };
Msg ms_unbanme   = { MSG_UNBANME, m_unbanme,  0, MAXPARA, TOK_UNBANME, 0L, 0 };
Msg ms_rmban     = { MSG_RMBAN,   m_rmban,    0, MAXPARA, TOK_UNBANME, 0L, 0 };
Msg ms_mkill     = { MSG_MKILL,   m_mkill,    0, 2,       NO_TOKEN,    0L, 0 };
Msg ms_wkill     = { MSG_WKILL,   m_wkill,    0, 2,       NO_TOKEN,    0L, 0 };
Msg ms_fquit     = { MSG_FQUIT,   m_fquit,    0, MAXPARA, TOK_FQUIT,   0L, 0 };  
Msg ms_qotd      = { MSG_QOTD,    m_qotd,     0, 1,       NO_TOKEN,    0L, 0 };
Msg ms_hash      = { MSG_HASH,    m_hash,     0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_dns       = { MSG_DNS,     m_dns,      0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_rehash    = { MSG_REHASH,  m_rehash,   0, MAXPARA, TOK_REHASH,  0L, 0 };
Msg ms_restart   = { MSG_RESTART, m_restart,  0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_die       = { MSG_DIE,     m_die,      0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_set       = { MSG_SET,     m_set,      0, MAXPARA, NO_TOKEN,    0L, 0 };
Msg ms_lang      = { MSG_LANG,    m_lang,     0, MAXPARA, NO_TOKEN,    0L, MSGFL_DVS };
Msg ms_dvs       = { MSG_DVS,     m_dvs,      0, MAXPARA, TOK_DVS,     0L, MSGFL_DVS };
Msg ms_allow     = { MSG_ALLOW,   m_allow,    0, MAXPARA, TOK_ALLOW,   0L, 0 };

struct Message *msgtab[] =
{
	&ms_private,
	&ms_notice,
	&ms_mode,
	&ms_join,
	&ms_cjoin,
	&ms_sajoin,
	&ms_nick,
	&ms_pong,
	&ms_ison,
	&ms_part,
	&ms_quit,
	&ms_svsmode,
	&ms_whois,
	&ms_user,
	&ms_topic,
	&ms_kick,
	&ms_away,
	&ms_ping,
	&ms_userhost,
	&ms_who,
	&ms_whowas,
	&ms_invite,
	&ms_hush,
	&ms_unhush,
	&ms_wallops,
	&ms_protoctl,
	&ms_watch,
	&ms_server,
	&ms_squit,
	&ms_list,
	&ms_names,
	&ms_trace,
	&ms_pass,
	&ms_lusers,
	&ms_time,
	&ms_oper,
	&ms_connect,
	&ms_version,
	&ms_stats,
	&ms_links,
	&ms_map,
	&ms_admin,
	&ms_samode,
	&ms_svskill,
	&ms_svsnick,
	&ms_svsnoop,
	&ms_chanserv,
	&ms_cs,
	&ms_nickserv,
	&ms_ns,
	&ms_operserv,
	&ms_os,
	&ms_memoserv,
	&ms_ms,
	&ms_services,
	&ms_identify,
	&ms_help,
	&ms_motd,
	&ms_close,
	&ms_silence,
	&ms_akill,
	&ms_sqline,
	&ms_unsqline,
	&ms_kline,
	&ms_unkline,
	&ms_zline,
	&ms_unzline,
	&ms_rakill,
	&ms_gnotice,
	&ms_goper,
	&ms_globops,
	&ms_chatops,
	&ms_locops,
	&ms_unbanme,
	&ms_rmban,
	&ms_info,
	&ms_contrib,
	&ms_mkill,
	&ms_wkill,
	&ms_fquit,
	&ms_kill,
	&ms_rkill,
	&ms_qotd,
	&ms_hash,
	&ms_dns,
	&ms_rehash,
	&ms_restart,
	&ms_die,
	&ms_set,
	&ms_lang,
	&ms_dvs,
	&ms_allow,
	NULL
};

#else
extern struct Message *msgtab[];
extern struct Message *msgmap[256];
#endif
#endif /* __msg_include__ */
