/*
 *   IRC - Internet Relay Chat, include/sys.h
 *   Copyright (C) 1990 University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__sys_include__
#define __sys_include__
#ifdef ISC202
#include <net/errno.h>
#else
#include <sys/errno.h>
#endif

#include "setup.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/param.h>

#ifdef	HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef	HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef	HAVE_STRINGS_H 
#include <strings.h>
#else
# ifdef	HAVE_STRING_H 
# include <string.h>
# endif
#endif
#define	strcasecmp	mycmp
#define	strncasecmp	myncmp
#if !defined(HAVE_INDEX)
#define   index   strchr
#define   rindex  strrchr
/*
extern	char	*index PROTO((char *, char));
extern	char	*rindex PROTO((char *, char));
*/
#endif
#ifdef NOBCOPY
#define bcopy(x,y,z)	memcpy(y,x,z)
#define bcmp(x,y,z)	memcmp(x,y,z)
#define bzero(p,s)	memset(p,0,s)
#endif

#ifdef AIX
#include <sys/select.h>
#endif
#if defined(HPUX )|| defined(AIX)
#include <time.h>
#ifdef AIX
#include <sys/time.h>
#endif
#else
#include <sys/time.h>
#endif

#define MyFree(x)	if ((x) != NULL) free(x)

#ifdef NEXT
#define VOIDSIG int	/* whether signal() returns int of void */
#else
#define VOIDSIG void	/* whether signal() returns int of void */
#endif

#ifdef SOL20
#define OPT_TYPE char	/* opt type for get/setsockopt */
#else
#define OPT_TYPE void
#endif

/*
 * Different name on NetBSD, FreeBSD, and BSDI
 */
#if defined(__NetBSD__) || defined(__FreeBSD__) || defined(__bsdi__) || defined(__linux__) || defined(__OpenBSD__) || (defined(HAVE___DN_SKIPNAME) && !defined(HAVE_DN_SKIPNAME))
#define dn_skipname  __dn_skipname
#endif

extern	VOIDSIG	dummy();

#ifdef	DYNIXPTX
#define	NO_U_TYPES
typedef unsigned short n_short;         /* short as received from the net */
typedef unsigned long   n_long;         /* long as received from the net */
typedef unsigned long   n_time;         /* ms since 00:00 GMT, byte rev */
#define _NETINET_IN_SYSTM_INCLUDED
#endif

#ifdef	NO_U_TYPES
typedef	unsigned char	u_char;
typedef	unsigned short	u_short;
typedef	unsigned long	u_long;
typedef	unsigned int	u_int;
#endif

#ifdef NEED_U_INT32_T
typedef unsigned int  u_int32_t;
#endif

typedef unsigned char uch;
typedef unsigned short ush;
typedef u_int32_t ui32;

#ifdef	USE_VARARGS
#include <varargs.h>
#endif

#define CloseSock(x) close(x)

#endif /* __sys_include__ */
