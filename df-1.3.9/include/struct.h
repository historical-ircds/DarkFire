/*
 *   IRC - Internet Relay Chat, include/struct.h
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__struct_include__
#define __struct_include__

#include "config.h"
#include "common.h"
#include "sys.h"
#include "dvs.h"

#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

#ifdef	HAVE_STDDEF_H
#include <stddef.h>
#endif

#ifdef	USE_SYSLOG
#include <syslog.h>
#ifdef	HAVE_SYS_SYSLOG_H
#include <sys/syslog.h>
#endif
#endif

#ifdef	pyr
#include <sys/time.h>
#endif

extern	int	ServicesOnline, DVSLock;

/* types */
typedef struct  SqlineItem aSqlineItem;
typedef	struct	ConfItem aConfItem;
typedef struct	Notify	aNotify;
typedef	struct 	Client	aClient;
typedef	struct	Channel	aChannel;
typedef	struct	User	anUser;
typedef	struct	Server	aServer;
typedef	struct	SLink	Link;
typedef struct	sMember	Member;
typedef	struct	sHush	Hush;
typedef struct	SBan	Ban;
typedef	struct	SMode	Mode;
typedef struct	ListOptions	LOpts;
typedef struct	Message	Msg;
typedef	struct	sModeFlag ModeFlag;

/* this probably shouldn't be here */
#ifndef VMSP
#include "class.h"
#include "dbuf.h"
#endif

/* Defines */
#define MAXPARA			15
#define	HOSTLEN			63		/* Hostname compliant w/ RFC1123 */
#define IPLEN			16
#define	NICKLEN			30
#define	USERLEN			10
#define	REALLEN	 		50
#define	TOPICLEN		307
#define AWAYLEN			115
#define	CHANNELLEN		32
#define	PASSWDLEN 		32
#define	KEYLEN			11
#define	BUFSIZE			512		/* WARNING: *DONT* CHANGE THIS!!!! */
#define	MAXRECIPIENTS 		20
#define	MAXKILLS		10
#define	MAXBANS			60
#define	MAXBANLENGTH		1024
#define	MAXSILES		5
#define	MAXSILELENGTH		128
#define MAXWATCH		128	/* See ERR_TOOMANYWATCH and PROTOCOL_SUPORTED */
#define	USERHOST_REPLYLEN	(NICKLEN+HOSTLEN+USERLEN+5)

/* NOTE: this must be down here so the stuff from struct.h IT uses works */
#include "whowas.h"

/*
 * 'offsetof' is defined in ANSI-C. The following definition
 * is not absolutely portable (I have been told), but so far
 * it has worked on all machines I have needed it. The type
 * should be size_t but...  --msa
 */
#ifndef offsetof
#define	offsetof(t,m) (int)((&((t *)0L)->m))
#endif

#define	elementsof(x) (sizeof(x)/sizeof(x[0]))

/*
 * Flags for bootup options (command line flags)
 */
#define	BOOT_CONSOLE	1
#define	BOOT_QUICK	2
#define	BOOT_DEBUG	4
#define	BOOT_INETD	8
#define	BOOT_TTY	16
#define	BOOT_OPER	32

/*
 * Status
 */
#define	STAT_LOG	-6	/* logfile for -x */
#define	STAT_MASTER	-5	/* Local ircd master before identification */
#define	STAT_CONNECTING	-4
#define	STAT_HANDSHAKE	-3
#define	STAT_ME		-2
#define	STAT_UNKNOWN	-1
#define	STAT_SERVER	0
#define	STAT_CLIENT	1

/*
 * Status macros
 */
#define	IsRegisteredUser(x)	((x)->status == STAT_CLIENT)
#define	IsRegistered(x)		((x)->status >= STAT_SERVER)
#define	IsConnecting(x)		((x)->status == STAT_CONNECTING)
#define	IsHandshake(x)		((x)->status == STAT_HANDSHAKE)
#define	IsMe(x)			((x)->status == STAT_ME)
#define	IsUnknown(x)		((x)->status == STAT_UNKNOWN || \
				 (x)->status == STAT_MASTER)
#define	IsServer(x)		((x)->status == STAT_SERVER)
#define	IsClient(x)		((x)->status == STAT_CLIENT)
#define IsPerson(x)		((x)->status == STAT_CLIENT)
#define	IsLog(x)		((x)->status == STAT_LOG)
#define IsService(x)		0

#define	SetMaster(x)		((x)->status = STAT_MASTER)
#define	SetConnecting(x)	((x)->status = STAT_CONNECTING)
#define	SetHandshake(x)		((x)->status = STAT_HANDSHAKE)
#define	SetMe(x)		((x)->status = STAT_ME)
#define	SetUnknown(x)		((x)->status = STAT_UNKNOWN)
#define	SetServer(x)		((x)->status = STAT_SERVER)
#define	SetClient(x)		((x)->status = STAT_CLIENT)
#define	SetLog(x)		((x)->status = STAT_LOG)
#define	SetService(x)		((x)->status = STAT_SERVICE)

/*
 * Flags
 */
#define	FLAGS_PINGSENT		0x0001	/* Unreplied ping sent */
#define	FLAGS_DEADSOCKET	0x0002	/* Local socket is dead--Exiting soon */
#define	FLAGS_KILLED		0x0004	/* Prevents "QUIT" from being sent for this */
#define	FLAGS_BLOCKED		0x0008	/* socket is in a blocked condition */
#define	FLAGS_CLOSING		0x0020	/* set when closing to suppress errors */
#define	FLAGS_LISTEN		0x0040  /* used to mark clients which we listen() on */
#define	FLAGS_CHKACCESS		0x0080  /* ok to check clients access if set */
#define	FLAGS_DOINGDNS		0x0100  /* client is waiting for a DNS response */
#define	FLAGS_AUTH		0x0200  /* client is waiting on rfc931 response */
#define	FLAGS_WRAUTH		0x0400	/* set if we havent writen to ident server */
#define	FLAGS_LOCAL		0x0800  /* set for local clients */
#define	FLAGS_GOTID		0x1000	/* successful ident lookup achieved */
#define	FLAGS_DOID		0x2000	/* I-lines say must use ident return */
#define	FLAGS_NONL		0x4000  /* No \n in buffer */
#define FLAGS_ULINE		0x10000 /* User/server is considered U-lined */
#define FLAGS_SQUIT		0x20000 /* Server has been /squit by an oper */
#define	FLAGS_GOTUSER		0x80000	/* got USER message from this client */
/*				0x200000   DVS Limited */
/*				0x400000   DVS Warning Sent */
/*				0x800000   DVS Lock (server linking) */
/*				0x1000000  DVS Tag */

/*
 * DVS Flags AND macros
 */
#ifdef USE_DVS
#define FLAGS_DVSLIMIT		0x200000 	/* DVS Limited */
#define FLAGS_DVSWARN		0x400000 	/* DVS Warning Sent */
#define FLAGS_DVSLOCK		0x800000 	/* DVS Lock (server linking) */
#define FLAGS_DVSTAG		0x1000000	/* DVS Tag (ever was DVS Limited) */

#define IsDVS(x)		((x)->flags & FLAGS_DVSLIMIT && \
					(ServicesOnline == 1 || DVSLock == 1))
#define IsSetDVS(x)		((x)->flags & FLAGS_DVSLIMIT)
#define SetDVS(x)		((x)->flags |= FLAGS_DVSLIMIT)
#define ClearDVS(x)		((x)->flags &= ~FLAGS_DVSLIMIT)

#define	SetDVStag(x)		((x)->flags |= FLAGS_DVSTAG)
#define	DVStagged(x)		((x)->flags & FLAGS_DVSTAG)

#define SetDVSWarn(x)		((x)->flags |= FLAGS_DVSWARN)
#define HasBeenDVSWarned(x)	((x)->flags & FLAGS_DVSWARN)
#define ClearDVSWarn(x)		((x)->flags &= ~FLAGS_DVSWARN)
#endif /* USE_DVS */

/*
 * Flag macros
 */
#define FLAGS_ID		(FLAGS_DOID|FLAGS_GOTID)
#define GotUser(x)		((x)->flags & FLAGS_GOTUSER)
#define IsUnixSocket(x)		((x)->flags & FLAGS_UNIX)
#define IsListening(x)		((x)->flags & FLAGS_LISTEN)
#define DoAccess(x)		((x)->flags & FLAGS_CHKACCESS)
#define IsLocal(x)		((x)->flags & FLAGS_LOCAL)
#define IsDead(x)		((x)->flags & FLAGS_DEADSOCKET)
#define IsBlocked(x)		((x)->flags & FLAGS_BLOCKED)
#define IsULine(x)		((x)->flags & FLAGS_ULINE)
#define DoingDNS(x)		((x)->flags & FLAGS_DOINGDNS)
#define DoingAuth(x)		((x)->flags & FLAGS_AUTH)
#define NoNewLine(x)		((x)->flags & FLAGS_NONL)

#define SetUnixSock(x)		((x)->flags |= FLAGS_UNIX)
#define SetDNS(x)		((x)->flags |= FLAGS_DOINGDNS)
#define SetAccess(x)		((x)->flags |= FLAGS_CHKACCESS)
#define SetBlocked(x)		((x)->flags |= FLAGS_BLOCKED)

#define ClearDNS(x)		((x)->flags &= ~FLAGS_DOINGDNS)
#define ClearAuth(x)		((x)->flags &= ~FLAGS_AUTH)
#define ClearAccess(x)		((x)->flags &= ~FLAGS_CHKACCESS)
#define ClearBlocked(x)		((x)->flags &= ~FLAGS_BLOCKED)

/*
 * User modes
 */
#define	UMODE_INVISIBLE		0x00001 /* makes user invisible */
#define	UMODE_OPER		0x00002 /* Operator */
#define	UMODE_WALLOP		0x00004 /* send wallops to them */
#define UMODE_FAILOP		0x00008 /* Shows some global messages */
#define UMODE_REGNICK		0x00020 /* Nick set by services as registered */
#define UMODE_SADMIN		0x00040 /* Services Admin */
#define UMODE_ADMIN		0x00080 /* Admin */

#define	UMODE_SERVNOTICE	0x00100 /* server notices such as kill */
#define	UMODE_LOCOP		0x00200 /* Local operator -- SRB */
#define UMODE_KILLS		0x00400 /* Show server-kills... */
#define UMODE_CLIENT		0x00800 /* Show client information */
#define UMODE_FLOOD		0x01000 /* Receive flood warnings */
#define UMODE_CHATOP		0x02000 /* can receive chatops */
#define	UMODE_JUNK		0x04000 /* receive annoying server junk */
#define	UMODE_CSOP		0x08000 /* user is a CSop */
#define UMODE_SRA		0x10000 /* user is a Services Root Administrator */
#define	UMODE_DISGUISE		0x20000 /* hide a user's most specific part of addy */
#define	UMODE_HUSHNOTICE	0x40000 /* display hush notices */
#define UMODE_FORCEDIS		0x80000 /* force user to be disguised */
#define	UMODE_LIMITREG		0x100000 /* only allow regnicks to contact person */
#define UMODE_ESPANOL		0x200000 /* Spanish numerics */
#define UMODE_CANFLOOD		0x400000 /* Oper Can Flood */

#define	UMODE_ANYOP	(UMODE_OPER|UMODE_LOCOP)
#define	ALL_UMODES	0xFFFFFFFF
#define SEND_UMODES	(UMODE_OPER|UMODE_INVISIBLE|UMODE_REGNICK|	\
			 UMODE_ADMIN|UMODE_SADMIN|UMODE_CSOP|UMODE_SRA|  \
			 UMODE_DISGUISE|UMODE_FORCEDIS|UMODE_WALLOP	\
			 |UMODE_LIMITREG|UMODE_ESPANOL)

/*
 * Usermode macros
 */
#define IsKillsF(x)		((x)->umodes & UMODE_KILLS)
#define IsClientF(x)		((x)->umodes & UMODE_CLIENT)
#define IsFloodF(x)		((x)->umodes & UMODE_FLOOD)
#define IsAdmin(x)		((x)->umodes & UMODE_ADMIN)
#define IsSAdmin(x)		((x)->umodes & UMODE_SADMIN)
#define SendFailops(x)		((x)->umodes & UMODE_FAILOP)
#define SendChatops(x)		((x)->umodes & UMODE_CHATOP)
#define	SendJunk(x)		((x)->umodes & UMODE_JUNK)
#define	IsOper(x)		((x)->umodes & UMODE_OPER)
#define	IsLocOp(x)		((x)->umodes & UMODE_LOCOP)
#define	IsInvisible(x)		((x)->umodes & UMODE_INVISIBLE)
#define	IsSRA(x)		((x)->umodes & UMODE_SRA)
#define	IsCSop(x)		((x)->umodes & UMODE_CSOP)
#define	IsAnOper(x)		((x)->umodes & (UMODE_OPER|UMODE_LOCOP))
#define	IsDisguise(x)		((x)->umodes & UMODE_DISGUISE)
#define IsForceDis(x)		((x)->umodes & UMODE_FORCEDIS)
#define IsARegNick(x)		((x)->umodes & UMODE_REGNICK)
#define IsRegNick(x)		((x)->umodes & UMODE_REGNICK)
#define IsEspanol(x)		((x)->umodes & UMODE_ESPANOL)
#define IsExemptFromFlood(x)	((x)->umodes & UMODE_CANFLOOD)

#define	IsPrivileged(x)		(IsAnOper(x) || IsServer(x))
#define	SendWallops(x)		((x)->umodes & UMODE_WALLOP)
#define	SendServNotice(x)	((x)->umodes & UMODE_SERVNOTICE)
#define	SendHushNotice(x)	((x)->umodes & UMODE_HUSHNOTICE)

#define SetKillsF(x)		((x)->umodes |= UMODE_KILLS)
#define SetClientF(x)		((x)->umodes |= UMODE_CLIENT)
#define SetFloodF(x)		((x)->umodes |= UMODE_FLOOD)
#define	SetOper(x)		((x)->umodes |= UMODE_OPER)
#define	SetLocOp(x)    		((x)->umodes |= UMODE_LOCOP)
#define	SetInvisible(x)		((x)->umodes |= UMODE_INVISIBLE)
#define	SetWallops(x)  		((x)->umodes |= UMODE_WALLOP)
#define SetEspanol(x)		((x)->umodes |= UMODE_ESPANOL)
#define SetCanFlood(x)		((x)->umodes |= UMODE_CANFLOOD)

#define ClearAdmin(x)		((x)->umodes &= ~UMODE_ADMIN)
#define ClearSAdmin(x)		((x)->umodes &= ~UMODE_SADMIN)
#define ClearKillsF(x)		((x)->umodes &= ~UMODE_KILLS)
#define ClearClientF(x)		((x)->umodes &= ~UMODE_CLIENT)
#define ClearFloodF(x)		((x)->umodes &= ~UMODE_FLOOD)
#define ClearFailops(x)		((x)->umodes &= ~UMODE_FAILOP)
#define ClearChatops(x)		((x)->umodes &= ~UMODE_CHATOP)
#define	ClearOper(x)		((x)->umodes &= ~UMODE_OPER)
#define	ClearInvisible(x)	((x)->umodes &= ~UMODE_INVISIBLE)
#define	ClearWallops(x)		((x)->umodes &= ~UMODE_WALLOP)
#define	ClearHushNotice(x)	((x)->umodes &= ~UMODE_HUSHNOTICE)
#define ClearDisguise(x)	((x)->umodes &= ~UMODE_DISGUISE)
#define ClearEspanol(x)		((x)->umodes &= ~UMODE_ESPANOL)
#define ClearCanFlood(x)	((x)->umodes &= ~UMODE_CANFLOOD)

/*
 * Protocol Levels
 */
#define PROLEV_USER		-1
#define PROLEV_GEN		0
#define PROLEV_NOQUIT		1
#define PROLEV_TOKEN		2
#define PROLEV_SERVHASH		3
#define PROLEV_COMPACT		4
#define	PROLEV_AKILL		4
#define PROLEV_CACHE		4
#define PROLEV_CJOIN		5
#define PROLEV_DVSNICK		6

#define PROLEV_MAX PROLEV_DVSNICK	/* Remember to change SERV_CAPAB */

#define	DontSendQuit(x)		((x)->prolev >= PROLEV_NOQUIT)
#define	IsToken(x)		((x)->prolev >= PROLEV_TOKEN)
#define IsServHash(x)		((x)->prolev >= PROLEV_SERVHASH)
#define	IsCompact(x)		((x)->prolev >= PROLEV_COMPACT)
#define	IsNewAkill(x)		((x)->prolev >= PROLEV_AKILL)

#define	SetUser(x)		((x)->prolev = PROLEV_USER)
#define AddProto(x,p)		(((x)->prolev<p)?((x)->prolev=p):0)

/*
 * Opererator (o:line) flags
 */
#define OFLAG_MODES	0x00000001	/* Oper can set opermodes */
#define OFLAG_LOCAL	0x00000002	/* Oper can do most local functions */
#define OFLAG_GLOBAL	0x00000004	/* Oper can do global functions */
#define OFLAG_REMOTE	0x00000010	/* Oper can kline/rehash remotely */
#define OFLAG_SUPER	0x00000020	/* Oper can use flood protection and other goodies, some require SA */
#define OFLAG_SADMIN	0x00000040	/* Oper can set +a */
#define OFLAG_ADMIN	0x00000080	/* Oper can set +A and use /set, /die, and /restart */

#define OFLAG_ISGLOBAL OFLAG_GLOBAL

/*
 * Operator macros (updated for limited flags)
 */

#define OPCanZline(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPCanRehash(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanDie(x)		((x)->oflag & OFLAG_ADMIN)
#define OPCanRestart(x)		((x)->oflag & OFLAG_ADMIN)
#define OPCanGlobOps(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPCanWallOps(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPCanLocOps(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanLRoute(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanGRoute(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPCanLKill(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanGKill(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPCanKline(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanUnKline(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanLNotice(x)		((x)->oflag & OFLAG_LOCAL)
#define OPCanGNotice(x)		((x)->oflag & OFLAG_GLOBAL)
#define OPIsAdmin(x)		((x)->oflag & OFLAG_ADMIN)
#define OPIsSAdmin(x)		((x)->oflag & OFLAG_SADMIN)
#define OPCanUModeC(x)		((x)->oflag & OFLAG_MODES)
#define OPCanUModeF(x)		((x)->oflag & OFLAG_MODES)
#define OPCanRemote(x)		((x)->oflag & OFLAG_REMOTE)
#define OPCanSet(x)		((x)->oflag & OFLAG_ADMIN)
#define OPCanSAWithServ(x)	((x)->oflag & OFLAG_SUPER)
#define OPCanFlood(x)		((x)->oflag & OFLAG_SUPER)

/*
 * defined debugging levels
 */
#define	DEBUG_FATAL  0
#define	DEBUG_ERROR  1	/* report_error() and other errors that are found */
#define	DEBUG_NOTICE 3
#define	DEBUG_DNS    4	/* used by all DNS related routines - a *lot* */
#define	DEBUG_INFO   5	/* general usful info */
#define	DEBUG_NUM    6	/* numerics */
#define	DEBUG_SEND   7	/* everything that is sent out */
#define	DEBUG_DEBUG  8	/* anything to do with debugging, ie unimportant :) */
#define	DEBUG_MALLOC 9	/* malloc/free calls */
#define	DEBUG_LIST  10	/* debug list use */

struct  SqlineItem	{
	unsigned int	status;
	char *sqline;
	char *reason;
	struct	SqlineItem *next;
};

struct	ConfItem	{
	unsigned int	status;	/* If CONF_ILLEGAL, delete when no clients */
	int	clients;	/* Number of *LOCAL* clients using this */
	struct	in_addr ipnum;	/* ip number of host field */
	char	*host;
	char	*passwd;
	char	*name;
	int	port;
	time_t	hold;	/* Hold action until this time (calendar time) */
	int	tmpconf;
#ifndef VMSP
	aClass	*class;  /* Class of connection */
#endif
	struct	ConfItem *next;
};

#define	CONF_ILLEGAL		0x80000000
#define	CONF_MATCH		0x40000000
#define CONF_NOOP_GLOBAL	0x20000000
#define CONF_NOOP_LOCAL		0x10000000
#define	CONF_QUARANTINED_SERVER	0x0001
#define	CONF_CLIENT		0x0002
#define	CONF_CONNECT_SERVER	0x0004
#define	CONF_NOCONNECT_SERVER	0x0008
#define	CONF_LOCOP		0x0010
#define	CONF_OPERATOR		0x0020
#define	CONF_ME			0x0040
#define	CONF_KILL		0x0080
#define	CONF_ADMIN		0x0100
#define	CONF_CLASS		0x0400
#define	CONF_SERVICE		0x0800
#define	CONF_LEAF		0x1000
#define	CONF_LISTEN_PORT	0x2000
#define	CONF_HUB		0x4000
#define	CONF_UWORLD		0x8000
#define CONF_QUARANTINED_NICK	0x10000
#define CONF_ZAP		0x20000
#define CONF_CONFIG             0x100000
#define CONF_MISSING		0x800000
#define CONF_SADMIN		0x1000000
#define CONF_DRPASS		0x2000000   /* DIE/RESTART pass - NikB */

#define	CONF_OPS		(CONF_OPERATOR | CONF_LOCOP)
#define	CONF_SERVER_MASK	(CONF_CONNECT_SERVER | CONF_NOCONNECT_SERVER)
#define	CONF_CLIENT_MASK	(CONF_CLIENT | CONF_SERVICE | CONF_OPS | \
				 CONF_SERVER_MASK)
#define CONF_QUARANTINE		(CONF_QUARANTINED_SERVER|CONF_QUARANTINED_NICK)

#define	IsIllegal(x)	((x)->status & CONF_ILLEGAL)
#define IsTemp(x)	((x)->tmpconf)

#define IsHushed(x)	(IsPerson(x) && (x)->u->hush)

/* Message flags */
#define MSGFL_HUSH		0x0001
#define	MSGFL_DVS		0x0002

/*
 * User structure.
 */
struct User
{
	char	nick[NICKLEN+1];  /* this ***MUST*** come first!!! */
	char	rhost[HOSTLEN+1]; /* REAL host, bypasses +d mode */
	char	dhost[HOSTLEN+1]; /* DISGUISED host, always present! */
	char	*host;		  /* points to host to be seen by normals */
	time_t	nickts;		  /* timestamp on this nick */
	ui32	sstamp;           /* Services' time stamp variable */
	Link	*silence;	  /* chain of silence pointer blocks */
	char	*away;		  /* pointer to away message; null if here */
	Hush	*hush;		  /* reason why the user is hushed */
	aName	*whowas;	  /* this user went by another nick before ? */
	Member	*channel;	  /* chain of channel pointer blocks */
	Link	*invited;	  /* chain of invite pointer blocks */
	int	joined;	  	  /* number of channels joined */
};

/* 
 * Server structure.
 *   NOTE: Keep this struct smaller than the user struct!
 *
 *   Special note on servid/shortn: shortn will contain the first four letters
 *  of the server's name and a trailing \0; the first four characters will be
 *  stored in servid and the \0 will be in fuzz2.  fuzz1 will align sndot
 *  properly so when sndot is accessed you will see the string ".serv"
 *  if the server's name begins with the letters serv.
 */
struct Server
{
	char		name[HOSTLEN+1];	/* server name, MUST BE FIRST HERE! */
	char		by[NICKLEN+1];

	int		downlinks;		/* number of direct downlinks */
	aClient		*down, *side;		/* down-link and side-link */
	aClient		*next;			/* next server in list */
	aConfItem 	*nline;			/* N-line pointer for this server */
	aClient 	*hnext;			/* next server in server hash */
	aClient		*lastsptr;		/* last sender ptr from this server */
	ui32		servid;			/* 32-bit servid where shortn is stored */
	char		sndot[1];		/* a dot before the shortn (.serv) */
	char		shortn[0];		/* 4-char short name for this serv */
};

/*
 * Client structure.
 *
 *   New features: the client and server part is stored statically in an
 *   internal union, rather than relying upon pointers.  This reduces the
 *   size of the client structure greatly.  Also, the name of the client
 *   is leeched from the server or user structure.  That way, we don't need
 *   to give an entire 63 characters for users' nicknames like before.
 *   Servers get 63 chars and users get 30 chars now.  That's why in the
 *   server and user struct the name field absolutely must come first!
 */
struct Client
{
#if defined(__cplusplus)||defined(c_plusplus)
	union { char name[HOSTLEN+1]; anUser u[1]; aServer srv[1]; };
#else
	char	name[0];	/* name leeching off of user/serv info area */
	anUser	u[0];		/* static equiv of cptr->user */
	aServer	s[0];		/* static equiv of cptr->serv */
	union { anUser u; aServer s; } idata;
#endif

	aClient *hnext, *prev, *next, **hbase;
	aClient	*from;		/* == self, if Local Client, *NEVER* NULL! */
	aClient	*srvptr;	/* server introducing this.  may be &me. */	
	
	ui32 	umodes;	/* user modes */
	ui32 	flags;	/* client flags */
	
	short	fd;		/* >= 0, for local clients */
	short	status;		/* Client type */
	short	hopcount;	/* number of servers to this, 0 = local */
	
	char	username[USERLEN+1]; /* username here now for auth stuff */
	char	info[REALLEN+1]; /* Free form additional client information */

	/*
	** The following fields are allocated only for local clients
	** (directly connected to *this* server with a socket.
	** The first of them *MUST* be the "count"--it is the field
	** to which the allocation is tied to! *Never* refer to
	** these fields, if (from != self).
	*/
	int	count;		/* Amount of data in buffer */
	char	buffer[BUFSIZE]; /* Incoming message buffer */
	short	lastsq;		/* # of 2k blocks when sendqueued called last*/
	dbuf	sendQ;		/* Outgoing message queue--if socket full */
	dbuf	recvQ;		/* Hold for data incoming yet to be parsed */
	long	oflag;		/* Operator access flags -Cabal95 */
	long	sendM;		/* Statistics: protocol messages send */
	long	sendK;		/* Statistics: total k-bytes send */
	long	receiveM;	/* Statistics: protocol messages received */
	long	receiveK;	/* Statistics: total k-bytes received */
	u_short	sendB;		/* counters to count upto 1-k lots of bytes */
	u_short	receiveB;	/* sent and received. */
	short	prolev;		/* protocol level */
	aClient	*acpt;		/* listening client which we accepted from */
	Link	*confs;		/* Configuration record associated */
	int	authfd;		/* fd for rfc931 authentication */
	struct	in_addr	ip;	/* keep real ip# too */
	u_short	port;		/* and the remote port# too :-) */
	struct	hostent	*hostp;
	u_short	notifies;	/* Keep track of count of notifies */
	Link	*notify;	/* Links to clients notify-structures */
	LOpts	*lopt;		/* Saved /list options */
	char	sockhost[HOSTLEN+1]; /* This is the host name from the socket
				     ** and after which the connection was
				     ** accepted.
				     */
	char	passwd[PASSWDLEN+1];
	time_t	lastdcc;	/* Last DCC */
	time_t	since;		/* last time we parsed something */
	time_t	lasttime;	/* ...should be only LOCAL clients? --msa */
	time_t	firsttime;	/* time client was created */
	time_t	ulast;		/* time used for idle computation */
#ifdef DEBUGMODE
	time_t	cputime;
#endif
#ifdef USE_DVS
	time_t	dvstime;	/* Time used for initial DVS availability */
	Member	*chbuf;		/* Channel join buffer */
#endif
};

#define	CLIENT_LOCAL_SIZE sizeof(aClient)
#define	CLIENT_REMOTE_SIZE offsetof(aClient,count)

struct ListOptions {
	LOpts	*next;
	Link	*yeslist, *nolist;
	int	starthash;
	short int	showall;
	unsigned short	usermin;
	int	usermax;
	time_t	currenttime;
	time_t	chantimemin;
	time_t	chantimemax;
	time_t	topictimemin;
	time_t	topictimemax;
};

/* mode structure for channels */

struct	SMode	{
	ui32 	mode;
	u_int16_t limit;
	u_int16_t maxrept;
	u_int16_t flood;
	char	key[KEYLEN+1];
};

/* Message table structure */

struct	Message	{
	char	cmd[16];
	int	(* func)();
	unsigned int	count;
	int	parameters;
	u_char	token[2]; /* Cheat for tokenized value */
	char	flags;
		/* bit 0 set means that this command is allowed to be used
		 * only on the average of once per 2 seconds -SRB */
	long	msgfl;
	unsigned long bytes;
	unsigned long l_utime;
	unsigned long r_utime;
};

/* ModeFlag structure */
struct sModeFlag
{
	ui32 	flag;
	char 	c;
	int 	extra;
};

/* Used for notify-hash buckets... -Donwulff */

struct Notify {
	aNotify	*hnext;
	time_t	lasttime;
	Link	*notify;
	char	nick[1];
};

/* general link structure used for chains */

struct	SLink	{
	struct	SLink	*next;
	int	flags;
	union {
		aClient	*cptr;
		aChannel *chptr;
		aConfItem *aconf;
		aNotify	*nptr;
		aName *whowas;
		char	*cp;
	} value;
};

struct sMember
{
	struct sMember *nextu; /* next user, in a channel userlist */
	struct sMember *nextc; /* next chan, in a user's chan list */
	aClient *cptr;
	aChannel *chptr;
	u_int16_t flags;
	u_int16_t msg1, msg2; /* hashes of the last 2 messages sent */
	u_char repeats; /* number of consecutive "identical" messages */
	ui32 since; /* timestamp (tenths of seconds) for flood ctrl */
};

struct	SBan
{
	struct	SBan	*next;
	time_t when;
	char *who;
	char *banhost;
	char *banuser;
	char banstr[0];
};

struct sHush
{
	time_t	end;
	char	by[HOSTLEN+1];
	char	s[0];
};

/* channel structure */

struct Channel
{
	struct	Channel *nextch, *prevch, *hnext;
	Mode	mode;
	time_t	channelts;
	char	topic[TOPICLEN+1];
	char	topic_nick[NICKLEN+1];
	time_t	topic_time;
	int	users;
	Member	*members;
	Member	**lmembers; /* local members */
	Link	*invites;
	Ban	*banlist;
	Ban	*exceptlist;
	char	chname[CHANNELLEN+1];
};

/*
** Channel Related macros follow
*/

/* Channel related flags */

#define	CHFL_CHANOP     0x0001 /* Channel operator */
#define	CHFL_VOICE      0x0002 /* the power to speak */
#define CHFL_HALFOP	0x0004 /* Restricted operator */
#define	CHFL_OVERLAP	(CHFL_CHANOP|CHFL_VOICE|CHFL_HALFOP)

#define CHFL_EXCEPT	0x0400 /* +e exception */
#define	CHFL_UNEXCEPT	0x0800 /* normal */
#define CHFL_EXCEPTSET	(CHFL_EXCEPT|CHFL_UNEXCEPT)

#define	CHFL_BAN	0x0040 /* ban channel flag */
#define CHFL_UNBAN	0x0080 /* unbanned */
#define CHFL_BANSET	(CHFL_BAN|CHFL_UNBAN)

#define CHFL_DELETE	0x2000 /* Delete channel */

/* Channel Visibility macros */
 
#define	MODE_PRIVATE	 0x00000001
#define	MODE_SECRET	 0x00000002
#define	MODE_MODERATED   0x00000004
#define	MODE_TOPICLIMIT  0x00000008
#define	MODE_INVITEONLY  0x00000010
#define	MODE_NOPRIVMSGS  0x00000020
#define MODE_RGSTR	 0x00000040
#define MODE_RGSTRONLY   0x00000080
#define	MODE_STRIPCOLOR	 0x00000100
#define	MODE_PRIVMSGONLY 0x00000200
#define MODE_NODCCS      0x00000400
#define MODE_NOSAJOIN    0x00000800
#define MODE_OPERONLY    0x00001000

#define	MODE_EXCEPT      0x01000000
#define	MODE_BAN	 0x10000000
#define	MODE_STRPARAM	 0x20000000
#define	MODE_INTPARAM	 0x40000000
#define	MODE_MEMBER	 0x80000000
#define	MODE_CLEAR       0

#define	MODE_WPARAS	(MODE_MEMBER|MODE_INTPARAM|MODE_STRPARAM|MODE_BAN|MODE_EXCEPT)
#define	MODE_SIMPLE	(~(MODE_WPARAS))


#define	SecretChannel(x)	(((x)->mode.mode & MODE_SECRET))
#define	ShowChannel(v,c)	(PubChannel(c) || IsMember((v),(c)))
#define	PubChannel(x)		(((x)->mode.mode & (MODE_PRIVATE | MODE_SECRET)) == 0)

#define	IsMember(u,c)		((int)find_member(c,u))

#define	IsChannelName(name)	((name) && (*(name) == '#'))

/* 
 * Counts - /lusers and dvs
 */
struct Counts
{
	time_t		lastcheck;	/* Last update */
	int		opers;
	int		localu;		/* All local users */
	int		globalu;	/* Combined global users */
	int		visible;	/* All -i */
	int		invisible;	/* All +i */
	int		maxlocalu;
	int		maxglobalu;
	int		localserv;	/* Local servers (not me) */
	int		servers;	/* All servers (including me) */
	int		unknown;
	int		channels;
#ifdef	USE_DVS
	int		localdvs;
	int		globaldvs;
	int		dvskills;
#endif
};

/* Misc macros */

#define	BadPtr(x) (!(x) || (*(x) == '\0'))

#define	isvalid(c) (((c) >= 'A' && (c) < '~') || isdigit(c) || (c) == '-')

#define	MyConnect(x)			((x)->fd >= 0)
#define	MyClient(x)			(IsClient(x->from))
#define	MyOper(x)			(IsOper(x->from))

/* Lifted somewhat from Undernet code --Rak */

#define IsSendable(x)		(DBufLength(&x->sendQ) < 2048)
#define DoList(x)		((x)->lopt)

/* String manipulation macros */

/* strncopynt --> strncpyzt to avoid confusion, sematics changed
   N must be now the number of bytes in the array --msa */
#define	strncpyzt(x, y, N) do{(void)strncpy(x,y,N);x[N-1]='\0';}while(0)
#define	strncatzt(x, y, N) do{(void)strncat(x,y,N);x[N-1]='\0';}while(0)
#define	StrEq(x,y)	(!strcmp((x),(y)))

/* return values for hunt_server() */

#define	HUNTED_NOSUCH	(-1)	/* if the hunted server is not found */
#define	HUNTED_ISME	0	/* if this server should execute the command */
#define	HUNTED_PASS	1	/* if message passed onwards successfully */

/* used when sending to #mask or $mask */

#define	MATCH_SERVER  1
#define	MATCH_HOST    2

/* used for async dns values */

#define	ASYNC_NONE	(-1)
#define	ASYNC_CLIENT	0
#define	ASYNC_CONNECT	1
#define	ASYNC_CONF	2
#define	ASYNC_SERVER	3

/* misc variable externs */

extern	char	*version, *infotext[], *contribtext[];
extern	char	*generation, *creation;

/* misc defines */

#define	FLUSH_BUFFER	-2
#define	UTMP		"/etc/utmp"
#define	COMMA		","

#include "alloc.h"

#endif /* __struct_include__ */
