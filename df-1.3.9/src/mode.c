/************************************************************************
 *   IRC - Internet Relay Chat, ircd/mode.c
 *   Copyright (C) 2001 Garry Boyer
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "channel.h"
#include "msg.h"
#include "h.h"

/*
 * mode.c - taken out of channel.c because the functions simply involved
 *  with reading and writing to mode buffers took up a large quantity of
 *  space in the midst of mostly unrelated functions
 *
 * Almost all of this is original code, Copyright (C) 2001-02 Garry Boyer
 *
 * NOTES: Heavily cleaned up, rewritten, etc.
 *  Bans and o/v don't have to be put into separate buffers at separate
 *  times when doing mode syncs, due to improved modular programming. :)
 *  Now you'll get combinations like
 *     +bbbbbovo a!*@* b!*@* c!*@* d!*@* e!*@* op1 ov1 ov1
 *     +ooovovontl op2 op3 op4 voice1 ov2 ov2 op5 768
 *     +a 1
 *  on separate lines, containing 8 parameters whenever possible.  Note
 *  that +ov is now properly synced! (expanded to 10 parameters)
 *
 *						-- binary
 *
 * NEW: Now, whenever a user sends a command that *might* cause a buffer
 *  overflow, the buffer is flushed and cleared before this might happen.
 *  With mb.pvar stuff gone, one mode command is not necessarily limited
 *  to 12 mode parameters.  Thus, the channel mode +0 not only bzeroes the
 *  mode struct but also removes all channel bans.  Perhaps some sort of
 *  limit should be imposed on users.
 * Now on the input side a mode command can have unlimited parameters; the
 * parse function reads it all as one big param and the mode reader does
 * all the separation.
 */

/* Channel */
extern aClient *find_chasing(aClient *, char *, int *);

/* Mode */
static 	void 	mb_clear();
static 	void 	mb_finalize();
static 	void 	mb_addp(void);
static 	void 	mb_write(char *, char *, int);
static 	void 	mb_writeps(char, char, char *);
static 	void 	mb_writepi(char, char, ush);
static 	void 	mb_writeflag(char, char);
static 	void 	mb_flush(int);

static 	void	mb_sendmemberflags();
static 	void	mb_sendbans();
static 	void	mb_unban(aClient *, char *, int, int);
static 	Ban 	**mb_delban(Ban **);

static 	void 	mb_getmodes(int);
static 	void 	mb_sendall(Mode *, Mode *, int);
static 	int  	mb_readchar(Mode *, ModeFlag *, char, char *, int);
static 	void 	do_mode(aChannel *, aClient *, char *, time_t, int, char);
static 	void 	bounce_mode(aChannel *, aClient *, char *);

static 	ModeBuf	mb; /* check channel.h for info about ModeBuf */

/* Ban */
static	int	find_banex_id(aChannel *chptr, char *banid, Ban **bpp);
static	int	del_banex_id(aClient *cptr, aChannel *chptr, char *banid, Ban **bpp, ui32 flag);
static	int	add_banex_id(aClient *cptr, aChannel *chptr, char *banid, Ban **bpp, ui32 flag, ui32 unflag);

/* Mode Authorization Level defines */
#define MAUTH_BOUNCE	-1
#define MAUTH_ULINE	0
#define MAUTH_SAMODE	1
#define MAUTH_ACCEPT	2
#define MAUTH_SYNC	3
#define MAUTH_OP	4
#define MAUTH_HALFOP	5

/*
 * chanmodes_u[] - unique mode flags that are handled separately,
 *	won't fit under typefied mode categories
 */
static ModeFlag chanmodes_u[] =
{
	{ MODE_BAN,		'b', 0				},
	{ MODE_EXCEPT,		'e', 0				},
	{ MODE_CLEAR,		'0', 0				},
	{ 0, 0, 0 }
};

/*
 * chanmodes_m[] - member flags
 */
static ModeFlag chanmodes_m[] =
{
	{ MODE_MEMBER,		'o', CHFL_CHANOP		},
	{ MODE_MEMBER,		'v', CHFL_VOICE			},
	{ MODE_MEMBER,		'h', CHFL_HALFOP		},
	{ 0, 0, 0 }
};

/*
 * chanmodes_i[] - ones with integer parameters
 */
static ModeFlag chanmodes_i[] =
{
	{ MODE_INTPARAM,	'a', offsetof(Mode, maxrept)	},
	{ MODE_INTPARAM,	'l', offsetof(Mode, limit)	},
	{ MODE_INTPARAM,	'f', offsetof(Mode, flood)	},
	{ 0, 0, 0 }
};

/*
 * chanmodes_s[] - ones with string parameters
 */
static ModeFlag chanmodes_s[] =
{
	{ MODE_STRPARAM,	'k', offsetof(Mode, key[0])	},
	{ 0, 0, 0 }
};

/*
 * chanmodes_f[] - array containing mode definitions for simple flags
 *   that are OR'd into chptr->mode.mode
 */
static ModeFlag chanmodes_f[] =
{
	{ MODE_NOPRIVMSGS,	'n', MAUTH_OP		},
	{ MODE_MODERATED,	'm', MAUTH_OP		},
	{ MODE_TOPICLIMIT,	't', MAUTH_OP		},
	{ MODE_STRIPCOLOR,	'c', MAUTH_OP		},
	{ MODE_SECRET,		's', MAUTH_OP		},
	{ MODE_PRIVATE,		'p', MAUTH_OP		},
	{ MODE_RGSTR,		'r', MAUTH_ULINE	},
	{ MODE_RGSTRONLY,	'R', MAUTH_OP		},
	{ MODE_INVITEONLY,	'i', MAUTH_OP		},
	{ MODE_PRIVMSGONLY,	'z', MAUTH_OP		},
	{ MODE_NODCCS,		'd', MAUTH_OP		},
	{ MODE_NOSAJOIN,	'S', MAUTH_ULINE	},
	{ 0, 0, 0 }
};

#define INTP(mode, mflag) ((ush *)((char *)(mode)+(size_t)mflag->extra))
#define STRP(mode, mflag) ((char *)((char *)(mode)+(size_t)mflag->extra))

static ModeFlag *chan_modemap[256];

/*
 * mode_init() -- init all the mode stuff that needs to be initialized
 *  at start of runtime
 *
 * Currently, initiliazes (for example) chan_modemap['n'] to be a pointer
 * to the ModeFlag struct for the specified channel mode 'n'
 */
void mode_init()
{
	int i;
	ModeFlag *mflag;
	
	for (i = 0; i < 256; i++)
		chan_modemap[i] = NULL;
	
	for (mflag = chanmodes_u; (i=(uch)mflag->c); mflag++)
		chan_modemap[i] = mflag;
	for (mflag = chanmodes_m; (i=(uch)mflag->c); mflag++)
		chan_modemap[i] = mflag;
	for (mflag = chanmodes_s; (i=(uch)mflag->c); mflag++)
		chan_modemap[i] = mflag;
	for (mflag = chanmodes_i; (i=(uch)mflag->c); mflag++)
		chan_modemap[i] = mflag;
	for (mflag = chanmodes_f; (i=(uch)mflag->c); mflag++)
		chan_modemap[i] = mflag;

}


 /**** MODEBUF FUNCTIONS (INIT/FINALIZE/WRITE/FLUSH) ****/


/*
 * mb_init()
 *  - initializes mode buffers and related variables; sets cptr as the
 *  destination client and chptr as the channel
 *  - if sign is non-zero, that character will be placed at the beginning
 *  - from is the client from whom the message came (never null when sending)
 *  - if not null, cptr is the client to send to; otherwise the message
 *  will be sent to all servers and channel members but 'from'
 */
void mb_init(aClient *from, aClient *cptr, aChannel *chptr,
	time_t sendts, char sign)
{
	mb.fsign = sign;
	mb.from = from;
	mb.dest = cptr;
	mb.chptr = chptr;
	mb.sendts = sendts;
	mb.sent = 0;
	mb_clear();
}

/*
 * mb_clear()
 *  - clear the buffers but leave the initial data alone
 */
void mb_clear()
{
	mb.mpos = mb.mbuf;
	if ((mb.sign = mb.fsign) != 0)
		*mb.mpos++ = mb.sign;
	*mb.mpos = '\0';
	mb.ppos = mb.pbuf;
	*mb.ppos = '\0';
	mb.params = 0;
}

/*
 * mb_finalize()
 *  - terminate the strings at the current positions, make mbuf "+" if empty
 */
void mb_finalize()
{
	if (mb.mpos == mb.mbuf)
		*mb.mpos++ = '+';
	*mb.mpos = '\0';
}

/*
 * mb_addp()
 *  - increase parameter and check param count and buffer size; send if
 *  we surpass max mode params or we encounter possible buffer overflow
 *  when reassembling the string before sending it
 */
static void mb_addp(void)
{
	if (mb.params >= 10 || mb.mpos-mb.mbuf+mb.ppos-mb.pbuf > MBSLEN)
		mb_flush(1); /* flush after 10 params or possible overflow */
	mb.params++;
}

/*
 * mb_write()
 *  - write to the modebuf; include string param if sparam is non-null,
 *  or integer param if iparam is non-zero; now with orphan control!
 */
void mb_write(char *change, char *sparam, int iparam)
{
	if (sparam || iparam)
		mb_addp(); /* make sure enough room for another param... */

	mb_writeflag(change[0], change[1]);

	if (sparam)
		mb.ppos += mysprintf(mb.ppos, " %s", sparam);
	else if (iparam)
		mb.ppos += mysprintf(mb.ppos, " %d", iparam);
}

/*
 * mb_writeps()
 *  - write a mode specifically with a string parameter
 */
void mb_writeps(char sign, char flag, char *param)
{
	if (param)
		mb_addp();
	mb_writeflag(sign, flag);
	if (param)
	{
		*mb.ppos = ' ';
		mb.ppos = strput(mb.ppos+1, param);
	}
}

/*
 * mb_writepi()
 *  - write a mode specifically with an integer parameter
 */
void mb_writepi(char sign, char flag, ush param)
{
	if (param)
		mb_addp();
	mb_writeflag(sign, flag);
	if (param)
		mb.ppos += mysprintf(mb.ppos, " %d", param);
}

/*
 * mb_writeflag()
 *  - write a simple mode (no parameters)
 */
void mb_writeflag(char sign, char flag)
{
	if (mb.sign != sign)
		*mb.mpos++ = mb.sign = sign;
	*mb.mpos++ = flag;
}

/*
 * mb_flush()
 *  - send all the data in the buffers, and wipe it out again
 *  - assumes that mb.dest and mb.chptr are already set to NON-NULL vals!
 *  - if sendempty == 0, data will be sent only if the mode bufs are not empty
 */
void mb_flush(int sendempty)
{
	mb_finalize();
	
	if (!sendempty && mb.mpos<mb.mbuf+2)
		return;
	
	if (mb.dest) /* we are sending to just one client */
	{
		if (!IsToken(mb.dest))
			sendto_one(mb.dest, ":%s MODE %s %s%s %d",
				mb.from->name, mb.chptr->chname, mb.mbuf,
				mb.pbuf, mb.sendts);
		else
			sendto_one(mb.dest, TOK_MODE" %s %s%s %d",
				mb.chptr->chname, mb.mbuf, mb.pbuf, mb.sendts);
	}
	else
	{
		sendto_serv_butone(mb.from, ":%n %m %s %s%s %d",
			mb.from, &ms_mode, mb.chptr->chname, mb.mbuf,
			mb.pbuf, mb.sendts);
		if (mb.mbuf[1]) /* don't send blanks to users */
			sendto_channel_butserv(mb.chptr, mb.from, "MODE %s %s%s",
				mb.chptr->chname, mb.mbuf, mb.pbuf);
		if (mb.sendts == TS_SAMODE)
			sendout_globops("%s used SAMODE cmd on %s (%s%s)", mb.from->name, mb.chptr->chname, mb.mbuf, mb.pbuf);
	}
	
	mb_clear();
	mb.sent = 1;
}


 /**** FUNCTIONS THAT PUT TOGETHER MODE STRINGS OR SYNC MODES ****/


/*
 * remove a ban
 */
Ban **mb_delban(Ban **bpp)
{
	Ban *ban = *bpp;
	
	*bpp = ban->next;
	mb_writeps('-', 'b', ban->banstr);
	free_ban(ban);
	
	return bpp;
}

/*
 * mb_unban(cptr, mask)
 *  - if mask specified, clear all bans matching that mask; else if cptr
 *  specified, clear all bans affecting that user; otherwise, clear all bans
 *  - reset banflags accordingly; after this, no user is marked as banned
 *  - tlim: +x, >=x secs old; -x, <= x secs old; 0, no restrictions
 *  - max: at most <max> bans are removed; if max is <=0, no real limit
 */
void mb_unban(aClient *cptr, char *mask, int tlim, int max)
{
	Ban **bpp = &mb.chptr->banlist, *bp;
	Member *mp;
	time_t tmin, tmax;
	
	tmin = 0;
	tmax = nowtime;
	
	if (tlim<0)
		tmin = nowtime + tlim;
	else
		tmax -= tlim;

	if (mask)
	{
	    while ((bp=*bpp))
	        if (!match(mask,bp->banstr)&&bp->when>=tmin&&bp->when<=tmax)
	        {
	            mb_delban(bpp);
	            if (--max==0) break;
	        }
	    else
	        bpp = &bp->next;
	}
	else
	    while ((bp=*(bpp = next_ban(cptr, bpp))))
	        if (bp->when>=tmin && bp->when<=tmax)
	        {
	            mb_delban(bpp);
	            if (--max==0) break;
	        }
	
	for (mp = mb.chptr->members; mp; mp = mp->nextu)
		mp->flags &= ~CHFL_BAN;
}

/*
 * mb_sendbans()
 *  - send channel bans to the already designated destinations
 *  (modebufs must be pre-initialized)
 *  - may be left over data at end
 */
void mb_sendbans()
{
	Ban *ban;

	for (ban = mb.chptr->banlist; ban; ban = ban->next)
		mb_writeps('+', 'b', ban->banstr);
	for (ban = mb.chptr->exceptlist; ban; ban = ban->next)
		mb_writeps('+', 'e', ban->banstr);
}

/*
 * mb_sendmemberflags()
 *  - send appropriate member flags; modebufs must be already initialized
 */
void mb_sendmemberflags()
{
	ModeFlag *mflag;
	Member *mp;
	
	for (mp = mb.chptr->members; mp; mp = mp->nextu)
	{
		if (!(mp->flags&CHFL_OVERLAP)) continue;
		for (mflag = chanmodes_m; mflag->extra; mflag++)
			if (mp->flags&mflag->extra)
				mb_writeps('+', mflag->c, mp->cptr->name);
	}
}

/*
 * this basically takes info for the selected mb.chptr and places a basic
 * channel mode string into mb.mbuf and mb.pbuf.  all the modebuf stuff
 * must be initialized first! (with the '+' sign)
 *  - send parameters only if sendp is nonzero
 */
void mb_getmodes(int sendp)
{
	ModeFlag *mflag;
	aChannel *chptr = mb.chptr;
	ui32 mode = chptr->mode.mode;
	
	for (mflag = chanmodes_f; mflag->flag; mflag++)
		if (mode & mflag->flag)
			*mb.mpos++ = mflag->c;
	
	for (mflag = chanmodes_i; mflag->extra; mflag++)
	{
		ush i = *INTP(&chptr->mode, mflag);
		if (i) mb_writepi('+', mflag->c, sendp?i:0);
	}
	for (mflag = chanmodes_s; mflag->extra; mflag++)
	{
		char *s = STRP(&chptr->mode, mflag);
		if (*s) mb_writeps('+', mflag->c, sendp?s:NULL);
	}
	
	mb_finalize();
}

/*
 * send "cptr" a full list of the modes for channel chptr.
 */
void send_channel_modes(aClient *cptr, aChannel *chptr)
{
	mb_init(&me, cptr, chptr, chptr->channelts, '+');
	if(cptr->prolev < PROLEV_CJOIN)
		mb_sendmemberflags();
	mb_sendbans();
	mb_getmodes(1);
	mb_flush(!mb.sent);
}

/* mb_sendall -- written by binary
 *	Constructs a string containing all the changes between two mode
 *  structures, and flushes at the end.  If the mode buffer is empty,
 *  it will only be sent if sendempty is true and no mode has been sent
 *  out already for this initialization of mb.
 */
void mb_sendall(Mode *oldm, Mode *newm, int sendempty)
{
	ModeFlag *mflag;
	ui32 x, diff;
	
	/* flag +<x> */
	if ((diff = (~oldm->mode)&newm->mode))
		for (mflag = chanmodes_f; (x = mflag->flag); mflag++)
			if ((diff&x))
				mb_writeflag('+', mflag->c);
	/* flag -<x> */
	if ((diff = (~newm->mode)&oldm->mode))
		for (mflag = chanmodes_f; (x = mflag->flag); mflag++)
			if ((diff&x))
				mb_writeflag('-', mflag->c);
	
	/* handle all modes with integer parameters */
	for (mflag = chanmodes_i; mflag->flag; mflag++)
	{
		ush oi = *INTP(oldm, mflag);
		ush ni = *INTP(newm, mflag);
		if (oi==ni) continue;
		mb_writepi(ni?'+':'-', mflag->c, ni);
	}
	
	/* handle all modes with string parameters */
	for (mflag = chanmodes_s; mflag->flag; mflag++)
	{
		char *os = STRP(oldm, mflag);
		char *ns = STRP(newm, mflag);
		if (!strcmp(os, ns)) continue;
		mb_writeps(*ns?'+':'-', mflag->c, *ns?ns:os);
	}
	
	mb_flush(sendempty&&!mb.sent);
}


 /**** FUNCTIONS TO READ A MODE COMMAND ****/


/* mb_readchar
 *  processes one mode character
 *  returns 1 if it ate up a param, otherwise 0
 */
int mb_readchar(Mode *mode, ModeFlag *mflag, char sign,
	char *param, int auth)
{
	ui32 modetype = mflag->flag;
	aClient *who;
	Member *mp;
	ush x, *iptr;
	char add, *key;
	
	/*
	 * Simple flag modes.
	 */
	if (modetype&MODE_SIMPLE)
	{
		if (auth > mflag->extra)
			return 0; /* cannot change this mode */
		
		if (sign == '+')
			mode->mode |= modetype;
		else
			mode->mode &= ~modetype;
		
		return 0;
	}

	add = (sign=='+')?1:0;
	if (auth==MAUTH_BOUNCE) sign = add ? '-' : '+';

	/*
	 * Member Flags (ov)
	 * Any additional member flags shall be handled here.
	 */	
	if (modetype&MODE_MEMBER)
	{
		if (!param)
			return 0;
		if (auth >= MAUTH_HALFOP && mflag->extra != CHFL_VOICE)
			return 1; /* halfops can only do voice */
		if (!(who = find_chasing(mb.from, param, NULL)))
			return 1;
		if (!(mp = find_member(mb.chptr, who)))
		{
			send_num(mb.from, ERR_USERNOTINCHANNEL, who->name,
				mb.chptr->chname);
			return 1;
		}
		x = mp->flags;
		if (add)
			x |= mflag->extra;
		else
			x &= ~mflag->extra;
		if (x != mp->flags || IsULine(mb.from))
		{
			if (auth != MAUTH_BOUNCE) mp->flags = x;
			mb_writeps(sign, mflag->c, who->name);
		}
		return 1;
	}
	
	/*
	 * Channel Ban
	 */	
	if (modetype == MODE_BAN)
	{
		if (!param) return 0;
		param = pretty_mask(param);
		if (auth != MAUTH_BOUNCE)
		{
			if ((!add && del_ban_id(mb.from, mb.chptr, param))
			   ||(add && add_ban_id(mb.from, mb.chptr, param)))
				if (!IsULine(mb.from))
					return 1;
		}
		else if (add == find_banex_id(mb.chptr, param,&(mb.chptr->banlist)))
			return 1;
		mb_writeps(sign, 'b', param);
		return 1;
	}
	
	/*
	 * Channel Except
	 */
	if (modetype == MODE_EXCEPT)
	{
		if (!param) return 0;
		param = pretty_mask(param);
		if (auth != MAUTH_BOUNCE)
		{
			if ((!add && del_except_id(mb.from, mb.chptr, param))
			   ||(add && add_except_id(mb.from, mb.chptr, param)))
				if (!IsULine(mb.from))
					return 1;
		}
		else if (add = find_banex_id(mb.chptr, param, &(mb.chptr->exceptlist)))
			return 1;
		mb_writeps(sign, 'e', param);
		return 1;
	}

	/*
	 * Modes utilizing integer parameters
	 */
	if (modetype&MODE_INTPARAM)
	{
		iptr = INTP(mode, mflag);
		if (add)
		{
			if (param)
			{
				x = atoi(param);
				if ((auth != MAUTH_SYNC) || (x > *iptr))
					*iptr = x;
				return 1;
			}
			else if (!*iptr) /* set equal to 1 only if not set */
				*iptr = 1;
		}
		else
			*iptr = 0;
		return 0;
	}
		
	/*
	 * Modes utilizing string parameters of size KEYLEN
	 */
	if (modetype&MODE_STRPARAM)
	{
		if (!param)
			return 0;
		key = STRP(mode, mflag);
		if (!add)
			*key = '\0';
		else if (auth != MAUTH_SYNC || strcmp(param, key) > 0)
			strncpyzt(key, param, KEYLEN+1);
		return 1;
	}

	/*
	 * Clear all modes and bans
	 * Note: Mode +0 removes all modes, mode -0 removes bans too
	 */
	if (modetype == MODE_CLEAR && auth != MAUTH_BOUNCE)
	{
		bzero(mode, sizeof(Mode));
		if (!add) mb_unban(NULL, NULL, 0, 0);
		return 0;
	}

	return 0;
}

/* mb_readmode
 *	written by binary
 */
void mb_readmode(Mode *mode, char *p, char bounce)
{
	char c, *s = strblanksplit(&p, ' '), *para = strblanksplit(&p, ' ');
	char sign = '+';
	ModeFlag *mflag;
	
	for (; (c = *s); s++)
	{
		if (!(mflag = chan_modemap[(uch)c]))
		{
			if (c == '+' || c == '-')
				sign = c;
			else if (MyClient(mb.from)) /* only yell at my users */
				send_num(mb.from, ERR_UNKNOWNMODE, c);
		}
		else if (mb_readchar(mode, mflag, sign, para, bounce))
			para = strblanksplit(&p, ' ');
	}
}

/* bounce_mode -- written by binary
 *	User or server is NOT authorized to change the mode.  This takes care
 * of making the bounce string and sending it out.
 */
void bounce_mode(aChannel *chptr, aClient *sptr, char *para)
{
	Mode badmode;
	
	bcopy(&chptr->mode, &badmode, sizeof(Mode));
	mb_init(&me, sptr, chptr, chptr->channelts, '\0');
	mb_readmode(&badmode, para, MAUTH_BOUNCE);
	mb_sendall(&badmode, &chptr->mode, 1); /* must send for timestamp */
}

/* do_mode -- written by binary
 *	User or server is authorized to do the mode.  This takes care of
 * setting the mode and relaying it to other users and servers.
 */
void do_mode(aChannel *chptr, aClient *sptr, char *para, time_t sendts,
	int auth, char sendempty)
{
	Mode oldmode;
	
	bcopy(&chptr->mode, &oldmode, sizeof(Mode));
	mb_init(sptr, NULL, chptr, sendts, '\0');
	mb_readmode(&chptr->mode, para, auth);
	mb_sendall(&oldmode, &chptr->mode, sendempty);
}



 /**** PROTOCOL FUNCTIONS FOR CHANNEL-MODE-RELATED COMMANDS ****/


/*
 * m_mode -- rewritten by binary (binary@darkfire.net)
 *  (should hopefully be more readable)
 *
 * parv[1] - channel
 */
int m_mode(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	time_t sendts = TS_NONE;
	int auth;
	Ban *ban;
	aChannel *chptr;
	Member *mp;
	char *s;
	char sendempty=0;

	if (check_registered(sptr) || check_params(sptr, "MODE", parc, 1))
		return 0;
	
	if (*parv[1] != '#')
		return m_umode(cptr, sptr, parc, parv);
	
	if (!(chptr = find_channel(parv[1], sptr)))
		return 0;

	if (parc < 3)
	{
		mb_init(NULL, sptr, chptr, 0, '+');
		mb_getmodes(IsMember(sptr, chptr)); /* send params if member */
		send_num(sptr, RPL_CHANNELMODEIS, chptr->chname, mb.mbuf, mb.pbuf);
		send_num(sptr, RPL_CREATIONTIME, chptr->chname, chptr->channelts);
		return 0;
	}
	
	if (IsPerson(sptr))
	{
		if (parc < 4)
		{
			if ((*parv[2]=='b' && parv[2][1]=='\0') ||
			    (*parv[2]=='+' && parv[2][1]=='b' &&
			    parv[2][2]=='\0'))
			{
				for (ban=chptr->banlist; ban; ban=ban->next)
					send_num(sptr, RPL_BANLIST, chptr->chname,
						 ban->banstr, ban->who, ban->when);
				send_num(sptr, RPL_ENDOFBANLIST, chptr->chname);
				return 0;
			}
			else if ((*parv[2]=='e' && parv[2][1]=='\0') ||
				 (*parv[2]=='+' && parv[2][1]=='e' &&
				 parv[2][2]=='\0'))
			{
				for (ban=chptr->exceptlist; ban; ban=ban->next)
					send_num(sptr, RPL_EXCEPTLIST, chptr->chname,
						 ban->banstr, ban->who, ban->when);
				send_num(sptr, RPL_ENDOFEXCEPTLIST, chptr->chname);
				return 0;
			}
		}

		/* ULine check would be with SAdmin check except a ulined
		 * client might be in a whole lot of channels, and they
		 * do make a large portion of mode changes changes. */
		if (IsULine(sptr))
			auth = MAUTH_ULINE;
		else if (!(mp = find_member(chptr, sptr))
		      || !(mp->flags&(CHFL_CHANOP|CHFL_HALFOP)))
		{
			if (cptr == sptr)
			{
				send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
				return 0;
			} /* ANY BOUNCE EXCEMPTIONS HERE */
			else if (!IsSAdmin(sptr))
				auth = MAUTH_BOUNCE;
			else
				auth = MAUTH_SAMODE;
		}
		else if (mp->flags&CHFL_CHANOP)
			auth = MAUTH_OP;
		else /* if (mp->flags&CHFL_HALFOP), not needed */
			auth = MAUTH_HALFOP;
	}
	else if (IsServer(sptr))
	{
		if (IsULine(sptr))
			auth = MAUTH_ULINE;
		else if ((s = rindex(parv[2],' ')) && (sendts = atoi(s+1)))
		{
			if (chptr->channelts && sendts>chptr->channelts)
				auth = MAUTH_BOUNCE;
			else if (sendts == chptr->channelts)
				auth = MAUTH_SYNC;
			else if (sendts < chptr->channelts || !chptr->channelts)
			{
				auth = MAUTH_ACCEPT;
				sendempty = 1; /* send in order to sync ts */
				chptr->channelts = sendts;
			}
		}
	}
	
	if (auth==MAUTH_BOUNCE)
	{
		bounce_mode(chptr, sptr, parv[2]);
		sendto_umode(UMODE_JUNK, "*** TS Notice -- Mode bounced on %s (%d) from %s (%d) [%s]",
			chptr->chname, chptr->channelts, sptr->name, sendts, parv[2]);
	}
	else
		do_mode(chptr, sptr, parv[2], sendts, auth, sendempty);
        
	return 0;
}

/*
 * m_samode
 * parv[1] = channel
 * parv[2] = modes
 * -taz
 */
int m_samode(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aChannel *chptr;
	char sendempty = 0;

	if (check_registered(cptr) || check_privs(sptr, UMODE_SADMIN, 0))
		return 0;

#ifdef ALLOW_SA_CMD_WITHSERV
	if(ServicesOnline && !OPCanSAWithServ(sptr))
#else
	if(ServicesOnline)
#endif
	{
		send_num(sptr, ERR_SANOSERVICES);
		return 0;
	}
	
	if (check_params(sptr, "SAMODE", parc, 2)
	   || !(chptr = find_channel(parv[1], sptr)))
		return 0;
	
	do_mode(chptr, sptr, parv[2], TS_SAMODE, MAUTH_SAMODE, sendempty);

	return 0;
}


/* help for /rmban command */
static char *rmban_help[] =
{
	"*** Help on /rmban *** ",
	"FUNCTION - Removes bans from a channel with given specifications.",
	" /rmban #channel [mask [time [max]]]",
	"The #channel field specifies which channel to remove bans in.",
	"The mask field can be:",
	" - a mask",
	"     /rmban #chan *!*@*.com [remove bans ending in .com in #chan]",
	"     /rmban #chan * [remove all bans in #chan]"
	" - a nickname of someone online, to unban all bans affecting that user",
	"     /rmban #chan JoeBob [remove all bans affecting JoeBob]",
	" - nothing: unban yourself",
	"The time field can be:",
	" - zero (0) or unspecified: no time restrictions on bans removed",
	" - ># : only bans older than # seconds. the '>' is optional.",
	"     /rmban #chan * 600 [remove bans in #chan older than 10 mins]",
	" - <# : only bans newer than # seconds.",
	"     /rmban #chan * <30 [remove bans less than 30 seconds old]",
	"The max field can be:",
	" - zero (0) or unspecified: no limit on number of bans to remove",
	" - not zero: remove only the 5 newest bans with that mask",
	"     /rmban #chan * 0 10 [remove 10 most recent bans]",
	"NOTE: The fields can be combined in any way, but if you specify",
	"time, you may not skip any fields.",
	"     /rmban #chan *!*@*.foobar.com 30 2",
	" [remove the 2 most recent bans older than 30 seconds for *.foobar.com]",
	"*** End of help ***",
	NULL
};

/*
** m_rmban - unban stuff, also m_unbanme
**  parv[1] = channel (if none, send help screen)
**  parv[2] = mask (if none, self assumed)
**  parv[3] = time limit (+x older than x secs, -x younger than x, 0 default)
**  parv[4] = max count to delete (60 default)
**
**  by RedLightning and sedition
**  expanded/renamed/etc by binary
*/
int m_rmban(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	Member *mp;
	aChannel *chptr;
	aClient *acptr = sptr;
	char *mask = NULL;
	int tlim, max;
	
	if (check_registered_user(sptr))
		return 0;
	if (!parv[1] || *parv[1]=='?')
		return dumpit(sptr, RPL_LISTSYNTAX, rmban_help);
	if (!(chptr = find_channel(parv[1], sptr)))
	{
		send_num(sptr, ERR_NOSUCHCHANNEL, parv[1]);
		return 0;
	}
	
	if (!((mp = find_member(chptr, sptr))
			&& (mp->flags&(CHFL_CHANOP|CHFL_HALFOP)))
		&& !IsULine(sptr))
	{
		if (!IsSAdmin(sptr) || msgptr!=&ms_unbanme)
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return 0;
		}
		sendout_globops("%s used unbanme %s %s %s %s", sptr->name,
			chptr->chname, parv[2]?parv[2]:"on self",
			parv[3] ? parv[3] : "0", parv[4] ? parv[4] : "0");
	}

/* 	if (!MyClient(sptr))
		sptr = &me; */ /* allow services to use w/o fake direction */
	
	if (parv[2] && !(acptr = find_chasing(NULL, parv[2], NULL)))
		mask = parv[2];
	
	if (parv[3])
	{
		if (*parv[3] == '<')
			tlim = -atoi(parv[3]+1);
		else if (*parv[3] == '>')
			tlim = atoi(parv[3]+1);
		else
			tlim = atoi(parv[3]); /* use +/- signs */
	}
	else tlim = 0;
	max = parv[4]?atoi(parv[4]):0;
	
	mb_init(sptr, NULL, chptr, TS_NONE, '\0');
	mb_unban(acptr, mask, tlim, max);
	mb_flush(0);

	if(MyClient(sptr) /* && !IsMe(sptr) */)
		sendto_one(sptr, ":%s NOTICE %s :Removed all bans on %s matching %s, %s %d secs, %d max",
			me.name, sptr->name, parv[1], parv[2]?parv[2]:sptr->name,
			tlim<0?"newer than":"older than", abs(tlim), max);

	return 0;
}

/*
** m_unbanme
**
**  allows SA to unban self from a channel
*/
int m_unbanme(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	return m_rmban(cptr, sptr, parc, parv);
}

/* 
 * Functions used by channel join sending/parsing -dave
 *
 * get_cjoin_modes		create a string of modes
 * send_bans			send bans seperately
 * clear_bans			clear our bans (!keepourmodes)
 */
void get_cjoin_modes(aClient *cptr, aChannel *chptr, char *modebuf,
	char *parabuf)
{
	mb_init(&me, cptr, chptr, chptr->channelts, '+');
	mb_getmodes(1);
	mb_finalize();
	if (mb.sent && mb.mpos<mb.mbuf+2)
		return;
	mysprintf(modebuf, "%s", mb.mbuf);
	mysprintf(parabuf, "%s", mb.pbuf);
	mb_clear();
	mb.sent = 1;
}

void send_bans(aClient *cptr, aChannel *chptr)
{
	mb_init(&me, cptr, chptr, chptr->channelts, '+');
	mb_sendbans();
	mb_flush(!mb.sent);
}

void clear_bans(aClient *cptr, aChannel *chptr, time_t ts)
{
	mb_init(cptr, NULL, chptr, ts, '\0');
	mb_unban((aClient *)NULL, "*", 0, 0);
	mb_flush(0);
}

/*
 * Channel Ban/Except Functions
 *
 * Most of the functions are based on the previous implementations in earlier
 * versions of channel.c despite the new names and dynamic changes to support
 * editing of both bans and excepts.
 *
 * Excepts will override all bans/modes in a channel (acting as a masked
 * overlap) in a channel and may be set by ops
 *
 * Heavily streamlined by dave, 02/01/2003
 */
Ban	*make_ban(char *banid, char *who)
{
	Ban	*ban;
	char	*s;
	int	blen=strlen(banid), wlen=strlen(who);

	ban = (Ban *)MyMalloc(sizeof(Ban)+blen+wlen+2); 
	strcpy(ban->banstr, banid);
	ban->banuser = index(ban->banstr, '!')+1;
	ban->banhost = index(ban->banuser, '@')+1;
	ban->who = ban->banstr + blen + 1;
	strcpy(ban->who, who);
	ban->when = nowtime; 

	return ban;
}

void	free_ban(Ban *ban)
{
	MyFree((char *)ban);
}

int	ban_match(aClient *cptr, Ban *ban, char *nick)
{
	int ret;

	ban->banuser[-1] = '\0';
	ban->banhost[-1] = '\0';

	ret = (!qmatch(ban->banstr, nick)
	    && !qmatch(ban->banuser, cptr->username)
	    && (!qmatch(ban->banhost, cptr->u->rhost)
	    || !qmatch(ban->banhost, cptr->u->dhost)
#ifdef LOCAL_NICKIP
	    || (MyConnect(cptr) &&
	        !qmatch(ban->banhost,(char *)inetntoa((char *)&cptr->ip)))
#endif
	    ));

	ban->banuser[-1] = '!';
	ban->banhost[-1] = '@';

	return ret;
}

Ban	**next_ban(aClient *cptr, Ban **bpp)
{
	Ban *ban = *bpp;

	if(!cptr || !ban) return bpp;

	while (!ban_match(cptr, ban, cptr->name) && (ban=*(bpp=&ban->next))) ;

	return bpp;
}

Ban	*is_banned(aClient *cptr, aChannel *chptr)
{
	if (!IsPerson(cptr)) return NULL;

	return *next_ban(cptr, &chptr->banlist);
}

Ban	*is_excepted(aClient *cptr, aChannel *chptr)
{
	if (!IsPerson(cptr)) return NULL;

	return *next_ban(cptr, &chptr->exceptlist);
}

int	find_banex_id(aChannel *chptr, char *banid, Ban **bpp)
{
	Ban *ban = *bpp;

	for (; ban; ban = ban->next)
		if(!mycmp(banid, ban->banstr))
			return 1;

	return 0;
}

int	add_banex_id(aClient *cptr, aChannel *chptr, char *banid, Ban **bpp, ui32 flag, ui32 unflag)
{
	Ban	*ban = *bpp;
	int	cnt = 0;
	Member	*mp = chptr->members;

	if(MyClient(cptr))
		for(; ban; ban = ban->next)
		{
			if ((++cnt > MAXBANS))
			{
				send_num(cptr, ERR_BANLISTFULL, chptr->chname, banid);
				return -1;
			}
			if (!qmatch(ban->banstr, banid))
				return -1;
		}
	else
		for (; ban; ban = ban->next, cnt++)
			if (!mycmp(ban->banstr, banid))
				return -1;

	ban = make_ban(banid, IsServer(cptr)?cptr->s->sndot:cptr->name);
	ban->next = *bpp;
	*bpp = ban;

	if (IsPerson(cptr) && !(chptr->mode.mode&MODE_MODERATED))
		for (cnt *= 2; mp; mp = mp->nextu)
		{
			if (MyConnect(mp->cptr)&&!(mp->flags&CHFL_OVERLAP))
			{
				if (!(mp->flags&unflag))
					continue;
				if (--cnt == 0)
					break;
				if (!ban_match(mp->cptr, ban, mp->cptr->name))
					continue;
				mp->flags |= flag;
			}
			mp->flags &= ~(unflag);
		}

	for(; mp; mp = mp->nextu)
		mp->flags &= ~(unflag);

	return 0;
}

int	del_banex_id(aClient *cptr, aChannel *chptr, char *banid, Ban **bpp, ui32 flag)
{
	Ban	*ban;
	Member	*mp;

	if(!banid)
		return -1;
	for(; (ban = *bpp); bpp = &ban->next)
		if (!mycmp(banid, ban->banstr))
			break;
	if(!ban)
		return -1;

	strcpy(banid, ban->banstr);
	*bpp = ban->next;
	free_ban(ban);

	for(mp = chptr->members; mp; mp = mp->nextu)
		mp->flags &= ~(flag);

	return 0;
}
