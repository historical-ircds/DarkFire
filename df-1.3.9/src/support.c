/************************************************************************
 *   IRC - Internet Relay Chat, common/support.c
 *   Copyright (C) 1990, 1991 Armin Gruner
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "config.h"
#ifdef DYNIXPTX
#include <sys/timers.h>
#include <stddef.h>
#endif
#include "struct.h"
#include "common.h"
#include "sys.h"
#include "h.h"
#include "msg.h"

extern	int errno; /* ...seems that errno.h doesn't define this everywhere */

#ifndef	CLIENT_COMPILE
extern	void	outofmemory();
#endif

/*
 * strput -- a lot like strcpy, but it actually returns something useful:
 *  the pointer to the NULL character of the destination string
 *					-- binary
 */
char *strput(char *d, char *s)
{
	for (; (*d = *s); s++, d++) ;
	return d;
}

/*
 * strsplit - like strtoken but works with char tokens and it's also efficient
 *					-- binary
 */
char *strsplit(rest, tok)
char **rest, tok;
{
	char *s;
	char *b;
	
	if ((b = *rest))
	{
		s = b;
		while(1)
		{
			if (*s == tok)
			{
				*s = '\0';
				*rest = ++s;
				break;
			}
			if (!*s)
			{
				*rest = NULL;
				break;
			}
			s++;
		}
	}
	
	return b;
}

/*
 * strblanksplit - like strslpit but ignore blank tokens -- dave
 */
char *strblanksplit(char **rest, char tok)
{
        char *s;
        char *b;

        if ((b = *rest))
        {
                s = b;
                while(1)
                {
                        if (*s == tok)
                        {
                                *s = '\0';
				while(*++s == tok);
                                *rest = s;
                                break;
                        }
                        if (!*s)
                        {
                                *rest = NULL;
                                break;
                        }
                        s++;
                }
        }

	return b;
}

#if !defined(HAVE_STRERROR)
/*
**	strerror - return an appropriate system error string to a given errno
**
**		   argv 11/90
**	$Id: support.c,v 1.7 2003/12/21 07:10:19 david Exp $
*/

char *strerror(err_no)
int err_no;
{
	extern	char	*sys_errlist[];	 /* Sigh... hopefully on all systems */
	extern	int	sys_nerr;
	static	char	buff[40];
	char	*errp;

	errp = (err_no > sys_nerr ? (char *)NULL : sys_errlist[err_no]);

	if (errp == (char *)NULL)
	    {
		errp = buff;
		mysprintf(errp, "Unknown Error %d", err_no);
	    }
	return errp;
}

#endif /* HAVE_STRERROR */

/*
**	inetntoa  --	changed name to remove collision possibility and
**			so behaviour is gaurunteed to take a pointer arg.
**			-avalon 23/11/92
**	inet_ntoa --	returned the dotted notation of a given
**			internet number (some ULTRIX don't have this)
**			argv 11/90).
**	inet_ntoa --	its broken on some Ultrix/Dynix too. -avalon
**	$Id: support.c,v 1.7 2003/12/21 07:10:19 david Exp $
*/

char	*inetntoa(in)
char	*in;
{
	static	char	buf[16];
	u_char	*s = (u_char *)in;
	int	a,b,c,d;

	a = (int)*s++;
	b = (int)*s++;
	c = (int)*s++;
	d = (int)*s++;
	mysprintf(buf, "%d.%d.%d.%d", a,b,c,d );

	return buf;
}

#ifndef HAVE_INET_NETOF
/*
**	inet_netof --	return the net portion of an internet number
**			argv 11/90
**	$Id: support.c,v 1.7 2003/12/21 07:10:19 david Exp $
**
*/

int inet_netof(in)
struct in_addr in;
{
    int addr = in.s_net;

    if (addr & 0x80 == 0)
	return ((int) in.s_net);

    if (addr & 0x40 == 0)
	return ((int) in.s_net * 256 + in.s_host);

    return ((int) in.s_net * 256 + in.s_host * 256 + in.s_lh);
}
#endif /* HAVE_INET_NETOF */

/*
 * return time in milliseconds, and update nowtime
 */
int utime()
{
	struct timeval tv;
	
	gettimeofday(&tv, NULL);
	nowtime = tv.tv_sec;
	return(tv.tv_usec);
}

/*
 * myrand() - simple 32-bit random number generator
 *
 *   Algorithm copyright (C) 2001 Garry Boyer.  All rights reserved.
 */
#define	MYRAND_SEED1	0x6C95485C
#define	MYRAND_SEED2	0x9B4D8E75
static struct {u_int32_t a, b, c;} rdat = {0x6FA545C1,0x2E1A7143,0x31ECF2BC};

u_int32_t myrand()
{
	rdat.a ^= ((rdat.b<<16)^(rdat.c>>17)) + MYRAND_SEED1;
	rdat.b ^= ((rdat.c<<15)^(rdat.a>>18)) + MYRAND_SEED2;
	rdat.c ^= ((rdat.a<<14)^(rdat.b>>16));
	return rdat.a^rdat.b^rdat.c;
}

void myrand_init()
{
	rdat.a += getpid();
	rdat.b += time(NULL);
	rdat.c += clock();
}

#if defined(DEBUGMODE) && !defined(CLIENT_COMPILE)
void	dumpcore(msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
char	*msg, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *p9;
{
	static	time_t	lastd = 0;
	static	int	dumps = 0;
	char	corename[12];
	time_t	now;
	int	p;

	now = time(NULL);

	if (!lastd)
		lastd = now;
	else if (now - lastd < 60 && dumps > 2)
		s_die();
	if (now - lastd > 60)
	    {
		lastd = now;
		dumps = 1;
	    }
	else
		dumps++;
	p = getpid();
	if (fork()>0) {
		kill(p, 3);
		kill(p, 9);
	}
	write_pidfile();
	mysprintf(corename, "core.%d", p);
	rename("core", corename);
	Debug((DEBUG_FATAL, "Dumped core : core.%d", p));
	sendto_ops("Dumped core : core.%d", p);
	Debug((DEBUG_FATAL, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9));
	sendto_ops(msg, p1, p2, p3, p4, p5, p6, p7, p8, p9);
		s_die();
}

#endif

char	*MyMalloc(x)
size_t	x;
{
	char *ret = (char *)malloc(x);
	return ret;
}

char	*MyRealloc(x, y)
char	*x;
size_t	y;
{
	char *ret = (char *)realloc(x, y);

	if (!ret)
		outofmemory();
	return ret;
}

static char myspbuf[64] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

/*
 * compact sprintf and vsprintf: (mysprintf/myvsprintf)
 *  handles only:
 *   %% - print %
 *   %s - print string specified in parameter
 *   %c - print character in parameter
 *   %t - timestamp
 *   %[[0]#]{d/x/X} - print unsigned integer:
 *      meanings of d/x: [d]ecimal (31), he[x] (1F)
 *      other meanings:
 *      0 - pad with zeroes
 *      # - field width (ie, mysprintf(buf, "%04d", 12) would yield "0012")
 *
 * pformat/pformatv contain also:
 *   %f - next param is u_int32_t; replace flags parameter with these
 *   %m - next param is Msg *; if PROLEV_TOKEN specified, use tokens.
 *   %n - print user or server name; if PROLEV_SERVHASH is specified, use the
 *      server hash short name with leading dot.
 *   %p - like a prefix; add !user@host for users if PROLEV_USER specified
 *
 */
int pformatv(int prolev, char *dest, char *pattern, va_list vl)
{
    u_int x;
    char *p = pattern, *d = dest, *s, *sx, c, c2;
    char filler;
    char field;

    while ((c = *p++))  
    {
        if (c != '%')
        {
            *d++ = c;
            continue;
        } 

        c2 = *p++;

        if (c2 == 'c') /* append character */
        {
            *d++ = (char)va_arg(vl, int);
            continue;
        }
        if (c2 == 's') /* append string */
        {
            if (!(s = va_arg(vl, char *))) s = "(null)";
            for (; (c = *s); s++)
                *d++ = c;
            continue;
        }
        if (c2 == 'm')
        {
            Msg *mptr = va_arg(vl, Msg *);
            if (prolev>=PROLEV_TOKEN && (char)*mptr->token != NOTOKCOMP)
                *d++ = (char)*mptr->token;
            else
                d = strput(d, mptr->cmd);
            continue;
        }
        if (c2 == 'n' || c2 == 'p')
        {
            aClient *acptr = va_arg(vl, aClient *);
            if (!acptr || !*acptr->name)
            {
                *d++ = '*';
                continue;
            }
            if (!IsPerson(acptr))
            {
                if (prolev>=PROLEV_SERVHASH)
                    d = strput(d, acptr->s->sndot);
                else
                    d = strput(d, acptr->name);
                continue;
            }
            d = strput(d, acptr->name);
            if (c2 == 'p' && (prolev==PROLEV_USER))
            {
                *d++ = '!';
                d = strput(d, acptr->username);
                *d++ = '@';
                d = strput(d, acptr->u->host);
            }
            continue;
        }
        if (c2 == 'f')
        {
            prolev = va_arg(vl, int);
            continue;
        }
        if (c2 <= '9')
        {
            if (c2 < '0') /* take care of %% situation plus some unknowns */
            {
                *d++ = c2;
                continue;
            }
            if (c2 == '0') /* %09d ; handle 0 to mean zero-padding */
            {
                filler = '0';
                c2 = *p++;
            }
            else
                filler = ' ';
            field = c2-'0'; /* %3d/%03d ; handle 3 to mean fieldwidth 3 */
            c2 = *p++;
        }
        else
            field = 0;

        if (c2 == 't')
            x = va_arg(vl, time_t);
        else
                x = va_arg(vl, u_int);
        s = &myspbuf[16];

        if (c2 == 'd') /* decimal conversion */
            do { *--s = '0'|(char)(x%10); } while ((x /= 10));
        else /* hexadecimal conversion */
            do { c=x&0xF; *--s=c+(c<10?'0':('A'-10)); } while ((x >>= 4));

        if (field)
            for (sx = d-(&myspbuf[16]-s)+field; d < sx;)
                *d++ = filler;

        for (; (c = *s); s++)
            *d++ = c;
    }

    *d = '\0';

    return d - dest;
}

int pformat(int prolev, char *dest, char *pattern, ...)
{
        va_list vl;
        int i;

        va_start(vl, pattern);
        i = pformatv(prolev, dest, pattern, vl);
        va_end(vl);

        return i;
}

int myvsprintf(char *dest, char *pattern, va_list vl)
{
    u_int x;
    char *p = pattern, *d = dest, *s, *sx, c, c2;
    char filler;
    char field;

    while ((c = *p++))
    {
        if (c != '%')
        {
            *d++ = c;
            continue;
        }

        c2 = *p++;

        if (c2 == 'c') /* append character */
        {
            *d++ = (char)va_arg(vl, int);
            continue;
        }
        if (c2 == 's') /* append string */
        {
            if (!(s = va_arg(vl, char *))) s = "(null)";
            for (; (c = *s); s++)
                *d++ = c;
            continue;
        }
        if (c2 <= '9')
        {
            if (c2 < '0') /* take care of %% situation plus some unknowns */
            {
                *d++ = c2;
                continue;
            }
            if (c2 == '0') /* %09d ; handle 0 to mean zero-padding */
            {
                filler = '0';
                c2 = *p++;
            }
            else
                filler = ' ';
            field = c2-'0'; /* %3d/%03d ; handle 3 to mean fieldwidth 3 */
            c2 = *p++;
        }
        else
            field = 0;

        x = va_arg(vl, u_int);
        s = &myspbuf[16];

        if (c2 == 'd') /* decimal conversion */
            do { *--s = '0'|(char)(x%10); } while ((x /= 10));
        else /* hexadecimal conversion */
            do { c=x&0xF; *--s=c+(c<10?'0':('A'-10)); } while ((x >>= 4));

        if (field)
            for (sx = d-(&myspbuf[16]-s)+field; d < sx;)
                *d++ = filler;

        for (; (c = *s); s++)
            *d++ = c;
    }

    *d = '\0';

    return d - dest;
}

int mysprintf(char *dest, char *pattern, ...)
{
        va_list vl;
        int i;

        va_start(vl, pattern);
        i = myvsprintf(dest, pattern, vl);
        va_end(vl);

        return i;
}

/*
** read a string terminated by \r or \n in from a fd
**
** Created: Sat Dec 12 06:29:58 EST 1992 by avalon
** Returns:
**	0 - EOF
**	-1 - error on read
**     >0 - number of bytes returned (<=num)
** After opening a fd, it is necessary to init dgets() by calling it as
**	dgets(x,y,0);
** to mark the buffer as being empty.
*/
int	dgets(fd, buf, num)
int	fd, num;
char	*buf;
{
	static	char	dgbuf[8192];
	static	char	*head = dgbuf, *tail = dgbuf;
	char	*s, *t;
	int	n, nr;

	/*
	** Sanity checks.
	*/
	if (head == tail)
		*head = '\0';
	if (!num)
	    {
		head = tail = dgbuf;
		*head = '\0';
		return 0;
	    }
	if (num > sizeof(dgbuf) - 1)
		num = sizeof(dgbuf) - 1;
dgetsagain:
	if (head > dgbuf)
	    {
		for (nr = tail - head, s = head, t = dgbuf; nr > 0; nr--)
			*t++ = *s++;
		tail = t;
		head = dgbuf;
	    }
	/*
	** check input buffer for EOL and if present return string.
	*/
	if (head < tail &&
	    ((s = index(head, '\n')) || (s = index(head, '\r'))) && s < tail)
	    {
		n = MIN(s - head + 1, num);	/* at least 1 byte */
dgetsreturnbuf:
		bcopy(head, buf, n);
		head += n;
		if (head == tail)
			head = tail = dgbuf;
		return n;
	    }

	if (tail - head >= num)		/* dgets buf is big enough */
	    {
		n = num;
		goto dgetsreturnbuf;
	    }

	n = sizeof(dgbuf) - (tail - dgbuf) - 1;
	nr = read(fd, tail, n);
	if (nr == -1)
	    {
		head = tail = dgbuf;
		return -1;
	    }
	if (!nr)
	    {
		if (head < tail)
		    {
			n = MIN(tail - head, num);
			goto dgetsreturnbuf;
		    }
		head = tail = dgbuf;
		return 0;
	    }
	tail += nr;
	*tail = '\0';
	for (t = head; (s = index(t, '\n')); )
	    {
		if ((s > head) && (s > dgbuf))
		    {
			t = s-1;
			for (nr = 0; *t == '\\'; nr++)
				t--;
			if (nr & 1)
			    {
				t = s+1;
				s--;
				nr = tail - t;
				while (nr--)
					*s++ = *t++;
				tail -= 2;
				*tail = '\0';
			    }
			else
				s++;
		    }
		else
			s++;
		t = s;
	    }
	*tail = '\0';
	goto dgetsagain;
}
