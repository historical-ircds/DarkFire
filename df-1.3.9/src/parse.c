/************************************************************************
 *   IRC - Internet Relay Chat, common/parse.c
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* -- Jto -- 03 Jun 1990
 * Changed the order of defines...
 */

#include "struct.h"
#include "common.h"
#define MSGTAB
#include "msg.h"
#undef MSGTAB
#include "sys.h"
#include "numeric.h"
#include "h.h"
#include "dvs.h"
#include <fcntl.h>
#include <sys/types.h>

/*
 * NOTE: parse() should not be called recursively by other functions!
 */
static	int	remove_unknown PROTO((aClient *, char *));
static	int	do_numeric PROTO((aClient *, aClient *, char *, char *));
static	Msg	*find_command PROTO((aClient *, char *));

Msg *msgptr;

/*
**  Find person by (nick)name.
*/
aClient *find_person(name)
char	*name;
{
	aClient	*c2ptr = find_client(name);

	if (c2ptr && IsClient(c2ptr))
		return c2ptr;
	else
		return NULL;
}

/*
 * parse()
 *  - compact parser, rewritten by binary; never call this recursively!
 */
int parse(aClient *cptr, char *buffer, char *bufend)
{
	static char *parv[MAXPARA+2];
	aClient *sptr = cptr;
	int parc, maxparc, i, tdif;
	char *s = buffer, *p, *q;
	Msg *mptr;
	
	if (IsDead(cptr))
		return 0;
	if (!IsServer(cptr))
#ifdef OPERS_CAN_FLOOD
		if(!IsAnOper(cptr) || !IsExemptFromFlood(cptr))
#endif
			cptr->since += 1 + ((bufend-buffer+24)>>6);
	
	for (i = 0; i < MAXPARA+2; i++)
		parv[i] = NULL;
	
	if (*s == ':')
	{
		for (p = ++s; *s > ' '; s++) ;
		if (!*s) return 0; /* just a sender.. */
		*s++ = '\0';
		if (IsServer(cptr))
		{
			if(*p)
			{
				if (!(sptr = find_client(p)))
					return remove_unknown(cptr, p);
				if (sptr->from != cptr)
					return 0; /* fake direction, ignore! */
				cptr->s->lastsptr = sptr;
			}
			else
				sptr = cptr->s->lastsptr;
		}
	}
	
	for (p = s; (u_char)*s > ' '; s++) ;
	if (*(q = s)) *s++ = '\0';
	
	if (q-p == 3 && isdigit(*p) && isdigit(p[1]) && isdigit(p[2]))
		return do_numeric(cptr, sptr, p, s);
	if (!(mptr = find_command(cptr, p)))
	{
		send_num(cptr, ERR_UNKNOWNCOMMAND, p);
		return 0;
	}
	
	parv[0] = sptr->name;
	for (parc = 0, i = mptr->parameters;;)
	{
		while (*s == ' ') s++; /* cure "FRENCH " infection */
		if (*s == ':')
		{
			parv[++parc] = s+1;
			break;
		}
		parv[++parc] = s;
		if (parc >= i) break;
		for (; *s != ' '; s++)
			if (!*s) goto parse_done;
		*s++ = '\0';
	}	
parse_done:

	if (parc && !*parv[parc]) /* blank - make NULL; don't inc parc */
		parv[parc] = NULL;
	else
		parc++;
	
	mptr->count++;
	mptr->bytes += bufend-buffer;

#ifdef USE_DVS
	if (IsPerson(cptr) && IsDVS(cptr) && !(mptr->msgfl&MSGFL_DVS))
	{
		send_num(cptr, ERR_DVSLIMITED);
		return 0;
	}
#endif

	if(IsPerson(cptr) && cptr->u->hush && !(mptr->msgfl&MSGFL_HUSH))
	{
		send_num(cptr, ERR_YOUREHUSHED, sptr->u->hush->s);
		return 0;
	}

	msgptr = mptr;
	
	microtime = utime();
	i = (*mptr->func)(cptr, sptr, parc, parv);
	if (i != FLUSH_BUFFER)
	{
		tdif = utime()-microtime;
		if (tdif<0) tdif += 1000000; /* assume won't take >1s */
		if (tdif>1000) tdif = 0; /* hack to weed out task switching */
		if (IsServer(cptr))
			mptr->r_utime += tdif;
		else
			mptr->l_utime += tdif;
	}
	return i;
}

/*
 * find command specified by user
 *  note: a sort of bubble/insertion sort implemented here -- each time a
 * command is found, it is bubbled up so the list should be sorted by
 * usage, most common at top
 */
Msg *find_command(aClient *cptr, char *cmd)
{
	Msg *mptr;
	int i;
	char *s1, *s2;
	
	if (!*cmd)
		return NULL;
	if (!cmd[1])
		return IsToken(cptr)?msgmap[(u_char)*cmd]:NULL;
	mystrupper(cmd);
	for (i = 0; (mptr = msgtab[i]); i++)
	    for (s1=cmd, s2=mptr->cmd; *s1==*s2; s1++, s2++)
		if (!*s1)
		{
			while (i != 0 && msgtab[i-1]->count <= mptr->count)
			{
				msgtab[i] = msgtab[i-1];
				i--;
			}
			return (msgtab[i] = mptr);
		}
	return NULL;
}

/*
 * do_numeric
 *   
 * Blind numeric relays.  In other words, we don't even peek at what we are
 * getting, EXCEPT for extracting the destination user, which is necessary.
 * Ignore persons, don't send back to the same link, etc.  However, we will
 * We no longer bother to relay numerics 000-099 as 100-199.
 *   
 * modified to allow numerics to channels upon a request.
 */
int     do_numeric(aClient *cptr, aClient *sptr, char *n, char *s)
{
        aClient *acptr;
        aChannel *chptr;
        char *p;

        if (!IsServer(sptr) || !s || !(p = index(s, ' ')))
                return 0;
        *p++ = '\0';
        if ((acptr = find_client(s)) && IsPerson(acptr) && acptr->from!=cptr)
                sendto_one(acptr, ":%n %s %s %s", sptr, n, s, p);
        else if (IsChannelName(s) && (chptr = hash_find_channel(s)))
                sendto_channel_members(cptr, chptr, ":%n %s %s %s", sptr, n, s, p);

        return 0;   
}

/*
 * remove unknown sender
 */
static	int	remove_unknown(cptr, sender)
aClient	*cptr;
char	*sender;
{
	if (!IsServer(cptr))
		return;
	/*
	 * Do kill if it came from a server because it means there is a ghost
	 * user on the other server which needs to be removed. -avalon
	 */
	if (index(sender, '.'))
		sendto_one(cptr, ":%s SQUIT %s :(Unknown from %s)",
			   me.name, sender, get_client_name(cptr, FALSE));
	else /* Now fquit.  MUCH less likely to cause a desync. */
		sendto_one(cptr, ":%s FQUIT %s :%s (Unknown from %s)",
			me.name, sender, me.name, get_client_name(cptr, FALSE));
	return -1;
}

/*
 * field breakup for ircd.conf file.
 */
char	*getfield(newline)
char	*newline;
{
	static	char *line = NULL;
	char	*end, *field;
	
	if (newline)
		line = newline;
	if (line == NULL)
		return(NULL);

	field = line;
	if ((end = (char *)index(line,':')) == NULL)
	    {
		line = NULL;
		if ((end = (char *)index(field,'\n')) == NULL)
			end = field + strlen(field);
	    }
	else
		line = end + 1;
	*end = '\0';
	return(field);
}

/*
 * inittoken
 * Cheat here, blah. Build the lookup tables from msgtab's,
 * call them msgmap's. Called in main() with other inits.
 */
void	inittoken(void)
{
	int loopy;
	Msg *mptr;

	/* Point all entries to it */
	for (loopy = 0; loopy<256; loopy++)
		msgmap[loopy] = NULL;
	/* Build references to existing commands */
	for (loopy = 0; (mptr = msgtab[loopy]); loopy++)
		if((char)*mptr->token != NOTOKCOMP)
			msgmap[(u_char)mptr->token[0]] = mptr;
}

