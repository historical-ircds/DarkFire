/****************************************************************************
 *
 *  alloc.c - two different algorithms for chunk memory allocation
 *     containing code from dalloc.c and zalloc.c
 *
 *					Copyright (C) 2001 Garry Boyer
 *					All Rights Reserved.
 */

#include "struct.h"


/****************************************************************************
 *  dalloc.c - chunk allocation that saves memory after "freed"
 *
 *    Memory allocation in chunks for better performance.
 *
 *    The algorithm used here is very very short and streamlined.  The
 *  drawback is that the dalloc functions will not handle well any sort
 *  of massive purge -- this algorithm makes no attempt to free memory
 *  once it's been allocated.  It is assumed that the memory usage will
 *  tip off at a sort of peak and stay more or less around that peak or
 *  reach that peak again and again periodically.  Total memory usage
 *  should not be affected too much -- free() itself often leaves many
 *  fragmented holes of empty space which are often unusable.
 *
 */


/*
 * allocate a new block from given master
 */
void *da_newblock(DMast *master)
{
	void *data, *last;
	size_t size = master->size;
	
	master->blocks++;
	data = malloc(size*master->count);
	last = data+size*(master->count-1);
	*(void**)last = master->free;
	master->free = data;
	
	while (data != last)
		data = *(void **)data = data+size;
	
	return master->free;
}

/*
 * initialize a master
 */
void da_masterinit(DMast *master, size_t size, int count)
{
	master->size = DA_ADJ_LONG(size);
	master->count = count;
	master->free = NULL;
	master->blocks = master->used = 0;
}

/*
 * allocate a chunk of memory sized master->size using block allocation
 * methods, first checking the free list and if there is no free item
 * allocate a new block.
 */
void *da_alloc(DMast *master)
{
	void *s;
	
	if (!(s = master->free)) s = da_newblock(master);
	master->free = *(void **)s;
	master->used++;
	
	return s;
}

/*
 * like da_alloc but also zeores the memory
 */
void *da_calloc(DMast *master)
{
	void *s;
	long *lptr;
	
	if (!(s = master->free)) s = da_newblock(master);
	master->free = *(void **)s;
	master->used++;
	
	lptr = (long *)(s + master->size);
	
	while (lptr != (long *)s)
		*--lptr = 0;
	
	return s;
}

/*
 * free a block of memory onto the top of the master's free list
 */
void da_free(DMast *master, void *s)
{
	if (!s) return;
	
	*(void **)s = master->free;
	master->free = s;
	master->used--;
}


/****************************************************************************
 *  zalloc.c
 *
 *  Memory allocation in blocks, tries to crystallize free memory so that
 *  it's possible to free entire blocks at a time.  Resolves issues of
 *  having huge stores of unused "freed" memory lying around after use
 *  in a burst period.  Included mainly for dbufs -- their usage is almost
 *  always around 4kb, but during peak periods or serious lag this figure
 *  can balloon to tens of megabytes.
 *
 */

ZBlock *zblock_alloc(ZMast *master);

/*
 * zmast_init()
 *
 * - initialize a zalloc master struct according to the user's specs:
 *    - size: the size (in bytes) of the chunks of memory that will be
 *    returned when calling zalloc with this particular master.
 */
void zmast_init(ZMast *master, size_t size, int count)
{
	ZBlock *block;
	
	master->size = size = ROUNDUP(size,sizeof(long));
	master->count = count;
	master->bsize = (size+offsetof(ZItem, data))*count
		+ offsetof(ZBlock, items);
	master->empty = master->last = master->full = NULL;
	
	master->n_blocks = 0;
	master->n_items = 0;
}

/*
 * zblock_alloc()
 *
 * - allocate this block of items in ram according to the specified item size
 * and item count, and initialize various properties of this block to
 * appropriate values.  this will be the only block in the master's empty
 * list, this is only called when there are no non-full blocks.
 *
 */
ZBlock *zblock_alloc(ZMast *master)
{
	size_t size;
	int count;
	ZBlock *block;
	ZItem *item, *last;
	
	if (master->empty) /* only allocate if list is empty */
		return master->empty;
	
	size = master->size+offsetof(ZItem,data);
	count = master->count;
	
	block = (ZBlock *)malloc(master->bsize);
	
	item = block->items;
	last = (ZItem *)(block->data + (count-1)*size);
	
	while (item <= last)
	{
		item->block = block;
		item = item->next = (ZItem *)((char *)item + size);
	}
	last->next = NULL;
	
	block->free = block->items;
	block->n_free = count;
	block->master = master;
	
	master->n_blocks++;
	
	block->prev = NULL;
	block->next = NULL;
	master->empty = block;
	master->last = block;
	
	return block;
}


/*
 * zalloc() - dynamic memory allocation in slabs
 *
 * - returns pointer to a new reserved data area of the size specified by
 *   master->size.
 *
 * - method: checks the first block on the master's list of empty blocks
 *    - if there is no block, allocate a new one
 *    - pick a free element from the block and return that pointer.  if
 *    the block ends up being filled, it is then put on the full list.
 *
 */
void *zalloc(ZMast *master)
{
	ZBlock *block = master->empty;
	ZBlock *curr;
	ZItem *item;
	
	if (!block)
		block = zblock_alloc(master);
	
	item = block->free;
	block->free = item->next;
	item->next = NULL;
	
	master->n_items++;
	
	if (--block->n_free == 0)
	{
		/* it's at the top, the only where to go is to other list */
		if ((master->empty = block->next))
			block->next->prev = NULL;
		else
			master->last = NULL;
		block->prev = NULL;
		if ((block->next = master->full))
			master->full->prev = block;
		master->full = block;
	}
	
	return (void *)item->data;
}

/*
 * zfree() - free dynamic memory obtained by zalloc()
 *
 *  - returns the item to the free list of its block
 *  - make sure block is in the right spot on the right list;
 *  if the block was previously full put it at top of empty list
 *  otherwise make sure it's in the right spot so that the list
 *  of empty ones is in ascending order based on n_free values
 *  (most heavily used blocks are at end of list)
 */
void zfree(void *s)
{
	ZItem *item = ZA_GETITEM(s);
	ZBlock *block = item->block;
	ZBlock *curr;
	ZMast *master;
	
	if (!ZB_INRANGE(item, block, master))
		return;

	item->next = block->free;
	block->free = item;
	master->n_items--;

	if (++block->n_free == 1)
	{
		if (block->prev)
			block->prev->next = block->next;
		else
			master->full = block->next;
		if (block->next)
			block->next->prev = block->prev;
		block->prev = NULL;
		if ((block->next = master->empty))
			master->empty->prev = block;
		else
			master->last = block;
		master->empty = block;
	}
	else if ((curr=block->next) && (block->n_free>curr->n_free))
	/* block is out of order -- move it */
	{
		/* detach block */
		curr->prev = block->prev;
		if (curr->prev)
			curr->prev->next = curr;
		else
			master->empty = curr;
		if (block->n_free >= master->last->n_free)
		{
			/* shortcut case to put it at end */
			block->next = NULL;
			block->prev = master->last;
			master->last->next = block;
			master->last = block;
		}
		else
		{
			/* this isn't the last block, and its free count is
			 * greater than the next free count but not greater
			 * than the last block's free count.  it must be in
			 * the middle.  to save time, just move it two
			 * spaces max.
			 */
			curr = curr->next;
			if (block->n_free>curr->n_free)
				curr = curr->next;
			block->next = curr;
			block->prev = curr->prev;
			curr->prev = block;
			block->prev->next = block;
		}
	}
}

/*
 * free up to n totally empty blocks for this master
 * returns actual number of empty blocks freed
 */
int zmast_freemem(ZMast *master, int n)
{
	int n2 = 0;
	ZBlock *block = master->last;
	ZBlock *prev;
	
	while (n2<n&&(block=master->last)&&(block->n_free==master->count))
	{
		if ((prev = master->last->prev))
			prev->next = NULL;
		else
			master->empty = NULL;
		master->last = prev;
		master->n_blocks--;
		free(block);
		n2++;
		block = prev;
	}
	return n2;
}

