/************************************************************************
 *   IRC - Internet Relay Chat, ircd/channel.c
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Co Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "channel.h"
#include "hash.h"	/* For CHANNELHASHSIZE */
#include "h.h"
#include "dvs.h"

aChannel *channel = NullChn;

static	void	add_invite PROTO((aClient *, aChannel *));
static	void	sub1_from_channel PROTO((aChannel *));

static	int	can_join PROTO((aClient *, aChannel *, char *));

void	clean_channelname PROTO((char *));
void	del_invite PROTO((aClient *, aChannel *));

int repeatcheck(Member *mp, char *msg);
int floodcheck(Member *mp, char *msg);

DA_MASTER_D(member_mast, sizeof(Member), 4072);
DA_MASTER_D(chan_mast, sizeof(aChannel), 16288);

extern	Ban *make_ban(char *banid, char *who);

/*
 * some buffers for rebuilding channel/nick lists with ,'s
 */ 
static	char	nbuf[BUFSIZE], buf[BUFSIZE], modebuf[MODEBUFLEN], parabuf[PARABUFLEN], chankey[CHANNELLEN+KEYLEN+2];

/*
 * return the length (>=0) of a chain of links.
 */
static	int	list_length(lp)
Link	*lp;
{
	int count = 0;

	for (; lp; lp = lp->next)
		count++;
	return count;
}

/*
** find_chasing
**	Find the client structure for a nick name (user) using history
**	mechanism if necessary. If the client is not found, an error
**	message (NO SUCH NICK) is generated. If the client was found
**	through the history, chasing will be 1 and otherwise 0.
*/
aClient *find_chasing(sptr, user, chasing)
aClient *sptr;
char	*user;
int	*chasing;
{
	aClient *who = find_client(user);

	if (who)
	{
		if (chasing)
			*chasing = 0;
		return who;
	}
	if (!(who = get_history(user, (long)KILLCHASETIMELIMIT)))
	{
		if (sptr&&sptr!=&me)
	    		send_num(sptr, ERR_NOSUCHNICK, user);
		return NULL;
	}
	if (chasing)
		*chasing = 1;
	return who;
}

/*
 * cannot_do_nick - can user do this nick change?
 */
int cannot_do_nick(Member *mp, char *nick)
{
	Ban *ban;
	char c;
	
	if (mp->flags&CHFL_OVERLAP)
		return 0;
	if ((c=can_member_send(mp))!=0)
	{
		send_num(mp->cptr, ERR_BANNICKCHANGE, mp->chptr->chname, c);
		return 1;
	}
	if (is_member_banned(mp) && !is_member_excepted(mp))
	{
		send_num(mp->cptr, ERR_BANONCHAN, mp->chptr->chname);
		return 1;
	}
	return 0;
}

/* Check Bans/Excepts */
extern int is_member_banned(Member *mp)
{
	if (!(mp->flags&(CHFL_BANSET)))
	{
		if (*next_ban(mp->cptr, &mp->chptr->banlist))
			mp->flags |= CHFL_BAN;
		else
			mp->flags |= CHFL_UNBAN;
	}
	return (mp->flags)&(CHFL_BAN);
}

extern int is_member_excepted(Member *mp)
{
	if (!(mp->flags&(CHFL_EXCEPTSET)))
	{
		if (*next_ban(mp->cptr, &mp->chptr->exceptlist))
			mp->flags |= CHFL_EXCEPT;
		else
			mp->flags |= CHFL_UNEXCEPT;
	}
	return (mp->flags)&(CHFL_EXCEPT);
}

/*
 * find_member
 *
 *  - Returns pointer to member struct if user is found in chan; else NULL
 */
extern Member *find_member(aChannel *chptr, aClient *cptr)
{
	Member *mp;
	
	if (!IsPerson(cptr) || !(mp = cptr->u->channel))
		return NULL;
	
	while ((mp->chptr != chptr) && (mp = mp->nextc))
		;

	return mp;
}

/*
 * find_chbuf_member
 * Return pointer to member struct if user has channel in the their chbuf
 */
Member *find_chbuf_member(char *chname, aClient *cptr)
{
	Member	*mp;
	char	*name, *key = NULL;

#ifdef USE_DVS
	if (!IsPerson(cptr) || !(mp = cptr->chbuf))
		return NULL;

	for(; mp; mp = mp->nextc)
	{
		name = (char*)mp->nextu;
		if ((key = index(name, ' ')))
		{
			*key = '\0';
			if (!strcasecmp((char*)mp->nextu, chname))
				break;
			*key = ' ';
		}
		else if (!strcasecmp((char*)mp->nextu, chname))
			break;
	}
#endif

	return mp;
}

/*
 * chbuf_del(mp)
 * Delete a channel buffer entry (used when DVS client exits)
 */
void	chbuf_del_all(aClient *sptr)
{
#ifdef USE_DVS
	Member *mp, **mpp = &sptr->chbuf;

	while((mp = *mpp))
	{
		mpp = &mp->nextc;
		free(mp->nextu);
		da_free(&member_mast, mp);
	}
	sptr->u->joined = 0;
#endif
}

/*
 * member_add(chptr, cptr, flags)
 *
 *   Creates member struct and inserts it at the beginning of chptr's user
 * list and cptr's channel list.  Sets flags accordingly.
 */
Member *member_add(aChannel *chptr, aClient *cptr, int flags)
{
	Member *mp = da_alloc(&member_mast);
	Member **mpp = (MyConnect(cptr))?chptr->lmembers:&chptr->members;

	mp->nextu = *mpp;
	*mpp = mp;
	if (!MyConnect(cptr) && (chptr->lmembers==&chptr->members))
		chptr->lmembers = &mp->nextu;
	chptr->users++;
	
	mp->nextc = cptr->u->channel;
	cptr->u->channel = mp;
	cptr->u->joined++;
	
	mp->chptr = chptr;
	mp->cptr = cptr;
	mp->flags = flags;
	mp->repeats = 0;
	mp->msg1 = 0;
	mp->msg2 = 0;
	if (MyConnect(cptr))
		mp->since = (nowtime*10+microtime/100000)-chptr->mode.flood;
	
	return mp;
}

/*
 * member_del(mp)
 *
 *   Removes user's member link from both the channel's and user's lists.
 */
void	member_del(Member *mp)
{
	aClient *cptr = mp->cptr;
	aChannel *chptr = mp->chptr;
	Member **mpp;

	mpp = &cptr->u->channel;
	while (*mpp != mp) mpp = &(*mpp)->nextc;
	*mpp = mp->nextc;

	mpp = MyConnect(cptr) ? chptr->lmembers : &chptr->members;
	while (*mpp != mp) mpp = &(*mpp)->nextu;
	*mpp = mp->nextu;
	if (&mp->nextu == chptr->lmembers)
		chptr->lmembers = mpp;

	da_free(&member_mast, mp);

	cptr->u->joined--;
	sub1_from_channel(chptr);
}

char can_member_send(Member *mp)
{
	if (!(mp->flags&(CHFL_OVERLAP) || is_member_excepted(mp)))
	{
		if (mp->chptr->mode.mode&MODE_MODERATED)
			return 'm';
		if (is_member_banned(mp))
			return 'b';
	}
	return 0;
}

char	can_send(aClient *cptr, aChannel *chptr, char *msg, int notice)
{
	Member *mp;
	u_int32_t mode;

	if (IsServer(cptr) || IsULine(cptr))
		return 0;

	mode = chptr->mode.mode;
	
	if (mode&MODE_STRIPCOLOR && MyConnect(cptr))
		strip_colors(msg);

	if (!(mp = find_member(chptr, cptr)))
	{
		if (mode&(MODE_NOPRIVMSGS|MODE_MODERATED))
			return ('n');
		if (is_banned(cptr, chptr) && !is_excepted(cptr, chptr))
			return ('b');
		return 0;
	}

	if (MyConnect(cptr))
	{
		if ((mp->flags & (CHFL_OVERLAP)) || is_member_excepted(mp) ||
		    IsOper(cptr))
			return 0;
		if (mode&MODE_MODERATED)
			return ('m');	
		if (is_member_banned(mp))
			return ('b');
		if (chptr->mode.flood && floodcheck(mp, msg))
			return ('f');
		if ((chptr->mode.maxrept) && repeatcheck(mp, msg))
			return 1;
		if (mode&MODE_PRIVMSGONLY && (notice ||
		   (*msg == 1 && strncmp(msg+1, "ACTION", 6))))
			return ('z');
		if (!ServicesOnline && (chptr->mode.mode & MODE_RGSTRONLY)
		    && !IsARegNick(cptr))
			return ('R');
	}
	
	return 0;
}

/*
 * floodcheck()
 *
 *   For +f channels, do some flood limit stuff.
 */
int floodcheck(Member *mp, char *msg)
{
	u_int32_t stime = (u_int32_t)nowtime*10+microtime/100000;
	int flood = mp->chptr->mode.flood;
	int len = strlen(msg);
	
	/* typecasting to int: compare which one is greater using the *
	 * a 'shorter distance' method, allowing wraparound, etc.     */
	if ((int)(mp->since-stime)<=0)
	{
		if (stime-mp->since>flood)
			mp->since = stime-flood;
		mp->since += 22; /* min penalty of 2.2 seconds */
	
		/* now incorporate message length - 1 second per 120 chars */
	
		mp->since += len/12;
	
		if ((int)(mp->since-stime)<=5)
			return 0; /* don't count this as a flood */

		/* probable flood */
		mp->since += 19;
	}
	else
	{
		/* almost certainly a flood -- second-level */
		/* this formula lets mp->since tip off at a certain penalty
		 * based on the rate of the message send, not dependant
		 * so much on the number of messages, to allow user to not
		 * have to wait forever to get out of the hole...
		 *  more wait for longer messages since the user will get
		 * penalized via the queue...
		 */
		mp->since += 1000/(60+mp->since-stime) + len/8;
	}
	
	sendto_one(mp->cptr, ":%s NOTICE %s :%s%s%s%d%s",
		me.name, mp->cptr->name,
		"You are flooding ", mp->chptr->chname, ". Please wait ",
		(mp->since-stime+40)/10, " seconds before attempting to send.");
	return -1;
}

/*
 * repeatcheck()
 *
 *   Check if message is an excessive repeat according to channel's limit.
 */
int repeatcheck(Member *mp, char *msg)
{
	u_int16_t stamp = messagestamp(msg)%65521;
	u_int16_t tmp;
	
	if (stamp != mp->msg1)
	{
		tmp = mp->msg2;
		mp->msg2 = mp->msg1;
		mp->msg1 = stamp;
		if (tmp != stamp)
		{
			mp->repeats = 1;
			return 0;
		}
	}
	
	if (mp->repeats >= mp->chptr->mode.maxrept)
		return -1;
	mp->repeats++;
	
	return 0;
}

aChannel *find_channel(chname, sptr)
char	*chname;
aClient	*sptr;
{
	aChannel *chptr = hash_find_channel(chname);
	if (!chptr && sptr)
		send_num(sptr, ERR_NOSUCHCHANNEL, chname);
	return chptr;
}



/* Now let _invited_ people join thru bans, +i and +l.
 * Checking if an invite exist could be done only if a block exists,
 * but I'm not too fancy of the complicated structure that'd cause,
 * when optimization will hopefully take care of it. Most of the time
 * a user won't have invites on him anyway. -Donwulff
 *
 * But, a user could pass through the ban if the inviter was a nonop.
 * In +i chans, only ops can invite users.
 */

static	int	can_join(aClient *sptr, aChannel *chptr, char *key)
{
	Link	*lp;

	if (is_excepted(sptr, chptr))
		return 0;

	for (lp = sptr->u->invited; lp; lp = lp->next)
		if (lp->value.chptr == chptr)
			return 0;

	if ((chptr->mode.mode & MODE_RGSTRONLY) && !IsARegNick(sptr))
	{
		if(ServicesOnline)
			return (ERR_NEEDREGGEDNICK);
		else
			send_num(sptr, ERR_REGGEDNICKNOSERV, chptr->chname);
	}

	if (*chptr->mode.key && (!key || strcmp(chptr->mode.key, key)))
		return (ERR_BADCHANNELKEY);
	
	if (chptr->mode.mode & MODE_INVITEONLY)
		return (ERR_INVITEONLYCHAN);

	if (chptr->mode.limit && chptr->users >= chptr->mode.limit)
		return (ERR_CHANNELISFULL);

	if (is_banned(sptr, chptr))
		return (ERR_BANNEDFROMCHAN);
		
	return 0;
}

/*
** Remove bells and commas from channel name
** AND, shorten it to CHANNELLEN if needed.
*/

void	clean_channelname(cn)
char	*cn;
{
	u_char	*ch = (u_char *)cn;


	for (; *ch; ch++)
		/* Don't allow any control chars, the space, the comma,
		 * or the "non-breaking space" in channel names.
		 * Might later be changed to a system where the list of
		 * allowed/non-allowed chars for channels was a define
		 * or some such.
		 *   --Wizzu
		 */
		if (*ch < 33 || *ch == ',' || *ch == 160)
		    {
			*ch = '\0';
			return;
		    }
	if ((char *)ch-(char *)cn>CHANNELLEN)
		cn[CHANNELLEN] = 0;
}

/*
**  Get Channel block for i (and allocate a new channel
**  block, if it didn't exist before).
*/
static	aChannel *get_channel(aClient *cptr, char *chname, int *flag, time_t ts)
{
	aChannel *chptr;
	int	len;

	if (BadPtr(chname) || !flag)
		return NULL;
	
	*flag = CHFL_UNBAN;

	len = strlen(chname);
	
	if ((chptr = hash_find_channel(chname)))
		return (chptr);

	chptr = da_calloc(&chan_mast);
	strcpy(chptr->chname, chname);
	if (channel)
		channel->prevch = chptr;
	chptr->prevch = NULL;
	chptr->nextch = channel;
	channel = chptr;
	chptr->lmembers = &chptr->members;
	chptr->channelts = MyClient(cptr)?ts:(time_t)0;
	hash_add_channel(chptr);
	
	if (MyConnect(cptr) && *chname != '+')
		*flag |= CHFL_CHANOP;

	return chptr;
}

/*
 * Slight changes in routine, now working somewhat symmetrical:
 *   First try to remove the client & channel pair to avoid duplicates
 *   Second check client & channel invite-list lengths and remove tail
 *   Finally add new invite-links to both client and channel
 * Should U-lined clients have higher limits?   -Donwulff
 */

static	void	add_invite(cptr, chptr)
aClient *cptr;
aChannel *chptr;
{
	Link	*inv, *tmp;

	del_invite(cptr, chptr);
	/*
	 * delete last link in chain if the list is max length
	 */
	if (list_length(cptr->u->invited) >= ConfMaxChannels)
	{
		for (tmp = cptr->u->invited; tmp->next; tmp = tmp->next)
			;
		del_invite(cptr, tmp->value.chptr);
 
	}
	/* We get pissy over too many invites per channel as well now,
	 * since otherwise mass-inviters could take up some major
	 * resources -Donwulff
	 */
	if (list_length(chptr->invites) >= ConfMaxChannels)
	{
		for (tmp = chptr->invites; tmp->next; tmp = tmp->next)
			;
		del_invite(tmp->value.cptr, chptr);
	}
	/*
	 * add client to the beginning of the channel invite list
	 */
	inv = make_link();
	inv->value.cptr = cptr;
	inv->next = chptr->invites;
	chptr->invites = inv;
	/*
	 * add channel to the beginning of the client invite list
	 */
	inv = make_link();
	inv->value.chptr = chptr;
	inv->next = cptr->u->invited;
	cptr->u->invited = inv;
}

/*
 * Delete Invite block from channel invite list and client invite list
 */
void	del_invite(cptr, chptr)
aClient *cptr;
aChannel *chptr;
{
	Link	**inv, *tmp;

	for (inv = &(cptr->u->invited); (tmp = *inv); inv = &tmp->next)
		if (tmp->value.chptr == chptr)
		    {
			*inv = tmp->next;
			free_link(tmp);
			break;
		    }

	if (!tmp)
		return;

	for (inv = &(chptr->invites); (tmp = *inv); inv = &tmp->next)
		if (tmp->value.cptr == cptr)
		    {
			*inv = tmp->next;
			free_link(tmp);
			break;
		    }
}

/*
**  Subtract one user from channel i (and free channel
**  block, if channel became empty).
*/
static	void	sub1_from_channel(aChannel *chptr)
{
	Ban	*ban, *next;
	Link	*lp;

	if (--chptr->users <= 0)
	{
		while ((lp = chptr->invites))
			del_invite(lp->value.cptr, chptr);

		for (ban = chptr->exceptlist; ban; ban = next)
		{
			next = ban->next;
			free_ban(ban);
		}

		for (ban = chptr->banlist; ban; ban = next)
		{
			next = ban->next;
			free_ban(ban);
		}

		if (chptr->prevch)
			chptr->prevch->nextch = chptr->nextch;
		else
			channel = chptr->nextch;
		if (chptr->nextch)
			chptr->nextch->prevch = chptr->prevch;
		hash_del_channel(chptr);
		da_free(&chan_mast, chptr);
	}
}

/*
 * Join segment
 * Handle JOIN and SAJOIN with appropriate timestamps.
 */

/*
 * m_join
 * parv[1] = channel
 * parv[2] = channel password (key)
 */
int m_join(aClient *cptr, aClient *sptr, int parc, char *parv[]) 
{ 
	return do_join(cptr,sptr,parc,parv,0); 
} 

/*
 * m_sajoin
 *	Force join a channel
 * parv[1] = channel
 */
int m_sajoin(aClient *cptr, aClient *sptr, int parc, char *parv[]) 
{ 
	if (check_registered(cptr) || !MyClient(sptr) || check_privs(sptr, UMODE_SADMIN, 0)) 
		return 0; 

	return do_join(cptr,sptr,parc,parv,1); 
} 

/*
 * do_join
 *	Process join or sajoin
 *	Use buffering for DVS Protection
 */
int	do_join(aClient *cptr, aClient *sptr, int parc, char *parv[], int sajoin)
{
	Member	*mp;
	aChannel *chptr;
	char	*name, *key = NULL;
	int	i, flags = 0;
	char	*p = NULL, *keys = NULL, *bpos, *s, *cpos;
	time_t	ts = (time_t)0;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0')
		return check_params(sptr, "JOIN", -1, 0);

	/* Execute DVS Join Buffer if possible */
#ifdef USE_DVS
	if(MyConnect(sptr) && DVSLock == 0 && ServicesOnline == 0 && sptr->chbuf)
		chbuf_empty(sptr);
#endif

	bpos = buf;
	*bpos = '\0';
	
	/*
	** Rebuild list of channels joined to be the actual result of the
	** JOIN.  Note that "JOIN 0" is the destructive problem.
	*/
	p = parv[1];
	while ((name = strsplit(&p, ',')))
	{
		if (MyConnect(sptr))
			clean_channelname(name);
		if (*name == '0')
			bpos = buf;
		else if (!IsChannelName(name))
		{
			if (MyClient(sptr))
				send_num(sptr, ERR_NOSUCHCHANNEL, name);
			continue;
		}
		bpos += mysprintf(bpos, "%s,", name);
	}
	
	if (bpos == buf)
		return 0;
	bpos[-1] = '\0';
	p = buf;
	keys = parv[2];
	
	while ((name = strsplit(&p, ',')))
	{
		if (*name == '0')
		{
			sendto_serv_butone(cptr, ":%n %m 0", sptr, &ms_join);
			while ((mp = sptr->u->channel))
			{
				sendto_channel_butserv(mp->chptr, sptr,
					"PART :%s", mp->chptr->chname);
				member_del(sptr->u->channel);
			}
			continue;
		}
		if (MyConnect(sptr) && sptr->u->joined >= ConfMaxChannels)
		{
			send_num(sptr, ERR_TOOMANYCHANNELS, name);
			return 0;
		}

		/* DVS Join Buffer */
#ifdef USE_DVS
		if(IsDVS(sptr) && MyConnect(sptr))
		{
			Member *bmp = da_alloc(&member_mast);
			cpos = chankey;
			*cpos = '\0';

			if(BadPtr(name) || find_chbuf_member(name,sptr))
				continue;

			if ((chptr = hash_find_channel(name)))
			{
				if ((mp=find_member(chptr, sptr)))
					continue;
				if (*chptr->mode.key)
				{
					key = strsplit(&keys, ',');
					cpos += mysprintf(cpos, "%s %s", name, key);
				}
			}

			bmp->nextc = sptr->chbuf;
			sptr->chbuf = bmp;
			sptr->u->joined++;

			if(cpos == chankey)
				bmp->nextu = (Member*)strdup(name);
			else
			{
				cpos = '\0';
				cpos = chankey;
				bmp->nextu = (Member*)strdup(cpos);
			}
			bmp->cptr = sptr;
			bmp->flags = flags|CHFL_UNBAN;
			bmp->repeats = 0;
			bmp->msg1 = 0;
			bmp->msg2 = 0;

			continue;
		}
#endif

		if (!(chptr = get_channel(sptr, name, &flags, nowtime)))
			continue;	/* invalid channel name */
		if ((mp=find_member(chptr, sptr)))
			continue;	/* already in the chan */
		if (*chptr->mode.key)
			key = strsplit(&keys, ',');
		if (MyConnect(sptr) && (i = can_join(sptr, chptr, key)))
		{ 
			if(sajoin == 0)
			{
				send_num(sptr, i, name);
				continue;
			}
			else if(chptr->mode.mode & MODE_NOSAJOIN)
			{
				sendto_one(sptr, ":%n %m %s :You cannot use SAJOIN on %s (+S no-sajoin mode)",
					&me, &ms_notice, sptr->name, name);
				continue;
			}
			else
			{
				sendout_globops("%s used SAJOIN cmd to force join %s",sptr->name,name);
				sendto_channelops_butone(&me, chptr, 
					"%s used SAJOIN cmd to force join %s",sptr->name,name);
			}
		}
		ts = (time_t)0;

		member_add(chptr, sptr, flags|CHFL_UNBAN);
		send_join(cptr, sptr, chptr, flags);
	}

	return 0;
}

/*
 * send_join
 *	Actually send a join to a user and other servers
 */
int	send_join(aClient *cptr, aClient *sptr, aChannel *chptr, int flags)
{
	char *parv[2];

	sendto_serv_butone(cptr, ":%n %m %s", sptr, &ms_join, chptr->chname);
	sendto_channel_butserv(chptr, sptr, "JOIN :%s", chptr->chname);

	if(MyConnect(sptr))
	{
		del_invite(sptr, chptr);
		if (flags & CHFL_CHANOP)
			sendto_serv_butone(cptr, "%m %s +o %s %d", &ms_mode,
				chptr->chname, sptr->name, chptr->channelts);
		if(chptr->topic[0] != '\0')
		{
			send_num(sptr, RPL_TOPIC, chptr->chname, chptr->topic);
			send_num(sptr, RPL_TOPICWHOTIME, chptr->chname,
				chptr->topic_nick, chptr->topic_time);
		}
		parv[1] = chptr->chname;
		m_names(cptr, sptr, 2, parv);
	}

	return 0;
}

/*
 * chbuf_empty
 *	Empty the Channel Join Buffer (DVS Authorization)
 */
int	chbuf_empty(aClient *sptr)
{
#ifdef USE_DVS
	Member	*mp, **mpp, *mpnext, *check;
	char	*name, *key;
	int	i; /* fail = 0; */

	for(mp = sptr->chbuf; mp; mp = mpnext)
	{
		mpnext = mp->nextc;
		/* Extract name and key */
		key = NULL;
		name = (char*)mp->nextu;
		if ((key = index(name, ' ')))
			*key++ = '\0';

		/* Build Channel */
		if(!(mp->chptr = hash_find_channel(name)))
		{
			mp->chptr = da_calloc(&chan_mast);
			strcpy(mp->chptr->chname, name);
			if(channel)
				channel->prevch = mp->chptr;
			mp->chptr->prevch = NULL;
			mp->chptr->nextch = channel;
			channel = mp->chptr;
			mp->chptr->lmembers = &mp->chptr->members;
			mp->chptr->channelts = nowtime;
			hash_add_channel(mp->chptr);
			mp->flags |= CHFL_CHANOP;
		}
		/* Check join to see if already on channel */
		else if ((check = find_member(mp->chptr, sptr)))
		{
			da_free(&member_mast, mp);
			sptr->u->joined--;
			continue;
		}
		/* Check join if channel already exists */
		else if ((i = can_join(sptr, mp->chptr, key)))
		{
			send_num(sptr, i, name);
			da_free(&member_mast, mp);
			sptr->u->joined--;
			continue;
		}

		/* Handle member list */
		free(mp->nextu);
		mpp = mp->chptr->lmembers;
		mp->nextu = *mpp;
		*mpp = mp;

		mp->chptr->users++;
		mp->since = (nowtime*10+microtime/100000)-mp->chptr->mode.flood;

		mp->nextc = sptr->u->channel;
		sptr->u->channel = mp;

		send_join(sptr, sptr, mp->chptr, mp->flags);
	}

	sptr->chbuf = (Member *)NULL;
#endif

	return 0;
}

/*
** m_part
**	parv[1] = channel
**	parv[2] = comment (added by Lefler)
*/
int	m_part(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int parc;
char *parv[];
{
	static char nocomment[] = "";
	aChannel *chptr;
	Member	*mp;
	char	*p = NULL, *name;
	char	*comment;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || parv[1][0] == '\0')
		return check_params(sptr, "PART", -1, 0);

	for (p = parv[1]; name = strsplit(&p, ',');)
	{
		if (!(chptr = find_channel(name, sptr))) continue;	
		if (!(mp=find_member(chptr, sptr)))
		{
			if (MyConnect(sptr))
				send_num(sptr, ERR_NOTONCHANNEL, name);
			continue;
		}
		if ((comment = parv[2]) && MyConnect(sptr))
		{
			if (can_member_send(mp)!=0)
				comment = NULL;
			else if ((chptr->mode.mode&MODE_STRIPCOLOR))
				strip_colors(comment);
		}
		sendto_serv_butone(cptr, ":%s %m %s :%s", sptr->name, &ms_part, 
			chptr->chname, comment ? comment : nocomment);
		sendto_channel_butserv(chptr, sptr, "PART %s :%s",
			chptr->chname, comment ? comment : nocomment);
		member_del(mp);
	}
	
	return 0;
}

/*
 * m_kick
 *	parv[1] = channel
 *	parv[2] = client to kick
 *	parv[3] = kick comment
 */
int	m_kick(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	static char noreason[] = "*";
	aClient *who;
	aChannel *chptr;
	int chasing = 0, halfop = 0;
	char *comment, *nicklist, *user, *user2;
	Member *mp;

	if (check_registered(sptr) || check_params(sptr, "KICK", parc, 2))
		return 0;

	comment = parv[3] ? parv[3] : noreason;
	if (strlen(comment) > TOPICLEN)
		comment[TOPICLEN] = '\0';

	if (!(chptr = find_channel(parv[1], sptr)))
		return 0;

	if(!IsServer(cptr))
	{
		if(!(mp = find_member(chptr, cptr)))
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return 0;
		}

		if(mp->flags&CHFL_HALFOP && !(mp->flags&CHFL_CHANOP))
			halfop = 1;
		else if(!(mp->flags&CHFL_CHANOP || mp->flags&CHFL_HALFOP))
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return 0;
		}
	}

	nicklist = parv[2];
	while ((user = strsplit(&nicklist, ',')))
	{
		if (!(who = find_chasing(sptr, user, &chasing)) ||
			!(mp = find_member(chptr, who)))
		{
			if (who && MyClient(sptr))
				send_num(sptr,ERR_USERNOTINCHANNEL,user,parv[1]);
			continue;
		}
		if(halfop && (mp->flags&CHFL_CHANOP || mp->flags&CHFL_HALFOP))
		{
			if(MyClient(sptr))
				send_num(sptr,ERR_HALFOPKICK,user,parv[1]);
			continue;
		}
		sendto_serv_butone(cptr, ":%n %m %s %s :%s",
			sptr, &ms_kick, chptr->chname, who->name, comment);
		sendto_channel_butserv(chptr, sptr, "KICK %s %s :%s",
			chptr->chname, who->name, comment);
		member_del(mp);
	}

	return (0);
}


int	count_channels()
{
	aChannel	*chptr;
	int	count = 0;

	for (chptr = channel; chptr; chptr = chptr->nextch)
		count++;
	return (count);
}

/*
** m_topic
**	parv[1] = channel
**      parv[2] = topic text
**
**	For servers using TS: (Lefler)
**	parv[1] = channel (we now assume one topic will affect only 1 channel)
**	parv[2] = topic nickname
**	parv[3] = topic time
**	parv[4] = topic text
*/
int	m_topic(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aChannel *chptr = NullChn;
	char	*topic = NULL, *tnick = NULL;
	time_t	ttime = 0;
	Member *mp;
	
	if (check_registered(sptr))
		return 0;

	if (parc < 2)
		return check_params(sptr, "TOPIC", -1, 0);

	if (!(chptr = find_channel(parv[1], sptr)))
		return 0;
		
	if (!(topic = parv[2]))  /* only asking for topic  */
	{
		if (chptr->topic[0] == '\0')
			send_num(sptr, RPL_NOTOPIC, chptr->chname);
		else
		{
			send_num(sptr, RPL_TOPIC, chptr->chname, chptr->topic);
			send_num(sptr, RPL_TOPICWHOTIME, chptr->chname,
				chptr->topic_nick, chptr->topic_time);
		}
		return 0;
	}
		
	if (parc > 4 && IsServer(cptr))
	{
		tnick = parv[2];
		ttime = atoi(parv[3]);
		topic = parv[4];
	}
	else
	{
		tnick = sptr->name;
		ttime = nowtime;
	}
	
	if (!IsServer(cptr))
	{
		mp = find_member(chptr, cptr);
		if ((chptr->mode.mode&MODE_TOPICLIMIT)
			&& !(mp&&(mp->flags&CHFL_CHANOP||mp->flags&CHFL_HALFOP)))
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return 0;
		}
	}
	else if (IsServer(sptr) && !IsULine(sptr))
	{
		if (chptr->topic_time!=0 && ttime>chptr->topic_time)
			return 0;
	}
		
	if (!strcmp(chptr->topic, topic))
		return 0;
	
	strncpyzt(chptr->topic, topic, sizeof(chptr->topic));
	strcpy(chptr->topic_nick, tnick);
	if (ttime)
		chptr->topic_time = ttime;
	
	sendto_serv_butone(cptr, ":%n %m %s %s %d :%s",
		sptr, &ms_topic, chptr->chname, chptr->topic_nick,
		chptr->topic_time, chptr->topic);
	
	if (IsServer(sptr) || IsULine(sptr))
		sendto_channel_butserv(chptr, sptr, "TOPIC %s :%s (%s)",
			chptr->chname, chptr->topic, tnick);
	else
		sendto_channel_butserv(chptr, sptr, "TOPIC %s :%s",
			chptr->chname, chptr->topic);
	
	return 0;
}

/*
** m_invite
**	parv[1] - user to invite
**	parv[2] - channel number
*/
int	m_invite(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	aChannel *chptr;
	Member *mp;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 3 || *parv[1] == '\0')
		return check_params(sptr, "INVITE", -1, 0);

	if (!(acptr = find_person(parv[1])))
	{
		send_num(sptr, ERR_NOSUCHNICK, parv[1]);
		return 0;
	}

	if (MyConnect(sptr))
		clean_channelname(parv[2]);

	if (!(chptr = find_channel(parv[2], sptr)))
		return 0;

	if (!IsULine(sptr) && !(mp = find_member(chptr, sptr)))
	{
		send_num(sptr, ERR_NOTONCHANNEL, parv[2]);
		return -1;
	}
	if (IsMember(acptr, chptr))
	{
		send_num(sptr, ERR_USERONCHANNEL, parv[1], parv[2]);
		return 0;
	}

	if (((chptr->mode.mode & MODE_INVITEONLY)
		|| (chptr->mode.limit) || is_banned(acptr, chptr))
		&& !is_excepted(acptr, chptr))
	{
		if(!(IsULine(sptr) || mp->flags&CHFL_CHANOP || mp->flags&CHFL_HALFOP))
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return -1;
		}
		if (MyConnect(acptr))
		{
			sendto_channelops_butone(&me, chptr,
			  "%s invited %s into the channel.",
			  sptr->name, acptr->name);
			add_invite(acptr, chptr);
		}
	}
	if (MyConnect(sptr))
	{
		send_num(sptr, RPL_INVITING, acptr->name,
			((chptr) ? (chptr->chname) : parv[2]));
		if (acptr->u->away)
			send_num(sptr, RPL_AWAY, acptr->name, acptr->u->away);
	}

	sendto_one(acptr, ":%p %m %s :%s", sptr, &ms_invite,
		acptr->name, ((chptr) ? (chptr->chname) : parv[2]));
	return 0;
}


/*
 * send_list
 *
 * The function which sends.
 * The function which sends the actual /list output back to the user.
 * Operates by stepping through the hashtable, sending the entries back if
 * they match the criteria.
 * cptr = Local client to send the output back to.
 * numsend = Number (roughly) of lines to send back. Once this number has
 * been exceeded, send_list will finish with the current hash bucket,
 * and record that number as the number to start next time send_list
 * is called for this user. So, this function will almost always send
 * back more lines than specified by numsend (though not by much,
 * assuming CHANNELHASHSIZE is was well picked). So be conservative
 * if altering numsend };> -Rak
 */
void	send_list(aClient *cptr, int numsend)
{
	int		hashptr, done = 0;
	aChannel	*chptr;
	int		s=0;

#define l cptr->lopt /* lazy shortcut */

	for(hashptr = l->starthash; hashptr < CHANNELHASHSIZE; hashptr++)
	{
		for(chptr = hash_get_chan_bucket(hashptr); chptr; chptr = chptr->hnext)
		{
			if (chptr->mode.mode&(MODE_SECRET|MODE_PRIVATE))
			{
				if(!IsOper(cptr))
					continue;
				s = 1;
			}
			else
				s = 0;
			if (!l->showall && ((chptr->users <= l->usermin) ||
			    (((u_int)chptr->users >= (u_int)l->usermax)) ||
			    ((chptr->channelts||1) <= l->chantimemin) ||
			    (chptr->topic_time < l->topictimemin) ||
			    (chptr->channelts >= l->chantimemax) ||
			    (chptr->topic_time > l->topictimemax)))
				continue;
			if (l->nolist &&
			    (find_str_match_link(&(l->nolist), chptr->chname) ||
			    find_str_match_link(&(l->nolist), chptr->topic))) 
				continue;
			if (l->yeslist &&
			    (!find_str_match_link(&(l->yeslist), chptr->chname) &&
			    !find_str_match_link(&(l->yeslist), chptr->topic)))
				continue;
			send_num(cptr, RPL_LIST, chptr->chname, s==1?", ":" ", chptr->users, chptr->topic);
			if(--numsend == 0)
				done = 1;
		}
		if(done && (++hashptr < CHANNELHASHSIZE))
		{
			l->starthash = hashptr;
			return;
		}
	}

	send_num(cptr, RPL_LISTEND);
	free_str_list(l->yeslist);
	free_str_list(l->nolist);
	MyFree(l);
	l = NULL;

	if (!IsPrivileged(cptr))
		cptr->since+=15;
	return;
}

/*
 * m_list
 *	parv[1,2,3...] = Channels or list options.
 */
int	m_list(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
    aChannel *chptr;
    char   *name, *p = NULL;
    LOpts  *lopt;
    short  int  showall = 0;
    Link   *yeslist = NULL, *nolist = NULL, *listptr;
    short  usermin = 0, usermax = -1;
    time_t currenttime = nowtime;
    time_t chantimemin = 0, topictimemin = 0;
    time_t chantimemax, topictimemax;

    static char *usage[] = {
	"Usage: /raw LIST options (on mirc) or /quote LIST options (ircII)",
	" ",
	"If you don't include any options, the default is to send you the",
	"entire unfiltered list of channels. Below are the options you can",
	"use, and what channels LIST will return when you use them.",
	">number  List channels with more than <number> people.",
	"<number  List channels with less than <number> people.",
	"C>number List channels created between now and <number> minutes ago.",
	"C<number List channels created earlier than <number> minutes ago.",
	"T>number List channels whose topics are older than <number> minutes",
	"         (Ie, they have not changed in the last <number> minutes.",
	"T<number List channels whose topics are not older than <number> minutes.",
	"*mask*   List channels that match *mask*",
	"!*mask*  List channels that do not match *mask*",
	NULL
    };


    /* None of that unregistered LIST stuff.  -- Barubary */
    if (check_registered(sptr)) return 0;

#ifdef USE_DVS
    if (IsDVS(cptr) && DoList(cptr))
	return 0;
#endif

    /*
     * I'm making the assumption it won't take over a day to transmit
     * the list... -Rak
     */
    chantimemax = topictimemax = currenttime + 86400;


    if ((parc == 2) && (!strcasecmp(parv[1], "?"))) {
	char **ptr = usage;

	for (; *ptr; ptr++)
	    send_num(sptr, RPL_LISTSYNTAX, *ptr);
	return 0;
    }

    /*
     * A list is already in process, for now we just interrupt the
     * current listing, perhaps later we can allow stacked ones...
     *  -Donwulff (Not that it's hard or anything, but I don't see
     *             much use for it, beyond flooding)
     */

    if(cptr->lopt)
    {
	free_str_list(cptr->lopt->yeslist);
	free_str_list(cptr->lopt->nolist);
	MyFree(cptr->lopt);
	cptr->lopt=NULL;
	send_num(sptr, RPL_LISTEND);
	/* Interrupted list, penalize 10 seconds */
	if(!IsPrivileged(sptr))
	    sptr->since+=10;
	
	return 0;
    }

    send_num(sptr, RPL_LISTSTART);

    /* LIST with no arguements */
    if (parc < 2 || BadPtr(parv[1])) {
	lopt = (LOpts *)MyMalloc(sizeof(LOpts));
	    if (!lopt)
		return 0;

	/*
	 * Changed to default to ignoring channels with only
	 * 1 person on, to decrease floods... -Donwulff
	 */
	bzero(lopt, sizeof(LOpts)); /* To be sure! */
	lopt->next = (LOpts *)lopt->yeslist=lopt->nolist=(Link *)NULL;
	lopt->usermin = 0; /* Default */
	lopt->usermax = -1;
	lopt->chantimemax = lopt->topictimemax = currenttime + 86400;
	cptr->lopt = lopt;
#ifdef USE_DVS
	if (!IsDVS(cptr) && IsSendable(cptr))
#else
	if (IsSendable(cptr))
#endif
	    send_list(cptr, 64);
	return 0;
    }


    /*
     * General idea: We don't need parv[0], since we can get that
     * information from cptr->name. So, let's parse each element of
     * parv[], setting pointer parv to the element being parsed.
     */
    while (--parc) {
	parv += 1;
	if (BadPtr(parv)) /* Sanity check! */
	    continue;

        p = *parv;

	while ((name = strsplit(&p, ','))) {
	  switch (*name) {
	    case '>':
		showall = 1;
		usermin = strtol(++name, (char **) 0, 10);
		break;

	    case '<':
		showall = 1;
		usermax = strtol(++name, (char **) 0, 10);
		break;

	    case 't':
	    case 'T':
		showall = 1;
		switch (*++name) {
		    case '>':
			topictimemax = currenttime - 60 *
				       strtol(++name, (char **) 0, 10);
			break;

		    case '<':
			topictimemin = currenttime - 60 *
				       strtol(++name, (char **) 0, 10);
			break;

		    case '\0':
			topictimemin = 1;
			break;

		    default:
			send_num(sptr, ERR_LISTSYNTAX);
			free_str_list(yeslist);
			free_str_list(nolist);
			send_num(sptr, RPL_LISTEND);
			return 0;
		}
		break;

		case 'c':
		case 'C':
		    showall = 1;
		    switch (*++name) {
			case '>':
			    chantimemin = currenttime - 60 *
					  strtol(++name, (char **) 0, 10);
			    break;

			case '<':
			    chantimemax = currenttime - 60 *
					  strtol(++name, (char **) 0, 10);
			    break;

			default:
			    send_num(sptr, ERR_LISTSYNTAX);
			    free_str_list(yeslist);
			    free_str_list(nolist);
			    send_num(sptr, RPL_LISTEND);
			    return 0;
		    }
		    break;

		default: /* A channel or channel mask */

		    /*
		     * new syntax: !channelmask will tell ircd to ignore
		     * any channels matching that mask, and then
		     * channelmask will tell ircd to send us a list of
		     * channels only masking channelmask. Note: Specifying
		     * a channel without wildcards will return that
		     * channel even if any of the !channelmask masks
		     * matches it.
		     */

		    if (*name == '!') {
			showall = 1;
			listptr = make_link();
			listptr->next = nolist;
			DupString(listptr->value.cp, name+1);
			nolist = listptr;
		    }
		    else if (strchr(name, '*') || strchr(name, '?')) {
			showall = 1;
			listptr = make_link();
			listptr->next = yeslist;
			DupString(listptr->value.cp, name);
			yeslist = listptr;
		    }
		    else {
			chptr = hash_find_channel(name);
			if (chptr && (ShowChannel(sptr, chptr) || IsOper(sptr)))
				send_num(sptr, RPL_LIST, name,
					ShowChannel(sptr,chptr) ? " " : ", ",
					chptr->users, chptr->topic);
		    }
	  } /* switch (*name) */
	} /* while(name) */
    } /* while(--parc) */

    if (!showall || (chantimemin>currenttime) || (topictimemin>currenttime))
    {
	free_str_list(yeslist);
	free_str_list(nolist);
	send_num(sptr, RPL_LISTEND);

	return 0;
    }

    lopt = (LOpts *)MyMalloc(sizeof(LOpts));

    lopt->showall = 0;
    lopt->next = NULL;
    lopt->yeslist = yeslist;
    lopt->nolist = nolist;
    lopt->starthash = 0;
    lopt->usermin = usermin;
    lopt->usermax = usermax;
    lopt->currenttime = currenttime;
    lopt->chantimemin = chantimemin;
    lopt->chantimemax = chantimemax;
    lopt->topictimemin = topictimemin;
    lopt->topictimemax = topictimemax;

    cptr->lopt = lopt;
#ifdef USE_DVS
    if(!IsDVS(cptr))
#endif
	send_list(cptr, 64);

    return 0;
}


/************************************************************************
 * m_names() - Added by Jto 27 Apr 1989
 ************************************************************************/


/*
** m_names
**	parv[1] = channel
**
**   Okay, networks are larger than Jto originally thought they would be.
**   So, doing a wildcard /names is no longer feasible.  That means that
**   we can seriously cut down on the size of it.  No longer do
**   we need recursion for comma-separated lists, either.
*/
int	m_names(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{ 
	aChannel *chptr;
	aClient *acptr;
	Member	*mp;
	aChannel *ch2ptr = NULL;
	int	showall, x = 0, blen;
	char	*s, *s2;
	char	*bpos, *startpos, c;

	if (check_registered(sptr)) return 0;
	
	if (!(s2 = parv[1]))
	{
		send_num(cptr, RPL_ENDOFNAMES, "*");
		return 0;
	}
	
	while (s = strsplit(&s2, ','))
	{
		if (!IsPerson(cptr) && !IsULine(sptr) && (++x > 10))
			break;	
		if (!(chptr = find_channel(s, sptr)))
			continue;
	
		showall = IsMember(sptr, chptr) || IsAnOper(sptr);
	
		if (!PubChannel(chptr))
		{
			if (!showall) continue;
			c = SecretChannel(chptr) ? '@' : '*';
		}
		else
			c = '=';
			
		bpos = startpos = nbuf+mysprintf(nbuf, ":%s 353 %s %c %s :",
			me.name, sptr->name, c, chptr->chname);
		
		for (mp = chptr->members; mp; mp = mp->nextu)
		{
			acptr = mp->cptr;
			if (!showall && IsInvisible(acptr))
				continue;
			if (mp->flags & CHFL_CHANOP)
				*bpos++ = '@';
			else if (mp->flags & CHFL_HALFOP)
				*bpos++ = '%';
			else if (mp->flags & CHFL_VOICE)
				*bpos++ = '+';
			bpos = strput(bpos, acptr->name);
			*bpos++ = ' ';
			if (bpos > nbuf+500-CHANNELLEN)
			{
				send_str(sptr, nbuf, bpos-nbuf);
				bpos = startpos; /* reuse same header */
			}
		}
		if (bpos != startpos)
			send_str(sptr, nbuf, bpos-nbuf);
		send_num(sptr, RPL_ENDOFNAMES, chptr->chname);
	}
	
	return 0;
}

void	send_user_joins(aClient *cptr, aClient *acptr)
{
	Member	*mp = acptr->u->channel;
	char	*bpos = buf;
	
	while (mp)
	{
		bpos += mysprintf(bpos, "%s,", mp->chptr->chname);
		if (bpos-buf > BUFSIZE-NICKLEN-CHANNELLEN-20
			|| !(mp = mp->nextc))
		{
			bpos[-1] = '\0';
			sendto_one(cptr, ":%s %m :%s", acptr->name,
				&ms_join, buf);
			bpos = buf;
		}
	}
}

/*
 * Channel-Based Join Code
 * by dave with modifications/ideas from binary
 * Replaces bulky send_user_joins
 */
void 	send_channel_join(aClient *dest, aChannel *chptr) 
{
	char buf[BUFSIZE];
	char *pos, *startpos;
	Member *mp;
	int lines = 0;

	if (!IsChannelName(chptr->chname))
		return;

	startpos = pos = buf + pformat(dest->prolev, buf, ":%n %m %s %d :",
		&me, &ms_cjoin, chptr->chname, chptr->channelts);

	for (mp = chptr->members; mp; mp = mp->nextu)
	{
		if (mp->flags&CHFL_CHANOP)
			*pos++ = '@';
		if (mp->flags&CHFL_HALFOP)
			*pos++ = '%';
		if (mp->flags&CHFL_VOICE)
			*pos++ = '+';
		pos = strput(pos, mp->cptr->name);
		if (pos-buf > (BUFSIZE-NICKLEN-20))
		{
			send_str(dest, buf, pos-buf);
			startpos = pos = buf + pformat(dest->prolev, buf,
				":%n %m %s %d :", &me, &ms_cjoin,
				chptr->chname, chptr->channelts);
			lines++;
		}
		else
			*pos++ = ' ';
	}

	if (pos!=startpos || !lines)
		send_str(dest, buf, pos-buf);
}

/*
 * m_cjoin()
 * Handles channel-based joins
 * parv[1]	channel
 * parv[2]	TS
 * parv[3]	user/flag list
 */

#define MSIGN(x,y)	if(msign != x) { *mbuf++=y; msign = x; }
#define AMODE(x)	para = x; if(mpos) { parabuf[mpos++] = ' '; } while(*para) { parabuf[mpos++] = *para++; }
#define AUSER(x)	para = x; if(upos) { nbuf[upos++] = ' '; } while(*para) { nbuf[upos++] = *para++; }

int	m_cjoin(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aChannel	*chptr;
	aClient		*acptr;
	time_t		newts, oldts, sendts;
	int		isnew, keepourmodes = 1, keepnewmodes = 1, flags = 0,
			mpos = 0, upos = 0, margs = 0, msign = 0, flag;
	char		*s, *sn, *j, *mbuf = modebuf, *para;
	Member		*mp;

	if(MyClient(sptr) || parc < 4 || !IsServer(cptr) ||
	   !IsChannelName(parv[1]))
		return 0;

	*mbuf		= '\0';
	*parabuf	= '\0';
	*nbuf		= '\0';

	newts = atol(parv[2]);
	isnew = ChannelExists(parv[1]) ? 0 : 1;
	chptr = get_channel(sptr, parv[1], &flags, nowtime);
	oldts = chptr->channelts;

	if(isnew)
		chptr->channelts = sendts = newts;
	else if (newts == oldts)
		sendts = oldts;
	else if (newts == 0 || oldts == 0)
		chptr->channelts = sendts = 0;
	else if (newts < oldts)
	{
		keepourmodes = 0;
		chptr->channelts = sendts = newts;
	}
	else
	{
		keepnewmodes = 0;
		sendts = oldts;
	}

	if(!keepourmodes)
	{
		msign = 0;
		for(mp = chptr->members; mp; mp = mp->nextu)
		{
			if(mp->flags & (CHFL_CHANOP|CHFL_HALFOP|CHFL_VOICE))
			{
				MSIGN(-1,'-')
				if(mp->flags&CHFL_CHANOP)
				{
					*mbuf++ = 'o';
					AMODE(mp->cptr->name)
					margs++;
				}
				if(mp->flags&CHFL_HALFOP)
				{
					*mbuf++ = 'h';
					AMODE(mp->cptr->name)
					margs++;
				}
				if(mp->flags&CHFL_VOICE)
				{
					*mbuf++ = 'v';
					AMODE(mp->cptr->name)
					margs++;
				}
				if(margs >= MAXMODEPARAMS || mbuf-modebuf+mpos > MBSLEN)
				{
					*mbuf = '\0';
					parabuf[mpos] = '\0';
					sendto_channel_butserv(chptr, sptr, "MODE %s %s %s", chptr->chname, modebuf, parabuf);
					mbuf = modebuf;
					*mbuf = '\0';
					margs = mpos = msign = 0;
				}
				mp->flags &= ~CHFL_CHANOP;
			}
		}

		if(mbuf != modebuf)
		{
			*mbuf = '\0';
			parabuf[mpos] = '\0';
			sendto_channel_butserv(chptr, sptr, "MODE %s %s %s", chptr->chname, modebuf, parabuf);
		}

		*modebuf = '\0';
		*parabuf = '\0';
	}

	mbuf = modebuf;
	mpos = margs = 0;
	*mbuf++ = '+';

	j = parv[3];
	while ((s = sn = strsplit(&j, ' ')))
	{
		flag = 0;
		while(*s == '@' || *s == '+' || *s == '%')
			*s++;
		if(keepnewmodes)
		{
			if(*sn == '@' || sn[1] == '@' || sn[2] == '@')
			{
				flag |= CHFL_CHANOP;
				*mbuf++ = 'o';
				AMODE(s)
				margs++;
			}
			if(*sn == '%' || sn[1] == '%' || sn[2] == '%')
			{
				flag |= CHFL_HALFOP;
				*mbuf++ = 'h';
				AMODE(s)
				margs++;
			}
			if(*sn == '+' || sn[1] == '+' || sn[2] == '+')
			{
				flag |= CHFL_VOICE;
				*mbuf++ = 'v';
				AMODE(s)
				margs++;
			}
		}
		if(!(acptr = find_chasing(sptr, s, NULL)))
			continue;
		if(acptr->from != cptr)
			continue;
		if(!IsMember(acptr, chptr))
		{
			member_add(chptr, acptr, flag|CHFL_UNBAN);
			sendto_channel_butserv(chptr, acptr, "JOIN :%s", chptr->chname);
			sendto_serv_butone_nproto(cptr, PROLEV_CJOIN,
				":%n %m %s", acptr, &ms_join, chptr->chname);
		}
		if(keepnewmodes)
		{
			AUSER(sn)
			if(margs >= MAXMODEPARAMS || mbuf-modebuf+mpos > MBSLEN)
			{
				*mbuf = '\0';
				parabuf[mpos] = '\0';
				sendto_serv_butone_nproto(cptr, PROLEV_CJOIN,
					":%s %m %s %s %s", parv[0], &ms_mode, chptr->chname, modebuf, parabuf);
				sendto_channel_butserv(chptr, sptr, "MODE %s %s %s", chptr->chname, modebuf, parabuf);
				mbuf = modebuf;
				*mbuf++ = '+';
				margs = mpos = 0;
			}
		}
		else
		{
			AUSER(s)
		}
	}

	*mbuf = '\0';
	parabuf[mpos] = '\0';
	if(margs)
	{
		sendto_serv_butone_nproto(cptr, PROLEV_CJOIN, ":%s %m %s %s %s",
			parv[0], &ms_mode, chptr->chname, modebuf, parabuf);
		sendto_channel_butserv(chptr, sptr, "MODE %s %s %s", chptr->chname, modebuf, parabuf);
	}
	nbuf[upos] = '\0';
	sendto_serv_butone_wproto(cptr, PROLEV_CJOIN,
		":%n %m %s %d :%s", sptr, &ms_cjoin, parv[1], sendts, nbuf);

	if(sendts != oldts && oldts != 0)
		sendto_umode(UMODE_JUNK, "*** TS Notice -- Changed TS for %s from %d to %d",
			chptr->chname, oldts, sendts);

	return 0;
}

#undef MSIGN
#undef AMODE
#undef AUSER
