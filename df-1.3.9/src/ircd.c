/************************************************************************
 *   IRC - Internet Relay Chat, ircd/ircd.c
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "sys.h"
#include "struct.h"
#include "common.h"
#include "numeric.h"
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/file.h>
#include <pwd.h>
#include <sys/time.h>
#ifdef HPUX
#define _KERNEL            /* HPUX has the world's worst headers... */
#endif
#include <sys/resource.h>
#ifdef HPUX
#undef _KERNEL
#endif
#include <errno.h>
#include "h.h"
#include "dvs.h"

#ifdef __FreeBSD__
char *malloc_options="h" MALLOC_FLAGS_EXTRA;
#endif

#ifdef SHOWCONNECTINFO
int	R_do_dns, R_fin_dns, R_fin_dnsc, R_fail_dns,
		R_do_id, R_fin_id, R_fail_id;
char	REPORT_DO_DNS[128], REPORT_FIN_DNS[128], REPORT_FIN_DNSC[128],
	REPORT_FAIL_DNS[128], REPORT_DO_ID[128], REPORT_FIN_ID[128],
	REPORT_FAIL_ID[128];
#endif

aClient me;			/* That's me */
aClient *client = &me;		/* Pointer to beginning of Client list */
aClient	*firstserv = &me;	/* Pointer to beinning of server list */

void	server_reboot(char *);
void	restart PROTO((char *));
static	void	open_debugfile(), setup_signals();
#ifdef USE_DVS
void	dvs_kill(aClient *);
#endif

char	**myargv;
int	portnum = -1;		    /* Server port number, listening this */
char	*configfile = CONFIGFILE;	/* Server configuration file */
int	debuglevel = -1;		/* Server debug level */
int	bootopt = 0;			/* Server boot option flags */
char	*debugmode = "";		/*  -"-    -"-   -"-  */
char	*sbrk0;				/* initial sbrk(0) */
static	int	dorehash = 0;
static	char	*dpath = DPATH;

time_t	nextconnect = 1;	/* time for next try_connections call */
time_t	nextping = 1;		/* same as above for check_pings() */
time_t	nextdnscheck = 0;	/* next time to poll dns to force timeouts */
time_t	nextexpire = 1;	/* next expire run on the dns cache */
time_t	nowtime = 0;
time_t	nextakillexpire = 0;	/* next time to expire akills */
int	microtime = 0;

int	ConfMaxClients		= MAXCONNECTIONS - 4;
int	ConfMaxChannels		= MAXCHANNELSPERUSER;
int	ConfDVSStatus		= -1; 	/* Used to override on boot only *
					 * DO NOT CHANGE THIS VALUE      */
int	ServicesOnline		= 0;
int	DVSStatus		= DVS_STATUS;
int	DVSLock			= 0;

struct Counts Count;

#if	defined(PROFIL)
extern	etext();

VOIDSIG	s_monitor()
{
	static	int	mon = 0;
#ifdef	POSIX_SIGNALS
	struct	sigaction act;
#endif

	moncontrol(mon);
	mon = 1 - mon;
#ifdef	POSIX_SIGNALS
	act.sa_handler = s_rehash;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGUSR1);
	sigaction(SIGUSR1, &act, NULL);
#else
	signal(SIGUSR1, s_monitor);
#endif
}
#endif

VOIDSIG s_die()
{
#ifdef	USE_SYSLOG
	syslog(LOG_CRIT, "Server Killed By SIGTERM");
#endif
	flush_connections(me.fd);
	exit(-1);
}

static VOIDSIG s_rehash()
{
#ifdef	POSIX_SIGNALS
	struct	sigaction act;
#endif
	dorehash = 1;
#ifdef	POSIX_SIGNALS
	act.sa_handler = s_rehash;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaction(SIGHUP, &act, NULL);
#else
	signal(SIGHUP, s_rehash);	/* sysV -argv */
#endif
}

void	restart(mesg)
char	*mesg;
{
#ifdef	USE_SYSLOG
	syslog(LOG_WARNING, "Restarting Server because: %s",mesg);
#endif
	server_reboot(mesg);
}

VOIDSIG s_restart()
{
	static int restarting = 0;

#ifdef	USE_SYSLOG
	syslog(LOG_WARNING, "Server Restarting on SIGINT");
#endif
	if (restarting == 0)
	    {
		/* Send (or attempt to) a dying scream to oper if present */

		restarting = 1;
		server_reboot("SIGINT");
	    }
}

void	server_reboot(mesg)
char	*mesg;
{
	int	i;

	Debug((DEBUG_NOTICE,"Restarting server... %s", mesg));
	flush_connections(me.fd);
	/*
	** fd 0 must be 'preserved' if either the -d or -i options have
	** been passed to us before restarting.
	*/
#ifdef USE_SYSLOG
	closelog();
#endif
	for (i = 3; i < MAXCONNECTIONS; i++)
		close(i);
/*	if (!(bootopt & (BOOT_TTY|BOOT_DEBUG)))
 *		close(2);
 */
	close(1);
	if ((bootopt & BOOT_CONSOLE) || isatty(0))
		close(0);
	if (!(bootopt & (BOOT_INETD|BOOT_OPER)))
		execv(MYNAME, myargv);
#ifdef USE_SYSLOG
	/* Have to reopen since it has been closed above */

	openlog(myargv[0], LOG_PID|LOG_NDELAY, LOG_FACILITY);
	syslog(LOG_CRIT, "execv(%s,%s) failed: %m\n", MYNAME, myargv[0]);
	closelog();
#endif
	Debug((DEBUG_FATAL,"Couldn't restart server: %s", strerror(errno)));
	exit(-1);
}

/* expire_akills
 * Expire any akills with a hold value > 0 and < nowtime
 */
static	time_t	expire_akills(time_t currenttime)
{
	aConfItem	*aconf;
	int		expired = 0, akills = 0;
	time_t		next = 0;

	for(aconf = conf; aconf; aconf = aconf->next)
	{
		if(aconf->status & CONF_KILL && aconf->tmpconf == 2)
		{
			if(aconf->hold != 0 && aconf->hold <= currenttime &&
			   aconf->tmpconf == KLINE_AKILL)
			{
				aconf->status &= ~(CONF_KILL);
				aconf->status |= CONF_ILLEGAL;
				expired++;
			}
			else
			{
				akills++;
				if(!next || aconf->hold < next)
					next = aconf->hold;
			}
		}
	}

	if(expired)
		sendto_umode(UMODE_JUNK, "Akill expire routine: %d akills removed, %d remain to be expired",
			   expired, akills);

	if(next)
	{
		if((next - nowtime) <  AKILLEXPIRE_LIMIT)
			return (nowtime + AKILLEXPIRE_LIMIT);
		return next;
	}

	return 0;
}


/*
** try_connections
**
**	Scan through configuration and try new connections.
**	Returns the calendar time when the next call to this
**	function should be made latest. (No harm done if this
**	is called earlier or later...)
*/
static	time_t	try_connections(currenttime)
time_t	currenttime;
{
	aConfItem *aconf;
	aClient *cptr;
	aConfItem **pconf;
	int	connecting, confrq;
	time_t	next = 0;
	aClass	*cltmp;
	aConfItem *cconf, *con_conf;
	int	con_class = 0;

	connecting = FALSE;
	Debug((DEBUG_NOTICE,"Connection check at   : %s",
		myctime(currenttime)));
	for (aconf = conf; aconf; aconf = aconf->next )
	    {
		/* Also when already connecting! (update holdtimes) --SRB */
		if (!(aconf->status & CONF_CONNECT_SERVER) || aconf->port <= 0)
			continue;
		cltmp = Class(aconf);
		/*
		** Skip this entry if the use of it is still on hold until
		** future. Otherwise handle this entry (and set it on hold
		** until next time). Will reset only hold times, if already
		** made one successfull connection... [this algorithm is
		** a bit fuzzy... -- msa >;) ]
		*/

		if ((aconf->hold > currenttime))
		    {
			if ((next > aconf->hold) || (next == 0))
				next = aconf->hold;
			continue;
		    }

		confrq = get_con_freq(cltmp);
		aconf->hold = currenttime + confrq;
		/*
		** Found a CONNECT config with port specified, scan clients
		** and see if this server is already connected?
		*/
		cptr = hash_find_serv(aconf->name);
		if (!cptr && (Links(cltmp) < MaxLinks(cltmp) || MaxLinks(cltmp) == 0) &&
		    (!connecting || (Class(cltmp) > con_class)))
		  {
			con_class = Class(cltmp);
			con_conf = aconf;
			/* We connect only one at time... */
			connecting = TRUE;
		  }
		if ((next > aconf->hold) || (next == 0))
			next = aconf->hold;
	    }
	if (connecting)
	    {
		if (con_conf->next)  /* are we already last? */
		    {
			for (pconf = &conf; (aconf = *pconf);
			     pconf = &(aconf->next))
				/* put the current one at the end and
				 * make sure we try all connections
				 */
				if (aconf == con_conf)
					*pconf = aconf->next;
			(*pconf = con_conf)->next = 0;
		    }
		if (connect_server(con_conf, (aClient *)NULL,
				   (struct hostent *)NULL, 1) == 0)
			sendto_umode(UMODE_JUNK, "Connection to %s activated (autoconnect)",
					con_conf->name);
	    }
	Debug((DEBUG_NOTICE,"Next connection check : %s", myctime(next)));
	if(next && next < nowtime + AUTOCONNECT_LIMIT)
		next = nowtime + AUTOCONNECT_LIMIT;
	return (next);
}

extern	time_t	check_pings(time_t currenttime)
{		
	aClient	*cptr;
	int	killflag;
	int	ping = 0, i;
	time_t	oldest = 0, timeout;

	for (i = 0; i <= highest_fd; i++)
	{
		if (!(cptr = local[i]) || IsMe(cptr) || IsLog(cptr))
			continue;

		/*
		** Note: No need to notify opers here. It's
		** already done when "FLAGS_DEADSOCKET" is set.
		*/
		if (cptr->flags & FLAGS_DEADSOCKET)
		{
			exit_client(cptr, cptr, &me, "Dead socket");
			continue;
		}
		
		if (IsPerson(cptr) && cptr->u->hush && cptr->u->hush->end
				&& currenttime >= cptr->u->hush->end)
			unhush(cptr, &me);

		if (!IsRegistered(cptr) && (DoingDNS(cptr) ||
			DoingAuth(cptr)) && nowtime - cptr->firsttime >= 10)
		{
			/* handle ident/dns problems earlier */
			if (cptr->authfd >= 0)
			{
				CloseSock(cptr->authfd);
				cptr->authfd = -1;
				cptr->count = 0;
				*cptr->buffer = '\0';
			}
#ifdef SHOWCONNECTINFO
			if (DoingDNS(cptr))
			{
				write(cptr->fd, REPORT_FAIL_DNS,
					R_fail_dns);
			}
			else
				write(cptr->fd, REPORT_FAIL_ID,
					R_fail_id);
#endif
			Debug((DEBUG_NOTICE, "DNS/AUTH timeout %s",
				get_client_name(cptr,TRUE)));
			del_queries((char *)cptr);
			ClearAuth(cptr);
			ClearDNS(cptr);
			SetAccess(cptr);
			cptr->firsttime = currenttime;
			cptr->lasttime = currenttime;
			continue;
		}
		
		ping = get_client_ping(cptr);

		/*
		 * If the client hasn't talked to us in 2*ping seconds
		 * and it has a ping time, then close its connection.
 		 */
		if ((currenttime - cptr->lasttime >= ping*2 &&
		    (cptr->flags & FLAGS_PINGSENT)) ||
		    (!IsRegistered(cptr) &&
		    (currenttime - cptr->firsttime) >= CONNECTTIMEOUT))
		{
			if (IsServer(cptr) || IsConnecting(cptr) || IsHandshake(cptr))
			{
				if(cptr->oflag & 0x10000000)
					sendto_umode(UMODE_JUNK, "No response from %s, closing link", cptr->name);
				else
					sendout_wallops("No response from %s, closing link", cptr->name);
			}
			exit_client(cptr, cptr, &me, "Ping Timeout");
			i--; /* check new client using this fd too! */
			continue;
		}
		else if (IsRegistered(cptr) &&
			(currenttime - cptr->lasttime >= ping) &&
			!(cptr->flags & FLAGS_PINGSENT))
		{
			/*
			 * if we havent PINGed the connection and we havent
			 * heard from it in a while, PING it to make sure
			 * it is still alive.
			 */
			cptr->flags |= FLAGS_PINGSENT;
			cptr->lasttime = currenttime - ping;
			sendto_one(cptr, "PING :%s", me.name);
		}

#if defined(USE_DVS) && (DVS_TIMEOUT > 0)
		if(cptr->dvstime != 0 && (nowtime - cptr->dvstime) >= DVS_TIMEOUT)
		{
			if(IsDVS(cptr))
			{
				dvs_kill(cptr);
				sendto_one(cptr, ":%n NOTICE %n :*** DVS -- You did not register/identify within 10 minutes. You will be disconnected now. For more help, type \2/dvs\2 or visit "DVS_URL, &me, cptr);
				sendto_umode(UMODE_JUNK, "DVS limited client %s did not identify -- disconnecting now.", cptr->name);
				exit_client(cptr, cptr, &me, "DVS Timeout");
				i--;
				continue;
			}
			else
				cptr->dvstime = 0;
		}
		else if (IsDVS(cptr) && cptr->dvstime == 0)
		{
			cptr->dvstime = nowtime;
			sendto_one(cptr, ":%n NOTICE %n :*** DVS -- Services is now online. You must register and/or identify with NickServ in order to continue using this network.", &me, cptr);
			sendto_one(cptr, ":%n NOTICE %n :*** DVS -- Until you register/identify, you will not be able to use this network.", &me, cptr);
		}
		else if (IsDVS(cptr) && !HasBeenDVSWarned(cptr) && (nowtime - cptr->dvstime) >= (DVS_TIMEOUT/2))
		{
			sendto_one(cptr, ":%n NOTICE %n :*** DVS -- You must register/identify with NickServ or you will be disconnected in 5 minutes. You may not use this network until you identify.", &me, cptr);
			SetDVSWarn(cptr);
		}
#endif
	}

	return (currenttime+20);
}

/* dvs_kill
 * Add a local 600-second akill if no other local clients match this host
 */
#ifdef USE_DVS
void	dvs_kill(aClient *cptr)
{
	int	i;
	aClient *acptr;

	Count.dvskills++;

	for (i = 0; i <= highest_fd; i++)
	{
		if(!(acptr = local[i]) || !IsClient(acptr) || IsSetDVS(acptr))
			continue;
#ifdef LOCAL_NICKIP
		if(!strcmp((char *)inetntoa((char *)&acptr->ip),(char *)inetntoa((char *)&cptr->ip))
#else
		if(!strcmp(acptr->u->rhost,cptr->u->rhost)
#endif
		   && strcmp(acptr->name, cptr->name))
			return;
	}
	add_temp_conf(CONF_KILL, 
#ifdef LOCAL_NICKIP
		      (char *)inetntoa((char *)&cptr->ip),
#else
		      cptr->u->rhost, 
#endif
		      "DVS Idle Timeout Auto-Kill", "*", 0, 0, 2, nowtime+600);
	if(!nextakillexpire || nextakillexpire > (nowtime+600))
		nextakillexpire = nowtime + 600;
}
#endif

/*
** bad_command
**	This is called when the commandline is not acceptable.
**	Give error message and exit without starting anything.
*/
static	int	bad_command()
{
	printf("Usage: ircd [options]\n");
	printf("       ircd -h (display more options)\n");
	printf("Server not started\n");
	exit (-1);
}

int	main(argc, argv)
int	argc;
char	*argv[];
{
	uid_t	uid, euid;
	time_t	delay = 0;
	int	portarg = 0;
#ifdef  FORCE_CORE
	struct  rlimit corelim;
#endif
		
	sbrk0 = (char *)sbrk((size_t)0);
	uid = getuid();
	euid = geteuid();
#ifdef	PROFIL
	monstartup(0, etext);
	moncontrol(1);
	signal(SIGUSR1, s_monitor);
#endif

#ifdef	CHROOTDIR
	if (chdir(dpath))
	    {
		fprintf(stderr,"ERROR: Unable to chdir (ircd.c: main)\n");
		exit(-1);
	    }
	res_init();
	if (chroot(DPATH))
	  {
	    fprintf(stderr,"ERROR: Unable to chroot (ircd.c: main)\n");
	    exit(5);
	  }
#endif /*CHROOTDIR*/

	myargv = argv;
	umask(077);                /* better safe than sorry --SRB */
	bzero((char *)&me, sizeof(me));

	setup_signals();

#ifdef FORCE_CORE
	corelim.rlim_cur = corelim.rlim_max = RLIM_INFINITY;
	if (setrlimit(RLIMIT_CORE, &corelim))
	  printf("unlimit core size failed; errno = %d\n", errno);
#endif

	/*
	** All command line parameters have the syntax "-fstring"
	** or "-f string" (e.g. the space is optional). String may
	** be empty. Flag characters cannot be concatenated (like
	** "-fxyz"), it would conflict with the form "-fstring".
	*/
	while (--argc > 0 && (*++argv)[0] == '-')
	    {
		char	*p = argv[0]+1;
		int	flag = *p++;

		if (flag == '\0' || *p == '\0')
			if (argc > 1 && argv[1][0] != '-')
			    {
				p = *++argv;
				argc -= 1;
			    }
			else
				p = "";

		switch (flag)
		    {
		    case 'c':
			bootopt |= BOOT_CONSOLE;
			break;
		    case 'q':
			bootopt |= BOOT_QUICK;
			break;
		    case 'd' :
                        setuid((uid_t)uid);
			dpath = p;
			break;
		    case 'o': /* Per user local daemon... */
                        setuid((uid_t)uid);
			bootopt |= BOOT_OPER;
		        break;
#ifdef CMDLINE_CONFIG
		    case 'f':
                        setuid((uid_t)uid);
			configfile = p;
			break;
#endif
		    case 'n':
		    	set_server_name(&me, p);
			break;
		    case 'i':
			bootopt |= BOOT_INETD;
		        break;
		    case 'p':
			if ((portarg = atoi(p)) > 0 )
				portnum = portarg;
			break;
		    case 't':
                        setuid((uid_t)uid);
			bootopt |= BOOT_TTY;
			break;
		    case 'v':
			printf("DarkFire IRCd Daemon. IRCd version %s\nCompile #%s (%s)\n", version, generation, creation);
			exit(0);
		    case 'x':
#ifdef	DEBUGMODE
                        setuid((uid_t)uid);
			debuglevel = atoi(p);
			debugmode = *p ? p : "0";
			bootopt |= BOOT_DEBUG;
			break;
#else
			fprintf(stderr, "ERROR: Cannot specify debuglevel unless DEBUGMODE is defined\n");
			exit(0);
#endif
		    case 'h':
			printf("DarkFire IRCd Daemon. IRCd version %s\nUsage: ircd [options]\n\nOptions:\n", version);
#ifdef CMDLINE_CONFIG
			printf("    -f config file   Specify location for ircd.conf\n");
#endif
			printf("    -n servername    Override config file server name\n");
			printf("    -p port          Override main port\n");
			printf("    -d dpath         Override configuration path\n");
			printf("    -x loglevel      Set debug log level\n");
			printf("    -t               Debug directly to tty\n");
			printf("    -c               Force forking if not run from console\n");
			printf("    -q               Skip name lookup for configured servers\n");
			printf("    -o               Boot from user local daemon\n");
			printf("    -i               Boot from inetd\n");
			printf("    -v               Display version info\n");
			exit(0);
		    default:
			bad_command();
			break;
		    }
	    }

#ifndef	CHROOT
	if (chdir(dpath))
	    {
		fprintf(stderr, "ERROR: Unable to chdir (ircd.c: main)\n");
		exit(-1);
	    }
#endif

#if !defined(IRC_UID)
	if ((uid != euid) && !euid)
	{
		fprintf(stderr, "ERROR: do not run ircd setuid root\n");
		exit(-1);
	}
#endif

#if (!defined(CHROOTDIR) || (defined(IRC_UID) && defined(IRC_GID)))
# ifndef	AIX
	setuid((uid_t)uid);
	setuid((uid_t)euid);
# endif

	if ((int)getuid() == 0)
	    {
# if defined(IRC_UID) && defined(IRC_GID)

		/* run as a specified user */
		fprintf(stderr,"WARNING: running ircd with uid = %d\n",
			IRC_UID);
		fprintf(stderr,"         changing to gid %d.\n",IRC_GID);
		setuid(IRC_UID);
		setgid(IRC_GID);
#else
		/* check for setuid root as usual */
		fprintf(stderr, "ERROR: do not run ircd setuid root.\n");
		exit(-1);
# endif	
	    } 
#endif /*CHROOTDIR/UID/GID*/

	/* didn't set debuglevel */
	/* but asked for debugging output to tty */
	if ((debuglevel < 0) &&  (bootopt & BOOT_TTY))
	    {
		fprintf(stderr, "ERROR: You must specify -x when using -t\n");
		exit(-1);
	    }

	if (argc > 0)
		return bad_command(); /* exit */

	myrand_init();
	mode_init();
	hash_init();
	inittoken();
	initlists();
	initclass();
	initwhowas();
	open_debugfile();
	if (portnum < 0)
		portnum = PORTNUM;
	me.port = portnum;
	init_sys();
	me.flags = FLAGS_LISTEN;
	if (bootopt & BOOT_INETD)
	    {
		me.fd = 0;
		local[0] = &me;
		me.flags = FLAGS_LISTEN;
	    }
	else
		me.fd = -1;

#ifdef USE_SYSLOG
	openlog(myargv[0], LOG_PID|LOG_NDELAY, LOG_FACILITY);
#endif
	*me.name = '\0';
	nowtime = time(NULL);
	if (initconf(bootopt) == -1)
	    {
		Debug((DEBUG_FATAL, "Failed in reading configuration file %s",
			configfile));
		fprintf(stderr, "ERROR: Unable to open config file %s\n",
			configfile);
		exit(-1);
	    }
	if (!(bootopt & BOOT_INETD))
	    {
		aConfItem	*aconf;

		if ((aconf = find_me()) && portarg <= 0 && aconf->port > 0)
			portnum = aconf->port;
		Debug((DEBUG_ERROR, "Port = %d", portnum));
		if (inetport(&me, aconf->passwd, portnum))
		{
			fprintf(stderr, "ERROR: Unable to bind port %d\n", portnum);
			exit(1);
		}
	    }
	else if (inetport(&me, "*", 0))
	{
		fprintf(stderr, "ERROR: port\n");
		exit(1);
	}

	setup_ping();
	strcpy(me.sockhost, me.name); /* HACK! */
	me.hopcount = 0;
	me.authfd = -1;
	me.confs = NULL;
	me.next = NULL;
	me.from = &me;
	SetMe(&me);
	me.srvptr = &me;
	me.s->down = NULL;
	me.s->downlinks = 0;

	if(ConfDVSStatus >= 0)
		DVSStatus = ConfDVSStatus;

	me.lasttime = me.since = me.firsttime = time(NULL);
	hash_add_client(&me);

#ifdef SHOWCONNECTINFO
	R_do_dns = mysprintf(REPORT_DO_DNS, ":%s %s", me.name, BREPORT_DO_DNS);
	R_fin_dns = mysprintf(REPORT_FIN_DNS, ":%s %s", me.name, BREPORT_FIN_DNS);
	R_fin_dnsc = mysprintf(REPORT_FIN_DNSC, ":%s %s", me.name, BREPORT_FIN_DNSC);
	R_fail_dns = mysprintf(REPORT_FAIL_DNS, ":%s %s", me.name, BREPORT_FAIL_DNS);
	R_do_id = mysprintf(REPORT_DO_ID, ":%s %s", me.name, BREPORT_DO_ID);
	R_fin_id = mysprintf(REPORT_FIN_ID, ":%s %s", me.name, BREPORT_FIN_ID);
	R_fail_id = mysprintf(REPORT_FAIL_ID, ":%s %s", me.name, BREPORT_FAIL_ID);
#endif

	check_class();
	if (bootopt & BOOT_OPER)
	    {
		aClient *tmp = add_connection(&me, 0);

		if (!tmp)
			exit(1);
		SetMaster(tmp);
	    }
	else
		write_pidfile();

	Debug((DEBUG_NOTICE,"Server ready..."));
#ifdef USE_SYSLOG
	syslog(LOG_NOTICE, "Server Ready");
#endif

	while (1)
	{
		nowtime = time(NULL);
		/*
		** We only want to connect if a connection is due,
		** not every time through.  Note, if there are no
		** active C lines, this call to Tryconnections is
		** made once only; it will return 0. - avalon
		*/
		if (nextconnect && nowtime >= nextconnect)
			nextconnect = try_connections(nowtime);
		/* Check expired akills
		 * nextakillexpire will be reset by m_akill and this call
		 */
		if (nextakillexpire && nowtime >= nextakillexpire)
			nextakillexpire = expire_akills(nowtime);
		/*
		** DNS checks. One to timeout queries, one for cache expiries.
		*/
		if (nowtime >= nextdnscheck)
			nextdnscheck = timeout_query_list(nowtime);
		if (nowtime >= nextexpire)
			nextexpire = expire_cache(nowtime);
		/*
		** take the smaller of the two 'timed' event times as
		** the time of next event (stops us being late :) - avalon
		** WARNING - nextconnect can return 0!
		*/
		if (nextconnect)
			delay = MIN(nextping, nextconnect);
		else
			delay = nextping;
		delay = MIN(nextdnscheck, delay);
		delay = MIN(nextexpire, delay);
		delay -= nowtime;
		/*
		** Adjust delay to something reasonable [ad hoc values]
		** (one might think something more clever here... --msa)
		** We don't really need to check that often and as long
		** as we don't delay too long, everything should be ok.
		** waiting too long can cause things to timeout...
		** i.e. PINGS -> a disconnection :(
		** - avalon
		*/
		if (delay < 1)
			delay = 1;
		else
			delay = MIN(delay, TIMESEC);
		read_message(delay);
		
		Debug((DEBUG_DEBUG ,"Got message(s)"));
		
		nowtime = time(NULL);
		/*
		** ...perhaps should not do these loops every time,
		** but only if there is some chance of something
		** happening (but, note that conf->hold times may
		** be changed elsewhere--so precomputed next event
		** time might be too far away... (similarly with
		** ping times) --msa
		*/
		if (nowtime >= nextping)
			nextping = check_pings(nowtime);

		if (dorehash)
		{
			rehash(&me, &me, 1);
			dorehash = 0;
		}
		/*
		** Flush output buffers on all connections now if they
		** have data in them (or at least try to flush)
		** -avalon
		*/
		flush_connections(me.fd);
	}
}

/*
 * open_debugfile
 *
 * If the -t option is not given on the command line when the server is
 * started, all debugging output is sent to the file set by LPATH in config.h
 * Here we just open that file and make sure it is opened to fd 2 so that
 * any fprintf's to stderr also goto the logfile.  If the debuglevel is not
 * set from the command line by -x, use /dev/null as the dummy logfile as long
 * as DEBUGMODE has been defined, else dont waste the fd.
 */
static	void	open_debugfile()
{
#ifdef	DEBUGMODE
	int	fd;
	aClient	*cptr;

	if (debuglevel >= 0)
	    {
		cptr = make_client(NULL, NULL);
		cptr->fd = 2;
		SetLog(cptr);
		cptr->port = debuglevel;
		cptr->flags = 0;
		cptr->acpt = cptr;
		local[2] = cptr;
		strcpy(cptr->sockhost, me.sockhost);
		printf("isatty = %d ttyname = 0x%X\n",
			isatty(2), (u_int)ttyname(2));
		if (!(bootopt & BOOT_TTY)) /* leave debugging output on fd 2 */
		    {
			truncate(LOGFILE, 0);
			if ((fd = open(LOGFILE, O_WRONLY | O_CREAT, 0600)) < 0) 
				if ((fd = open("/dev/null", O_WRONLY)) < 0)
				{
					fprintf(stderr, "ERROR: Unable to open logfile\n");
					exit(-1);
				}
			if (fd != 2)
			    {
				dup2(fd, 2);
				close(fd); 
			    }
			strncpyzt(cptr->name, LOGFILE, HOSTLEN+1);
		    }
		else if (isatty(2) && ttyname(2))
			strncpyzt(cptr->name, ttyname(2), HOSTLEN+1);
		else
			strcpy(cptr->name, "FD2-Pipe");
		Debug((DEBUG_FATAL, "Debug: File <%s> Level: %d at %s",
			cptr->name, cptr->port, myctime(time(NULL))));
	    }
	else
		local[2] = NULL;
#endif
	return;
}

static	void	setup_signals()
{
#ifdef	POSIX_SIGNALS
	struct	sigaction act;

	act.sa_handler = SIG_IGN;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGPIPE);
	sigaddset(&act.sa_mask, SIGALRM);
# ifdef	SIGWINCH
	sigaddset(&act.sa_mask, SIGWINCH);
	sigaction(SIGWINCH, &act, NULL);
# endif
	sigaction(SIGPIPE, &act, NULL);
	act.sa_handler = dummy;
	sigaction(SIGALRM, &act, NULL);
	act.sa_handler = s_rehash;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaction(SIGHUP, &act, NULL);
	act.sa_handler = s_restart;
	sigaddset(&act.sa_mask, SIGINT);
	sigaction(SIGINT, &act, NULL);
	act.sa_handler = s_die;
	sigaddset(&act.sa_mask, SIGTERM);
	sigaction(SIGTERM, &act, NULL);

#else
# ifndef	HAVE_RELIABLE_SIGNALS
	signal(SIGPIPE, dummy);
#  ifdef	SIGWINCH
	signal(SIGWINCH, dummy);
#  endif
# else
#  ifdef	SIGWINCH
	signal(SIGWINCH, SIG_IGN);
#  endif
	signal(SIGPIPE, SIG_IGN);
# endif
	signal(SIGALRM, dummy);   
	signal(SIGHUP, s_rehash);
	signal(SIGTERM, s_die); 
	signal(SIGINT, s_restart);
#endif
}
