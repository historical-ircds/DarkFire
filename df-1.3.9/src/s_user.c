/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_user.c (formerly ircd/s_msg.c)
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   See file AUTHORS in IRC package for additional names of
 *   the programmers. 
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "msg.h"
#include "channel.h"
#include <sys/stat.h>
#include <utmp.h>
#include <fcntl.h>
#include "h.h"
#include "dvs.h"

extern	int	chbuf_empty PROTO((aClient *));
extern	int	check_class_dvs PROTO((aClient *, int));

void	send_umode_out PROTO((aClient*, aClient *, int));
void	send_svsmode_out PROTO((aClient*, aClient *, aClient *, int));
void	send_umode PROTO((aClient *, aClient *, int, int, char *));
static	is_silenced PROTO((aClient *, aClient *));

int	remove_dvs PROTO((aClient *, int));

static char buf[BUFSIZE], buf2[BUFSIZE], buf3[BUFSIZE];

DA_MASTER_D(away_mast, AWAYLEN+1, 4072);

static int user_modes[] =
{ 
                                UMODE_OPER,		'o',
                                UMODE_LOCOP,		'O',
                                UMODE_INVISIBLE,	'i',
                                UMODE_WALLOP,		'w',
                                UMODE_FAILOP,		'g',
                                UMODE_SERVNOTICE,	's',
                                UMODE_KILLS,		'k',
                                UMODE_SADMIN,		'a',
                                UMODE_ADMIN,		'A',
                                UMODE_CLIENT,		'c',
                                UMODE_FLOOD,		'f',
                                UMODE_REGNICK,		'r',
                                UMODE_JUNK,		'j',
                                UMODE_DISGUISE,		'd',
                                UMODE_LIMITREG,		'R',
                                UMODE_SRA,		'S',
                                UMODE_CSOP,		'C', 
                                UMODE_CHATOP,		'b',
                                UMODE_HUSHNOTICE,	'u',
				UMODE_ESPANOL,		'E',
#ifdef OPERS_CAN_FLOOD
				UMODE_CANFLOOD,		'F',
#endif
                                0, 0
};

/*
** m_functions execute protocol messages on this server:
**
**	cptr	is always NON-NULL, pointing to a *LOCAL* client
**		structure (with an open socket connected!). This
**		identifies the physical socket where the message
**		originated (or which caused the m_function to be
**		executed--some m_functions may call others...).
**
**	sptr	is the source of the message, defined by the
**		prefix part of the message if present. If not
**		or prefix not found, then sptr==cptr.
**
**		(!IsServer(cptr)) => (cptr == sptr), because
**		prefixes are taken *only* from servers...
**
**		(IsServer(cptr))
**			(sptr == cptr) => the message didn't
**			have the prefix.
**
**			(sptr != cptr && IsServer(sptr) means
**			the prefix specified servername. (?)
**
**			(sptr != cptr && !IsServer(sptr) means
**			that message originated from a remote
**			user (not local).
**
**		combining
**
**		(!IsServer(sptr)) means that, sptr can safely
**		taken as defining the target structure of the
**		message in this server.
**
**	*Always* true (if 'parse' and others are working correct):
**
**	1)	sptr->from == cptr  (note: cptr->from == cptr)
**
**	2)	MyConnect(sptr) <=> sptr == cptr (e.g. sptr
**		*cannot* be a local connection, unless it's
**		actually cptr!). [MyConnect(x) should probably
**		be defined as (x == x->from) --msa ]
**
**	parc	number of variable parameter strings (if zero,
**		parv is allowed to be NULL)
**
**	parv	a NULL terminated list of parameter pointers,
**
**			parv[0], sender (prefix string), if not present
**				this points to an empty string.
**			parv[1]...parv[parc-1]
**				pointers to additional parameters
**			parv[parc] == NULL, *always*
**
**		note:	it is guaranteed that parv[0]..parv[parc-1] are all
**			non-NULL pointers.
*/


/*
** hunt_server
**
**	Do the basic thing in delivering the message (command)
**	across the relays to the specific server (server) for
**	actions.
**
**	Note:	The command is a format string and *MUST* be
**		of prefixed style (e.g. ":%s COMMAND %s ...").
**		Command can have only max 8 parameters.
**
**	server	parv[server] is the parameter identifying the
**		target server.
**
**	*WARNING*
**		parv[server] is replaced with the pointer to the
**		real servername from the matched client (I'm lazy
**		now --msa).
**
**      redone by binary, though works almost the same:
**		- check if there's the server parameter, if not assume me
**		- first check if there's a client with that name explicitly
**		or if it matches a server hash (first four characters)
**		- if all else fails, go through the server list and do match
**	
**	returns: (see #defines)
*/
int	hunt_server(aClient *cptr, aClient *sptr, char *command, int server,
		int parc, char *parv[])
{
	aClient *acptr;
	char *mask;

	/*
	** Assume it's me, if no server
	*/
	if (parc <= server || !(mask = parv[server]))
		return (HUNTED_ISME);
	/*
	 * find the client exactly or try matching the first 4 characters
	 * with server hashes; if we get a possible fake direction we quit
	 * with HUNTED_NOSUCH but don't send any error message.
	 */
	if (!(acptr = find_client(mask)) && !(acptr = hash_find_serv(mask)))
	{
		collapse(mask);
		for (acptr = firstserv; acptr; acptr = acptr->s->next)
			if (!qmatch(mask, acptr->name))
				break;
		if (!acptr)
		{
			send_num(sptr, ERR_NOSUCHSERVER, mask);
			return(HUNTED_NOSUCH);
		}
 	}
	if (IsMe(acptr) || MyClient(acptr))
		return HUNTED_ISME;
	if (acptr->from == cptr)
		return HUNTED_NOSUCH;
	if (IsServHash(acptr->from))
		parv[server] = (IsServer(acptr)?acptr:acptr->srvptr)->s->sndot;
	else
		parv[server] = acptr->name;
	sendto_one(acptr, command, parv[0], parv[1], parv[2], parv[3],
		parv[4], parv[5], parv[6], parv[7], parv[8]);
	return(HUNTED_PASS);
}

/*
 * do_username
 *
 *   Set up the user's username, filter out offensive chars, etc.
 */
void do_username(aClient *cptr, char *username)
{
	char uname[USERLEN+1];
	char *u2, *u1 = cptr->username;
	
	if ((cptr->flags & FLAGS_GOTID))
		strncpyzt(uname, cptr->username, USERLEN+1);
	else 
	{
		strncpyzt(uname, username, USERLEN);
		if ((cptr->flags&FLAGS_DOID))
			*u1++ = '~'; /* put it in final nick */
	}

	for (u2 = uname; *u2; u2++)
		if (isallowed(*u2))
			*u1++ = *u2;
	*u1 = '\0';
	if (!*cptr->username) /* blank, or no chars accepted */
		strcpy(cptr->username, "unknown");
}


/*
** 'do_nick_name' ensures that the given parameter (nick) is
** really a proper string for a nickname (note, the 'nick'
** may be modified in the process...)
**
**	RETURNS the length of the final NICKNAME (0, if
**	nickname is illegal)
**
**  Nickname characters are in range
**	'A'..'}', '_', '-', '0'..'9'
**  anything outside the above set will terminate nickname.
**  In addition, the first character cannot be '-'
**  or a Digit.
**
**  Note:
**	'~'-character should be allowed, but
**	a change should be global, some confusion would
**	result if only few servers allowed it...
*/
int	do_nick_name(char *nick)
{
	char *ch;

	if (*nick == '-' || isdigit(*nick)) /* first character in [0..9-] */
		return 0;

	for (ch = nick; *ch && (ch - nick) < NICKLEN; ch++)
		if (!isvalid(*ch))
			break;

	*ch = '\0';

	return (ch - nick);
}


/*
** change_nick
**  - change user's nick, send necessary protocol messages, reset various
**  flags, update the hash table, do watch notification, add to whowas list...
*/
extern	void change_nick(aClient *cptr, aClient *sptr, char *nick, time_t ts)
{ 
	Member *mp;
	
	if (!IsPerson(sptr))
		return;

	add_history(sptr);
	
	sendto_serv_butone(cptr, ":%n %m %s %d", sptr, &ms_nick, nick, ts);
	sendto_common_channels(sptr, "NICK %s", nick);

	if (ts!=0)
		sptr->u->nickts = ts;
	
	sptr->umodes &= ~UMODE_REGNICK;
	for (mp = sptr->u->channel; mp; mp = mp->nextc)
		mp->flags &= ~CHFL_BANSET;

	hash_check_notify(sptr, RPL_LOGOFF);
	set_client_name(sptr, nick);
	hash_check_notify(sptr, RPL_LOGON);
}

/*
** m_svsnick
**	parv[1] = old nickname
**	parv[2] = new nickname
**	parv[3] = timestamp
*/
int	m_svsnick(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	time_t ts;
	aClient *acptr, *ac2ptr;

	if (!IsULine(sptr) || parc < 4)
		return 0;

	if (!(acptr = find_client(parv[1])))
		return 0;
	if (MyClient(acptr))
	{
		if (find_client(parv[2]) || strlen(parv[2]) > NICKLEN)
			return exit_client(NULL, acptr, sptr, "SVSNICK collision");
		ts = atoi(parv[3]);
		change_nick(NULL, acptr, parv[2], ts?ts:nowtime);
	}
	else if (acptr->from != cptr)
		sendto_one(acptr, ":%s %m %s %s :%s", parv[0], &ms_svsnick,
			parv[1], parv[2], parv[3]);
	
	return 0; 
}

/*
 * New user segment -- modularized
 * Cleaned up by dave on 12/29/2002
 *
 * intro_user_to_one	send information about a user to one server
 *			  used by send_netburst in s_serv.c
 * intro_user_to_all	similar use, but for connecting nicks
 *			  used by register_user in s_user.c (below)
 * register_user	process a new user once both NICK and USER messages
 *			  have been received
 * m_nick		process nick command
 * m_user		process user command
 */

/* List of toplevel domains to default to spanish language
 * Also for 148.* IP address */
static char *spanish_tlds[] = {
	"ar", 		/* Argentina */
	"bo",		/* Bolivia */
	"cl",		/* Chile */
	"co",		/* Colombia */
	"cr",		/* Costa Rica */
	"cu",		/* Cuba */
	"dm",		/* Dominica */
	"do",		/* Dominican Republic */
	"ec",		/* Ecuador */
	"es",		/* Spain */
	"gt",		/* Guatemala */
	"hn",		/* Honduras */
	"mx",		/* Mexico */
	"ni",		/* Nicaragua */
	"pa",		/* Panama */
	"pe",		/* Peru */
	"pr",		/* Puerto Rico */
	"py", 		/* Paraguay */
	"sv",		/* El Salvador */
	"uy",		/* Uruguay */
	"ve",		/* Venezuela */
	NULL,
};

/* user_language */
int	user_language(aClient *acptr)
{
	char *s;
	char **m = spanish_tlds;

	if(!strncmp("148.",(char *)inetntoa((char *)&acptr->ip),4))
	{
		SetEspanol(acptr);
		return 1;
	}

	s = rindex(acptr->u->rhost, '.') + 1;

	for(; *m; m++)
	{
		if(!strcasecmp(*m,s))
		{
			SetEspanol(acptr);
			return 1;
		}
	}

	return 0;
}

/*
 * intro_user_to_one
 *	Introduce a user to one server
 */
void	intro_user_to_one(aClient *cptr, aClient *acptr)
{
	if(cptr->prolev >= PROLEV_DVSNICK)
	{
		sendto_one(cptr,
			"%m %s +%s %d %s %s %s %d %d :%s", 
                        &ms_nick,
                        acptr->name,
                        get_umode_string(acptr, 0, SEND_UMODES, buf),
                        acptr->u->nickts,
                        acptr->username,
                        acptr->u->rhost,
                        acptr->srvptr->s->shortn,
                        acptr->u->sstamp,
#ifdef USE_DVS
			IsSetDVS(acptr)?2:1,
#else
			1,
#endif
                        acptr->info);
	}
	else if(IsCompact(cptr))
	{
		sendto_one(cptr,
			"%m %s +%s %d %s %s %s %d :%s",
			&ms_nick,
			acptr->name,
			get_umode_string(acptr, 0, SEND_UMODES, buf),
			acptr->u->nickts,
			acptr->username,
			acptr->u->rhost,
			acptr->srvptr->s->shortn,
			acptr->u->sstamp,
			acptr->info);
	}
	else
	{
		sendto_one(cptr,
			"%m %s %d %d %s %s %s %d :%s",
			&ms_nick,
			acptr->name,
			acptr->hopcount+1,
			acptr->u->nickts,
			acptr->username,
			acptr->u->rhost,
			acptr->srvptr->name,
			acptr->u->sstamp,
			acptr->info);
		send_umode(cptr, acptr, 0, SEND_UMODES, buf);
	}
	if(cptr->prolev < PROLEV_CJOIN)
		send_user_joins(cptr, acptr);
	if(acptr->u->hush)
		sendto_one(cptr,":%n %m %n %d :%s", &me, &ms_hush, acptr,
			acptr->u->hush->end==0 ? 0 : acptr->u->hush->end - nowtime,
			acptr->u->hush->s);
	if(acptr->u->away)
		sendto_one(cptr,":%p %m :%s", acptr, &ms_away, acptr->u->away);
}

/*
 * intro_user_to_all
 *	Introduce a user to all servers except cptr
 */
void	intro_user_to_all(aClient *cptr, aClient *acptr)
{
	sendto_serv_butone_wproto(cptr, PROLEV_DVSNICK,
		TOK_NICK" %s +%s %d %s %s %s %d %d :%s",
                acptr->name,
                get_umode_string(acptr, 0, SEND_UMODES, buf),
                acptr->u->nickts,
                acptr->username,
                acptr->u->rhost,
                acptr->srvptr->s->shortn,
                acptr->u->sstamp,
#ifdef USE_DVS
		IsSetDVS(acptr)?2:1,
#else
		1,
#endif
                acptr->info);
	sendto_serv_butone_bw_proto(cptr, PROLEV_SERVHASH, PROLEV_CJOIN,
		TOK_NICK" %s +%s %d %s %s %s %d :%s",
		acptr->name,
		get_umode_string(acptr, 0, SEND_UMODES, buf),
		acptr->u->nickts,
		acptr->username,
		acptr->u->rhost,
		acptr->srvptr->s->shortn,
		acptr->u->sstamp,
		acptr->info);
	sendto_serv_butone_nproto(cptr, PROLEV_SERVHASH,
		"%m %s %d %d %s %s %s %d :%s",
		&ms_nick,
		acptr->name,
		acptr->hopcount+1,
		acptr->u->nickts,
		acptr->username,
		acptr->u->rhost,
		acptr->srvptr->name,
		acptr->u->sstamp,
		acptr->info);
	send_umode(NULL, acptr, 0, SEND_UMODES, buf);
	if(*buf)
	{
		sendto_serv_butone_proto(cptr, PROLEV_SERVHASH,
			":%n %m %s", acptr, &ms_mode, buf);
		sendto_serv_butone_nproto(cptr, PROLEV_SERVHASH,
			":%n %m %s :%s", acptr, &ms_mode, acptr->name, buf);
	}
}

/*
 * register_user
 *	This function is called when both NICK and USER messages
 *	have been accepted for the client, in whatever order.
*/
int	register_user(aClient *cptr, aClient *sptr, char *username, int clientdvs)
{
	char	*parv[3], *tmpstr;
	anUser	*user = sptr->u;
	aClient *nsptr, *acptr;
	int	i;

	if (MyConnect(sptr))
	{
		sptr->ulast = nowtime;
		if ((i = check_client(sptr)))
		{
			sendto_umode(UMODE_CLIENT,
				"*** Notice -- %s from %s.",
				i == -3 ? "Too many connections" :
				"Unauthorized connection",
				get_client_host(sptr));
			return exit_client(cptr, sptr, &me, i == -3 ?
			  "This server is full.  Please try irc.darkfire.net" :
			  "You are not authorized to connect to this server");
		}
		
		if (!BadPtr(sptr->confs->value.aconf->passwd))
		{
			if (!StrEq(sptr->passwd,sptr->confs->value.aconf->passwd))
			{
				send_num(sptr, ERR_PASSWDMISMATCH);
				return exit_client(cptr, sptr, &me, "Bad Password");
			}
			else *sptr->passwd = '\0';
		}
		
		do_username(sptr, username);
		
		if (sptr->hostp)
		{
			/* No control-chars or ip-like dns replies... I cheat :)
			   -- OnyxDragon */
			for (tmpstr = sptr->sockhost; *tmpstr > ' ' &&
				*tmpstr < 127; tmpstr++);
			if (*tmpstr || isdigit(*(tmpstr-1)))
				strncpyzt(sptr->u->rhost, (char *)inetntoa((char *)&sptr->ip), HOSTLEN+1); /* Fix the sockhost for debug jic */
			else
				strncpyzt(sptr->u->rhost, sptr->sockhost, HOSTLEN+1);
		}
		else
			strncpyzt(sptr->u->rhost, sptr->sockhost, HOSTLEN+1);

		if (find_kill(sptr))
			return exit_client(cptr, sptr, &me, "K-lined");
		SetUser(sptr);
		user_language(sptr); /* English or Spanish? */
		sptr->umodes |= UMODE_DISGUISE; /* Automatic umode +d */
	}
	else
		strncpyzt(sptr->username, username, USERLEN+1);
	
	SetClient(sptr); /* set this client's status as a person */
	
	/* Disguise host AHEAD of time... */
	disguise(sptr->u->dhost, sptr->u->rhost);

	if(IsDisguise(sptr))
		sptr->u->host = sptr->u->dhost;

#ifdef USE_DVS
	if(clientdvs == 2 || (clientdvs != 1 && !(IsRegNick(sptr) || IsOper(sptr) 
	   || IsULine(sptr) || IsULine(sptr->srvptr)) 
	   && (!MyConnect(sptr) || (DVSStatus == 3 || (DVSStatus == 2 && 
	   !check_class_dvs(sptr, 20)) || (DVSStatus == 1 && 
	   ((sptr->flags&FLAGS_DOID && !(sptr->flags&FLAGS_GOTID)) 
	   || check_class_dvs(sptr, 10)))))))
	{
		SetDVS(sptr);
		SetDVStag(sptr);
	}
#endif

	intro_user_to_all(cptr, sptr);
	hash_check_notify(sptr, RPL_LOGON);

	if (MyConnect(sptr))
	{
		parv[1] = parv[2] = NULL;
		if (sptr->passwd[0])
		{
			if ((nsptr = find_person(NickServ)))
				sendto_one(nsptr, ":%s PRIVMSG %s :IDENTIFY %s",
				sptr->name, NickServ, sptr->passwd);
			bzero(sptr->passwd, sizeof(sptr->passwd));
		}

		send_num(sptr, RPL_WELCOME, sptr->name, sptr->username, sptr->u->rhost);
		send_num(sptr, RPL_YOURHOST, me.name, version);
		send_num(sptr, RPL_CREATED, creation);
		send_num(sptr, RPL_MYINFO, me.name, version);
		send_num(sptr, RPL_PROTOCTL, PROTOCTL_SUPPORTED);

		m_lusers(sptr, sptr, 1, parv);

		send_num(sptr, RPL_MOTDSTART, me.name);
		send_num(sptr, RPL_MOTD, "Please type /motd to see the actual motd.");
		send_num(sptr, RPL_ENDOFMOTD);

		if(IsEspanol(sptr))
			sendto_one(sptr, ":%s NOTICE %s :Si quiere recibir mensajes en ingl�s, pon \2/lang en\2 (To change your language to English, type \2/lang en\2)",
				me.name, sptr->name, sptr->name, sptr->name);

#ifdef USE_DVS
		sendto_one(sptr, ":%n NOTICE %n :*** DVS -- This network is protected by the DarkFire Verification System", &me, sptr);

		if(IsSetDVS(sptr))
		{
			if(ServicesOnline == 0 && DVSLock == 0)
			{
				sendto_one(sptr, ":%n NOTICE %n :*** DVS -- Services is currently offline. When it returns, you will be given 10 minutes to register and/or identify with NickServ.", &me, sptr);
				sendto_one(sptr, ":%n NOTICE %n :*** DVS -- You may now use the network as normal. After services return, you will be restricted until you identify.", &me, sptr);
			}
			else
				sendto_one(sptr, ":%n NOTICE %n :*** DVS -- You must register and/or identify with NickServ in order to use this network.", &me, sptr);

			sendto_one(sptr, ":%n NOTICE %n :*** DVS -- For information about DVS, type \2/dvs\2 or visit "DVS_URL, &me, sptr);
		}
		else
			sendto_one(sptr, ":%n NOTICE %n :*** DVS -- You are exempt from DVS Protection (you may use the network as normal)", &me, sptr);
#endif

		sendto_umode(UMODE_CLIENT,"*** Notice -- Client connecting on port %d: %s (%s@%s)",
			sptr->acpt->port, sptr->name, sptr->username,
			sptr->u->host);

		send_umode_out(cptr,sptr,0);
	}
	else if (IsULine(sptr->srvptr))
		sptr->flags |= FLAGS_ULINE;

	return 0;
}


/*
 * m_nick
 *	parv[1] = nickname
 *  if from new client  -taz
 *	parv[2] = nick password
 *  if from server:
 *      parv[2] = from 'compact' server this is modes, otherwise ignored
 *      parv[3] = timestamp
 *      parv[4] = username
 *      parv[5] = hostname
 *      parv[6] = servername
 *      parv[7] = sstamp
 *	parv[8] = info
 */
int	m_nick(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aConfItem *aconf;
	aSqlineItem *asqline;
	aClient *acptr, *serv;
	char	nick[NICKLEN+2], *s, c, *m;
	Member	*mp;
	time_t	nickts = (time_t)0;
	int	differ, welose, flag, *x;
	
	if (parc < 2)
	{
		send_num(sptr, ERR_NONICKNAMEGIVEN);
		return 0;
	}

	strncpyzt(nick, parv[1], NICKLEN+1);
	
	if (IsServer(cptr) && (parc > 7 && (!(serv = hash_find_serv(parv[6]))
		|| serv->from != cptr->from)))
	    return 0;	

	/* make sure nick name is valid */
	if (strlen(nick)>NICKLEN || (!IsServer(cptr) && do_nick_name(nick)==0))
	{
		send_num(sptr, ERR_ERRONEOUSNICKNAME, parv[1],
			"Illegal characters");
		if (IsServer(cptr))
		{
			sendto_umode(UMODE_JUNK, "Bad Nick: %s From: %s %s",
				   parv[1], sptr->name, get_client_name(cptr, FALSE));
			sendto_one(cptr, ":%s KILL %s :%s (Illegal Nick)",
				   me.name, parv[1], me.name);
			if (sptr != cptr)
			{ /* bad nick change */
				sendto_serv_butone(cptr,
					":%n %m %s :%s (Bad Nick Change)",
					&me, &ms_kill, sptr->name, me.name);
				sptr->flags |= FLAGS_KILLED;
				return exit_client(cptr,sptr,&me,"Bad Nick");
			}
		}
		return 0;
	}

	/* New/Improved QLINE/SQLINE check
	 * Check all nicks and take appropriate action depending on type
	 */
	if ((aconf = find_conf_name(nick, CONF_QUARANTINED_NICK)) ||
	    (asqline = find_sqline_match(nick)))
	{
		if(IsPerson(cptr) || IsUnknown(cptr))
		{
			if(IsAnOper(sptr))
				sendto_realops("Q-lined nick %s from IRCop %s",
					       nick, sptr->name);
			else
			{
				send_num(sptr, ERR_ERRONEOUSNICKNAME, nick,
					aconf ? (BadPtr(aconf->passwd) ?
					"forbidden" : aconf->passwd) :
					(BadPtr(asqline->reason) ? "forbidden"
					: asqline->reason));
				return 0;
			}
		}
	}

	if (IsPerson(cptr) && !IsAnOper(cptr))
		cptr->since += 3; /* Nick flood protection */

	if ((acptr = find_client(nick)))
	{
		if (acptr == sptr)
		{ /* same user changing case - return if no case change */
			if (!strcmp(acptr->name, nick))
				return 0;
		}
		else if (IsUnknown(acptr) && MyConnect(acptr))
		{ /* override my user who has sent nick but no user */
			if (acptr == cptr)
				return 0;
			acptr->flags |= FLAGS_KILLED;
			exit_client(NULL, acptr, &me, "Overridden");
		}
		else if (IsPerson(cptr) || IsUnknown(cptr))
		{ /* my user */
			send_num(sptr, ERR_NICKNAMEINUSE, nick);
			return 0;
		}
		else if (cptr == acptr->from)
		{ /* users with same nick on same side of net - fix ghost */
			exit_client(cptr, acptr, &me, "Ghosted");
		}
		/*
		 * Nick collision.  If the user on our side of the network is
		 * wrong, we remove the user.  Otherwise, if it's a nick
		 * change, kill their user user under the old nick on our
		 * side and ignore the NICK, assuming they will acknowledge
		 * their loss.  Actively nuking their user will result in
		 * massive desyncing -- it's been tried before.
		 */
		else if (IsServer(sptr))
		{
			differ = 1;
			welose = 0;
			if (parc>3)
			{
				nickts = IsULine(sptr)?0:atoi(parv[3]);
				if (parc > 5)
				 differ = (mycmp(acptr->username, parv[4]) ||
					mycmp(acptr->u->host, parv[5]));
			}
			else
				nickts = acptr->u->nickts;
			if (nickts == acptr->u->nickts)
				welose = -1; /* both die */
			else if (differ==(acptr->u->nickts > nickts))
				welose = 1; /* mine dies */
			if (welose != 0)
			{
				sendto_serv_butone(cptr, ":%n %m %s :%s (Nick Collision (=))",
					&me, &ms_kill, acptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(cptr, acptr, &me, "Nick Collision");
			}
			if (welose != 1) /* don't accept... */
				return 0; /* hope other server handles it */
		}
		else /* a NICK change has collided */
		{
			differ = 1;
			welose = 0;
			nickts = (parc >= 2) ? atoi(parv[2]) : acptr->u->nickts;
			differ = (mycmp(acptr->username, sptr->username) ||
				mycmp(acptr->u->rhost, sptr->u->rhost));
			if (nickts == acptr->u->nickts)
				welose = -1; /* both lose */
			else if (differ == (acptr->u->nickts > nickts))
				welose = 1; /* just ours loses */
			if (welose != 0)
			{
				/* our user lost - kill our user */
				sendto_serv_butone(cptr, ":%n %m %s :%s (Nick Collision)",
					&me, &ms_kill, acptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(NULL, acptr, &me, "Nick Collision");
			}
			if (welose != 1)
			{
				/* their user lost - let my servers know */
				sendto_serv_butone(cptr, ":%n %m %s :%s (Nick Collision)",
					&me, &ms_kill, sptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(NULL, sptr, &me, "Nick Collision");
				return 0;
	 		}
		}
	}

	/*
	** We are now going to accept the nick and handle it appropriately.
	*/
	if (IsUnknown(sptr))
	{ /* user on my server and choosing first nick */
		set_client_name(sptr, nick);
		sptr->u->nickts = nowtime;		

		/* Copy password to the passwd field if it's given after NICK
		 * - originally by taz, modified by Wizzu
		 */
		if((parv[2]) && (strlen(parv[2]) < sizeof(sptr->passwd)))
			strcpy(sptr->passwd,parv[2]);

		if (GotUser(sptr))
			return register_user(cptr, sptr, sptr->username, 0);
	}
	else if (IsPerson(sptr))
	{ /* this is only a nick change. */
		if (IsPerson(cptr))
		    for (mp = cptr->u->channel; mp; mp = mp->nextc)
			if (cannot_do_nick(mp, nick))
			    return 0;

		if (mycmp(sptr->name, nick))
			sptr->u->nickts = (IsPerson(cptr) || !parv[2]) ?
				nowtime:atoi(parv[2]);
		
		change_nick(cptr, sptr, nick, sptr->u->nickts);
	}
	else if (IsServer(sptr))
	{ /* a server introducing a new client, replace source w/ new user */
		cptr->s->lastsptr = sptr = make_client(cptr, serv);
		add_client_to_list(sptr);
		sptr->hopcount = serv->hopcount;
		if (parc > 3)
			sptr->u->nickts = atoi(parv[3]);
		else
			sptr->u->nickts = nowtime;
		set_client_name(sptr, nick);
		if(IsCompact(cptr))
		{
			m = &parv[2][1];
			while(*m)
			{
				for (x = user_modes; (flag = *x); x += 2)
					if (*m == *(x + 1))
					{
						sptr->umodes |= flag & SEND_UMODES;
						break;
					}
				m++;
			}
		}
		if (parc>7)
		{
			parv[3]=nick;
			return m_user(cptr, sptr, parc-3, &parv[3]);
		}
	}

	
	return 0;
}

/*
 * m_user
 *	parv[1] = username (login name, account)
 *	parv[2] = client host name (used only from other servers)
 *	parv[3] = server host name (used only from other servers)
 *      parv[4] = sstamp
 *      parv[parc-2] = DVS 0/1
 *	parv[parc-1] = users real name info
 */
int	m_user(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*host, *server;
	anUser	*user;
 
	if (!IsUnknown(sptr))
	{
		send_num(sptr, ERR_ALREADYREGISTRED);
		return 0;
	}

	if (parc > 2 && (host = (char *)index(parv[1],'@')))
		*host = '\0'; 
	if (parc <= 4)
		return check_params(sptr, "USER", 0, 0);
	
	sptr->flags |= FLAGS_GOTUSER;

	user = make_user(sptr);
	user->sstamp = (parc>=6&&IsServer(cptr))?atol(parv[4]):0;
	strncpyzt(user->rhost, parv[2], HOSTLEN+1);
	strncpyzt(sptr->info, parv[parc-1], sizeof(sptr->info));

	if (sptr->name[0])
		return register_user(cptr, sptr, parv[1], 
			(cptr->prolev>=PROLEV_DVSNICK?atoi(parv[parc-2]):0));
	
	strncpyzt(sptr->username, parv[1], USERLEN+1);
	
	return 0;
}

 /******************************************************************
  * m_message segment                                              *
  ******************************************************************/

/*
 * check_privmsg(sptr, acptr, text)
 *  - Checks to see if sptr is trying to send a dcc to acptr and returns -1
 *  and prints appropriate notices if sptr is not allowed to send it.
 *  - Expanded to do other things.
 */
int check_privmsg(aClient *sptr, aClient *acptr, char *text)
{
        if (!IsPerson(sptr))
                return 0;

        if (!(IsRegNick(sptr) || IsULine(sptr)) && (acptr->umodes&UMODE_LIMITREG))
        {
		send_num(sptr, ERR_NONONREG, acptr->name);
                return -1;
        }

        if (is_silenced(sptr, acptr))
                return -1;

        if (!MyConnect(sptr))
                return 0;

        /* these checks only performed for local senders */

        if (duplicate_target(acptr))
                return -1;

        if (msgptr!=&ms_notice)
        {
                if (acptr->u->hush && !IsAnOper(sptr))
                        send_num(sptr, RPL_USERISHUSHED,
                                acptr->name, acptr->u->hush->by, acptr->u->hush->s);
                else if (acptr->u->away)
                        send_num(sptr, RPL_AWAY, acptr->name, acptr->u->away);
        }

        /* This checks for mIRC-style CTCP's only --                *
         * we assume the first char will be a ^A or it's not a DCC. */
        /* This is an icky algorithm.  Replacing it might be a good idea. */

        if (*text == 1 && !myncmp(text+1, "DCC SEND", 8) && !(IsAnOper(sptr) ||
	    IsULine(acptr)))
        {
          Member *mp;
          for (mp = sptr->u->channel; mp; mp = mp->nextc)
          {
            if ((mp->chptr->mode.mode&MODE_NODCCS)&&!(mp->flags&CHFL_OVERLAP))
            {
              /* allow moderate DCC sending... stop if they try to send
               * a third one in less than 30 secs, or a fourth in less than
               * 150 secs, fifth in 270, etc... and let the penalties keep
               * accumulating so infected users will never be able to send.
               */
              sptr->lastdcc += 120;
              if (sptr->lastdcc>=nowtime)
              {
                sendto_one(sptr, ":%s NOTICE %s :%s%s%s%s%s%d%s", me.name,
                  sptr->name, "Channel ", mp->chptr->chname,
                  " prohibits rapid DCC sending.  Please /part ",
                  mp->chptr->chname, " or wait ",
                  sptr->lastdcc-nowtime, " seconds to send.");
		sendto_channelops_butone(&me, mp->chptr, 
		  "%s has exceeded the maximum DCC send rate on +d channel %s",
		  sptr->name, mp->chptr->chname);
                return -1;
              }
              else if (sptr->lastdcc < nowtime-210)
                sptr->lastdcc = nowtime-210;
              break;
            }
          }
        }

	return 0;
}

static void *targetlist[16];
static int targetnum;

/*
 * use this instead of CPU-heavy canonize...
 */
int duplicate_target(void *target)
{
	int i;
	
	if (targetnum >= 16)
		return -1;
	
	for (i = targetnum; i != 0;)
		if (targetlist[--i] == target)
			return -1;
	
	targetlist[targetnum++] = target;
	
	return 0;
}


/*
 * m_message (used in m_private() and m_notice())
 * the general function to deliver MSG's between users/channels
 *
 *	parv[1] = receiver list
 *	parv[2] = message text
 *
 * massive cleanup
 * rev argv 6/91
 *
 */
int	m_message(aClient *cptr, aClient *sptr, int parc, char *parv[], int notice)
{
	aClient *acptr;
	aChannel *chptr;
	char *nick, *server, *p, *s, send;
	int flags = 0;
	
	targetnum = 0;

	if (parc < 3)
	{
		if (parc < 2)
			send_num(sptr, ERR_NORECIPIENT, msgptr->cmd);
		else
			send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if (IsPerson(sptr) && sptr->u->hush)
		return 0; /* don't allow foreign hushed users to send msgs */
	
	for (p = parv[1]; nick = strsplit(&p, ',');)
	{
		/* channel? now must start with # */
		if (IsChannelName(nick))
		{
			if (!(chptr = hash_find_channel(nick)))
				goto m_msg_nosuch;
#ifdef USE_DVS
			if(IsDVS(sptr) && MyConnect(sptr))
			{
				send_num(sptr, ERR_DVSLIMITED);
				continue;
			}
#endif
			if (duplicate_target(chptr))
				continue;
			if ((send = can_send(sptr, chptr, parv[2], notice)) == 0)
				sendto_channel_members(sptr, chptr, ":%p %m %s :%s", sptr, msgptr, chptr->chname, parv[2]);
			else if (!notice && send != 1)
				send_num(sptr, ERR_CANNOTSENDTOCHAN, nick, send);
			continue;
		}

		/* Channel flag messages: <flags>#chname
		 * Flags can be combined in any order, with messages going to
		 * only the levels specified.
		 * %+#darkfire sends a message to halfops and voices, not ops
		 */
		if(nick[0] == '@' || nick[0] == '%' || nick[0] == '+')
		{
			while(!IsChannelName(nick))
			{
				if(*nick == '@')
					flags |= CHFL_CHANOP;
				if(*nick == '%')
					flags |= CHFL_HALFOP;
				if(*nick == '+')
					flags |= CHFL_VOICE;
				nick++;
			}

#ifdef USE_DVS
                        if(IsDVS(sptr) && MyConnect(sptr))   
                        {
                                send_num(sptr, ERR_DVSLIMITED);
                                continue;
                        }
#endif

			if(!(chptr = hash_find_channel(nick)))
				goto m_msg_nosuch;
			if(duplicate_target(chptr))
				continue;
			if((send=can_send(sptr, chptr, parv[2], 1)) != 0)
			{
				if(send!=1)
					send_num(sptr, ERR_CANNOTSENDTOCHAN, nick, send);
				continue;
			}
			send_channel_msg_byflag(sptr, chptr, flags, msgptr,
						parv[2]);
			continue;
		}
		
		/* $servermask addressed? */
		if ((*nick == '$') && IsAnOper(sptr))
		{
			sendto_match_butone(IsServer(cptr) ? cptr : NULL, 
				sptr, nick + 1, "%s %s :%s",
				msgptr->cmd, nick, parv[2]);
			continue;
		}
		

		/* nickname addressed? */		 
		if ((acptr = find_person(nick)))
		{
#ifdef USE_DVS
                        if(IsDVS(sptr) && !IsULine(acptr))
                        {
                                send_num(sptr, ERR_DVSLIMITED);
                                continue;
                        }

			if(IsDVS(acptr) && !(IsAnOper(sptr) || IsULine(sptr)))
			{
				send_num(sptr, ERR_MSGDVSLIMITED, nick);
				continue;
			}
#endif
			if (check_privmsg(sptr, acptr, parv[2]) < 0)
				continue;
			sendto_one(acptr, ":%p %m %s :%s", sptr, msgptr, nick, parv[2]);
			continue;
		}

		/* user@server addressed? */
		if ((server = index(nick, '@')) &&
			(acptr = hash_find_serv(server+1)))
		{
#ifdef USE_DVS
                        if(IsDVS(sptr) && !IsULine(acptr))   
                        {
                                send_num(sptr, ERR_DVSLIMITED);
                                continue;
                        }

                        if(IsDVS(acptr) && !(IsAnOper(sptr) || IsULine(sptr)))
                        {
                                send_num(sptr, ERR_MSGDVSLIMITED, nick);
				continue;
                        }
#endif

			if (!IsMe(acptr))
			{
				sendto_one(acptr, ":%s %m %s :%s", sptr->name,
					msgptr, nick, parv[2]);
				continue;
			}

			*server = '\0';
			acptr = hash_find_nickserver(nick, &me);
			if (acptr)
			{
				if (!duplicate_target(acptr))
					sendto_one(acptr, ":%p %m %s :%s",
						sptr, msgptr, acptr->name, parv[2]);
				continue;
			}
		}

m_msg_nosuch:
		send_num(sptr, ERR_NOSUCHNICK, nick);
	}
	return 0;
}


/*
 * m_private
 *	parv[1] = receiver list
 *	parv[2] = message text
 */
int	m_private(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (MyConnect(sptr))
		cptr->ulast = nowtime; /* update idle time */
	if (check_registered_user(sptr)) return 0;
	return m_message(cptr, sptr, parc, parv, 0);
}


/*
 * m_notice
 *	parv[1] = receiver list
 *	parv[2] = notice text
 */
int	m_notice(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (check_registered(sptr)) return 0;
	return m_message(cptr, sptr, parc, parv, 1);
}


 /******************************************************************
  * m_who segment                                                  *
  ******************************************************************/

static char *w_help[] =
{
	"*** Usage for /who *** ",
	" /who <mask>[;][<flags>]",
	"The mandatory mask field contains either:",
	" - channel name (ex. #channel), lists users in channel",
	" - general format [nick](!)[[user@][host.name]][:server][/gcos]",
	" lists all noninvisible users that match all specified fields",
	" ex: /who CoderGuy [match nick exact]",
	"     /who ppp123.blah.com [match host exact]",
	"     /who henryj@ [match ident exact]",
	"     /who henryj@*.blah.com [match ident and host]",
	"     /who Coder*!*henry*@*.blah.com [match nick, ident, and host]",
	"     /who *:server.darkfire.net [match server]",
	"     /who /henry?jay [match real name only]",
	"     /who Code*!henryj@*.blah.com:server.darkfire.net/henry?jay",
	"      [match all fields]",
	"The optional flags field can contain:",
	" - flags that combine and require all to be true for a match:",
	"     o - IRC operator       w - mode +w",
	"     A - server admin       a - services admin",
	"     G - /away              H - here",
	"     & - local to this server",
	" ex: /who * o lists all IRC operators online",
	" - flags that test channel status for /who #channel only",
	"     @ - channel operator   + - voice",
	"     \2%\2 - halfop",
	"     (multiple chan flags require only one match, logical OR)",
	" ex: /who #darkfire @ lists all channel operators on #darkfire",
	"*** End of help ***",
	NULL
};

char *w_nick, *w_user, *w_host, *w_real;
char w_isoper;

static	int	who_match(aClient *acptr)
{
	if (w_nick && qmatch(w_nick, acptr->name))
		return 0;
	if (w_user && qmatch(w_user, acptr->username))
		return 0;
	if (w_host && !w_isoper && 
	    qmatch(w_host, IsDisguise(acptr)?acptr->u->dhost:acptr->u->rhost))
		return 0;
	if (w_host && w_isoper && qmatch(w_host, acptr->u->rhost) && 
	    match(w_host, acptr->u->dhost))
		return 0;
	if (w_real && qmatch(w_real, acptr->info))
		return 0;
	return 1;
}

static	void	do_who(aClient *sptr, aClient *acptr, Member *mp)
{
	static char status[10];
	char *s = status;

#ifdef USE_DVS
	if(IsDVS(acptr))
		*s++ = 'D';
#endif

	if(mp && mp->chptr->mode.mode&(MODE_SECRET|MODE_PRIVATE))
		*s++ = 's';

	*s++ = acptr->u->away ? 'G' : 'H';
	if (IsInvisible(acptr))
		*s++ = 'i';
	if (IsDisguise(acptr))
		*s++ = 'd';
	if (IsAnOper(acptr))
	{
		*s++ = '*';
		if (IsSAdmin(acptr))
			*s++ = 'a';
		if (IsAdmin(acptr))
			*s++ = 'A';
	}
	if (IsHushed(acptr))
		*s++ = '-';
	if (mp && mp->flags&CHFL_OVERLAP)
		*s++ = (mp->flags&CHFL_CHANOP) ? '@' : ((mp->flags&CHFL_HALFOP) ? '%' : '+');
	*s = '\0';
	send_num(sptr, RPL_WHOREPLY, (mp) ? (mp->chptr->chname) : "*",
		acptr->username, w_isoper?acptr->u->rhost:acptr->u->host, 
		acptr->srvptr->s->sndot, acptr->name, status, acptr->hopcount, 
		acptr->info);
}

Member *find_who_chan(aClient *acptr)
{
	Member *mp;
	
	for (mp = acptr->u->channel; mp; mp = mp->nextc)
		if (PubChannel(mp->chptr)) return mp;

	return (w_isoper)?acptr->u->channel:NULL; /* 1 channel for ircops */
}

#define MAX_WHOREPLIES	64
#define CHK_WHO_USER(x)	(						    \
	(((x)->umodes&userflags) != userflags)				    \
	|| (servptr && (x)->srvptr!=servptr)				    \
	|| (awaystat && ((((x)->u->away==NULL)?1:-1)!=awaystat))	    \
			)

#define CHK_MASK(x) (x=(x&&*x&&(*x!='*'||x[1])&&(*x!='?'||x[1]!='*'||x[2]))?x:NULL)

/*
** m_who
**	parv[1] = mask; 0 #channel nick!user@host:serv/real
**	parv[2] = additional selection flag, only 'o' for now.
**		Now more! hwi@+HG (+h, +w, +i, chanop, voice, here, gone)
**		even more! Aa& (admin, services admin, local client)
** Expanded by binary.  Not QUITE so extensive as hybrid's monster,
** but instead short, sweet, and much more intuitive.
*/
int	m_who(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
    aClient	*acptr, *servptr = NULL;
    Member	*mp;
    aChannel	*chptr = NULL;
    char	*mask = parv[1], *flags = parv[2];
    char	*s;
    u_int32_t	userflags = 0, chanflags = 0;
    char	showp, awaystat = 0;
    int		num_whoreplies = 0;

    if (check_registered_user(sptr))
        return 0;
    
    if (!mask || (mask[1]==0 && *mask=='?'))
        return dumpit(cptr, RPL_LISTSYNTAX, w_help);

    /* fix broken clients that can't do who flags */
    if (parc == 2 && (flags = index(mask, ';')))
    {
	parc = 3;
	*flags++ = '\0';
    }

    if (parc > 2)
    {
        for (s = flags; *s; s++)
        {
            switch(*s)
            {
                case 'o': userflags |= UMODE_OPER; break;
                case 'w': userflags |= UMODE_WALLOP; break;
                case 'i': userflags |= UMODE_INVISIBLE; break;
                case 'A': userflags |= UMODE_ADMIN; break;
                case 'a': userflags |= UMODE_SADMIN; break;
                case '&': servptr = &me; break;
                case 'H': awaystat = 1; break;
                case 'G': awaystat = -1; break;
                case '@': chanflags |= CHFL_CHANOP; break;
		case '%': chanflags |= CHFL_HALFOP; break;
                case '+': chanflags |= CHFL_VOICE; break;
            }
        }
    }
    
    w_isoper = IsAnOper(sptr);

    if (mask&&IsChannelName(mask))
    {
        /* List all users on a given channel */
        if (!(chptr = hash_find_channel(mask)))
            goto done_with_who;
        showp = (IsMember(sptr, chptr) || w_isoper);
        if (!PubChannel(chptr) && !showp)
            goto done_with_who;
            
        for (mp = chptr->members; mp; mp = mp->nextu)
        {
            acptr = mp->cptr;
            if (!showp && IsInvisible(acptr))
                continue;
            if ((chanflags) && ((mp->flags&chanflags)==0))
                continue;
            if (CHK_WHO_USER(acptr))
                continue;
            do_who(sptr, acptr, mp);
        }
    }
    else
    {
        /* Mask Preprocessor */
        w_nick = NULL;
        w_user = NULL;
        w_host = NULL;
        w_real = NULL;
        if (mask && (*mask != '0' || mask[1]))
        {
            collapse(mask);
            if ((w_real = index(mask, '/')))
                *w_real++ = '\0';
            if ((s = index(mask, ':')))
            {
                *s++ = '\0';
                if (!(servptr = hash_find_serv(s)))
                    goto done_with_who;
            }
            if ((w_host = index(mask, '@')))
            {
                *w_host++ = '\0';
                if ((w_user = index(mask, '!')))
                {
                    *w_user++ = '\0';
                    w_nick = mask;
                }
                else
                    w_user = mask;
            }
            else if (index(mask, '.'))
                w_host = mask;
            else
                w_nick = mask;
            CHK_MASK(w_nick);
            CHK_MASK(w_user);
            CHK_MASK(w_host);
            CHK_MASK(w_real);
            /* so end of who comes out correctly... */
            mysprintf((mask = buf), "%s!%s@%s:%s/%s", w_nick?w_nick:"*",
                w_user?w_user:"*", w_host?w_host:"*",
                servptr?servptr->name:"*", w_real?w_real:"*");
            if (w_nick && !index(w_nick, '?') && !index(w_nick, '*'))
            {
                if ((acptr = find_person(w_nick)) && !CHK_WHO_USER(acptr) &&
                   who_match(acptr))
                    do_who(sptr, acptr, find_who_chan(acptr));
                goto done_with_who;
            }

        }
        else
            mask = NULL;
        
        /* THIS IS THE BIG LOOP! */
        
        for (acptr = client; acptr; acptr = acptr->next)
        {
            if ((IsInvisible(acptr)&&!w_isoper) || !IsPerson(acptr)
               || CHK_WHO_USER(acptr) || !who_match(acptr))
                continue;
             
            if (++num_whoreplies > MAX_WHOREPLIES && !IsAnOper(sptr))
            {
                send_num(sptr, ERR_MAXWHO, MAX_WHOREPLIES);
                break;
            }

	    do_who(sptr, acptr, find_who_chan(acptr));
        }
    }
done_with_who:
    send_num(sptr, RPL_ENDOFWHO, mask?mask:"*");
    return 0;
}


 /******************************************************************
  * m_whois segment                                                *
  ******************************************************************/

/*
 * send_whois(cptr, acptr)
 *
 *    Output whois info on acptr to recipient cptr.
 */
void send_whois(aClient *cptr, aClient *acptr)
{
	aChannel *chptr;
	aClient *srvptr;
	anUser *user;
	Member *mp;
	char *bp, *s, *name;
	int show;
	
	if (!IsPerson(acptr))
		return;
	
	name = acptr->name;
	srvptr = acptr->srvptr;
	
	send_num(cptr, RPL_WHOISUSER, name, acptr->username,
		acptr->u->host, acptr->info);
	
	if (IsDisguise(acptr) && (IsAnOper(cptr) || cptr==acptr))
		send_num(cptr, RPL_WHOISRHOST, name, !IsEspanol(cptr)?"is":"es",
			acptr->username, acptr->u->rhost);
		
	if (IsARegNick(acptr))
		send_num(cptr, RPL_WHOISREGNICK, name);

#ifdef USE_DVS
	if (IsDVS(acptr))
	{
		send_num(cptr, RPL_WHOISDVS, name);

		if(MyConnect(acptr) && IsAnOper(cptr) && (mp = acptr->chbuf))
		{
			char *cname, *key = NULL;

			s = bp = buf + mysprintf(buf, ":%s NOTICE %s :*** Chbuf entry for %s: ",
				me.name, cptr->name, name);
			for(; mp; mp = mp->nextc)
			{
				if (bp > buf+500-CHANNELLEN)
				{
					send_str(cptr, buf, bp-buf-1);
					bp = s;
				}

				cname = (char*)mp->nextu;
				if ((key = index(cname, ' ')))
				{
					*key = '\0';
					bp = strput(bp, (char*)mp->nextu);
					*key = ' ';
				}
				else
					bp = strput(bp, (char*)mp->nextu);

				*bp++ = ' ';
			}
			if(bp != s)
				send_str(cptr, buf, bp-buf-1);
		}
	}
	else if(!DVStagged(acptr) && IsAnOper(cptr))
		send_num(cptr, RPL_WHOISDVSEX, name);
#endif

	if ((mp = acptr->u->channel))
	{		
		s = bp = buf + mysprintf(buf, ":%s 319 %s %s :", me.name,
			cptr->name, name);
		for (; mp; mp = mp->nextc)
		{
			chptr = mp->chptr;
			if (!(ShowChannel(cptr, chptr)))
			{
				if (IsOper(cptr))
					*bp++ = '*';
				else
					continue;
			}
			if (bp > buf+500-CHANNELLEN)
			{
				if (!IsAnOper(cptr))
				{	/* Truncate list; let user know. */
					bp = strput(bp, ". . . ");
					break;
				}
				send_str(cptr, buf, bp-buf-1);
				bp = s;
			}
			if (mp->flags&CHFL_OVERLAP)
				*bp++ = (mp->flags&CHFL_CHANOP)?'@':((mp->flags&CHFL_HALFOP)?'%':'+');
			bp = strput(bp, chptr->chname);
			*bp++ = ' ';
		}
		if (bp!=s)
			send_str(cptr, buf, bp-buf-1);
	}
	
	send_num(cptr, RPL_WHOISSERVER, name, srvptr->name, srvptr->info);

	if (acptr->u->away)
		send_num(cptr, RPL_AWAY, name, acptr->u->away);
			
	if (acptr->u->hush)
		send_num(cptr, RPL_USERISHUSHED, name, acptr->u->hush->by, acptr->u->hush->s);

	if (IsAnOper(acptr))
	{
		if (IsAdmin(acptr))
			bp = "a Server Administrator";
		else
			bp = "an IRC Operator";
		if (IsSRA(acptr))
			s = "SRA";
		else if (IsCSop(acptr))
			s = "CSop";
		else if (IsSAdmin(acptr))
			s = "Services Administrator";
		else if (IsOper(acptr))
			s = "Global";
		else
			s = "Local";
		send_num(cptr, RPL_WHOISOPERATOR, name, bp, s);
	}
	
	if (MyConnect(acptr))
		send_num(cptr, RPL_WHOISIDLE, name,
			nowtime - acptr->ulast, acptr->firsttime);
}

/*
** m_whois
**	parv[1] = nickname masklist
*/
int	m_whois(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*nick, *tmp;
	int	found;
	int	total = 0;

	if (check_registered_user(sptr))
		return 0;

    	if (parc < 2)
	{
		send_num(sptr, ERR_NONICKNAMEGIVEN);
		return 0;
	}

	if (parc > 2)
	{
		if (hunt_server(cptr,sptr,":%s WHOIS %s :%s", 1,parc,parv) !=
		    HUNTED_ISME)
			return 0;
		parv[1] = parv[2];
	}

	tmp = parv[1];
	while (nick = strsplit(&tmp, ','))
	{
		if (!*nick)
			continue;

		found = 0;
		
		if (!index(nick, '?') && !index(nick, '*'))
		{
		    if ((acptr = find_client(nick)) && IsPerson(acptr))
		    {
		        found++;
		        total++;
		        send_whois(sptr, acptr);
		    }
		}
		else if (IsServer(cptr))
			continue;
		else
		{
		    collapse(nick);
		    for (acptr = client; acptr; acptr = acptr->next)
		    {
			if (!IsPerson(acptr))
			    continue;
			if (IsInvisible(acptr) && acptr!=sptr && !IsAnOper(sptr))
			    continue;
			if (qmatch(nick, acptr->name) != 0)
			    continue;

			send_whois(sptr, acptr);
			if (++found > 32 || ++total > 64)
			{
			    sendto_one(sptr, ":%s NOTICE %s :Exceeded maximum "
			        "number of /whois replies.  Truncating list.",
			        me.name, sptr->name, total);
			    break;
			}
		    }
		}
		if (!found)
			send_num(sptr, ERR_NOSUCHNICK, nick);
	}
	send_num(sptr, RPL_ENDOFWHOIS, parv[1]);

	return 0;
}


/*
** m_quit
**	parv[1] = comment
*/
int	m_quit(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	Member *mp, *mp2;
	char *comment;

	if (IsPerson(cptr))
	    for (mp = sptr->u->channel; mp; mp = mp2)
	    {
	        mp2 = mp->nextc;
	        if (can_member_send(mp)!=0)
	        {
	            sendto_serv_butone(NULL, ":%n %m %s :Quit",
	                sptr, &ms_part, mp->chptr->chname);
	            sendto_channel_butserv(mp->chptr, sptr, "PART %s :Quit",
	                mp->chptr->chname);
	            member_del(mp);
	        }
	    }
	
	if (!parv[1])
		comment = "Quit";
	else if (!IsPerson(cptr))
		comment = parv[1];
	else
	{
		if (strlen(parv[1]) > 290)
			parv[1][290] = '\0';
		mysprintf(buf, "Quit: %s", parv[1]);
		for (mp = sptr->u->channel; mp; mp = mp->nextc)
			if (mp->chptr->mode.mode&MODE_STRIPCOLOR)
				{ strip_colors(buf+6); break; }
		comment = buf;
	}
	return exit_client(cptr, sptr, sptr, comment);
}

/* do_kill
 * Process m_kill and m_fquit
 */
int	do_kill(aClient *cptr, aClient *sptr, char *users, char *path, int fquit)
{
	char	nickhost[NICKLEN+HOSTLEN+2];
	char	*reason = NULL;
	char	*nick = NULL;
	int	x, targets=0, gwarn=0;
	char	*bpos = buf;
	aClient	*acptr;
	Msg	*mptr = fquit ? &ms_fquit : &ms_kill;

	if(IsPerson(cptr))
	{
		reason = path;
		mysprintf(nickhost, "%s!%s", sptr->u->host, sptr->name);
		path = nickhost;
	}
	else
	{
		if (!(reason = index(path, ' ')))
		{
			reason = path;
			path = sptr->name;
		}
		else
			*reason++ = '\0';
	}

	if (strlen(path) > TOPICLEN)
		path[TOPICLEN-1] = '\0';

	*buf = *buf3 = '\0';

	while ((nick = strsplit(&users, ',')))
	{
		if(targets > 10)
		{
                        bpos[-1] = '\0';
                        sendto_serv_butone(cptr, ":%n %m %s :%s %s", sptr,
                                mptr, buf, path, reason);
                        bpos = buf;
                        *bpos = '\0';
                        targets = 0;
		}
                if (!(acptr = find_client(nick)))
                {
                        if (!(acptr = get_history(nick, (long)KILLCHASETIMELIMIT)))
                        {
                                send_num(sptr, ERR_NOSUCHNICK, nick);
                                continue;
                        }
                        sendto_one(sptr,":%n %m %n :KILL changed from %s to %s",
                                &me, &ms_notice, sptr, nick, acptr->name);
                }
                if (!IsPerson(acptr) || sptr == acptr)
                        continue; /* can't kill server or self */

                if (IsPerson(cptr) &&
                        ((MyConnect(acptr) && !OPCanLKill(sptr)) ||
                        (!MyConnect(acptr) && !OPCanGKill(sptr))))
                {
                        if (!gwarn)
                                send_num(sptr, ERR_NOPRIVILEGES);
                        gwarn = 1;
                        continue;
                }

		sendto_umode(UMODE_KILLS,
			     "*** Notice -- Received KILL message for %s!%s@%s from %s Path: %s (%s)",
			     acptr->name, acptr->username, acptr->u->host,
			     IsPerson(sptr)?sptr->name:sptr->s->shortn, path, reason);

                if (!fquit || !MyConnect(acptr))
                        bpos += mysprintf(bpos, "%s,", acptr->name);
                else if (!MyConnect(sptr)) /* global fquit's only */
                {
                        /* FORCE them to quit, but ONLY if it is OUR user. */
                        sendto_serv_butone(cptr,
                                ":%n %m %s :%s %s", sptr, &ms_fquit,
                                acptr->name, path, reason);
                        /* send fquit msg before user quits */
                }
                if (!fquit || MyConnect(acptr))
                {
                        if (!*buf3)
                        {
                                mysprintf(buf3, IsPerson(cptr)
                                        ?"Local kill by %s (%s)":"Killed (%s (%s))",
                                        sptr->name, reason);
                                buf3[sizeof(buf3)-1] = '\0';
                        }

                        if (!fquit) acptr->flags |= FLAGS_KILLED;

                        exit_client(NULL, acptr, &me, buf3);
                }

                targets++;
        }
        if (*buf)
        {
                bpos[-1] = '\0'; /* murder last comma */
                sendto_serv_butone(cptr, ":%n %m %s :%s %s", sptr,
                        mptr, buf, path, reason);
        }
        return 0;
}

/*
** m_fquit
**      parv[1] = kill vitim(s) - comma separated list
**      parv[2] = kill path/reason
**
**      Similar to m_kill, but this is less likely to cause desyncs.
**      Basically, this is a forced quit.  Ghosts won't be generated as often
**      when using this.  When an oper uses /kill, this will be called instead.
**
**      written by binary
*/
int     m_fquit(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
        if (check_privs(sptr, 0, 0) || check_params(sptr, msgptr->cmd, parc, 2))
                return 0;

        return do_kill(cptr, sptr, parv[1], parv[2], 1);
}

/*
** m_kill
**      parv[1] = kill victim(s) - comma separated list
**      parv[2] = kill path
**
** didn't like old code so ripped it off old kill cmd
*/
int     m_kill(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
        int fquit = 0;

        if (check_privs(sptr, 0, 0) || check_params(sptr, msgptr->cmd, parc, 2))
                return 0;

        if (IsPerson(cptr))
        {
                if (*parv[1] == '/')
                        parv[1]++;
                else if (msgptr != &ms_rkill)
                        fquit = 1;
        }

        return do_kill(cptr, sptr, parv[1], parv[2], fquit);
}

/* m_mkill() - kills all users with the host provided or with the same host
 *	as a nickname typed in.  Now allows masks; finds the first match
 *      for a hostmask and kills all clients with that specific host.
 */
int m_mkill(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	char *lookhost, *bpos;
	aClient	*acptr, *next = client;
	int i = 0;
	
	if (check_registered_user(sptr) || check_privs(sptr, 0, 0))
		return 0;
	if (parc < 3 || *parv[2] == '\0')
		return check_params(sptr, "MKILL", -1, 0);
	
	if (!index(parv[1], '.'))
	{
		if ((acptr = find_client(parv[1])))
			lookhost = acptr->u->rhost;
		else
		{
			send_num(sptr, ERR_NOSUCHNICK, parv[1]);
			return 0;
		}
	}
	else
	{
		lookhost = parv[1];
		if (index(lookhost, '?') || index(lookhost, '*'))
		{
			for (acptr = client; acptr; acptr = acptr->next)
				if (IsPerson(acptr) &&
				    !match(lookhost, acptr->u->rhost))
				{
					lookhost = acptr->u->rhost;
					break;
				}
			if (!acptr)
				return 0;
		}
	}
	
	parv[1] = bpos = buf2;
	
	while ((acptr = next))
	{
		next = acptr->next;
		if (!IsPerson(acptr) || (!MyConnect(acptr) && !IsOper(sptr))
				|| IsAnOper(acptr))
			continue;
		if (!mycmp(acptr->u->rhost, lookhost))
		{
			bpos += mysprintf(bpos, "%s,", acptr->name);
			if (bpos >= buf2+100 || ++i >= 10)
			{
				bpos[-1] = '\0';
				m_fquit(cptr, sptr, parc, parv);
				bpos = buf2;
				i = 0;
			}
		}
	}
	
	if (bpos != buf2)
	{
		bpos[-1] = '\0';
		m_fquit(cptr, sptr, parc, parv);
	}
	
	return 0;
}
		
/*
 * m_wkill
 * "whokill" kills users based on same pattern matching as m_who
 * Added by dave 12/30/02
 * parv[1] = who pattern
 * parv[2] = reason
 */
#define CHECK_WKILL(x) (srvptr && (x)->srvptr != srvptr)	
int	m_wkill(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	char		target[BUFSIZE];
	char 		*mask = parv[1], *bpos, *s, *tpos = target;
	aClient 	*acptr, *srvptr = NULL;
	Member 		*mp;
	int 		i=0, t=0;
	aChannel	*chptr = NULL;

	if(check_registered(cptr) || check_privs(sptr, UMODE_SADMIN, 0))
		return 0;

	if(parc < 3 || *parv[2] == '\0')
		return 0;

	w_isoper = 1;
	parv[1] = bpos = buf2;

	if(mask&&IsChannelName(mask))
	{
		if (!(chptr = hash_find_channel(mask)) || chptr->mode.mode&MODE_NOSAJOIN)
			return 0;
		tpos += mysprintf(tpos, "%s", chptr->chname);
		mp = chptr->members;
		while(mp)
		{
			acptr = mp->cptr;
			if(CHECK_WKILL(acptr) || !IsPerson(acptr) ||
			   (!MyConnect(acptr) && !IsOper(sptr)) ||
			   IsAnOper(acptr))
				continue;

			/* This must come before m_fquit is processed! */
			mp = mp->nextu;

			bpos += mysprintf(bpos, "%s,", acptr->name);
			if (bpos >= buf2+100 || ++i >= 10)
			{
				bpos[-1] = '\0';
				m_fquit(cptr, sptr, parc, parv);
				bpos = buf2;
				i = 0;
			}
			t++;
		}
	}
	else
	{
		w_nick = w_user = w_host = w_real = NULL;
		if(mask && (*mask != '0' || mask[1]))
		{
			collapse(mask);
			if ((w_real = index(mask, '/')))
				*w_real++ = '\0';
			if ((s = index(mask, ':')))
			{
				*s++ = '\0';
				if(!(srvptr = hash_find_serv(s)))
					return 0;
			}
			if((w_host = index(mask, '@')))
			{
				*w_host++ = '\0';
				if ((w_user = index(mask, '!')))
				{
					*w_user++ = '\0';
					w_nick = mask;
				}
				else
					w_user = mask;
			}
			else if (index(mask, '.'))
				w_host = mask;
			else
				w_nick = mask;
			CHK_MASK(w_nick);
			CHK_MASK(w_user);
			CHK_MASK(w_host);
			CHK_MASK(w_real);
			tpos += mysprintf(tpos, "%s!%s@%s:%s/%s", w_nick?w_nick:"*",
				w_user?w_user:"*", w_host?w_host:"*",
				srvptr?srvptr->name:"*", w_real?w_real:"*");
			if (w_nick && !index(w_nick, '?') && !index(w_nick, '*'))
			{
				if ((acptr = find_person(w_nick)) && !CHECK_WKILL(acptr) && who_match(acptr))
				{
					parv[1] = acptr->name;
					m_fquit(cptr, sptr, parc, parv);
				}
				else
					send_num(sptr, ERR_NOSUCHNICK, w_nick);
				return 0;
			}
		}
		else
			return 0;

		for (acptr = client; acptr; acptr = acptr->next)
		{
			if(!IsPerson(acptr) || CHECK_WKILL(acptr) ||
			   !who_match(acptr) || IsAnOper(acptr) ||
			   (!MyConnect(acptr) && !IsOper(sptr)))
				continue;

			bpos += mysprintf(bpos, "%s,", acptr->name);
			if(bpos >= buf2+100 || ++i >= 10)
			{
				bpos[-1] = '\0';
				m_fquit(cptr, sptr, parc, parv);
				bpos = buf2;
				i = 0;
			}
			t++;
		}
	}

	if(bpos != buf2)
	{
		bpos[-1] = '\0';
		m_fquit(cptr, sptr, parc, parv);
	}

	*tpos = '\0';

	if(t)
		sendout_globops("%s initiated wkill for %s (\2%d\2 users killed)", sptr->name, target, t);

	return 0;
}
#undef CHECK_WKILL

/***********************************************************************
 * m_away() - Added 14 Dec 1988 by jto. 
 *            Not currently really working, I don't like this
 *            call at all...
 *
 *            ...trying to make it work. I don't like it either,
 *	      but perhaps it's worth the load it causes to net.
 *	      This requires flooding of the whole net like NICK,
 *	      USER, MODE, etc messages...  --msa
 ***********************************************************************/

/*
** m_away
**	parv[1] = away message
*/
int	m_away(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (check_registered_user(sptr))
		return 0;

	if (!parv[1])
	{
		if (MyConnect(sptr))
			send_num(sptr, RPL_UNAWAY);
		if (sptr->u->away)
		{
			da_free(&away_mast, sptr->u->away);
			sptr->u->away = NULL;
			sendto_serv_butone(cptr, ":%n %m", sptr, &ms_away);
		}

		return 0;
	}

	if (strlen(parv[1]) > AWAYLEN)
		parv[1][AWAYLEN] = '\0';
	sendto_serv_butone(cptr, ":%n %m :%s", sptr, &ms_away, parv[1]);

	if (!sptr->u->away)
		sptr->u->away = da_alloc(&away_mast);

	strcpy(sptr->u->away, parv[1]);
	if (MyConnect(sptr))
		send_num(sptr, RPL_NOWAWAY);
	return 0;
}

/*
** m_ping
**	parv[1] = origin
**	parv[2] = destination
*/
int	m_ping(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*origin, *destination;

        if (check_registered(sptr))
                return 0;
 
 	if (!parv[1])
	{
		send_num(sptr, ERR_NOORIGIN, sptr->name);
		return 0;
	}
	origin = parv[1];
	destination = parv[2];

	acptr = find_client(origin);
	if (!acptr)
		acptr = hash_find_serv(origin);
	if (acptr && acptr != sptr)
		origin = cptr->name;
	if (!BadPtr(destination))
	{
		if (!(acptr = hash_find_serv(destination)))
		{
			send_num(sptr, ERR_NOSUCHSERVER, destination);
			return 0;
		}
		else if (!IsMe(acptr))
		{
			sendto_one(acptr, ":%n %m %s :%s", sptr, &ms_ping,
				   origin, acptr->name);
			return 0;
		}
	}
	sendto_one(sptr, ":%n %m %s :%s", &me, &ms_pong,
		   (destination) ? destination : me.name, origin);
	return 0;
    }

/*
** m_pong
**	parv[1] = origin
**	parv[2] = destination
*/
int	m_pong(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*origin, *destination;

	if (parc < 2 || *parv[1] == '\0')
	{
		send_num(sptr, ERR_NOORIGIN);
		return 0;
	}

	origin = parv[1];
	destination = parv[2];
	cptr->flags &= ~FLAGS_PINGSENT;
	sptr->flags &= ~FLAGS_PINGSENT;

	if (!BadPtr(destination) && mycmp(destination, me.name) != 0)
	    {
		if ((acptr = find_client(destination)))
		{
			if (!IsServer(cptr) && !IsServer(acptr))
			{
				send_num(sptr, ERR_NOSUCHSERVER, destination);
				return 0;
			}
			else
			sendto_one(acptr,":%n %m %s %s",
				   sptr, &ms_ping, origin, destination);
		}
		else
		    {
			send_num(sptr, ERR_NOSUCHSERVER, destination);
			return 0;
		    }
	    }
#ifdef	DEBUGMODE
	else
		Debug((DEBUG_NOTICE, "PONG: %s %s", origin,
		      destination ? destination : "*"));
#endif
	return 0;
    }

/*
** m_oper
**	parv[1] = oper name
**	parv[2] = oper password
*/
int	m_oper(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aConfItem *aconf;
	char	*name, *password, *encr;
#ifdef CRYPT_OPER_PASSWORD
	char	salt[3];
	extern	char *crypt();
#endif

	if (check_registered_user(sptr))
		return 0;

	name = parc > 1 ? parv[1] : NULL;
	password = parc > 2 ? parv[2] : NULL;

	if (!IsServer(cptr) && (BadPtr(name) || BadPtr(password)))
		return check_params(sptr, "OPER", -1, 0);
	
	if ((IsServer(cptr) || IsMe(cptr)) && !IsOper(sptr))
	{
		sptr->umodes |= UMODE_OPER;
		sendto_serv_butone(cptr, ":%n %m %s :+o", sptr, &ms_mode, sptr->name);
		if (IsMe(cptr))
			send_num(sptr, RPL_YOUREOPER);
		return 0;
	}
	else if (IsOper(sptr))
	{
		if (MyConnect(sptr))
			send_num(sptr, RPL_YOUREOPER);
		return 0;
	}

	if (!(aconf = find_conf_exact(name, sptr->username, sptr->sockhost, CONF_OPS)) &&
	    !(aconf = find_conf_exact(name, sptr->username, inetntoa((char *)&cptr->ip), CONF_OPS)))
	{
		send_num(sptr, ERR_NOOPERHOST);
                sendto_realops("Failed OPER attempt by %s (%s@%s)",
                	sptr->name, sptr->username, sptr->sockhost);
		sptr->since += 10;
		return 0;
	}
#ifdef CRYPT_OPER_PASSWORD
        salt[0] = '\0';
        if (password && aconf->passwd && aconf->passwd[0] && aconf->passwd[1])
	{
        	salt[0] = aconf->passwd[0];
		salt[1] = aconf->passwd[1];
		salt[2] = '\0';
		encr = crypt(password, salt);
	}
	else
		encr = "";
#else
	encr = password;
#endif

	if (!aconf->passwd)
	{
		sendto_one(sptr, ":%s NOTICE %s :ERROR! NULL PASSWD!!!",
			me.name, sptr->name);
		return 0;
	}

	if ((aconf->status & CONF_OPS) && !(aconf->status & CONF_ILLEGAL) &&
	    StrEq(encr, aconf->passwd) && !attach_conf(sptr, aconf))
	    {
		int old = (sptr->umodes & ALL_UMODES);
		char *s;

		if (!(s = index(aconf->host, '@')))
		{
			sendto_one(sptr, ":%s NOTICE %s :INTERNAL ERROR!!!!",
				me.name, sptr->name);
			return 0;
		}
		*s++ = '\0';
		sptr->oflag = aconf->port;
		if(sptr->oflag & OFLAG_GLOBAL)
		{
			SetOper(sptr);
			remove_dvs(sptr,0);
		}
		else if (sptr->oflag & OFLAG_LOCAL)
			SetLocOp(sptr);
		*--s =  '@';
		sendto_ops("%s (%s!%s@%s) is now operator (%c)", parv[1],
			   sptr->name, sptr->username,
			   sptr->u->host, IsOper(sptr) ? 'O' : (IsLocOp(sptr) ? 'o' : 'm'));
		if (IsAnOper(sptr))
			sptr->umodes |= (UMODE_SERVNOTICE|UMODE_WALLOP|UMODE_FAILOP|
					UMODE_CHATOP|UMODE_KILLS);
		if (sptr->oflag & OFLAG_MODES)
			sptr->umodes |= (UMODE_HUSHNOTICE|UMODE_FLOOD);
                if (!IsAdmin(sptr) && OPIsAdmin(sptr))
                        sptr->umodes |= UMODE_ADMIN;
                if (!IsSAdmin(sptr) && OPIsSAdmin(sptr))
                        sptr->umodes |= UMODE_SADMIN;
		send_umode_out(cptr, sptr, old);
 		send_num(sptr, RPL_YOUREOPER);
#if !defined(CRYPT_OPER_PASSWORD) && (defined(FNAME_OPERLOG) ||\
    (defined(USE_SYSLOG) && defined(SYSLOG_OPER)))
		encr = "";
#endif
#if defined(USE_SYSLOG) && defined(SYSLOG_OPER)
		syslog(LOG_INFO, "OPER (%s) (%s) by (%s!%s@%s)",
			name, encr,
			sptr->name, sptr->username, sptr->sockhost);
#endif
#ifdef FNAME_OPERLOG
	      {
                int     logfile;

                /*
                 * This conditional makes the logfile active only after
                 * it's been created - thus logging can be turned off by
                 * removing the file.
                 *
                 * stop NFS hangs...most systems should be able to open a
                 * file in 3 seconds. -avalon (curtesy of wumpus)
                 */
                if (IsPerson(sptr) &&
                    (logfile = open(FNAME_OPERLOG, O_WRONLY|O_APPEND)) != -1)
		{
                        mysprintf(buf, "%s OPER (%s) (%s) by (%s!%s@%s)\n",
				      myctime(nowtime), name, encr,
				      sptr->name, sptr->username,
				      sptr->sockhost);
		  write(logfile, buf, strlen(buf));
		  close(logfile);
		}
                /* Modification by pjg */
	      }
#endif
	}
	else
	{
		detach_conf(sptr, aconf);
		send_num(sptr, ERR_PASSWDMISMATCH);
#ifdef  FAILOPER_WARN
		sendto_one(sptr,":%s NOTICE :Your attempt has been logged.",me.name);
#endif
                sendout_globops("Failed OPER attempt by %s (%s@%s) using UID %s [Invalid Pass]",
                   sptr->name, sptr->username, sptr->sockhost, name);
		sptr->since += 7;
#ifdef FNAME_OPERLOG
              {
                int     logfile;

                /*
                 * This conditional makes the logfile active only after
                 * it's been created - thus logging can be turned off by
                 * removing the file.
                 *
                 * stop NFS hangs...most systems should be able to open a
                 * file in 3 seconds. -avalon (curtesy of wumpus)
                 */
                if (IsPerson(sptr) &&
                    (logfile = open(FNAME_OPERLOG, O_WRONLY|O_APPEND)) != -1)
                {
                        mysprintf(buf, "%s FAILED OPER (%s) (%s) by (%s!%s@%s)\n PASSWORD %s",
                                      myctime(nowtime), name, encr,
                                      sptr->name, sptr->username,
                                      sptr->sockhost, password);
                  write(logfile, buf, strlen(buf));
                  close(logfile);
                }
              }
#endif
	}
	return 0;
}

/***************************************************************************
 * m_pass() - Added Sat, 4 March 1989
 ***************************************************************************/

/*
** m_pass
**	parv[1] = password
*/
int	m_pass(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
	char *password = parc > 1 ? parv[1] : NULL;

	if (BadPtr(password))
		return check_params(sptr, "PASS", -1, 0);
	if (!MyConnect(sptr) || (!IsUnknown(cptr) && !IsHandshake(cptr)))
	    {
		send_num(cptr, ERR_ALREADYREGISTRED);
		return 0;
	    }
	strncpyzt(cptr->passwd, password, sizeof(cptr->passwd));
	return 0;
    }

/*
 * m_userhost added by Darren Reed 13/8/91 to aid clients and reduce
 * the need for complicated requests like WHOIS. It returns user/host
 * information only (no spurious AWAY labels or channels).
 */
int	m_userhost(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*p = NULL;
	aClient	*acptr;
	char	*s;
	char    *curpos, *head;
	int	i = 0;

	if (check_registered(sptr))
		return 0;

	if (parc < 2)
		return check_params(sptr, "USERHOST", -1, 0);

	/*
	 * use curpos to keep track of where we are in the output buffer,
	 * and use resid to keep track of the remaining space in the
	 * buffer
	 */
	head = buf+mysprintf(buf, ":%s 302 %s :", me.name, sptr->name);
	curpos = head;

	/*
	 * for each user found, print an entry if it fits.
	 */
	for (p = parv[1]; (s = strsplit(&p, ' '));)
	{
		if ((acptr = find_person(s)))
		{
			if (curpos > buf+BUFSIZE-NICKLEN-USERLEN-HOSTLEN-20)
			{
				send_str(sptr, buf, curpos-buf-1);
				curpos = head;
				i++;
			}
			curpos += mysprintf(curpos, "%s%s=%c%s@%s ",
				acptr->name, IsAnOper(acptr) ? "*" : "",
				acptr->u->away ? '-' : '+', acptr->username,
				sptr==acptr||IsAnOper(sptr)?acptr->u->rhost:
				acptr->u->host);
		}
	}

	if (!i || curpos != head)
	{
		if (curpos != head)
			curpos--;
		send_str(sptr, buf, curpos-buf);
	}
	return 0;
}


/*
 * m_ison added by Darren Reed 13/8/91 to act as an efficent user indicator
 * with respect to cpu/bandwidth used. Implemented for NOTIFY feature in
 * clients. Designed to reduce number of whois requests. Can process
 * nicknames in batches as long as the maximum buffer length.
 *
 * format:
 * ISON :nicklist
 *
 * cleaned by binary
 */
int     m_ison(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
{
	aClient *acptr;
	char *bpos;
	char *p, *nick;
	char *user, *host;
	
	if (check_registered_user(sptr))
		return 0;
	if (check_params(sptr, "ISON", parc, 1))
		return 0;
	
	bpos = buf;
	*bpos = '\0';
	
	for (p = parv[1]; (nick = strsplit(&p, ' '));)
	{
		if (!*nick)
			continue;
		if ((user = index(nick, '!')))
			*user++ = '\0';
		if ((acptr = find_client(nick)) && IsPerson(acptr))
		{
			if (user && (host = index(user, '@')))
			{
				*host++ = '\0';
				if (  match(user, acptr->username)
				   || match(host, acptr->u->host)
				   )	continue;
			}
			bpos += mysprintf(bpos, "%s ", acptr->name);
			if (bpos-buf > BUFSIZE-NICKLEN-10)
				break;
		}
	}
	
	if (bpos != buf)
		bpos[-1] = '\0'; /* Murder the final space... */
		
	send_num(sptr, RPL_ISON, buf);
	
	return 0;
}

/*
 * m_umode() added 10/15/1991 by Darren Reed -- fixed by dave 1/11/2004
 * parv[1] - username (maybe)
 * parv[2] - modes to change
 */
int	m_umode(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	int	flag;
	int	*s;
	char	**p, *m;
	int	setflags;
	char	sign = '+';

	if (check_registered_user(sptr))
		return 0;
	
	if (parc < 3)
	{
		if (*parv[1] != '+' && *parv[1] != '-')
		{
			m = buf;
			*m++ = '+';
			for (s = user_modes; (flag = *s); s += 2)
				if ((sptr->umodes & flag))
					*m++ = (char)(*(s+1));
			*m = '\0';
			send_num(sptr, RPL_UMODEIS, buf);
			return 0;
		}
		parv[2] = parv[1]; /* allow /mode +modes for users */
	}

	/* find flags already set for user */
	setflags = 0;
	for (s = user_modes; (flag = *s); s += 2)
		if ((sptr->umodes & flag))
			setflags |= flag;

	/* parse mode change string(s) */
	for (p = &parv[2]; p && *p; p++)
	    for (m = *p; *m; m++)
		switch(*m)
		{
			case '+':
			case '-':
				sign = *m;
				break;
			case ' ':
			case 'C':
			case 'S':
			case 'r':
				break;
			case 'x':
				if(sign == '+')
					sptr->umodes |= UMODE_DISGUISE;
				else
					sptr->umodes &= ~UMODE_DISGUISE;
				break;
	  		default :
				for (s = user_modes; (flag = *s); s += 2)
					if (*m == (char)(*(s+1)))
					{
						if (sign == '+')
							sptr->umodes |= flag;
						else
							sptr->umodes &= ~flag;
						break;
					}
				if (flag == 0 && MyConnect(sptr))
					send_num(sptr, ERR_UMODEUNKNOWNFLAG, *m);
				break;
		}
	
	if (!IsServer(cptr))
	{	
		sptr->umodes &= (setflags&UMODE_ANYOP)|(~UMODE_ANYOP);
		if(!sptr->oflag)
			sptr->umodes &= ~(UMODE_CLIENT|UMODE_ADMIN|UMODE_FAILOP|
				UMODE_SADMIN|UMODE_FLOOD|UMODE_HUSHNOTICE|
				UMODE_CHATOP|UMODE_CANFLOOD);
		else
		{
			if (IsAdmin(sptr) && !OPIsAdmin(sptr)) ClearAdmin(sptr);
			if (IsSAdmin(sptr) && !OPIsSAdmin(sptr)) ClearSAdmin(sptr);
			if (IsClientF(sptr) && !OPCanUModeC(sptr)) ClearClientF(sptr);
			if (IsFloodF(sptr) && !OPCanUModeF(sptr)) ClearFloodF(sptr);
			if (IsExemptFromFlood(sptr) && !OPCanFlood(sptr)) ClearCanFlood(sptr);
			if (SendHushNotice(sptr) && !(sptr->oflag & OFLAG_MODES)) ClearHushNotice(sptr);
		}

		if ((setflags & UMODE_ANYOP) && !IsAnOper(sptr))
		{
			det_confs_butmask(sptr, CONF_CLIENT & ~CONF_OPS);
			sptr->oflag = 0;
		}
	}
		
	/* Disguise the user */
	if (!(setflags & UMODE_DISGUISE) && IsDisguise(sptr))
		sptr->u->host = sptr->u->dhost;
	else if ((setflags & UMODE_DISGUISE) && !IsDisguise(sptr))
	{
		if (IsForceDis(sptr) && cptr == sptr)
			sptr->umodes |= UMODE_DISGUISE;
		else
			sptr->u->host = sptr->u->rhost;
	}

	if(IsOper(sptr) || IsRegNick(sptr))
		remove_dvs(sptr,0);

	send_umode_out(cptr, sptr, setflags);

	return 0;
}




/*
 * m_svsmode() added by taz
 * parv[1] - username to change mode for
 * parv[2] - modes to change
 * parv[3] - Service Stamp (if mode == d)
 */
int	m_svsmode(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	int	flag;
	int	*s;
	char	**p, *m;
	aClient	*acptr;
	int	setflags;
	char	sign = '+';

	if (!IsULine(sptr))
		return 0;

	if (parc < 3)
		return 0;

	if (!(acptr = find_person(parv[1])))
		return 0;

	setflags = 0;
	for (s = user_modes; (flag = *s); s += 2)
		if (acptr->umodes & flag)
			setflags |= flag;
	/*
	 * parse mode change string(s)
	 */
	for (p = &parv[2]; p && *p; p++ )
	    for (m = *p; *m; m++)
		switch(*m)
		{
		case '+' :
		case '-' :
			sign = *m;
			break;
		case 'l' :
			if(parv[3] && isdigit(*parv[3]))
				Count.maxglobalu = atoi(parv[3]);
			break;
		case 'd' :
			if(parv[3] && isdigit(*parv[3]))
				acptr->u->sstamp = atol(parv[3]);
			break;
			
		case 'D' :
			if (sign=='+')
				acptr->umodes |= UMODE_FORCEDIS;
			else
				acptr->umodes &= ~UMODE_FORCEDIS;
		case 'X' :
			if (sign=='+')
			{
				acptr->umodes |= UMODE_DISGUISE;
				acptr->u->host = acptr->u->dhost;
			}
			else
			{
				acptr->umodes &= ~UMODE_DISGUISE;
				acptr->u->host = acptr->u->rhost;
			}
			if (MyConnect(acptr))
				sendto_one(acptr, ":%p %m %s %cd", acptr, 
					&ms_mode, acptr->name, sign);
			break;
#ifdef USE_DVS
		case 'Z' :
			if(IsOper(acptr) || IsRegNick(acptr) || IsULine(acptr))
				break;
			if (sign=='+')
			{
				SetDVS(acptr);
				SetDVStag(acptr);
				if(MyClient(acptr))
				{
					sendto_one(acptr, ":%n NOTICE %n :*** DVS -- Services have placed you under DVS restrictions. You must register and/or identify to nickserv before you continue using this network. For more information, type \2/dvs\2", &me, acptr);
					acptr->dvstime = ((ServicesOnline||DVSLock)?nowtime:0);
				}
			}
			else
				remove_dvs(acptr,0);
			break;
#endif
		default :
			for (s = user_modes; (flag = *s); s += 2)
				if (*m == (char)(*(s+1)))
			    {
				if (sign=='+')
					acptr->umodes |= flag;
				else
					acptr->umodes &= ~flag;	
				break;
			    }
			break;
		}
	if(parc > 3)
		sendto_serv_butone(cptr, ":%n %m %s %s %s",
			sptr, &ms_svsmode, parv[1], parv[2], parv[3]);
	else
		sendto_serv_butone(cptr, ":%n %m %s %s", sptr, &ms_svsmode,
			parv[1], parv[2]);

	if(IsOper(acptr) || IsRegNick(acptr))
		remove_dvs(acptr,0);

	return 0;
}
	
/*
 * send the MODE string for user (user) to connection cptr
 * -avalon
 */
void	send_umode(cptr, sptr, old, sendmask, umode_buf)
aClient *cptr, *sptr;
int	old, sendmask;
char	*umode_buf;
{
	int	*s, flag;
	char	*m;
	char	sign = '\0';

	/*
	 * build a string in umode_buf to represent the change in the user's
	 * mode between the new (sptr->flag) and 'old'.
	 */
	m = umode_buf;
	*m = '\0';
	for (s = user_modes; (flag = *s); s += 2)
	    {
		if (MyConnect(sptr) && !(flag & sendmask))
			continue;
		if ((flag & old) && !(sptr->umodes & flag))
		    {
			if (sign=='-')
				*m++ = *(s+1);
			else
			    {
				*m++ = sign = '-';
				*m++ = *(s+1);
			    }
		    }
		else if (!(flag & old) && (sptr->umodes & flag))
		    {
			if (sign=='+')
				*m++ = *(s+1);
			else
			    {
				*m++ = sign = '+';
				*m++ = *(s+1);
			    }
		    }
	    }
	*m = '\0';
	if (*umode_buf && cptr)
	{
		if (IsServHash(cptr))
			sendto_one(cptr, ":%n "TOK_MODE" %s",
				sptr, umode_buf);
		else
			sendto_one(cptr, ":%n %m %s :%s", sptr, &ms_mode, 
				sptr->name, umode_buf);
	}
}

/* 
 * Added 11-27-02 by dave (dave@darkfire.net)
 * This is not a sending function, only a formating function
 */
char	*get_umode_string(aClient *sptr, int old, int sendmask, char *umode_buf)
{
	int 	*s, flag;
	char 	*m;
	char	sign = '\0';

	m = umode_buf;
	*m = '\0';
	for (s = user_modes; (flag = *s); s += 2)
	{
		if(MyConnect(sptr) && !(flag & sendmask))
			continue;
		if(!(flag & old) && (sptr->umodes & flag))
			*m++ = *(s+1);
	}
	*m = '\0';
	return umode_buf;
}

/*
 * added Sat Jul 25 07:30:42 EST 1992
 */
void	send_umode_out(cptr, sptr, old)
aClient *cptr, *sptr;
int	old;
{
	send_umode(NULL, sptr, old, SEND_UMODES, buf);

	if (*buf)
	{	/* ServHash ==> lenient m_umode (:%s MODE +modes) */
		sendto_serv_butone_wproto(cptr, PROLEV_SERVHASH,
			":%n "TOK_MODE" %s", sptr, buf);
		sendto_serv_butone_nproto(cptr, PROLEV_SERVHASH,
			":%n %m %s :%s", sptr, &ms_mode, sptr->name, buf);
	}

	if (cptr && MyClient(cptr))
		send_umode(cptr, sptr, old, ALL_UMODES, buf);
}

/*
 * added by taz
 */
void	send_svsmode_out(cptr, sptr, bsptr, old)
aClient *cptr, *sptr, *bsptr;
int	old;
{
	send_umode(NULL, sptr, old, SEND_UMODES, buf);

	sendto_serv_butone(cptr, ":%n %m %s :%s", bsptr, &ms_svsmode, sptr->name, buf);

}

/***********************************************************************
 * m_silence() - Added 19 May 1994 by Run. 
 *
 ***********************************************************************/

/*
 * is_silenced : Does the actual check wether sptr is allowed
 *               to send a message to acptr.
 *               Both must be registered persons.
 * If sptr is silenced by acptr, his message should not be propagated,
 * but more over, if this is detected on a server not local to sptr
 * the SILENCE mask is sent upstream.
 */
static int is_silenced(sptr, acptr)
aClient *sptr;
aClient *acptr;
{ Link *lp;
  static char sender[HOSTLEN+NICKLEN+USERLEN+5];

  if (!(lp = acptr->u->silence))
      return 0;
  mysprintf(sender,"%s!%s@%s",sptr->name,sptr->username,sptr->u->host);
  for (; lp; lp = lp->next)
  { if (!match(lp->value.cp, sender))
    { if (!MyConnect(sptr))
      { sendto_one(sptr->from, ":%s SILENCE %s :%s",acptr->name,
            sptr->name, lp->value.cp);
        lp->flags=1; }
      return 1; } }
  return 0;
}

int del_silence(sptr, mask)
aClient *sptr;
char *mask;
{ Link **lp;
  Link *tmp;

  for (lp = &(sptr->u->silence); *lp; lp = &((*lp)->next))
    if (mycmp(mask, (*lp)->value.cp)==0)
    { tmp = *lp;
      *lp = tmp->next;
      MyFree(tmp->value.cp);
      free_link(tmp);
      return 0; }
  return -1;
}

static int add_silence(sptr, mask)
aClient *sptr;
char *mask;
{ Link *lp;
  int cnt = 0, len = 0;

  for (lp = sptr->u->silence; lp; lp = lp->next)
  { len += strlen(lp->value.cp);
    if (MyConnect(sptr))
      if ((len > MAXSILELENGTH) || (++cnt >= MAXSILES))
      { send_num(sptr, ERR_SILELISTFULL, mask);
	return -1; }
      else
      { if (!match(lp->value.cp, mask))
	  return -1; }
    else if (!mycmp(lp->value.cp, mask))
      return -1;
  }
  lp = make_link();
  bzero((char *)lp, sizeof(Link));
  lp->next = sptr->u->silence;
  lp->value.cp = (char *)MyMalloc(strlen(mask)+1);
  strcpy(lp->value.cp, mask);
  sptr->u->silence = lp;
  return 0;
}

/*
** m_silence
** From local client:
**	parv[1] = mask (NULL sends the list)
** From remote client:
**	parv[1] = nick that must be silenced
**      parv[2] = mask
*/
int m_silence(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
  Link *lp;
  aClient *acptr;
  char c, *cp;

  if (check_registered_user(sptr)) return 0;

  if (IsPerson(cptr))
  {
    acptr = sptr;
    if (parc < 2 || *parv[1]=='\0')
    { if (!IsPerson(acptr)) return 0;
      for (lp = acptr->u->silence; lp; lp = lp->next)
	send_num(sptr, RPL_SILELIST, acptr->name, lp->value.cp);
      send_num(sptr, RPL_ENDOFSILELIST);
      return 0; }
    cp = parv[1];
    c = *cp;
    if (c=='-' || c=='+') cp++;
    else if (!(index(cp, '@') || index(cp, '.') ||
	index(cp, '!') || index(cp, '*')))
    { send_num(sptr, ERR_NOSUCHNICK, parv[1]);
      return -1; }
    else c = '+';
    cp = pretty_mask(cp);
    if ((c=='-' && !del_silence(sptr,cp)) ||
        (c!='-' && !add_silence(sptr,cp)))
    { sendto_one(sptr, ":%p %m %c%s", sptr, &ms_silence, c, cp);
      if (c=='-')
	sendto_serv_butone(NULL, ":%n %m * -%s", sptr, &ms_silence, cp);
    }
  }
  else if (parc < 3 || *parv[2]=='\0')
  {
    return check_params(sptr, "SILENCE", -1, 0);
  }
  else if ((c = *parv[2])=='-' || (acptr = find_person(parv[1])))
  {
    if (c=='-')
    { if (!del_silence(sptr,parv[2]+1))
	sendto_serv_butone(cptr, ":%n %m %s :%s",
	    sptr, &ms_silence, parv[1], parv[2]); }
    else
    { add_silence(sptr,parv[2]);
      if (!MyClient(acptr))
        sendto_one(acptr, ":%s %m %s :%s",
            sptr->name, &ms_silence, parv[1], parv[2]); }
  }
  else
  {
    send_num(sptr, ERR_NOSUCHNICK, parv[1]);
    return -1;
  }
  return 0;
}

/* Hush */
int m_hush(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aClient *acptr;
	time_t	hush_time = 0;
		
	if (check_registered(sptr))
		return 0;
	if (parc < 4 || !parv[3] || !*parv[3])
		return check_params(sptr, "HUSH", -1, 0);
	if (!(acptr = find_person(parv[1])))
	{
		send_num(sptr, ERR_NOSUCHNICK, parv[1]);
		return 0;
	}
	if (MyClient(sptr) && ((!MyConnect(acptr) && !OPCanGKill(sptr)) ||
	    (MyConnect(acptr) && !OPCanLKill(sptr)) || IsULine(acptr)))
	{
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	}

	hush_time = atoi(parv[2]);

	if (acptr->u->hush)
		MyFree(acptr->u->hush);
	acptr->u->hush = (Hush *)MyMalloc(sizeof(Hush) + strlen(parv[3]) + 3);
	mysprintf(acptr->u->hush->by, "%s", IsServer(sptr)?sptr->s->sndot:sptr->name);
	mysprintf(acptr->u->hush->s, "%s", parv[3]);
	acptr->u->hush->end = (hush_time == 0) ? 0 : nowtime + hush_time;

	sendto_serv_butone(cptr, ":%n %m %n %s :%s", sptr, &ms_hush, acptr,
		parv[2], parv[3]);

	if (MyConnect(acptr))
	{
		send_num(acptr, ERR_YOUREHUSHED, acptr->u->hush->s);
		sendto_one(acptr, ":%n %m %n :*** You have been hushed by %s (%s)",
			&me, &ms_notice, sptr, acptr->u->hush->by, acptr->u->hush->s);
		if (hush_time)
			sendto_one(acptr, ":%n %m %n :*** Your hush will expire in %d seconds.",
				&me, &ms_notice, sptr, hush_time);
	}

	if (hush_time)
		sendto_umode(UMODE_HUSHNOTICE,
			"*** Hush Notice -- %s hushed %s for %d seconds (%s)",
			sptr->name, acptr->name, hush_time, parv[3]);
	else
		sendto_umode(UMODE_HUSHNOTICE,
			"*** Hush Notice -- %s hushed %s indefinitely (%s)",
			sptr->name, acptr->name, parv[3]);				
	return 0;
}

void unhush(aClient *cptr, aClient *sptr)
{
	if (!cptr || !IsPerson(cptr) || !cptr->u->hush)
		return;
	
	free(cptr->u->hush);
	cptr->u->hush = (Hush *)NULL;

	if (MyConnect(cptr))
		sendto_one(cptr,":%s NOTICE %s :*** You are no longer hushed.",
			me.name, cptr->name);
	sendto_serv_butone(sptr, ":%n %m %s", sptr, &ms_unhush, cptr->name);
	sendto_umode(UMODE_HUSHNOTICE, "%s was unhushed %s%s", cptr->name,
		sptr ? "by " : "(time elapsed)", sptr ? sptr->name : "");
}

int m_unhush(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aClient *acptr;
		
	if (check_registered(sptr))
		return 0;
	if (parc < 2 || !parv[1] || parv[1] == '\0')
		return check_params(sptr, "UNHUSH", -1, 0);
	if (!(acptr = find_person(parv[1])))
	{
		send_num(sptr, ERR_NOSUCHNICK, parv[1]);
		return 0;
	}	
	if (IsPerson(cptr) && ((!MyConnect(acptr) && !OPCanGKill(sptr)) ||
	    (MyConnect(acptr) && !OPCanLKill(sptr))))
	{
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	}

	if (!acptr->u->hush)
	{
		sendto_one(sptr, ":%s NOTICE %s :User %s is not hushed.",
			me.name, sptr->name, acptr->name);
		return 0;
	}
	unhush(acptr, sptr);
	return 0;
}

int	m_chanserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(ChanServ))) {
		if(IsULine(acptr)) {
			sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
				ChanServ, parv[1]);
			return 0;
		}
	}

	send_num(sptr, ERR_SERVICESDOWN, ChanServ);
}

int	m_memoserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(MemoServ))) {
		if(IsULine(acptr)) {
			sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
				MemoServ, parv[1]);
			return 0;
		}
	}

	send_num(sptr, ERR_SERVICESDOWN, MemoServ);
}

int	m_nickserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(NickServ))) {
		if(IsULine(acptr)) {
			sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
				NickServ, parv[1]);
			return 0;
		}
	}

	send_num(sptr, ERR_SERVICESDOWN, NickServ);
}

int	m_operserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(OperServ))) {
		if(IsULine(acptr)) {
			sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
				OperServ, parv[1]);
			return 0;
		}
	}

	send_num(sptr, ERR_SERVICESDOWN, OperServ);
}

/* Automatic NickServ/Chanserv direction of the identify command. */
int     m_identify(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
{
        aClient *acptr;

        if (check_registered_user(sptr))
                return 0;

        if (parc < 2 || *parv[1] == '\0') {
                send_num(sptr, ERR_NOTEXTTOSEND);
                return -1;
        }

        if (*parv[1]) {
                if((*parv[1] == '#') && ((char *)index(parv[1], ' '))) {
                        if ((acptr = find_person(ChanServ)) && IsULine(acptr))
                                sendto_one(acptr,":%s PRIVMSG %s :IDENTIFY %s", sptr->name,
                                        ChanServ, parv[1]);
                        else
                                send_num(sptr, ERR_SERVICESDOWN, ChanServ);
                } else {
                        if ((acptr = find_person(NickServ)) && IsULine(acptr))
                                sendto_one(acptr,":%s PRIVMSG %s :IDENTIFY %s", sptr->name,
                                        NickServ, parv[1]);
                        else
                                send_num(sptr, ERR_SERVICESDOWN, NickServ);
                }
        }
}


/*
 * Automatic NickServ/ChanServ parsing. If the second word of parv[1]
 * starts with a '#' this message goes to ChanServ. If it starts with 
 * anything else, it goes to NickServ. If there is no second word in 
 * parv[1], the message defaultly goes to NickServ. If parv[1] == 'help'
 * the user in instructed to /cs, /ns or /ms HELP for the help they need.
 * -taz
 */

int	m_services(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char *tmps;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((strlen(parv[1])>=4) && (!strncmp(parv[1], "help", 4))) {
		sendto_one(sptr, ":%s NOTICE %s :For ChanServ help use: /chanserv help", me.name, sptr->name);
		sendto_one(sptr, ":%s NOTICE %s :For NickServ help use: /nickserv help", me.name, sptr->name);
		sendto_one(sptr, ":%s NOTICE %s :For MemoServ help use: /memoserv help", me.name, sptr->name);
		return 0;
	}

	if ((tmps = (char *)index(parv[1], ' '))) {
		tmps++;
		if(*tmps == '#')
			return m_chanserv(cptr, sptr, parc, parv);
		else
			return m_nickserv(cptr, sptr, parc, parv);
	}

	return m_nickserv(cptr, sptr, parc, parv);

}

/* m_lang
 * parv[1] = Language code (en, es)
 * Added by dave 06/01/2003
 */
int	m_lang(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	int flag, *s, setflags = 0;

	if (check_registered_user(sptr))
		return 0;

	if(!parv[1])
	{
		send_num(sptr, ERR_NOSUCHLANG);
		return 0;
	}

	for (s = user_modes; (flag = *s); s += 2)
		if ((sptr->umodes & flag))
			setflags |= flag;

	if(!strcasecmp(parv[1],"en"))
	{
		ClearEspanol(sptr);
		sendto_one(sptr, ":%s NOTICE %s :You will now receive messages in English.", me.name, sptr->name);
	}
	else if(!strcasecmp(parv[1],"es"))
	{
		SetEspanol(sptr);
		sendto_one(sptr, ":%s NOTICE %s :Ya recibir� mensajes en espa�ol.", me.name, sptr->name);
	}
	else
	{
		send_num(sptr, ERR_NOSUCHLANG);
	}

	send_umode_out(cptr, sptr, setflags);

	return 0;
}

/* remove_dvs */
int	remove_dvs(aClient *acptr, int hide)
{
#ifdef USE_DVS
	if(!IsSetDVS(acptr))
		return 0;

	ClearDVS(acptr);
	if(MyClient(acptr))
	{
		acptr->dvstime = 0;
		if(!hide)
			sendto_one(acptr, ":%n NOTICE %n :*** DVS -- You are no longer subject to DVS restrictions. You may now use this network. Please read the /motd", &me, acptr);
		if(acptr->lopt)
			send_list(acptr, 64);
		chbuf_empty(acptr);
	}
#endif
	return 0;
}
