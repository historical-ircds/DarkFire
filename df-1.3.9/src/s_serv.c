/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_serv.c (formerly ircd/s_msg.c)
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   See file AUTHORS in IRC package for additional names of
 *   the programmers. 
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "msg.h"
#include "channel.h"
#if defined(PCS) || defined(AIX) || defined(DYNIXPTX) || defined(SVR3)
#include <time.h>
#endif
#include <sys/stat.h>
#include <fcntl.h>
#include <utmp.h>
#include <time.h>
#include "h.h"
#include "dvs.h"

static	char	buf[BUFSIZE];
static	char	nuhostbuf[NICKLEN + USERLEN + HOSTLEN + 5];
char	qotd[TOPICLEN + 1];

int     max_connection_count = 1, max_client_count = 1;

int	send_netburst PROTO((aClient *));

extern	int	move_server PROTO((aConfItem *, aClient *, struct hostent *, aClient *));
extern	int	completed_connection PROTO((aClient *));
extern	char	*show_operflags PROTO((int));

void	update_lusers();

/*
** m_functions execute protocol messages on this server:
**
**	cptr	is always NON-NULL, pointing to a *LOCAL* client
**		structure (with an open socket connected!). This
**		identifies the physical socket where the message
**		originated (or which caused the m_function to be
**		executed--some m_functions may call others...).
**
**	sptr	is the source of the message, defined by the
**		prefix part of the message if present. If not
**		or prefix not found, then sptr==cptr.
**
**		(!IsServer(cptr)) => (cptr == sptr), because
**		prefixes are taken *only* from servers...
**
**		(IsServer(cptr))
**			(sptr == cptr) => the message didn't
**			have the prefix.
**
**			(sptr != cptr && IsServer(sptr) means
**			the prefix specified servername. (?)
**
**			(sptr != cptr && !IsServer(sptr) means
**			that message originated from a remote
**			user (not local).
**
**		combining
**
**		(!IsServer(sptr)) means that, sptr can safely
**		taken as defining the target structure of the
**		message in this server.
**
**	*Always* true (if 'parse' and others are working correct):
**
**	1)	sptr->from == cptr  (note: cptr->from == cptr)
**
**	2)	MyConnect(sptr) <=> sptr == cptr (e.g. sptr
**		*cannot* be a local connection, unless it's
**		actually cptr!). [MyConnect(x) should probably
**		be defined as (x == x->from) --msa ]
**
**	parc	number of variable parameter strings (if zero,
**		parv is allowed to be NULL)
**
**	parv	a NULL terminated list of parameter pointers,
**
**			parv[0], sender (prefix string), if not present
**				this points to an empty string.
**			parv[1]...parv[parc-1]
**				pointers to additional parameters
**			parv[parc] == NULL, *always*
**
**		note:	it is guaranteed that parv[0]..parv[parc-1] are all
**			non-NULL pointers.
*/

/*
** m_version
**	parv[1] = remote server
*/
int	m_version(cptr, sptr, parc, parv)
aClient *sptr, *cptr;
int	parc;
char	*parv[];
{
	extern	char	serveropts[];

	if (check_registered(sptr))
		return 0;

	if (hunt_server(cptr,sptr,":%s VERSION :%s",1,parc,parv)==HUNTED_ISME)
#ifdef USE_DVS
		send_num(sptr, RPL_VERSION, version, debugmode, me.name,
			serveropts, PROLEV_MAX, "DVS");
#else
		send_num(sptr, RPL_VERSION, version, debugmode, me.name,
			serveropts, PROLEV_MAX, "");
#endif
	return 0;
}

/*
** m_squit
**	parv[1] = server name
**	parv[parc-1] = comment
*/
int	m_squit(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
	aConfItem *aconf;
	char	*server;
	aClient	*acptr;
	char	*comment = (parc > 2 && parv[parc-1]) ?
	             parv[parc-1] : cptr->name;

        if (check_registered(sptr) || check_privs(sptr, 0, 0))
                 return 0;

	if (parc > 1)
	    {
		server = parv[1];
		/*
		** To accomodate host masking, a squit for a masked server
		** name is expanded if the incoming mask is the same as
		** the server name for that link to the name of link.
		*/
		if ((*server=='*') && IsServer(cptr) && (aconf=cptr->s->nline)
		   && !mycmp(server, my_name_for_link(me.name, aconf)))
			server = cptr->name;

		/*
		** The following allows wild cards in SQUIT. Only usefull
		** when the command is issued by an oper.
		*/
		for (acptr = firstserv; acptr; acptr = acptr->s->next)
			if (!match(server, acptr->name))
				break;
		if (acptr && IsMe(acptr))
		    {
			acptr = cptr;
			server = cptr->sockhost;
		    }
	    }
	else
	    {
		/*
		** This is actually protocol error. But, well, closing
		** the link is very proper answer to that...
		*/
		server = cptr->sockhost;
		acptr = cptr;
	    }

	/*
	** SQUIT semantics is tricky, be careful...
	**
	** The old (irc2.2PL1 and earlier) code just cleans away the
	** server client from the links (because it is never true
	** "cptr == acptr".
	**
	** This logic here works the same way until "SQUIT host" hits
	** the server having the target "host" as local link. Then it
	** will do a real cleanup spewing SQUIT's and QUIT's to all
	** directions, also to the link from which the orinal SQUIT
	** came, generating one unnecessary "SQUIT host" back to that
	** link.
	**
	** One may think that this could be implemented like
	** "hunt_server" (e.g. just pass on "SQUIT" without doing
	** nothing until the server having the link as local is
	** reached). Unfortunately this wouldn't work in the real life,
	** because either target may be unreachable or may not comply
	** with the request. In either case it would leave target in
	** links--no command to clear it away. So, it's better just
	** clean out while going forward, just to be sure.
	**
	** ...of course, even better cleanout would be to QUIT/SQUIT
	** dependant users/servers already on the way out, but
	** currently there is not enough information about remote
	** clients to do this...   --msa
	*/
	if (!acptr)
	    {
		send_num(sptr, ERR_NOSUCHSERVER, server);
		return 0;
	    }
	if (MyClient(sptr) && ((!OPCanGRoute(sptr) && !MyConnect(acptr)) ||
			       (!OPCanLRoute(sptr) && MyConnect(acptr))))
	    {
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	    }
	/*
	**  Notify all opers, if my local link is remotely squitted
	*/
	if (MyConnect(acptr) && !IsAnOper(cptr))
	{
		sendout_wallops("Received SQUIT %s from %s (%s)",
			acptr->name, sptr->name, comment);
#if defined(USE_SYSLOG) && defined(SYSLOG_SQUIT)
	    syslog(LOG_DEBUG,"SQUIT From %s : %s (%s)",
		   sptr->name, server, comment);
#endif
	}
	else if (MyConnect(acptr))
	{
		if (IsPerson(acptr))
		{
			sendto_one(sptr, ":%s NOTICE %s :Don't squit yourself.  Try /quit.",
				me.name, sptr->name);
			return 0;
		}
		sendout_wallops("Received SQUIT %s from %s (%s)",
			acptr->name, sptr->name, comment);
	}
	if (IsAnOper(sptr))
	{
		/*
		 * It was manually /squit'ed by a human being(we hope),
		 * there is a very good chance they don't want us to
		 * reconnect right away.  -Cabal95
		 */
		acptr->flags |= FLAGS_SQUIT;
	}
	return exit_client(cptr, acptr, sptr, comment);
    }

/*
 * m_protoctl
 *	parv[1+] = Options
 */
int	m_protoctl(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	int	i, x;
	char	*proto, *options;

	for (i = 1; i < parc; i++)
	{
		proto = parv[i];
		options = index(proto,'=');
		if(options)
			*options++ = '\0';

		if (!strcmp(proto, "NOQUIT"))
			AddProto(cptr, PROLEV_NOQUIT);
		else if (!strcmp(proto, "TOKEN"))
			AddProto(cptr, PROLEV_TOKEN);
		else if (!strcmp(proto, "SERVHASH"))
			AddProto(cptr, PROLEV_SERVHASH);
		else if (!strcmp(proto,"LEV")&&options&&(x=atoi(options))>cptr->prolev)
			cptr->prolev = x;
#ifdef USE_DVS
		else if (!strcmp(proto,"DVS"))
			SetDVSWarn(cptr); /* Use this normal user flag
					   * instead of creating a new one */
		else if (!strcmp(proto,"DVSLOCK"))
			cptr->flags |= FLAGS_DVSLOCK;
		else if (!strcmp(proto,"DVSSTATUS")&&options&&(x=atoi(options))>=0&&x<=3)
			cptr->dvstime = x+1; /* won't be 0 (0-3 = 1-4) */
#endif
	}

	return 0;
}

/*
 * Server link procedures -- now modularized
 * Cleaned up by dave on 12/29/2002
 *
 * register_server	set internals and make the server known
 * m_server		receive the server command, determine how to handle
 * m_server_remote	configure a server not directly connected
 * m_server_estab	process a link request from incoming client
 * send_netburst	send our information (servers, nicks, chans, sqlines)
 *			  to our new link
 *
 * Takes advantage of intro_user_to_one
 */

int register_server(aClient *sptr, aClient *acptr, char *name, char *info)
{
	if(acptr->oflag & 0x10000000)
		acptr->oflag &= ~(0x10000000);
	acptr->srvptr = sptr;
	acptr->hopcount = sptr->hopcount+1;
	acptr->s->lastsptr = acptr;
	set_server_name(acptr, name);                
	if (info != acptr->info) 
		strncpyzt(acptr->info, info, REALLEN+1);
	SetServer(acptr);
	add_server(acptr);
	acptr->umodes = UMODE_WALLOP;
	sendto_serv_butone(acptr->from, ":%n %m %s %d :%s", sptr, &ms_server,
		name, acptr->hopcount+1, info);
	if (find_conf_host(sptr->from->confs, name, CONF_UWORLD))
		acptr->flags |= FLAGS_ULINE;
#ifdef USE_DVS
	else if (MyConnect(acptr) && !HasBeenDVSWarned(acptr) && !IsULine(acptr))
		sendout_globops("Warning - %s does not support DVS", acptr->name);
#endif
	return 0;
}

int	m_server(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aClient		*acptr;
	char		*name, *info;

	if(IsPerson(cptr))
	{
		send_num(cptr, ERR_UNKNOWNCOMMAND, "SERVER");
		return 0;
	}
	if(parc < 4)
	{
		sendto_one(cptr, "ERROR :Bad SERVER syntax");
		return 0;
	}

	name = parv[1];
	info = parv[parc-1];

	if (strlen(name) > HOSTLEN || !index(name, '.'))
	{
		sendto_one(cptr, "ERROR :Bad servername (%s)", name);
		return exit_client(cptr, cptr, &me, "Invalid server name");
	}

	if ((acptr = find_client(name)))
	{
		sendto_one(cptr,"ERROR :Server %s already exists from %s", name, acptr->from);
		if (IsServer(cptr))
			sendout_wallops("Delinking %s, trying to reintroduce %s.", cptr->name, name);
		else
			sendto_umode(UMODE_JUNK, "Connection from %s@%s trying to register as %s",
				     cptr->username, cptr->sockhost, name);
		return exit_client(cptr, cptr, &me, "Server already exists");
	}

	if(IsServer(cptr))
		return m_server_remote(cptr, sptr, name, info);

	if (!IsUnknown(cptr) && !IsHandshake(cptr))
		return 0;

	strcpy(cptr->name, name);
	strncpyzt(cptr->info, info, REALLEN+1);

	return m_server_estab(cptr);
}

int	m_server_remote(aClient *cptr, aClient *sptr, char *name, char *info)
{
	aConfItem	*aconf;
	aClient		*acptr;

	if ((aconf = find_conf_host(cptr->confs, name, CONF_LEAF)) &&
	    (!aconf->port || (sptr->hopcount >= aconf->port)))
	{
		sendto_ops("Leaf-only link %s introduced %s.",
			   get_client_name(cptr, TRUE), name);
		sendto_one(cptr, "ERROR :Leaf-only link (L-line)");
		return exit_client(cptr, cptr, cptr, "Leaf Only");
	}
	if (!(aconf = find_conf_host(cptr->confs, name, CONF_HUB)) ||
	    (aconf->port && (sptr->hopcount >= aconf->port)))
	{
		sendto_ops("Non-Hub link %s introduced %s.",
			   get_client_name(cptr, TRUE), name);
		sendto_one(cptr, "ERROR :Not a hub (H-line)");
		return exit_client(cptr, cptr, cptr, "Too many servers");
	}
	if ((aconf = find_conf_name(name, CONF_QUARANTINED_SERVER)))
	{
		sendout_wallops("Closing link to %s with Q-lined %s (%s)",
			get_client_name(cptr,FALSE), name,
			aconf->passwd ? "reason unspecified" : aconf->passwd);
		sendto_one(cptr, "ERROR :%s is not welcome: %s.",
			name, BadPtr(aconf->passwd) ?
			"reason unspecified" : aconf->passwd);
		return exit_client(cptr, cptr, cptr, "Q-Lined Server");
	}

	acptr = make_client(cptr, sptr);
	add_client_to_list(acptr);
	return register_server(sptr, acptr, name, info);
}

int	m_server_estab(aClient *cptr)
{
	aConfItem	*aconf, *bconf;
	char		*name, *info, *s, *pass, *inpath;

	inpath	= get_client_name(cptr,TRUE);
	name	= cptr->name;
	info	= cptr->info;

#ifndef HUB
	if(me.s->down)
	{
		sendto_one(cptr, "ERROR :Cannot accept link (Leaf!)");
		return exit_client(cptr, cptr, cptr, "Cannot accept link");
	}
#endif

	if(check_server_init(cptr))
	{
		sendto_umode(UMODE_JUNK, "*** Notice -- Received unauthorized connection from %s.", get_client_host(cptr));
		return exit_client(cptr, cptr, cptr, "No C/N conf lines");
	}

	if (!(aconf = find_conf(cptr->confs, name, CONF_NOCONNECT_SERVER)))
	{
		sendto_one(cptr, "ERROR :Access denied. No N line for server %s", inpath);
		sendto_umode(UMODE_JUNK, "Access denied. No N line for server %s", inpath);
		return exit_client(cptr, cptr, cptr, "No N line for server");
	}
	if (!(bconf = find_conf(cptr->confs, name, CONF_CONNECT_SERVER)))
	{
		sendto_one(cptr, "ERROR :Only N (no C) field for server %s", inpath);
		sendto_umode(UMODE_JUNK, "Only N (no C) field for server %s",inpath);
		return exit_client(cptr, cptr, cptr, "No C line for server");
	}

#ifdef CRYPT_LINK_PASSWORD
	if(*cptr->passwd)
	{
		char    salt[3];
		extern  char *crypt();

		salt[0]=aconf->passwd[0];
		salt[1]=aconf->passwd[1];
		salt[2]='\0';
		pass = crypt(cptr->passwd, salt);
	}
	else
		pass = "";
#else
	pass = cptr->passwd;
#endif

	if (*aconf->passwd && strcmp(aconf->passwd, pass))
	{
		sendto_one(cptr, "ERROR :No Access (passwd mismatch) %s", inpath);
		sendto_ops("Access denied (passwd mismatch) %s", inpath);
		return exit_client(cptr, cptr, cptr, "Bad Password");
	}
	bzero(cptr->passwd, sizeof(cptr->passwd));

	if ((s = index(aconf->host, '@')))
	{
		*s = '\0';
		if(match(aconf->host, cptr->username))
		{
			*s = '@';
			sendto_umode(UMODE_JUNK, "Username mismatch [%s]v[%s] : %s",
				     aconf->host, cptr->username, cptr->name);
			sendto_one(cptr, "ERROR :No Username Match");
			return exit_client(cptr, cptr, cptr, "Username mismatch");
		}
		*s = '@';
	}

	if (IsUnknown(cptr))
	{
#ifdef USE_DVS
		if(ServicesOnline)
			sendto_one(cptr, "PROTOCTL DVS %s", SERV_CAPAB);		
		else if(DVSLock)
			sendto_one(cptr, "PROTOCTL DVS DVSLOCK DVSSTATUS=%d %s",
				   DVSStatus, SERV_CAPAB);
		else
			sendto_one(cptr, "PROTOCTL DVS DVSSTATUS=%d %s",
				   DVSStatus, SERV_CAPAB);
#else
		sendto_one(cptr, "PROTOCTL %s", SERV_CAPAB);
#endif
		if (bconf->passwd[0])
			sendto_one(cptr, "PASS :%s", bconf->passwd);
		sendto_one(cptr, "SERVER %s 1 :%s", me.name, me.info);
	}

	det_confs_butmask(cptr,
	    CONF_LEAF|CONF_HUB|CONF_NOCONNECT_SERVER|CONF_UWORLD);
	
	register_server(&me, cptr, name, info);

	nextping = nowtime;
	sendout_wallops("Link with %s established.", cptr->name);
	cptr->s->nline = aconf;
	if (find_conf_host(cptr->confs, name, CONF_UWORLD))
		cptr->flags |= FLAGS_ULINE;

	return send_netburst(cptr);
}

int	send_netburst(aClient *cptr)
{
	aClient		*acptr;
	aChannel	*chptr;
	aSqlineItem	*tmp;

	for (acptr = &me; acptr; acptr = acptr->prev)
	{
		if (acptr->from == cptr)
			continue;
		if (IsServer(acptr))
			sendto_one(cptr, ":%n %m %s %d :%s",
	   			   acptr->srvptr, &ms_server, acptr->name,
				   acptr->hopcount+1, acptr->info);
	}

#ifdef USE_DVS
	if(ServicesOnline == 1)
		sendto_one(cptr, ":%n %m %d %s", &me, &ms_dvs, DVSStatus, (DVSLock?"LOCK":"UNLOCK"));
	else
	{
		if(cptr->dvstime > 0 && cptr->dvstime > (DVSStatus+1))
			DVSStatus = (cptr->dvstime - 1);
		if(cptr->flags & FLAGS_DVSLOCK)
			DVSLock = 1;
		sendto_serv_butone(cptr, ":%n %m %d %s", &me, &ms_dvs, DVSStatus, (DVSLock?"LOCK":"UNLOCK"));
	}
#endif

	for (acptr = &me; acptr; acptr = acptr->prev)
	{
		if (acptr->from == cptr)
			continue;
		if (IsPerson(acptr))
			intro_user_to_one(cptr, acptr);
	}

	for (chptr = channel; chptr; chptr = chptr->nextch)
	{
		if(cptr->prolev >= PROLEV_CJOIN)
			send_channel_join(cptr, chptr);
		send_channel_modes(cptr, chptr);
		if (chptr->topic_time)
			sendto_one(cptr,"%m %s %s %d :%s", &ms_topic,
				   chptr->chname, chptr->topic_nick,
				   chptr->topic_time, chptr->topic);
	}

	for(tmp=sqline;tmp;tmp=tmp->next)
	{
		if(tmp->status != CONF_ILLEGAL)
			if(tmp->reason)
				sendto_one(cptr,":%n %m %s :%s", &me, &ms_sqline,
					tmp->sqline, tmp->reason);
			else
				sendto_one(cptr,":%n %m %s", &me, &ms_sqline,
					tmp->sqline);
	}
	
	return 0;
}

/* m_info */
int	m_info(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	char **text = infotext;

	if (check_registered(sptr))
		return 0;

	if (hunt_server(cptr,sptr,":%s INFO :%s",1,parc,parv) == HUNTED_ISME)
	    {
		while (*text)
			send_num(sptr, RPL_INFO, *text++);

		send_num(sptr, RPL_INFO, "");
		sendto_one(sptr, ":%s %d %s :Compile #%s: %s",
			   me.name, RPL_INFO, sptr->name, generation, creation);
		sendto_one(sptr, ":%s %d %s :On-line since %s",
			   me.name, RPL_INFO, sptr->name, myctime(me.firsttime));
		send_num(sptr, RPL_ENDOFINFO);
	    }

    return 0;
}

/* m_contrib */
int	m_contrib(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	char **text = contribtext;

	if(check_registered(sptr))
		return 0;

	if (hunt_server(cptr,sptr,":%s CONTRIB :%s",1,parc,parv) == HUNTED_ISME)
	{
		while(*text)
			send_num(sptr, RPL_INFO, *text++);

		send_num(sptr, RPL_ENDOFINFO);
	}
	return 0;
}

/*
 * RPL_NOWON	- Online at the moment (Succesfully added to WATCH-list)
 * RPL_NOWOFF	- Offline at the moement (Succesfully added to WATCH-list)
 * RPL_WATCHOFF	- Succesfully removed from WATCH-list.
 * ERR_TOOMANYWATCH - Take a guess :>  Too many WATCH entries.
 */
static	void	show_watch(cptr, name, rpl1, rpl2)
aClient	*cptr;
char	*name;
int	rpl1, rpl2;
{
	aClient *acptr;


	if ((acptr = find_person(name)))
		send_num(cptr, rpl1, acptr->name, acptr->username,
			   acptr->u->dhost, acptr->u->nickts);
	else
		send_num(cptr, rpl2, name, "*", "*", 0);
}

/*
 * m_watch
 */
int	m_watch(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
    aClient	*acptr;
    char	*s;
    char	*user;
    char	*p = NULL;


    if (check_registered(sptr))
	return 0;

    if (parc < 2) {
    	/*
	 * Default to 'l' - list who's currently online
	 */
	parv[1] = "l";
    }

    for (p = parv[1]; s = strsplit(&p, ' ');)
    {
	if ((user = (char *)index(s, '!')))
	    *user++ = '\0'; /* Not used */

	/*
	 * Prefix of "+", they want to add a name to their WATCH
	 * list.
	 */
	if (*s == '+') {
	    if (do_nick_name(s+1)) {
		if (sptr->notifies >= MAXWATCH) {
		    send_num(sptr, ERR_TOOMANYWATCH, s+1);

		    continue;
		}

		add_to_notify_hash_table(s+1, sptr);
	    }

	    show_watch(sptr, s+1, RPL_NOWON, RPL_NOWOFF);
	    continue;
	}

	/*
	 * Prefix of "-", coward wants to remove somebody from their
	 * WATCH list.  So do it. :-)
	 */
	if (*s == '-') {
	    del_from_notify_hash_table(s+1, sptr);
	    show_watch(sptr, s+1, RPL_WATCHOFF, RPL_WATCHOFF);

	    continue;
	}

	/*
	 * Fancy "C" or "c", they want to nuke their WATCH list and start
	 * over, so be it.
	 */
	if (*s == 'C' || *s == 'c') {
	    hash_del_notify_list(sptr);

	    continue;
	}

	/*
	 * Now comes the fun stuff, "S" or "s" returns a status report of
	 * their WATCH list.  I imagine this could be CPU intensive if its
	 * done alot, perhaps an auto-lag on this?
	 */
	if (*s == 'S' || *s == 's') {
	    Link	*lp;
	    aNotify	*anptr;
	    int	count = 0;

	    /*
	     * Send a list of how many users they have on their WATCH list
	     * and how many WATCH lists they are on.
	     */
	    anptr = hash_get_notify(sptr->name);
	    if (anptr)
		for (lp = anptr->notify, count = 1; (lp = lp->next); count++)
		    ;
	    send_num(sptr, RPL_WATCHSTAT, sptr->notifies, count);

	    /*
	     * Send a list of everybody in their WATCH list. Be careful
	     * not to buffer overflow.
	     */
	    if ((lp = sptr->notify) == NULL) {
		send_num(sptr, RPL_ENDOFWATCHLIST, *s);
		continue;
	    }
	    *buf = '\0';
	    strcpy(buf, lp->value.nptr->nick);
	    count = strlen(sptr->name)+strlen(me.name)+10+strlen(buf);
	    while ((lp = lp->next)) {
		if (count+strlen(lp->value.nptr->nick)+1 > BUFSIZE - 2) {
		    send_num(sptr, RPL_WATCHLIST, buf);
		    *buf = '\0';
		    count = strlen(sptr->name)+strlen(me.name)+10;
		}
		strcat(buf, " ");
		strcat(buf, lp->value.nptr->nick);
		count += (strlen(lp->value.nptr->nick)+1);
	    }
	    send_num(sptr, RPL_WATCHLIST, buf);

	    send_num(sptr, RPL_ENDOFWATCHLIST, *s);
	    continue;
	}

	/*
	 * Well that was fun, NOT.  Now they want a list of everybody in
	 * their WATCH list AND if they are online or offline? Sheesh,
	 * greedy arn't we?
	 */
	if (*s == 'L' || *s == 'l') {
	    Link	*lp = sptr->notify;

	    while (lp) {
		if ((acptr = find_person(lp->value.nptr->nick)))
		    send_num(sptr, RPL_NOWON, acptr->name, acptr->username,
			       acptr->u->host, acptr->u->nickts);
		/*
		 * But actually, only show them offline if its a capital
		 * 'L' (full list wanted).
		 */
		else if (*s=='L')
		    send_num(sptr, RPL_NOWOFF,
			       lp->value.nptr->nick, "*", "*",
			       lp->value.nptr->lasttime);
		lp = lp->next;
	    }

	    send_num(sptr, RPL_ENDOFWATCHLIST, *s);

	    continue;
	}

	/*
	 * Hmm.. unknown prefix character.. Ignore it. :-)
	 */
    }

    return 0;
}


/*
** m_links
**	parv[1] = servername mask
** or
**	parv[1] = server to query 
**      parv[2] = servername mask
*/
int	m_links(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	char *mask;
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;
    
	if (parc > 2 && IsOper(cptr))
	{
		if (hunt_server(cptr, sptr, ":%s LINKS %s :%s", 1, parc, parv)
				!= HUNTED_ISME)
			return 0;
		mask = parv[2];
	}
	else
		mask = parc < 2 ? NULL : parv[1];

	for (acptr = firstserv; acptr; acptr = acptr->s->next)
	{
		if (mask && match(mask, acptr->name))
			continue;
		send_num(sptr, RPL_LINKS, acptr->name, acptr->srvptr->name,
			   acptr->hopcount, (acptr->info[0] ? acptr->info :
			   "(IN THE UNKNOWN)"));
	}
	
	send_num(sptr, RPL_ENDOFLINKS, BadPtr(mask) ? "*" : mask);
	return 0;
}

/* m_map() - makes a branched "map" of the servers on the network
** written by binary
*/
int	m_map(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aClient *acptr = &me;
	int recurse = 0;
	
	if (check_registered_user(sptr))
		return(0);

	do
	{
		if (!acptr->s->side) buf[recurse*2+1] = '`';
		buf[recurse*2+2] = '\0';
		send_num(sptr, RPL_MAP, buf+2, acptr->name, recurse);
		if (!acptr->s->side) buf[recurse*2+1] = ' ';
		if (acptr->s->down)
		{
			recurse++;
			buf[recurse*2] = ' ';
			buf[recurse*2+1] = '|';
			acptr = acptr->s->down;
		}
		else
		{
			while (!acptr->s->side)
			{
				recurse--;
				acptr = acptr->srvptr;
				if (acptr==&me) goto donemap;
			}
			acptr = acptr->s->side;
		}
	} while (acptr);
	
donemap:
	send_num(sptr, RPL_ENDOFMAP);
	return 0;
}


/*
** m_stats
**	parv[1] = statistics selector (defaults to Message frequency)
**	parv[2] = server name (current server defaulted, if omitted)
**
**	Currently supported are:
**		M = Message frequency (the old stat behaviour)
**		L = Local Link statistics
**              C = Report C and N configuration lines
*/
/*
** m_stats/stats_conf
**    Report N/C-configuration lines from this server. This could
**    report other configuration lines too, but converting the
**    status back to "char" is a bit akward--not worth the code
**    it needs...
**
**    Note:   The info is reported in the order the server uses
**            it--not reversed as in ircd.conf!
*/

static int report_array[15][3] = {
		{ CONF_CONNECT_SERVER,    RPL_STATSCLINE, 'C'},
		{ CONF_NOCONNECT_SERVER,  RPL_STATSNLINE, 'N'},
		{ CONF_CLIENT,            RPL_STATSILINE, 'I'},
		{ CONF_KILL,              RPL_STATSKLINE, 'K'},
		{ CONF_ZAP,		  RPL_STATSKLINE, 'Z'},
		{ CONF_QUARANTINED_NICK,  RPL_STATSQLINE, 'Q'},
		{ CONF_LEAF,		  RPL_STATSLLINE, 'L'},
		{ CONF_OPERATOR,	  RPL_STATSOLINE, 'O'},
		{ CONF_HUB,		  RPL_STATSHLINE, 'H'},
		{ CONF_LOCOP,		  RPL_STATSOLINE, 'o'},
		{ CONF_UWORLD,		  RPL_STATSULINE, 'U'},
		{ CONF_MISSING,		  RPL_STATSXLINE, 'X'},
		{ CONF_NOOP_GLOBAL,	  RPL_STATSOLINE, 'O'},
		{ CONF_NOOP_LOCAL,	  RPL_STATSOLINE, 'o'},
		{ 0, 0}
				};

static  void	report_sqlined_nicks(sptr)
aClient *sptr;
{
	aSqlineItem *tmp;
	char *nickmask, *reason;

	for(tmp = sqline; tmp; tmp=tmp->next) {
		if(tmp->status != CONF_ILLEGAL) {
			nickmask = BadPtr(tmp->sqline) ? "<NULL>" : tmp->sqline;
			reason = BadPtr(tmp->reason) ? "<NULL>" : tmp->reason;
			send_num(sptr, RPL_SQLINE_NICK, nickmask, reason);
		}
	}
}

static	void	report_configured_links(sptr, mask)
aClient *sptr;
int	mask;
{
	static	char	null[] = "<NULL>";
	aConfItem *tmp;
	int	*p, port, tmpmask;
	char	c, *host, *pass, *name, *flags;
	
	tmpmask = (mask == CONF_MISSING) ? CONF_CONNECT_SERVER : mask;

	for (tmp = conf; tmp; tmp = tmp->next)
		if (tmp->status & tmpmask)
		    {
			for (p = &report_array[0][0]; *p; p += 3)
				if (*p == tmp->status)
					break;
			if (!*p)
				continue;
			c = (char)*(p+2);
			host = BadPtr(tmp->host) ? null : tmp->host;
			pass = BadPtr(tmp->passwd) ? null : tmp->passwd;
			name = BadPtr(tmp->name) ? null : tmp->name;
			port = (int)tmp->port;
			/*
			 * On K line the passwd contents can be
			 * displayed on STATS reply. 	-Vesa
			 */
			/* Same with Z-lines and q/Q-lines -- Barubary */
			if ((tmp->status == CONF_KILL) || (tmp->status &
			    CONF_QUARANTINE) || (tmp->status == CONF_ZAP))
			{
/* These mods are to tell the difference between the different kinds
 * of klines.  the only effect it has is in the display.  --Russell
 */
/* Now translates spaces to _'s to show comments in klines -- Barubary */
				char *temp;
				if (!pass) strcpy(buf, "<NULL>");
				else {
					strcpy(buf, pass);
					for (temp = buf; *temp; temp++)
					if (*temp == ' ') *temp = '_';
				}
				/* semicolon intentional -- Barubary */
				if (tmp->status == CONF_QUARANTINED_NICK);
				/* Hide password for servers -- Barubary */
				else if (tmp->status & CONF_QUARANTINE)
					strcpy(buf, "*");
				else {
				/* KLINE_PERM == 0 - watch out when doing
				   Z-lines. -- Barubary */
				if (tmp->status != CONF_ZAP)
                                {
				if (tmp->tmpconf == KLINE_PERM) c = 'K';
				if (tmp->tmpconf == KLINE_TEMP) c = 'k';
				if (tmp->tmpconf == KLINE_AKILL) c = 'A';
                                }
                                 else
                                {
				if (tmp->tmpconf == KLINE_PERM) c = 'Z';
				if (tmp->tmpconf == KLINE_TEMP) c = 'z';
				if (tmp->tmpconf == KLINE_AKILL) c = 'S';
                                }
                                }
				send_num(sptr, p[1], c,  host,
					   buf, name, port,
					   get_conf_class(tmp));
			}
			/* Only display on X if server is missing */
			else if (mask == CONF_MISSING) {
			    if (!hash_find_serv(name))
				send_num(sptr, RPL_STATSXLINE, name, port);
			}
			else if (mask&(CONF_OPS|CONF_NOOP_GLOBAL|CONF_NOOP_LOCAL)) {
			    flags = show_operflags(port);
			    send_num(sptr, p[1], c, host, name, flags, get_conf_class(tmp));
			}
			else {
			    if(!IsOper(sptr)&&(mask&CONF_NOCONNECT_SERVER ||
				mask&CONF_CONNECT_SERVER))
				send_num(sptr, p[1], c, "*", name, port,
					   get_conf_class(tmp));
			    else
				send_num(sptr, p[1], c, host, name, port,
					   get_conf_class(tmp));
                   	}
		    }
	return;
}

/* Used to blank out ports -- Barubary */
char *get_client_name2(aClient *acptr, int showports)
{
	char *pointer = get_client_name(acptr, TRUE);
	char *s;

	if (!pointer) return NULL;
	if (showports) return pointer;
	strcpy(nuhostbuf, pointer);
	if (!(s = (char *)strrchr(nuhostbuf, '.'))) return NULL;
	strcpy(s, ".0]");

	return nuhostbuf;
}

int	m_stats(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
#ifndef DEBUGMODE
      	static	char	Sformat[]  = ":%s %d %s SendQ SendM SendBytes RcveM RcveBytes Open_since :Idle";
	static	char	Lformat[]  = ":%s %d %s %s %d %d %d %d %d %d :%d";
#else
      	static	char	Sformat[]  = ":%s %d %s SendQ SendM SendBytes RcveM RcveBytes Open_since CPU :Idle";
	static	char	Lformat[]  = ":%s %d %s %s %d %d %d %d %d %d %s";
	char	pbuf[96];	/* Should be enough for to ints */
#endif
	struct	Message	*mptr;
	aClient	*acptr;
	char	stat = parc > 1 ? parv[1][0] : '\0';
	int	i;
	int	doall = 0, wilds = 0, showports	= IsAnOper(sptr), remote=0;
	char	*name = NULL;
	aClass	*cltmp;

	if (check_registered(sptr))
		return 0;

	if (hunt_server(cptr,sptr,":%s STATS %s :%s",2,parc,parv)!=HUNTED_ISME)
		return 0;

	if (parc > 2)
	    {
		name = parv[2];
		collapse(name);
		if (!mycmp(name, me.name))
			doall = 2;
		else if (match(name, me.name) == 0)
			doall = 1;
		if (index(name, '*') || index(name, '?'))
			wilds = 1;
	    }
	else
	{
		name = me.name;
		doall = 1;
	}

	switch (stat)
	{
	case 'A' : case 'a' :
		sendto_one(sptr,":%s NOTICE %s :Current time....... %d",me.name,sptr->name,nowtime);
		sendto_one(sptr,":%s NOTICE %s :Nextconnect........ %d (%d)",me.name,sptr->name,nextconnect,nextconnect-nowtime);
		sendto_one(sptr,":%s NOTICE %s :Nextakillexpire.... %d (%d)",me.name,sptr->name,nextakillexpire,nextakillexpire-nowtime);
		sendto_one(sptr,":%s NOTICE %s :Last Count update.. %d (%ds ago)",me.name,sptr->name,Count.lastcheck,nowtime-Count.lastcheck);
		sendto_one(sptr,":%s NOTICE %s :Services online.... %s ("SERVICES_NAME")",me.name,sptr->name,ServicesOnline?"Yes":"No");
#ifdef USE_DVS
		sendto_one(sptr,":%s NOTICE %s :Enable DVS......... %s (See /stats D)",me.name,sptr->name,DVSStatus?"Yes":"No");
#endif
		break;
#ifdef USE_DVS
	case 'D' : case 'd' :
		sendto_one(sptr,":%n NOTICE %n :DVS Status........ %s (%d)", &me, sptr, (DVSStatus == 3 ? "Full" : (DVSStatus == 2 ? "Expanded" : (DVSStatus == 1 ? "Limited" : "Off"))), DVSStatus);
		sendto_one(sptr,":%n NOTICE %n :DVS Lock.......... %s (%d)", &me, sptr, (DVSLock == 1 ? "On - Locked" : "Off - Unlocked"), DVSLock);
		if (Count.lastcheck+2 <= nowtime)
			update_lusers();
		sendto_one(sptr,":%n NOTICE %n :Local DVS Kills... %d", &me, sptr, Count.dvskills);
		sendto_one(sptr,":%n NOTICE %n :Local DVS Users... %d", &me, sptr, Count.localdvs);
		sendto_one(sptr,":%n NOTICE %n :Global DVS Users.. %d", &me, sptr, Count.globaldvs);
		break;
#endif
	case 'L' : case 'l' :
		/*
		 * send info about connections which match, or all if the
		 * mask matches me.name.  Only restrictions are on those who
		 * are invisible not being visible to 'foreigners' who use
		 * a wild card based search to list it.
		 */
	  	sendto_one(sptr, Sformat, me.name, RPL_STATSLINKINFO, sptr->name);
		if (IsServer(cptr))
		{
			remote = 1;
			wilds = 0;
		}
		for (i = 0; i <= highest_fd; i++)
		    {
			if (!(acptr = local[i]))
				continue;
			if (stat=='l' && !IsServer(acptr) && doall)
				continue;
			if (IsPerson(acptr) && !IsAnOper(sptr))
				continue;
			if (doall==2 && qmatch(name, acptr->name))
				continue;
#ifdef DEBUGMODE
			mysprintf(pbuf, "%d :%d", acptr->cputime,
				(MyClient(acptr)) ?
				nowtime - acptr->ulast : 0);
#endif
		if(IsOper(sptr))
			sendto_one(sptr, Lformat, me.name,
				   RPL_STATSLINKINFO, sptr->name,
				   (isupper(stat)) ?
				   get_client_name2(acptr, showports) :
				   get_client_name(acptr, FALSE),
				   (int)DBufLength(&acptr->sendQ),
				   (int)acptr->sendM, (int)acptr->sendK,
				   (int)acptr->receiveM, (int)acptr->receiveK,
				   nowtime - acptr->firsttime,
#ifndef DEBUGMODE
				   (MyClient(acptr)) ?
				    nowtime - acptr->ulast : 0);
#else
				   pbuf);
#endif
			else
			sendto_one(sptr, Lformat, me.name,
				   RPL_STATSLINKINFO, sptr->name,
				   (isupper(stat)) ?
				   get_client_name2(acptr, showports) :
				   get_client_name(acptr, FALSE),
				   (int)DBufLength(&acptr->sendQ),
				   (int)acptr->sendM, (int)acptr->sendK,
				   (int)acptr->receiveM, (int)acptr->receiveK,
				   nowtime - acptr->firsttime,
#ifndef DEBUGMODE
				   (MyClient(acptr)) ?
				    nowtime - acptr->ulast : 0);
#else
				   pbuf);
#endif
		    }
		break;
	case 'C' : case 'c' :
                report_configured_links(sptr, CONF_CONNECT_SERVER|
					CONF_NOCONNECT_SERVER);
		break;
	case 'H' : case 'h' :
                report_configured_links(sptr, CONF_HUB|CONF_LEAF);
		break;
	case 'I' : case 'i' :
		report_configured_links(sptr, CONF_CLIENT);
		break;
	case 'K' : case 'k' :
		report_configured_links(sptr, CONF_KILL|CONF_ZAP);
		break;
	case 'M' : case 'm' :
		for (i = 0; (mptr = msgtab[i]); i++)
			send_num(sptr, RPL_STATSCOMMANDS, mptr->cmd,
				mptr->count, mptr->bytes, mptr->l_utime,
				mptr->r_utime, 0,
				(mptr->l_utime+mptr->r_utime)/(mptr->count+1));
		break;
	case 'n' : case 'N' :
#ifndef SHOWOPERS
		if(IsOper(sptr))
#endif
			report_configured_links(sptr, CONF_NOOP_GLOBAL|CONF_NOOP_LOCAL);
		break;
	case 'o' : case 'O' :
#ifndef SHOWOPERS
		if(IsOper(sptr))
#endif
			report_configured_links(sptr, CONF_OPS);
		break;
	case 'Q' :
		report_configured_links(sptr, CONF_QUARANTINE);
		break;
	case 'q' :
		report_sqlined_nicks(sptr);
		break;
	case 'R' : case 'r' :
#ifdef DEBUGMODE
		send_usage(sptr,sptr->name);
#endif
		break;
	case 'U' :
                report_configured_links(sptr, CONF_UWORLD);
		break;
	case 'u' :
	    {
		time_t tmpnow;

		tmpnow = nowtime - me.since;
		send_num(sptr, RPL_STATSUPTIME, tmpnow/86400,
			(tmpnow/3600)%24, (tmpnow/60)%60, tmpnow%60);
                send_num(sptr, RPL_STATSCONN,
			   max_connection_count, max_client_count);
		break;
	    }
	case 'w' :
		sendto_one(sptr,":%s NOTICE %s :Class PingFreq ConnFreq Links MaxLinks MaxSendq",me.name,sptr->name);
		for (cltmp = FirstClass(); cltmp; cltmp = NextClass(cltmp))
			sendto_one(sptr,":%s NOTICE %s :%d %d %d %d %d %d",me.name,sptr->name,Class(cltmp), 
				   PingFreq(cltmp), ConFreq(cltmp), Links(cltmp), MaxLinks(cltmp), MaxSendq(cltmp));
		break;
	case 'X' : case 'x' :
		report_configured_links(sptr, CONF_MISSING);
		break;
	case 'Y' : case 'y' :
		report_classes(sptr);
		break;
	case 'Z' : case 'z' :
		count_memory(sptr, sptr->name);
		break;
	default :
		stat = '*';
		break;
	}
	send_num(sptr, RPL_ENDOFSTATS, stat);
	return 0;
    }


/*
** Note: At least at protocol level ERROR has only one parameter,
** although this is called internally from other functions
** --msa
**
**	parv[*] = parameters
*/
int	m_error(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*para;

	para = (parc > 1 && *parv[1] != '\0') ? parv[1] : "<>";

	Debug((DEBUG_ERROR,"Received ERROR message from %s: %s",
	      sptr->name, para));
	/*
	** Ignore error messages generated by normal user clients
	** (because ill-behaving user clients would flood opers
	** screen otherwise). Pass ERROR's from other sources to
	** the local operator...
	*/
	if (!IsServer(cptr) && !IsConnecting(cptr))
		return 0;
	if (cptr == sptr)
		sendout_wallops("ERROR :from %s -- %s",
			cptr->name, para);
	else
		sendout_wallops("ERROR :from %s via %s -- %s", sptr->name,
			cptr->name, para);
	return 0;
}


/*
 * m_help - directs a user to the appropriate source of help -- see config.h
 */
int	m_help(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered_user(sptr))
        	return 0;

#ifdef HELP_MESSAGE
	sendto_one(sptr, ":%s NOTICE %s :%s", me.name, sptr->name, HELP_MESSAGE);
#endif
	
	return 0;
}

/*
 * parv[1] = meaningless vestigial feature; means server if no parv[2]
 * parv[2] = server to query
 */
int	 m_lusers(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered_user(sptr))
		return 0;

	if (parv[1])
	{
		if (parv[2])
			parv[1] = parv[2];
		if (hunt_server(cptr, sptr, ":%s LUSERS * :%s", 1, parc, parv)
		    != HUNTED_ISME)
			return 0;
	}

        /* Update if necessary */
        if (Count.lastcheck+2 <= nowtime)
		update_lusers();

        /* Report */
        send_num(sptr, RPL_LUSERCLIENT, Count.visible, Count.invisible, Count.servers);
        if (Count.opers)
                send_num(sptr, RPL_LUSEROP, Count.opers);
        if (Count.unknown > 0)
                send_num(sptr, RPL_LUSERUNKNOWN, Count.unknown);
        if (Count.channels > 0)
                send_num(sptr, RPL_LUSERCHANNELS, Count.channels);
        send_num(sptr, RPL_LUSERME, Count.localu, Count.localserv);
        send_num(sptr, RPL_LOCALUSERS, Count.localu, Count.maxlocalu);
        send_num(sptr, RPL_GLOBALUSERS, Count.globalu, Count.maxglobalu);

        return 0;
}

/* update_lusers */
void	update_lusers()
{
	aClient	*acptr;

	Count.lastcheck = nowtime;

	Count.opers = Count.localu = Count.globalu = Count.visible = Count.invisible =
	Count.localserv = Count.servers = Count.unknown = Count.channels = 0;
#ifdef	USE_DVS
	Count.localdvs = Count.globaldvs = 0;
#endif

	for (acptr = client; acptr; acptr = acptr->next)
		switch (acptr->status)
		{
			case STAT_CLIENT:
				if (IsAnOper(acptr))
					Count.opers++;
				if (MyConnect(acptr))
					Count.localu++;
				if (!IsInvisible(acptr))
					Count.visible++;
				else
					Count.invisible++;
#ifdef USE_DVS
				if (IsDVS(acptr))
				{
					Count.globaldvs++;
					if (MyConnect(acptr))
						Count.localdvs++;
				}
#endif
				break;
			case STAT_SERVER:
				if (MyConnect(acptr))
					Count.localserv++;
			case STAT_ME:
				Count.servers++;
				break;
			default:
				Count.unknown++;
				break;
		}

	Count.globalu = Count.visible + Count.invisible;
	if(Count.localu > Count.maxlocalu)
		Count.maxlocalu = Count.localu;
	if(Count.globalu > Count.maxglobalu)
		Count.maxglobalu = Count.globalu;
	Count.channels = count_channels();
}
  
/***********************************************************************
 * m_connect() - Added by Jto 11 Feb 1989
 ***********************************************************************/

/*
** m_connect
**	parv[1] = servername
**	parv[2] = port number
**	parv[3] = remote server
*/
int	m_connect(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
	int	port, tmpport, retval;
	aConfItem *aconf, *cconf;
	aClient *acptr;

	if (check_registered(sptr) || check_privs(sptr, 0, 0))
                 return 0;
  
	if (parc > 3 && check_privs(sptr, 0, OFLAG_GLOBAL))
		return 0;
	if (parc <= 3 && check_privs(sptr, 0, OFLAG_LOCAL))
		return 0;   
	if (hunt_server(cptr,sptr,":%s CONNECT %s %s :%s",
		       3,parc,parv) != HUNTED_ISME)
		return 0;

	if (parc < 2 || *parv[1] == '\0')
		return check_params(sptr, "CONNECT", -1, 0);

	if ((acptr = hash_find_serv(parv[1])))
	    {
		sendto_one(sptr, ":%s NOTICE %s :Connect: Server %s %s %s.",
			   me.name, sptr->name, parv[1], "already exists from",
			   acptr->from->name);
		return 0;
	    }

	for (aconf = conf; aconf; aconf = aconf->next)
		if (aconf->status == CONF_CONNECT_SERVER &&
		    match(parv[1], aconf->name) == 0)
		  break;
	/* Checked first servernames, then try hostnames. */
	if (!aconf)
        	for (aconf = conf; aconf; aconf = aconf->next)
                	if (aconf->status == CONF_CONNECT_SERVER &&
                            (match(parv[1], aconf->host) == 0 ||
                             match(parv[1], index(aconf->host, '@')+1) == 0))
                  		break;

	if (!aconf)
	    {
	      sendto_one(sptr,
			 "NOTICE %s :Connect: Host %s not listed in ircd.conf",
			 sptr->name, parv[1]);
	      return 0;
	    }
	/*
	** Get port number from user, if given. If not specified,
	** use the default form configuration structure. If missing
	** from there, then use the precompiled default.
	*/
	tmpport = port = aconf->port;
	if (parc > 2 && !BadPtr(parv[2]))
	    {
		if ((port = atoi(parv[2])) <= 0)
		    {
			sendto_one(sptr,
				   "NOTICE %s :Connect: Illegal port number",
				   sptr->name);
			return 0;
		    }
	    }
	else if (port <= 0 && (port = PORTNUM) <= 0)
	    {
		sendto_one(sptr, ":%s NOTICE %s :Connect: missing port number",
			   me.name, sptr->name);
		return 0;
	    }

	/*
	** Notify all operators about remote connect requests
	*/
	if (!IsAnOper(cptr))
	{
		sendout_wallops("Remote CONNECT %s %s from %s",
			parv[1], parv[2]  ? parv[2] : "",
			sptr->name);
#if defined(USE_SYSLOG) && defined(SYSLOG_CONNECT)
		syslog(LOG_DEBUG, "CONNECT From %s : %s %d", sptr->name, parv[1], parv[2] ? parv[2] : "");
#endif
	}
	aconf->port = port;
	switch (retval = connect_server(aconf, sptr, NULL, 0))
	{
	case 0:
		sendto_one(sptr,
			   ":%s NOTICE %s :*** Connecting to %s[%s].",
			   me.name, sptr->name, aconf->host, aconf->name);
		break;
	case -1:
		sendto_one(sptr, ":%s NOTICE %s :*** Couldn't connect to %s.",
			   me.name, sptr->name, aconf->host);
		break;
	case -2:
		sendto_one(sptr, ":%s NOTICE %s :*** Host %s is unknown.",
			   me.name, sptr->name, aconf->host);
		break;
	default:
		sendto_one(sptr,
			   ":%s NOTICE %s :*** Connection to %s failed: %s",
			   me.name, sptr->name, aconf->host, strerror(retval));
	}
	aconf->port = tmpport;
	return 0;
    }

/* Message functions (wallops, gnotice, goper, globops, locops, chatops) */

/* m_wallops (+w)
 * parv[1] = message
 */
int	m_wallops(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered(sptr) || check_privs(sptr, 0, OFLAG_GLOBAL))
		return 0;
	if (!parv[1]) return check_params(sptr, "WALLOPS", -1, 0);
	sendto_ops_butone(IsServer(cptr)?cptr:NULL, sptr, "WALLOPS :%s", parv[1]); 
	return 0;
}

/* m_globops (+g)
 * For globops and gnotice
 */
int	m_globops(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered(sptr) || check_privs(sptr, 0, OFLAG_GLOBAL))
		return 0;
	if (!parv[1])
		return check_params(sptr, "GLOBOPS", -1, 0);
	sendto_serv_butone(cptr, ":%n %m :%s", sptr, &ms_globops, parv[1]);
	sendto_umode(UMODE_FAILOP, "*** Global (from %s): %s",
		sptr->name, parv[1]);
	return 0;
}

/* m_locops (+g)
 * Local only!
 */
int	m_locops(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered_user(cptr) || check_privs(sptr, 0, OFLAG_LOCAL))
		return 0;
	if (!parv[1])
		return check_params(sptr, "LOCOPS", -1, 0);
	sendto_umode(UMODE_FAILOP, "*** Local (from %s): %s",
		sptr->name, parv[1]);
	return 0;
}

/* m_chatops (+b)
 * parv[1] = message
 */
int	m_chatops(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered(sptr) || check_privs(sptr, UMODE_CHATOP, 0))
		return 0;
	if (!parv[1])
		return check_params(sptr, "CHATOPS", -1, 0);
	sendto_serv_butone(cptr, ":%n %m :%s", sptr, &ms_chatops, parv[1]);
	sendto_umode(UMODE_CHATOP, "*** ChatOps (from %s): %s", 
		sptr->name, parv[1]);
	return 0;
}
 
/* m_goper (+o)
 * parv[1] = message
 */
int     m_goper(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	if (check_registered(sptr) || check_privs(sptr, UMODE_SADMIN, 0))
		return 0;
	if (!parv[1])
		return check_params(sptr, "GOPER", -1, 0);
	sendto_serv_butone(cptr, ":%n %m :%s", sptr, &ms_goper, parv[1]);
	sendto_umode(UMODE_OPER, "*** Oper (from %s): %s", 
		sptr->name, parv[1]);
	return 0;
}

/* End of Message Functions */

/*
** m_time
**	parv[1] = servername
*/
int	m_time(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (check_registered_user(sptr))
		return 0;
	if (hunt_server(cptr,sptr,":%s TIME :%s",1,parc,parv) == HUNTED_ISME)
		send_num(sptr, RPL_TIME, me.name, date((long)0));
	return 0;
}

/*
** m_svskill
**	parv[1] = client
**	parv[2] = kill message
*/
int	m_svskill(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
 
	if (!IsULine(sptr))
		return 0;
	if (!parv[1] || (!(acptr = find_client(parv[1]))))
		return 0;
	if (!IsPerson(acptr))
		return 0;
	if (!parv[2]) parv[2] = "Services Kill";
	if (MyConnect(acptr))
		return exit_client(NULL, acptr, sptr, parv[2]);
	
	sendto_one(acptr, ":%n %m %s :%s", sptr, &ms_svskill, acptr->name,
		parv[2]);
	return 0;
}

/*
** m_admin
**	parv[1] = servername
*/
int	m_admin(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aConfItem *aconf;

	/* Users may want to get the address in case k-lined, etc. -- Barubary
	if (check_registered(sptr))
		return 0; */

	/* Only allow remote ADMINs if registered -- Barubary */
	if (IsPerson(sptr) || IsServer(cptr))
	if (hunt_server(cptr,sptr,":%s ADMIN :%s",1,parc,parv) != HUNTED_ISME)
		return 0;
	if ((aconf = find_admin()))
	    {
		send_num(sptr, RPL_ADMINME, me.name);
		send_num(sptr, RPL_ADMINLOC1,
			(aconf->host ? aconf->host : "-"));
		send_num(sptr, RPL_ADMINLOC2,
			(aconf->passwd ? aconf->passwd : "-"));
		send_num(sptr, RPL_ADMINEMAIL,
			(aconf->name ? aconf->name : "-"));
	    }
	else
		send_num(sptr, ERR_NOADMININFO, me.name);
	return 0;
}

/*
** m_rehash
**
*/
int	m_rehash(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	int x;
	
	if ((!MyClient(sptr) && !IsAnOper(sptr)) ||
		(MyClient(sptr) && !OPCanRehash(sptr)))
	    {
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	    }
	x = 0;
	if (!BadPtr(parv[1]))
	{
		if (check_privs(sptr, 0, OFLAG_REMOTE))
			return 0;
		if ((x=hunt_server(cptr,sptr,":%s REHASH %s",1,parc,parv))
				!= HUNTED_ISME)
			return 0;
	}

	if (cptr != sptr)
	{
#ifdef NO_REMOTE_REHASH
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
#endif
		sendout_wallops("%s is remotely rehashing server config file", sptr->name);
	}
	else
		sendto_ops("%s is rehashing server config file", sptr->name);
	send_num(sptr, RPL_REHASHING, configfile);
#ifdef USE_SYSLOG
	syslog(LOG_INFO, "REHASH From %s\n", get_client_name(sptr, FALSE));
#endif
	return rehash(cptr, sptr, 0);
}

/*
** m_restart
**
** parv[1] - password *OR* reason if no X:line
** parv[2] - reason for restart (optional & only if X:line exists)
**
** The password is only valid if there is a matching X line in the
** config file. If it is not,  then it becomes the 
*/
int	m_restart(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
    char *pass = NULL;
    
    if (check_registered(sptr))
	return 0;
    
    if (!MyClient(sptr) || !OPCanRestart(sptr))
	    return 0;
	
    if ((pass = find_restartpass()))
	{	
	    if (parc < 2)
	    	return check_params(sptr, "RESTART", -1, 0);
		
	    if (strcmp(pass, parv[1]))
		{
		    send_num(sptr, ERR_PASSWDMISMATCH);
		    return 0;
		}
	    /* Hack to make the code after this if { } easier: we assign the comment to the
	     * first param, as if we had not had an X:line. We do not need the password
	     * now anyways. Accordingly we decrement parc ;)	-- NikB
	     */
	    parv[1] = parv[2];
	    parc--;
	}
	
#ifdef USE_SYSLOG
	syslog(LOG_WARNING, "Server RESTART by %s - %s\n",
		get_client_name(sptr,FALSE), (parv[1]&&!pass ? parv[1] : "No reason"));
#endif	
	sendout_wallops("Server is restarting by request of %s", sptr->name);
	server_reboot((parv[1]&&!pass ? parv[1] : "No Reason"));
 	return 0;	
}

/*
** m_trace
**	parv[1] = servername
*/
int	m_trace(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	int	i;
	aClient	*acptr;
	aClass	*cltmp;
	char	*tname;
	int	doall, link_s[MAXCONNECTIONS], link_u[MAXCONNECTIONS];
	int	cnt = 0, wilds, dow;
	time_t  now;

	if (check_registered(sptr) || check_privs(sptr, 0, 0))
		return 0;

	if (parc > 2)
		if (hunt_server(cptr, sptr, ":%s TRACE %s :%s",
				2, parc, parv))
			return 0;

	if (parc > 1)
		tname = parv[1];
	else
		tname = me.name;

	switch (hunt_server(cptr, sptr, ":%s TRACE :%s", 1, parc, parv))
	{
	case HUNTED_ISME:
		break;
	default:
		return 0;
	}

	doall = !parv[1] || (hash_find_serv(tname) == &me);
	wilds = !parv[1] || index(tname, '*') || index(tname, '?');
	dow = wilds || doall;

	bzero(link_s, sizeof(link_s));
	bzero(link_u, sizeof(link_u));

	if (doall)
		for (acptr = client; acptr; acptr = acptr->next)
			if (IsPerson(acptr))
				link_u[acptr->from->fd]++;
			else if (IsServer(acptr))
				link_s[acptr->from->fd]++;

	/* report all direct connections */
	
	now = nowtime;
	for (i = 0; i <= highest_fd; i++)
	    {
		char	*name;
		int	class;

		if (!(acptr = local[i])) /* Local Connection? */
			continue;
 		if (!IsOper(sptr) &&
		    !IsAnOper(acptr) && (acptr != sptr))
			continue;
		if (!doall && wilds && match(tname, acptr->name))
			continue;
		if (!dow && mycmp(tname, acptr->name))
			continue;
		name = get_client_name(acptr,FALSE);
		class = get_client_class(acptr);

		switch(acptr->status)
		{
		case STAT_CONNECTING:
			send_num(sptr, RPL_TRACECONNECTING, class, name);
			cnt++;
			break;
		case STAT_HANDSHAKE:
			send_num(sptr, RPL_TRACEHANDSHAKE, class, name);
			cnt++;
			break;
		case STAT_ME:
			break;
		case STAT_UNKNOWN:
			send_num(sptr, RPL_TRACEUNKNOWN, class, name);
			cnt++;
			break;
		case STAT_CLIENT:
			/* Only opers see users if there is a wildcard
			 * but anyone can see all the opers.
			 */
			if (IsOper(sptr) ||
			    (IsAnOper(acptr) && !IsInvisible(acptr)))
			    {
				if (IsAnOper(acptr))
					send_num(sptr, RPL_TRACEOPERATOR,
						class, name,
						now - acptr->lasttime);
				else
					send_num(sptr, RPL_TRACEUSER,
						class, name,
						now - acptr->lasttime);
				cnt++;
			    }
			break;
		case STAT_SERVER:
			send_num(sptr, RPL_TRACESERVER, class, link_s[i],
				   link_u[i], name, *(acptr->s->by) ?
				   acptr->s->by : "*", "*", me.name,
				   now - acptr->lasttime);
			cnt++;
			break;
		case STAT_LOG:
			send_num(sptr, RPL_TRACELOG, LOGFILE, acptr->port);
			cnt++;
			break;
		default: /* ...we actually shouldn't come here... --msa */
			send_num(sptr, RPL_TRACENEWTYPE, name);
			cnt++;
			break;
		}
	    }
	/*
	 * Add these lines to summarize the above which can get rather long
         * and messy when done remotely - Avalon
         */
       	if (!IsAnOper(sptr) || !cnt)
	    {
		if (cnt)
			return 0;
		/* let the user have some idea that its at the end of the
		 * trace
		 */
		send_num(sptr, RPL_TRACESERVER, 0, link_s[me.fd],
			link_u[me.fd], me.name, "*", "*", me.name);
		return 0;
	    }
	for (cltmp = FirstClass(); doall && cltmp; cltmp = NextClass(cltmp))
		if (Links(cltmp) > 0)
			send_num(sptr, RPL_TRACECLASS, Class(cltmp), Links(cltmp));
	return 0;
    }

/*
** m_qotd - sets quote of the day
*/
int	m_qotd(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	int fd;
	
	if (check_registered(sptr))
		return 0;
	if (!MyClient(sptr) || !OPIsAdmin(sptr))
	{
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	}
	if (parc < 2)
		qotd[0] = '\0';
	else
		strncpy(qotd, parv[1], sizeof(qotd));
	qotd[sizeof(qotd)-1] = '\0';

	if ((fd = open(DPATH"/ircd.qotd", O_WRONLY|O_CREAT|O_TRUNC, 0600))<0)
	{
		sendto_one(sptr, ":%s NOTICE %s :Error opening "DPATH"/ircd.qotd",
			me.name, sptr->name);
		return 0;
	}
	(void)write(fd, qotd, strlen(qotd));
	(void)close(fd);
	sendto_one(sptr, ":%s NOTICE %s :Quote of the day has been %s%s.",
		me.name, sptr->name, parc < 2 ? "deleted" : "changed to: ",
		qotd);
	return 0;
}
	
	
/*
** m_motd
**	parv[1] = servername
*/
int	m_motd(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	int	fd, nr;
	char	line[80];
	char	*tmp;
	struct	stat	sb;
	struct	tm	*tm;

	if (check_registered(sptr))
		return 0;

	if (hunt_server(cptr, sptr, ":%s MOTD :%s", 1,parc,parv)!=HUNTED_ISME)
		return 0;
	fd = open(MOTD, O_RDONLY);
	if (fd == -1)
	    {
		send_num(sptr, ERR_NOMOTD);
		return 0;
	    }
	(void)fstat(fd, &sb);
	send_num(sptr, RPL_MOTDSTART, me.name);
	tm = localtime(&sb.st_mtime);
	sendto_one(sptr, ":%s %d %s :- %d/%d/%d %d:%02d", me.name, RPL_MOTD,
		   sptr->name, tm->tm_mday, tm->tm_mon + 1, 1900 + tm->tm_year,
		   tm->tm_hour, tm->tm_min);
	(void)dgets(-1, NULL, 0); /* make sure buffer is at empty pos */
	while ((nr=dgets(fd, line, sizeof(line)-1)) > 0)
	    {
	    	line[nr]='\0';
		if ((tmp = (char *)index(line,'\n')))
			*tmp = '\0';
		if ((tmp = (char *)index(line,'\r')))
			*tmp = '\0';
		send_num(sptr, RPL_MOTD, line);
	    }
	(void)dgets(-1, NULL, 0); /* make sure buffer is at empty pos */
	(void)close(fd);	
	
	if ((fd = open(DPATH"/ircd.qotd",O_RDONLY)))
	{
		(void)read(fd, qotd, sizeof(qotd)-1);
		(void)close(fd);
	}
	if (*qotd)
		sendto_one(sptr, ":%s %d %s :- Quote of the day: %s", me.name,
			RPL_MOTD, sptr->name, qotd);
	send_num(sptr, RPL_ENDOFMOTD);

	return 0;
    }

/*
** m_close - added by Darren Reed Jul 13 1992.
*/
int	m_close(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient	*acptr;
	int	i;
	int	closed = 0;

	if (check_registered(sptr))
	   return 0;

	if (!MyOper(sptr))
	    {
	    	send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	    }

	for (i = highest_fd; i; i--)
	    {
		if (!(acptr = local[i]))
			continue;
		if (!IsUnknown(acptr) && !IsConnecting(acptr) &&
		    !IsHandshake(acptr))
			continue;
		send_num(sptr, RPL_CLOSING, get_client_name(acptr, TRUE), acptr->status);
		(void)exit_client(acptr, acptr, acptr, "Oper Closing");
		closed++;
	    }
	send_num(sptr, RPL_CLOSEEND, closed);
	return 0;
}

/* m_die, this terminates the server, and it intentionally does not
 * have a reason. If you use it you should first do a GLOBOPS and 
 * then a server notice to let everyone know what is going down...
 */
int	m_die(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient	*acptr;
	int	i;
	char *pass = NULL;

        if (check_registered(sptr))
                 return 0; 
	   
	if (!MyClient(sptr) || !OPCanDie(sptr))
	    {
	    	send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	    }
	
	if ((pass = find_diepass())) 	/* See if we have and DIE/RESTART password */
	    {   
		if (parc < 2)		/* And if so, require a password :) */
			return check_params(sptr, "DIE", -1, 0);
	    
	    if (strcmp(pass, parv[1])) 
		{
		    send_num(sptr, ERR_PASSWDMISMATCH);
		    return 0;
		}    
	    }
	
	sendout_wallops("Server terminating by request of %s", sptr->name);
	
	for (i = 0; i <= highest_fd; i++)
	    {
		if (!(acptr = local[i]))
			continue;
		if (IsClient(acptr))
			sendto_one(acptr,
				   ":%s NOTICE %s :Server Terminating by request of %s",
				   me.name, acptr->name, sptr->name);
		else if (IsServer(acptr))
			sendto_one(acptr, ":%s ERROR :Terminated by %s",
				   me.name, get_client_name(sptr, TRUE));
	    }
	(void)s_die();
	return 0;
}

static char *dvs_help[] =
{
	"*** \2DarkFire Verification System (DVS)\2 ***",
	"DVS is a system designed to make IRC more secure by ensuring that abusive clients",
	"such as clones, floodbots, and spambots are deterred from using our network.",
	"",
	"If you are told that you must register/identify, this means your client meets the",
	"current requirements to be restricted (DVS Limited). Unless you identify, you will",
	"not be able to use IRC (most commands are blocked). After you register or identify,",
	"you will be able to use IRC as usual.",
	"",
	"To register your nickname on DarkFire (done once):",
	"(1) Visit http://verify.darkfire.net and enter your nickname",
	"(2) Type \037/msg NickServ register <password> <DVS Key from webpage>\037 on IRC",
	"",
	"To identify to your nickname (every other time you connect):",
	"Type \037/msg NickServ identify <password>\037",
	"",
	"For more info, visit "DVS_URL,
	"*** \2End of Help\2 ***",
	NULL,
};


/*
 * DVS Control
 * Allows operators with OFLAG_SUPER (S) and servers (namely services and
 * connecting uplinks) to change the DVS Status on a network-wide basis.
 */
int	m_dvs(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
#ifdef	USE_DVS
	int x;
	aClient *acptr;
#endif

	if(check_registered(cptr))
		return 0;

	if((parv[1] && *parv[1] == '?') || (!IsServer(cptr) && !(IsAnOper(cptr) && cptr->oflag&OFLAG_SUPER)))
		return dumpit(cptr, RPL_LISTSYNTAX, dvs_help);

	if(!parv[1])
		return 0;

#ifdef USE_DVS
	if(!strcasecmp(parv[1],"LOCK"))
	{
		if(DVSLock != 1)
		{
			sendto_realops("DVS Lock Status changed by %s (%s -> LOCK)", sptr->name, (DVSLock?"LOCK":"UNLOCK"));
			DVSLock = 1;
		}
	}
	else if(!strcasecmp(parv[1],"UNLOCK"))
	{
		if(DVSLock != 0)
		{
			sendto_realops("DVS Lock Status changed by %s (%s -> UNLOCK)", sptr->name, (DVSLock?"LOCK":"UNLOCK"));
			DVSLock = 0;
		}
	}
	else if (*parv[1] == '0' || *parv[1] == '1' || *parv[1] == '2' || *parv[1] == '3')
	{
		x = atoi(parv[1]);
		if(x < 0 || x > 3)
		{
			sendto_one(sptr,":%n NOTICE %n :Invalid DVS level", &me, sptr);
			return 0;
		}
		if(DVSStatus != x)
		{
			sendto_realops("DVS Status changed by %s (%d -> %d)", sptr->name, DVSStatus, x);
			DVSStatus = x;
		}
		if(parv[2])
		{
			if(!strcasecmp(parv[2],"LOCK"))
			{
				if(DVSLock != 1)
				{
					sendto_realops("DVS Lock Status changed by %s (%s -> LOCK)", sptr->name, (DVSLock?"LOCK":"UNLOCK"));
					DVSLock = 1;
				}
			}
			else if(!strcasecmp(parv[2],"UNLOCK"))
			{
				if(DVSLock != 0)
				{
					sendto_realops("DVS Lock Status changed by %s (%s -> UNLOCK)", sptr->name, (DVSLock?"LOCK":"UNLOCK"));
					DVSLock = 0;
				}
			}
		}
	}
	else if(MyClient(sptr))
	{
		sendto_one(sptr, ":%n NOTICE %n :DVS Syntax (1): /DVS (Numerical Level 0-3)", &me, sptr);
		sendto_one(sptr, ":%n NOTICE %n :DVS Syntax (2): /DVS (Numerical Level 0-3) (LOCK|UNLOCK)", &me, sptr);
		sendto_one(sptr, ":%n NOTICE %n :DVS Syntax (3): /DVS (LOCK|UNLOCK)", &me, sptr);
		sendto_one(sptr, ":%n NOTICE %n :DVS Syntax (4): /DVS ? -- Show DVS help for users", &me, sptr);
		return 0;
	}

	sendto_serv_butone(cptr, ":%n %m %d %s", sptr, &ms_dvs, DVSStatus, (DVSLock?"LOCK":"UNLOCK"));
#else
	if(parv[2])
		sendto_serv_butone(cptr, ":%n %m %s %s", sptr, &ms_dvs, parv[1], parv[2]);
	else
		sendto_serv_butone(cptr, ":%n %m %s", sptr, &ms_dvs, parv[1]);
#endif
	return 0;
}

/*
 * Allow
 * Accept a user (from DVS Status) without requiring +r or +o
 */
int	m_allow(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aClient *acptr;

	if(!IsServer(cptr) && (check_registered(cptr) || check_privs(cptr, 0, OFLAG_GLOBAL)))
		return 0;

	if(!parv[1] || !(acptr = find_client(parv[1])))
		return 0;

#ifdef	USE_DVS
	if(MyClient(sptr))
		sendto_one(sptr, ":%n NOTICE %n :Removing the DVSLIMIT flag from %n", &me, sptr, acptr);
	if(MyClient(acptr))
		sendto_one(acptr, ":%n NOTICE %n :*** DVS -- An operator (%n) has exempted you from DVS Protection. You may now use this network.", &me, acptr, sptr);
	remove_dvs(acptr,1);
#endif

	sendto_serv_butone(cptr, ":%n %m %s", sptr, &ms_allow, acptr->name);

	return 0;
}
