/************************************************************************
 *   IRC - Internet Relay Chat, ircd/hash.c
 *   Copyright (C) 1991 Darren Reed
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <limits.h>
#include "numeric.h"
#include "struct.h"
#include "common.h"
#include "sys.h"
#include "hash.h"
#include "h.h"

/*
** Hash tables rewritten by binary.
** These hash functions should be short and sweet... enjoy.
*/

#define lc(x) tolower((u_char)(x))

aClient *client_table[CLIENTHASHSIZE];
aChannel *channel_table[CHANNELHASHSIZE];
aClient *serv_table[SERVHASHSIZE];

    /****** INTIALIZE ALL HASH TABLES ******/

/*
 * hash_init()
 *  - initialize all hash tables
 */
void hash_init()
{
	hash_clear_client_table();
	hash_clear_channel_table();
	hash_clear_serv_table();
	clear_notify_hash_table();
}


    /************ FIND HASH KEY ************/

/*
 * hash_key(name)
 *  - assumed: name is not null
 *  - returns: case insensitive hash key value derived by name
 */
u_int32_t hash_key(char *name)
{
	u_int32_t val;
	u_char c;
	
	if (!(c = *name))
		return 0;
	
	val = c|0x20;
	
	while ((c = *++name))
		val = ((val<<5)|(val>>27))^(c|0x20);
		/* will compilers invoke bit rotation instructions? */
	
	return val;
}

/*
 * make_shortn(u_char *d, u_char *s)
 *  takes first 4 characters of that aren't dots (if they exist)
 *  put them in d.
 */
void make_shortn(u_char *d, u_char *s)
{
	int i;
	for (i = 4; i != 0; i--)
	{
		while (*s == '.') s++;
		if (!*s) break;
		*d = (*s)|0x20;
		d++;
		s++;
	}
	*d = '\0';
}

/*
 * serv_key(name)
 *  - returns: case insensitive servername hash from first four characters;
 *  will skip over a leading '.' if present 
 */
u_int32_t serv_key(char *s)
{
	int i;
	u_int32_t c = 0;
	
	for (i = 4; i != 0; i--)
	{
		while (*s=='.') s++;
		if (!*s) break;
		c <<= 8;
		c |= (u_int32_t)*s;
		s++;
	}
	
	return c|0x20202020;
}


    /************ CLIENT HASHING ************/

/*
 * hash_clear_client_table()
 *  - nullify the client hash table
 */
void hash_clear_client_table()
{
	int x;
	
	for (x = 0; x < CLIENTHASHSIZE; x++)
		client_table[x] = NULL;
}

/*
 * hash_add_client(cptr)
 *  - add this user to hash tables; key based on cptr->name
 */
void hash_add_client(aClient *cptr)
{
	aClient **acptr;
	
	if (cptr->hbase != NULL)
		return;
	
	acptr = &client_table[hash_key(cptr->name)%CLIENTHASHSIZE];
	cptr->hbase = acptr;
	cptr->hnext = *acptr;
	*acptr = cptr;
}

/*
 * hash_del_client(cptr)
 *  - remove this user from the hash tables
 *  - returns: 0 (cptr found and deleted from table), -1 (failure)
 */
int hash_del_client(aClient *cptr)
{
	aClient **acptr = cptr->hbase;
	
	for (; *acptr != cptr; acptr = &(*acptr)->hnext)
		if (!*acptr)
			return -1;
	
	*acptr = cptr->hnext;
	cptr->hbase = NULL;
	
	return 0;
}

/*
 * set_client_name(cptr, name)
 *  - sets new name for this client; does not matter whether this is a
 *  client or server, nor whether it is picking its first name or changing.
 *  should be universally useful and the extra checks will be only minimally
 *  CPU-intensive -- plus save a lot of headaches over the often negative
 *  side effects of duplicate hash entries :)
 */
void set_client_name(aClient *cptr, char *name)
{
	aClient **acptr = cptr->hbase;
	aClient **bcptr = &client_table[hash_key(name)%CLIENTHASHSIZE];

	if(cptr->name != name)
		strcpy(cptr->name, name);
	
	if (acptr == bcptr)
		return;
	if (acptr)
		for (; *acptr; acptr = &(*acptr)->hnext)
			if (*acptr == cptr)
			{
				*acptr = cptr->hnext;
				break;
			}
	
	if (!*cptr->name)
		return; /* blank name? simply eradicate from hash table. */
	
	cptr->hbase = bcptr;
	cptr->hnext = *bcptr;
	*bcptr = cptr;
}


/*
 * hash_find_client(name)
 *  - searches for client on the hash table with specified name
 *  - returns: pointer to client or NULL if not found
 */
aClient *hash_find_client(char *name)
{
	aClient **acptr, **bcptr, *cptr;
	char *s1, *s2;
	
	if (*name == '.')
		return hash_find_serv(name);

	bcptr = &client_table[hash_key(name)%CLIENTHASHSIZE];
	if (!(cptr = *bcptr))
		return NULL;
	
	for (s1=name, s2=cptr->name; lc(*s1) == lc(*s2); s1++, s2++)
		if (!*s1) return cptr;
	
	for (acptr = &cptr->hnext; (cptr = *acptr); acptr = &cptr->hnext)
	{
		for (s1=name, s2=cptr->name; lc(*s1) == lc(*s2); s1++, s2++)
		{
			if (!*s1)
			{
				*acptr = cptr->hnext;
				cptr->hnext = *bcptr;
				return (*bcptr = cptr);
			}
		}
	}
	return NULL;
}

/*
 * hash_find_nickserver(name, srvptr)
 *  - find user by name, who is on server specified by srvptr
 */
aClient *hash_find_nickserver(char *name, aClient *srvptr)
{
	aClient **acptr, **bcptr, *cptr;
	char *s1, *s2;

	bcptr = &client_table[hash_key(name)%CLIENTHASHSIZE];
	
	for (acptr = bcptr; (cptr = *acptr); acptr = &cptr->hnext)
	{
		if (cptr->srvptr != srvptr)
			continue;
		for (s1=name, s2=cptr->name; lc(*s1) == lc(*s2); s1++, s2++)
		{
			if (!*s1)
			{
				*acptr = cptr->hnext;
				cptr->hnext = *bcptr;
				return (*bcptr = cptr);				
			}
		}
	} 
	return NULL;
}


    /************ SERVER HASHING ************/

/*
 * hash_clear_serv_table()
 *  - nullify the serv hash table
 */
void hash_clear_serv_table()
{
	int x;
	
	for (x = 0; x < SERVHASHSIZE; x++)
		serv_table[x] = NULL;
}

/*
 * hash_add_serv(cptr)
 *  - add this user to hash tables; key based on cptr->name
 */
void hash_add_serv(aClient *cptr)
{
	aClient **acptr;
	
	acptr = &serv_table[cptr->s->servid%SERVHASHSIZE];
	cptr->s->hnext = *acptr;
	*acptr = cptr;
}

/*
 * hash_del_serv(cptr)
 *  - remove this user from the hash tables
 *  - returns: 0 (cptr found and deleted from table), -1 (failure)
 */
int hash_del_serv(aClient *cptr)
{
	aClient **acptr = &serv_table[cptr->s->servid%SERVHASHSIZE];
	
	for (; *acptr != cptr; acptr = &(*acptr)->s->hnext)
		if (!*acptr)
			return -1;
	
	*acptr = cptr->s->hnext;
	
	return 0;
}

/*
 * set_server_name(cptr, name)
 *  - set server's name to name, deal accordingly with hashes
 */
void set_server_name(aClient *cptr, char *name)
{
	set_client_name(cptr, name);
	make_shortn(cptr->s->shortn, name);
	cptr->s->sndot[0] = '.';
	cptr->s->servid = serv_key(cptr->s->shortn);
	hash_add_serv(cptr);
}

/*
 * hash_find_serv(name)
 *  - find server by the first four characters of its name
 */
aClient *hash_find_serv(char *name)
{
	u_int32_t k = serv_key(name);
	aClient *cptr;
	
	for (cptr = serv_table[k%SERVHASHSIZE]; cptr; cptr = cptr->s->hnext)
		if (k == cptr->s->servid)
			return cptr;
	
	return NULL;
}


    /************ CHANNEL HASHING ************/

/*
 * hash_clear_channel_table()
 *  - nullify channel hash table; prepare for use
 */
void hash_clear_channel_table()
{
	int x;
	
	for (x = 0; x < CHANNELHASHSIZE; x++)
		channel_table[x] = NULL;
}

/*
 * hash_add_channel(chptr)
 *  - add chptr to the channel hash table based on chptr->name
 */
void hash_add_channel(aChannel *chptr)
{
	aChannel **achptr;
	
	achptr = &channel_table[hash_key(chptr->chname)%CHANNELHASHSIZE];
	chptr->hnext = *achptr;
	*achptr = chptr;
}

/*
 * hash_del_channel(chptr)
 *  - remove chptr from the channel hash table
 *  - return: 0 (success), -1 (fail)
 */
int hash_del_channel(aChannel *chptr)
{
	aChannel **achptr;
	
	achptr = &channel_table[hash_key(chptr->chname)%CHANNELHASHSIZE];
	for (; *achptr != chptr; achptr = &(*achptr)->hnext)
		if (!*achptr) return -1;
	*achptr = chptr->hnext;
	return 0;
}

/*
 * hash_find_channel(name)
 *  - searches for channel on the hash table with specified name
 *  - returns: pointer to channel or NULL if not found
 */
aChannel *hash_find_channel(char *name)
{
	aChannel **achptr, **bchptr, *chptr;
	char *s1, *s2;

	bchptr = &channel_table[hash_key(name)%CHANNELHASHSIZE];
	if (!(chptr = *bchptr))
		return NULL;
	
	for (s1=name, s2=chptr->chname; lc(*s1) == lc(*s2); s1++, s2++)
		if (!*s1)
			return chptr;
	
	for (achptr = &chptr->hnext; (chptr = *achptr); achptr = &chptr->hnext)
	{
		for (s1=name, s2=chptr->chname; lc(*s1) == lc(*s2); s1++, s2++)
		{
			if (!*s1)
			{
				*achptr = chptr->hnext;
				chptr->hnext = *bchptr;
				return (*bchptr = chptr);
			}
		}
	}
	return NULL;
}

/*
 * hash_get_chan_bucket(n)
 *  - get the nth channel bucket; for use outside of hash.c
 */
aChannel *hash_get_chan_bucket(int n)
{
	if (n >= CHANNELHASHSIZE)
		return NULL;
	else
		return channel_table[n];
}



 /**** NOTIFY HASHING ****/

/* Quick & dirty inline version of mycmp for hash-tables -Donwulff */
#define thecmp(str1, str2, where) { \
                                    char *st1=str1, *st2=str2; \
                                    while (tolower(*st1)==tolower(*st2)) \
                                    { \
                                      if (!*st1) goto where; \
                                      st1++; st2++; \
                                    } \
                                  }

static	aNotify	*notifyTable[NOTIFYHASHSIZE];

/*
 * Hashing.
 *
 *   The server uses a chained hash table to provide quick and efficient
 * hash table mantainence (providing the hash function works evenly over
 * the input range).  The hash table is thus not susceptible to problems
 * of filling all the buckets or the need to rehash.
 *    It is expected that the hash table would look somehting like this
 * during use:
 *                   +-----+    +-----+    +-----+   +-----+
 *                ---| 224 |----| 225 |----| 226 |---| 227 |---
 *                   +-----+    +-----+    +-----+   +-----+
 *                      |          |          |
 *                   +-----+    +-----+    +-----+
 *                   |  A  |    |  C  |    |  D  |
 *                   +-----+    +-----+    +-----+
 *                      |
 *                   +-----+
 *                   |  B  |
 *                   +-----+
 *
 * A - GOPbot, B - chang, C - hanuaway, D - *.mu.OZ.AU
 *
 * The order shown above is just one instant of the server.  Each time a
 * lookup is made on an entry in the hash table and it is found, the entry
 * is moved to the top of the chain.
 */

void	clear_notify_hash_table(void)
{
	bzero((char *)notifyTable, sizeof(notifyTable));
}

/*
 * Rough figure of the datastructures for notify:
 *
 * NOTIFY HASH      cptr1
 *   |                |- nick1
 * nick1-|- cptr1     |- nick2
 *   |   |- cptr2                cptr3
 *   |   |- cptr3   cptr2          |- nick1
 *   |                |- nick1
 * nick2-|- cptr2     |- nick2
 *       |- cptr1
 *
 * add-to-notify-hash-table:
 * del-from-notify-hash-table:
 * hash-del-notify-list:
 * hash-check-notify:
 * hash-get-notify:
 */

/*
 * count_watch_memory
 */
void	count_watch_memory(u_int *countp, u_int *memoryp)
{
	int	i = NOTIFYHASHSIZE;
	aNotify	*anptr;
	u_int count = 0, memory = 0;

	do
	{
		anptr = notifyTable[--i];
		while (anptr)
		{
			count++;
			memory += sizeof(aNotify)+strlen(anptr->nick);
			anptr = anptr->hnext;
		}
	} while (i);
	
	*countp = count;
	*memoryp = memory;
}

/*
 * add_to_notify_hash_table
 */
int	add_to_notify_hash_table(nick, cptr)
char	*nick;
aClient	*cptr;
{
	int	hashv;
	aNotify	*anptr;
	Link	*lp;


	/* Get the right bucket... */
	hashv = hash_key(nick)%NOTIFYHASHSIZE;

	/* Find the right nick (header) in the bucket, or NULL... */
	if ((anptr = (aNotify *)notifyTable[hashv]))
		while (anptr && mycmp(anptr->nick, nick))
			anptr = anptr->hnext;

	/* If found NULL (no header for this nick), make one... */
	if (!anptr) {
		anptr = (aNotify *)MyMalloc(sizeof(aNotify)+strlen(nick));
		anptr->lasttime = 0;
		strcpy(anptr->nick, nick);

		anptr->notify = NULL;

		anptr->hnext = notifyTable[hashv];
		notifyTable[hashv] = anptr;
	}

	/* Is this client already on the notify-list? */
	if ((lp = anptr->notify))
		while (lp && (lp->value.cptr != cptr))
			lp = lp->next;

 	/* No it isn't, so add it in the bucket and client add it */
	if (!lp) {
		lp = anptr->notify;
		anptr->notify = make_link();
		anptr->notify->value.cptr = cptr;
		anptr->notify->next = lp;

		lp = make_link();
		lp->next = cptr->notify;
		lp->value.nptr = anptr;
		cptr->notify = lp;
		cptr->notifies++;
	}

	return 0;
}

/*
 * hash_check_notify
 */
int	hash_check_notify(cptr, reply)
aClient	*cptr;
int	reply;
{
	int	hashv;
	aNotify	*anptr;
	Link	*lp;


	/* Get us the right bucket */
	hashv = hash_key(cptr->name)%NOTIFYHASHSIZE;

	/* Find the right header in this bucket */
	if ((anptr = (aNotify *)notifyTable[hashv]))
		while (anptr && mycmp(anptr->nick, cptr->name))
			anptr = anptr->hnext;
	if (!anptr)
		return 0;	/* This nick isn't on notify */

	/* Update the time of last change to item */
	anptr->lasttime = time(NULL);

	/* Send notifies out to everybody on the list in header */
	for (lp = anptr->notify; lp; lp = lp->next)
		send_num(lp->value.cptr, reply, cptr->name,
			(IsPerson(cptr)?cptr->username:"<N/A>"),
			(IsPerson(cptr)?cptr->u->dhost:"<N/A>"),
			anptr->lasttime, cptr->info);

	return 0;
}

/*
 * hash_get_notify
 */
aNotify	*hash_get_notify(name)
char	*name;
{
	int	hashv;
	aNotify	*anptr;


	hashv = hash_key(name)%NOTIFYHASHSIZE;

	if ((anptr = (aNotify *)notifyTable[hashv]))
		while (anptr && mycmp(anptr->nick, name))
			anptr = anptr->hnext;

	return anptr;
}

/*
 * del_from_notify_hash_table
 */
int	del_from_notify_hash_table(nick, cptr)
char	*nick;
aClient	*cptr;
{
	int	hashv;
	aNotify	*anptr, *nlast = NULL;
	Link	*lp, *last = NULL;


	/* Get the bucket for this nick... */
	hashv = hash_key(nick)%NOTIFYHASHSIZE;

	/* Find the right header, maintaining last-link pointer... */
	if ((anptr = (aNotify *)notifyTable[hashv]))
		while (anptr && mycmp(anptr->nick, nick)) {
			nlast = anptr;
			anptr = anptr->hnext;
		}
	if (!anptr)
		return 0;	/* No such notify */

	/* Find this client from the list of notifies... with last-ptr. */
	if ((lp = anptr->notify))
		while (lp && (lp->value.cptr != cptr)) {
			last = lp;
			lp = lp->next;
		}
	if (!lp)
		return 0;	/* No such client to notify */

	/* Fix the linked list under header, then remove the notify entry */
	if (!last)
		anptr->notify = lp->next;
	else
		last->next = lp->next;
	free_link(lp);

	/* Do the same regarding the links in client-record... */
	last = NULL;
	if ((lp = cptr->notify))
		while (lp && (lp->value.nptr != anptr)) {
			last = lp;
			lp = lp->next;
		}

	/*
	 * Give error on the odd case... probobly not even neccessary
	 *
	 * No error checking in ircd is unneccessary ;) -Cabal95
	 */
	if (!lp)
		sendto_ops("WATCH debug error: del_from_notify_hash_table "
			   "found a watch entry with no client "
			   "counterpoint processing nick %s on client %s!",
			   nick, cptr->name);
	else {
		if (!last) /* First one matched */
			cptr->notify = lp->next;
		else
			last->next = lp->next;
		free_link(lp);
	}

	/* In case this header is now empty of notices, remove it */
	if (!anptr->notify) {
		if (!nlast)
			notifyTable[hashv] = anptr->hnext;
		else
			nlast->hnext = anptr->hnext;
		MyFree(anptr);
	}

	/* Update count of notifies on nick */
	cptr->notifies--;

	return 0;
}

/*
 * hash_del_notify_list
 */
int	hash_del_notify_list(cptr)
aClient	*cptr;
{
	int	hashv;
	aNotify	*anptr;
	Link	*np, *lp, *last;


	if (!(np = cptr->notify))
		return 0;	/* Nothing to do */

	cptr->notify = NULL;	/* Break the notify-list for client */
	while (np) {
		/* Find the notify-record from hash-table... */
		anptr = np->value.nptr;
		last = NULL;
		for (lp = anptr->notify; lp && (lp->value.cptr != cptr);
		     lp = lp->next)
			last = lp;

		/* Not found, another "worst case" debug error */
		if (!lp)
			sendto_ops("WATCH Debug error: hash_del_notify_list "
				   "found a WATCH entry with no table "
				   "counterpoint processing client %s!",
				   cptr->name);
		else {
			/* Fix the notify-list and remove entry */
			if (!last)
				anptr->notify = lp->next;
			else
				last->next = lp->next;
			free_link(lp);

			/*
			 * If this leaves a header without notifies,
			 * remove it. Need to find the last-pointer!
			 */
			if (!anptr->notify) {
				aNotify	*np2, *nl;

				hashv = hash_key(anptr->nick)%NOTIFYHASHSIZE;

				nl = NULL;
				np2 = notifyTable[hashv];
				while (np2 != anptr) {
					nl = np2;
					np2 = np2->hnext;
				}

				if (nl)
					nl->hnext = anptr->hnext;
				else
					notifyTable[hashv] = anptr->hnext;
				MyFree(anptr);
			}
		}

		lp = np;	/* Save last pointer processed */
		np = np->next;	/* Jump to the next pointer */
		free_link(lp);	/* Free the previous */
	}

	cptr->notifies = 0;

	return 0;
}

/*
 * NOTE: this command is not supposed to be an offical part of the ircd
 *       protocol.  It is simply here to help debug and to monitor the
 *       performance of the hash functions and table, enabling a better
 *       algorithm to be sought if this one becomes troublesome.
 *       -avalon
 *       don't know how the last one worked... imposed a very simple one
 *      in its place (sum of squares of counts for each bucket compared
 *      to total # of items), gives a slightly pessimistic estimate for
 *	number of compares for an average lookup
 *	 - added some more stuff to compare with ideal and best possible
 *	tot2 values
 */

void sendhashinfo(aClient *sptr, char *str, int size, int tot, int tot2)
{
	int avs, p_real, p_ideal, p_best;
	
	if (tot==0||tot2==0||size==0)
		return;
	
	avs = tot/size;
	p_real = tot2*1000/tot;
	p_ideal = (tot+size)*1000/size;
	p_best = ((size*avs*avs) + ((tot%size)*(2*avs+1)))*1000/tot;
	
	sendto_one(sptr, ":%n NOTICE %n :%s: %d/%d/%d (%d/%d/%d)", &me,
		sptr, str, tot2, tot, size, p_real, p_ideal, p_best);
}

int	m_hash(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	int i, tot, tot2, z, x;
	aClient *acptr;
	aChannel *chptr;
	
	if (!IsAnOper(sptr))
		return 0;
	
	sendto_one(sptr, ":%n NOTICE %n :HASH TABLE STATUS", &me, sptr);
	sendto_one(sptr, ":%n NOTICE %n :Type: tot2/tot/buckets (p_real/p_ideal/p_best)",
		&me, sptr);
	sendto_one(sptr, ":%n NOTICE %n :tot2 = sum of squares of # of items in each bucket",
		&me, sptr);
	sendto_one(sptr, ":%n NOTICE %n :p_real = tot2/tot*1000 (typical # of compares for 1000 hash lookups)",
		&me, sptr);
	sendto_one(sptr, ":%n NOTICE %n :p_ideal = ideal/predicted, p_best = best possible",
		&me, sptr);
	
	tot2 = tot = 0;
	for (i = 0; i < CLIENTHASHSIZE; i++)
	{
		x = 0;
		for (acptr = client_table[i]; acptr; acptr = acptr->hnext)
			x++;
		tot += x;
		tot2 += x*x;
	}
	sendhashinfo(sptr, "Clients", CLIENTHASHSIZE, tot, tot2);

	tot2 = tot = 0;
	for (i = 0; i < CHANNELHASHSIZE; i++)
	{
		x = 0;
		for (chptr = channel_table[i]; chptr; chptr = chptr->hnext)
			x++;
		tot += x;
		tot2 += x*x;
	}
	sendhashinfo(sptr, "Channels", CHANNELHASHSIZE, tot, tot2);


	tot2 = tot = 0;
	for (i = 0; i < SERVHASHSIZE; i++)
	{
		x = 0;
		for (acptr = serv_table[i]; acptr; acptr = acptr->s->hnext)
			x++;
		tot += x;
		tot2 += x*x;
	}
	sendhashinfo(sptr, "Servers", SERVHASHSIZE, tot, tot2);
}
