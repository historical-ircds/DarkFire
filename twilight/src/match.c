/*
 *   IRC - Internet Relay Chat, common/match.c
 *   Copyright (C) 1990 Jarkko Oikarinen
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef lint
static  char sccsid[] = "%W% %G% (C) 1988 University of Oulu, \
Computing Center and Jarkko Oikarinen";
#endif

#include "struct.h"
#include "common.h"
#include "sys.h"

/*
 *  Compare if a given string (name) matches the given
 *  mask (which can contain wild cards: '*' - match any
 *  number of chars, '?' - match any single character.
 *
 *	return	0, if match
 *		1, if no match
 */

u_char touppertab[], tolowertab[];
#define tolowertab2 tolowertab

#define lc(x) (tolower(x))
#define uc(x) (toupper(x))

/*
 * match()
 *  written by binary
 *
 *  returns:
 *    zero if matches
 *    nonzero if no match
 */
char match(u_char *m, u_char *n)
{
    u_char cm;
    u_char *wsn;
    u_char *wsm = NULL;
	
    for (; 1; m++, n++)
    {
        cm = *m;
        if (cm == '*')
        {
match_found_star:
            while ((cm = *++m) == '*') ;
            if (!cm) return cm;
            if (cm == '?')
            {
                do
                {
                   if (!*n) return cm;
                   m++;
                   n++;
                } while ((cm = *m) == '?');
                if (!cm) return cm;
                if (cm == '*') goto match_found_star;
            }
            for (cm = uc(cm); (uc(*n) != cm); n++)
                if (!*n) return cm;
            wsm = m;
            wsn = n;
            continue;
        }
        if (uc(*n) != uc(cm))
        {
            if (cm == '?')
            {
                if (!*n) return cm;
                continue;
            }
            if (!(m = wsm)) return 1;
            n = wsn;
            for (cm = uc(*m); (cm != uc(*++n));)
                if (!*n) return cm;
            wsn = n;
            continue;
        }
        if (!cm) return cm;
    }
}

/*
 * qmatch() - matches strings with *? wildcards but assumes input has already
 *  been formatted with collapse (that means the mask string is in all
 *  lowers, a ? will never be found immediately after a *, and that there
 *  will never be more than one asterisk in a chain).  also, this function
 *  handles well strings without wildcards that don't really need match()
 *  by shortcutting the no-* case.
 *
 *  returns:
 *    if match, NULL
 *    else, pointer to the character in the mask where a mismatch was found
 *    (the return value isn't guaranteed to be very useful!)
 */
char *qmatch(u_char *m, u_char *n)
{
    u_char cm;
    u_char cn;
    u_char *wsn;
    u_char *wsm;

    while(1)
    {
        if ((cm = *m) == '*') goto qmatch_found_star;
        if (lc((cn = *n)) != cm && (cm != '?' || !cn)) return m;
        if (!cm) return NULL;
        m++;
        n++;
    }
    
    while(1)
    {
        n++;
        cm = *++m;
        if (cm == '*')
        {
qmatch_found_star:
            if (!(cm = *++m)) return NULL;
            for (; (lc((cn = *n)) != cm); n++)
                if (!cn) return m;
            wsm = m;
            wsn = n;
            continue;
        }
        if (lc((cn = *n)) != cm)
        {
            if (!cn) return m;
            if (cm == '?') continue;
            n = wsn;
            cm = *wsm;
            while (lc((cn = *++n)) != cm)
                if (!cn) return m;
            m = wsm;
            wsn = n;
            continue;
        }
        if (!cm) return NULL;
    }
}

/*
 * take a mask string, ie "bLaH**?**?*?****.*.**" and do several things:
 *  - removing duplicate/unnecessary *'s
 *  - putting all ? marks before *'s
 *  - make all letters lowercase
 * in order to have a final result, in the case above, of "blah???*.*.*"
 *
 * The major purpose of this function is to make a mask usable by qmatch,
 * which shortcuts several cases.
 */
char *collapse(char *mask)
{
	char *s = mask, *d = mask;
	char c;
	
	while (1)
	{
		c = *s;
		if (c == '*')
		{
			/* following code looks a bit tricky; as an example, *
			 * we would want all of the following to convert     *
			 * to ????* : ????*, *????, ?*?*?*?*, **?**?**?**?*, *
			 * and so on.  count ? marks, put them all first,    *
			 * and at the end add only a SINGLE * character      */
			while (1)
			{
				if ((c = *++s) == '?')
					*d++ = '?';
				else if (c != '*')
					break;
			}
			*d++ = '*';
		}
		if (!(*d++ = lc(c)))
			break;
		s++;
	}
	return mask;
}


/*
 *  Case insensitive comparison of two NULL terminated strings.
 *
 *	returns	0 if s1 equal to s2, else nonzero
 */

u_char smycmp(u_char *s1, u_char *s2)
{
	for (; uc(*s1) == uc(*s2); s1++, s2++)
		if (!*s1) return 0;
	return 1;
}


u_char myncmp(u_char *s1, u_char *s2, int n)
{ 
	for (; uc(*s1) == uc(*s2); s1++, s2++)
		if (!*s1 || --n == 0) return 0;
	return (1);
}

void mystrupper(u_char *s)
{
	u_char c;
	
	for (; (c = *s); s++)
		*s = uc(c);
}

u_char tolowertab[] = {
  0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa,
  0xb, 0xc, 0xd, 0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14,
  0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
  0x1e, 0x1f,
  ' ', '!', '"', '#', '$', '%', '&', 0x27, '(', ')',
  '*', '+', ',', '-', '.', '/',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  ':', ';', '<', '=', '>', '?',
  '@', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
  'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
  't', 'u', 'v', 'w', 'x', 'y', 'z', '[', '\\', ']', '^',
  '_',
  '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
  'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
  't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',
  0x7f,
  0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
  0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
  0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
  0x20, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9,
  0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
  0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9,
  0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
  0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9,
  0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
  0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9,
  0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
  0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9,
  0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
  0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9,
  0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};

u_char touppertab[] = {
  0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa,
  0xb, 0xc, 0xd, 0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14,
  0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
  0x1e, 0x1f,
  ' ', '!', '"', '#', '$', '%', '&', 0x27, '(', ')',
  '*', '+', ',', '-', '.', '/',
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  ':', ';', '<', '=', '>', '?',
  '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
  'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
  'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^',
  0x5f,
  '`', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
  'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
  'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~',
  0x7f,
  0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
  0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
  0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99,
  0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f,
  0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9,
  0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
  0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9,
  0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
  0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9,
  0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
  0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9,
  0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,
  0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9,
  0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
  0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9,
  0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};

u_char char_atribs[] = {
/* 0-7 */	CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL,
/* 8-12 */	CNTRL, CNTRL|SPACE, CNTRL|SPACE, CNTRL|SPACE, CNTRL|SPACE,
/* 13-15 */	CNTRL|SPACE, CNTRL, CNTRL,
/* 16-23 */	CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL,
/* 24-31 */	CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL, CNTRL,
/* space */	PRINT|SPACE,
/* !"#$%&'( */	PRINT, PRINT, PRINT, PRINT, PRINT, PRINT, PRINT, PRINT,
/* )*+,-./ */	PRINT, PRINT, PRINT, PRINT, PRINT|ALLOW, PRINT|ALLOW, PRINT,
/* 012 */	PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW,
/* 345 */	PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW,
/* 678 */	PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW, PRINT|DIGIT|ALLOW,
/* 9:; */	PRINT|DIGIT|ALLOW, PRINT, PRINT,
/* <=>? */	PRINT, PRINT, PRINT, PRINT,
/* @ */		PRINT,
/* ABC */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* DEF */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* GHI */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* JKL */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* MNO */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* PQR */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* STU */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* VWX */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* YZ[ */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA,
/* \]^ */	PRINT|ALPHA, PRINT|ALPHA, PRINT|ALPHA,
/* _`  */	PRINT|ALLOW,PRINT,
/* abc */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* def */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* ghi */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* jkl */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* mno */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* pqr */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* stu */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* vwx */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW,
/* yz{ */	PRINT|ALPHA|ALLOW, PRINT|ALPHA|ALLOW, PRINT|ALPHA,
/* |}~ */	PRINT|ALPHA, PRINT|ALPHA, PRINT|ALPHA,
/* del */	0,
/* 80-8f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 90-9f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* a0-af */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* b0-bf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* c0-cf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* d0-df */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* e0-ef */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* f0-ff */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
