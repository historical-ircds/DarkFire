/************************************************************************
 *   IRC - Internet Relay Chat, common/send.c
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *		      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef lint
static  char sccsid[] = "@(#)send.c	2.32 2/28/94 (C) 1988 University of Oulu, Computing Center and Jarkko Oikarinen";
#endif

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "h.h"
#include "msg.h"
#include <stdio.h>
#ifdef _WIN32
#include <io.h>
#endif

#define NEWLINE	"\r\n"

static	char	nbuf[2048];
static	char	mbuf[2048];
static	char	sbuf[2048];

static struct
{
	int num; /* processors work fastest with their native int */
	int id[MAXCONNECTIONS];
} sent = { ~0, {} };

static	int	send_message PROTO((aClient *, char *, int));
static	void	init_sent();

#define	AlreadySent(i) (sent.id[i] == sent.num)
#define SetSent(i) (sent.id[i] = sent.num)

/* init_sent()
 *  - initialize the sent; increase sent.num but if it's already at the
 *  maximum value zero it first
 */
void init_sent()
{
	sent.num++;
	if (sent.num == 0)
	{
		bzero(&sent, sizeof(sent));
		sent.num++;
	}
}

/*
** dead_link
**	An error has been detected. The link *must* be closed,
**	but *cannot* call ExitClient (m_bye) from here.
**	Instead, mark it with FLAGS_DEADSOCKET. This should
**	generate ExitClient from the main loop.
**
**	If 'notice' is not NULL, it is assumed to be a format
**	for a message to local opers. It can contain only one
**	'%s', which will be replaced by the sockhost field of
**	the failing link.
**
**	Also, the notice is skipped for "uninteresting" cases,
**	like Persons and yet unknown connections...
*/
static	int	dead_link(to, notice)
aClient *to;
char	*notice;
{
	to->flags |= FLAGS_DEADSOCKET;
	/*
	 * If because of BUFFERPOOL problem then clean dbuf's now so that
	 * notices don't hurt operators below.
	 */
	DBufClear(&to->recvQ);
	DBufClear(&to->sendQ);
	if (!IsPerson(to) && !IsUnknown(to) && !(to->flags & FLAGS_CLOSING))
		sendto_ops(notice, get_client_name(to, FALSE));
	Debug((DEBUG_ERROR, notice, get_client_name(to, FALSE)));
	return -1;
}

/*
** flush_connections
**	Used to empty all output buffers for all connections. Should only
**	be called once per scan of connections. There should be a select in
**	here perhaps but that means either forcing a timeout or doing a poll.
**	When flushing, all we do is empty the obuffer array for each local
**	client and try to send it. if we cant send it, it goes into the sendQ
**	-avalon
*/
void	flush_connections(fd)
int	fd;
{
	int	i;
	aClient *cptr;

	if (fd == me.fd)
	    {
		for (i = highest_fd; i >= 0; i--)
			if ((cptr = local[i]) && DBufLength(&cptr->sendQ) > 0)
				(void)send_queued(cptr);
	    }
	else if (fd >= 0 && (cptr = local[fd]) && DBufLength(&cptr->sendQ) > 0)
		(void)send_queued(cptr);
}

/*
** send_message
**	Internal utility which delivers one message buffer to the
**	socket. Takes care of the error handling and buffering, if
**	needed.
*/
static	int	send_message(to, msg, len)
aClient	*to;
char	*msg;	/* if msg is a null pointer, we are flushing connection */
int	len;
{
	if (IsDead(to)||IsMe(to))
		return 0; /* This socket has already been marked as dead */
	if (DBufLength(&to->sendQ) > get_sendq(to))
	    {
		if (IsServer(to))
			sendto_ops("Max SendQ limit exceeded for %s: %d > %d",
			   	get_client_name(to, FALSE),
				DBufLength(&to->sendQ), get_sendq(to));
		return dead_link(to, "Max Sendq exceeded");
	    }
	else if (dbuf_put(&to->sendQ, msg, len) < 0)
		return dead_link(to, "Buffer allocation error for %s");
	/*
	** Update statistics. The following is slightly incorrect
	** because it counts messages even if queued, but bytes
	** only really sent. Queued bytes get updated in SendQueued.
	*/
	to->sendM += 1;
	me.sendM += 1;
	if (to->acpt != &me)
		to->acpt->sendM += 1;
	/*
	** This little bit is to stop the sendQ from growing too large when
	** there is no need for it to. Thus we call send_queued() every time
	** 2k has been added to the queue since the last non-fatal write.
	** Also stops us from deliberately building a large sendQ and then
	** trying to flood that link with data (possible during the net
	** relinking done by servers with a large load).
	*/
	if (DBufLength(&to->sendQ)>2048)
		send_queued(to);
	return 0;
}

/*
** send_queued
**	This function is called from the main select-loop (or whatever)
**	when there is a chance the some output would be possible. This
**	attempts to empty the send queue as far as possible...
*/
int	send_queued(to)
aClient *to;
{
	char	*msg;
	int	len, rlen;

#ifndef pyr
	if (IsBlocked(to)) return 0; /* Can't write to already blocked socket */
#endif /* pyr */

	/*
	** Once socket is marked dead, we cannot start writing to it,
	** even if the error is removed...
	*/
	if (IsDead(to))
	    {
		/*
		** Actually, we should *NEVER* get here--something is
		** not working correct if send_queued is called for a
		** dead socket... --msa
		*/
		return -1;
	    }
	while (DBufLength(&to->sendQ) > 0)
	    {
		msg = dbuf_map(&to->sendQ, &len);
					/* Returns always len > 0 */
		if ((rlen = deliver_it(to, msg, len)) < 0)
			return dead_link(to,"Write error to %s, closing link");
		(void)dbuf_delete(&to->sendQ, rlen);
		to->lastsq = DBufLength(&to->sendQ)/1024;
		if (rlen < len) {
			/* If we can't write full message, mark the socket
			 * as "blocking" and stop trying. -Donwulff */
			SetBlocked(to);
			break;
		}
	    }

	return (IsDead(to)) ? -1 : 0;
}

/* format a message w/ a va_list -- used internally */
int	vformat_message(char *buf, char *pattern, va_list vl)
{
	int i;
	
	i = myvsprintf(buf, pattern, vl);
	
	if (i > 506)
		i = 506;

	buf[i++] = '\r';
	buf[i++] = '\n';
	buf[i] = '\0';
	
	return i;
}

/*
 * format a message -- sprintf's to the buf, takes care of size,
 *  and puts in the \r and \n
 */
int format_message(char *buf, char *pattern, ...)
{
	va_list vl;
	int i;
	
	va_start(vl, pattern);
	i = vformat_message(buf, pattern, vl);
	va_end(vl);
	
	return i;
}

/* format a message with prefix w/ a va_list -- used internally */
int vformat_wprefix(aClient *from, char *buf, char *pattern, va_list vl)
{
	char *s = buf;
	
	if (*pattern != ':')
		if (!IsPerson(from))
			s += mysprintf(s, ":%s ", from->s->sndot);
		else
			s += mysprintf(s, ":%s!%s@%s ", from->name,
				from->username, from->u->host);
	s += myvsprintf(s, pattern, vl);
	
	if (s > buf+506)
		s = buf+506;
	*s++ = '\r';
	*s++ = '\n';
	*s = '\0';
	
	return s-buf;
}

/*
 * format message with prefix
 *  if pattern begins with a ':' format it as is; otherwise, tack the
 *  proper prefix for a user to receive from the given sender
 */
int format_wprefix(aClient *from, char *buf, char *pattern, ...)
{
	va_list vl;
	int i;
	
	va_start(vl, pattern);
	i = vformat_wprefix(from, buf, pattern, vl);
	va_end(vl);
	
	return i;
}

/*
** send message to single client
*/
void sendto_one(aClient *to, char *pattern, ...)
{
	va_list vl;
	int i;
	
	va_start(vl, pattern);
	i = vformat_message(sbuf, pattern, vl);
	va_end(vl);

	if (to->from)
		to = to->from;
	if (to->fd < 0)
	    {
		Debug((DEBUG_ERROR,
		      "Local socket %s with negative fd... AARGH!",
		      to->name));
	    }
	else if (IsMe(to))
	    {
		sendto_ops("Trying to send [%s] to myself!", sbuf);
		return;
	    }

	send_message(to, sbuf, i);
}

/*
 * sendto_prefix_one()
 *
 * to - destination client
 * from - client which message is from
 *
 *  - send message to the client "to" and tack on a prefix at the beginning
 * from the client "from" that is properly chosen based on user/server
 * status of to and from.
 *  - This function intentionally does not shorten the servernames to users.
 */
void sendto_prefix_one(aClient *to, aClient *from, char *pattern, ...)
{
	char *s = sbuf;
	va_list vl;
	
	if (MyClient(to) && IsPerson(from))
		s += mysprintf(s, ":%s!%s@%s ", from->name, from->username,
			from->u->host);
	else
		s += mysprintf(s, ":%s ", from->name);
	
	va_start(vl, pattern);
	s += myvsprintf(s, pattern, vl);
	va_end(vl);
	
	if (s > sbuf+506) s = sbuf+506;
	*s++ = '\r';
	*s++ = '\n';
	*s = '\0';
		
	send_message(to->from, sbuf, s-sbuf);
}

/*
 * send_num()
 *  - send a numeric to a person
 */
void send_num(aClient *to, int num, ...)
{
	char *s = sbuf;
	va_list vl;
	
	s += mysprintf(s, ":%s %03d %s ",
		IsServHash(to->from)?me.s->sndot:me.name, num,
		*to->name?to->name:"*");
	va_start(vl, num);
	if (!numerics[num]) // debugging -- hopefully this code can go!
	{
		s = strput(s, ":Numeric Error!");
		sendout_wallops("help, i'm gonna die, numeric error for %03d!?!", num);
	}
	else
		s += myvsprintf(s, &numerics[num][11], vl);
	va_end(vl);
	
	if (s > sbuf+506) s = sbuf+506;
	*s++ = '\r';
	*s++ = '\n';
	*s = '\0';
	
	send_message(to->from, sbuf, s-sbuf);
}

/*
 * send_str()
 *  - send a string literal to user without recopying the string
 */
void send_str(aClient *to, char *str, int len)
{
	if (len > 506)
		len = 506;
	str[len++] = '\r';
	str[len++] = '\n';
	str[len] = '\0';
	send_message(to->from, str, len);
}

/*
 * send_channel_msg()
 *
 *  sends specifically /msg and /notice and takes advantage of tokens
 */
void send_channel_msg(aClient *from, aChannel *chptr,
	char *cmd, char *tok, char *text)
{
	Member	*mp;
	aClient *acptr, *one = from->from;
	int len = 0;
	
	init_sent();
	for (mp = chptr->members; mp; mp = mp->nextu)
	{
	    if ((acptr = mp->cptr->from) == one) continue;
	    if (IsPerson(acptr))
	    {
	        if (!len)
	            len = format_wprefix(from, mbuf, "%s %s :%s",
	                cmd, chptr->chname, text);
	        send_message(acptr, mbuf, len);
	    }
	    else if (!AlreadySent(acptr->fd))
	    {
	    	SetSent(acptr->fd);
	        sendto_one(acptr, ":%s %s %s :%s", from->name, IsToken(acptr)
	            ? tok : cmd, chptr->chname, text);
	    }
	}
}

/*
 * send_channel_msg_byflag()
 *
 *  sends specifically /msg and /notice and takes advantage of tokens
 *  while matching member flags
 */
void send_channel_msg_byflag(aClient *from, aChannel *chptr, int flags,
	char *cmd, char *tok, char *text)
{
	char pf[4], *s;
	Member	*mp;
	aClient *acptr, *one = from->from;
	int	len = 0;
	
	s = pf;
	if (flags&CHFL_CHANOP) *s++ = '@';
	if (flags&CHFL_VOICE) *s++ = '+';
	*s = '\0';
	
	init_sent();
	for (mp = chptr->members; mp; mp = mp->nextu)
	{
	    if (!(mp->flags&flags) || ((acptr=mp->cptr->from)==one)) continue;
	    if (IsPerson(acptr))
	    {
	        if (!len)
	            len = format_wprefix(from, mbuf, "%s %s%s :%s",
	                cmd, pf, chptr->chname, text);
	        send_message(acptr, mbuf, len);
	    }
	    else if (!AlreadySent(acptr->fd))
	    {
	        SetSent(acptr->fd);
	        sendto_one(acptr, ":%s %s %s%s :%s", from->name, IsToken(acptr)
	            ? tok : cmd, pf, chptr->chname, text);
	    }
	}
}

/*
 * send a message to channel operators -- used for internally generated
 *  opnotices
 */
void sendto_channelops_butone(aClient *from, aChannel *chptr,
	char *pattern, ...)
{
	va_list vl;
	va_start(vl, pattern);
	myvsprintf(nbuf, pattern, vl);
	va_end(vl);
	send_channel_msg_byflag(from, chptr, CHFL_CHANOP, MSG_NOTICE,
		TOK_NOTICE, nbuf);
}

/*
 * sendto_serv_butone
 *
 * Send a message to all connected servers except the client 'one'.
 */
void sendto_serv_butone(aClient *one, char *pattern, ...)
{
	va_list vl;
	int i = 0;
	aClient *cptr;

	if (one) one = one->from;
	
	va_start(vl, pattern);
	for (cptr = me.s->down; cptr; cptr = cptr->s->side)
		if (cptr != one)
		{
			if (!i) i = vformat_message(sbuf, pattern, vl);
			send_message(cptr, sbuf, i);
		}
	va_end(vl);
}

/*
 * sendto_serv_butone_nproto
 *
 * Send a message to all connected servers except the client 'one' without
 * the specified protoctl flag
 */
void sendto_serv_butone_nproto(aClient *one, u_int proto, char *pattern, ...)
{
	va_list vl;
	int i = 0;
	aClient	*cptr;

	if (one) one = one->from;
	
	va_start(vl, pattern);
	for (cptr = me.s->down; cptr; cptr = cptr->s->side)
		if (!(cptr->proto&proto) && cptr != one)
		{
			if (!i) i = vformat_message(sbuf, pattern, vl);
			send_message(cptr, sbuf, i);
		}
	va_end(vl);
}

/*
 * sendto_serv_butone_wproto
 *
 * Send a message to all connected servers except the client 'one' with
 * the specified protoctl flag
 */
void sendto_serv_butone_wproto(aClient *one, u_int proto, char *pattern, ...)
{
	va_list vl;
	int i = 0;
	aClient	*cptr;

	if (one) one = one->from;
	
	va_start(vl, pattern);
	for (cptr = me.s->down; cptr; cptr = cptr->s->side)
		if ((cptr->proto&proto) && cptr != one)
		{
			if (!i) i = vformat_message(sbuf, pattern, vl);
			send_message(cptr, sbuf, i);
		}
	va_end(vl);
}

/*
 * sendto_channel_butserv
 *
 * Send a message to all members of a channel that are connected to this
 * server.  Prefix is tacked on, based on client from.
 */
void sendto_channel_butserv(aChannel *chptr, aClient *from,
	char *pattern, ...)
{
	Member	*mp;
	int	len = 0;
	va_list	vl;
	
	va_start(vl, pattern);
	for (mp = chptr->members; mp; mp = mp->nextu)
	    if (MyConnect(mp->cptr))
	    {
		if (!len) len = vformat_wprefix(from, sbuf, pattern, vl);
		send_message(mp->cptr, sbuf, len);
	    }
	va_end(vl);
}

/*
 * sendto_common_channels()
 *
 * Sends a message to all people (including user) on local server who are
 * in same channel with user.
 */
void sendto_common_channels(aClient *user, char *pattern, ...)
{
	aClient	*cptr;
	Member	*mp, *mp2;
	int	len;
	va_list	vl;
	
	va_start(vl, pattern);
	len = vformat_wprefix(user, mbuf, pattern, vl);
	va_end(vl);

	init_sent();
	for (mp = user->u->channel; mp; mp = mp->nextc)
 	    for (mp2 = mp->chptr->members; mp2; mp2 = mp2->nextu)
	    {
	    	cptr = mp2->cptr;
		if (!MyConnect(cptr) || AlreadySent(cptr->fd))
			continue;
		SetSent(cptr->fd);
		send_message(cptr, mbuf, len);
	    }
	if (MyConnect(user) && !AlreadySent(user->fd))
		send_message(user, mbuf, len);
}

/*
 * sendto_match_butone
 *
 * Send to all clients which match the specified server
 * Might seem a little wasteful to send to all servers...
 * but how often are these messages sent anyways?
 */
void	sendto_match_butone(one, from, mask, pattern, p1, p2, p3, p4)
aClient *one, *from;
char	*mask, *pattern, *p1, *p2, *p3, *p4;
{
	int i, forme;
	aClient *cptr;
	
	if (MyConnect(from) && !OPCanLNotice(from)) return;
	
	forme = !match(mask, me.name);
	
	for (i = 0; i <= highest_fd; i++)
	{
		if (!(cptr = local[i]) || cptr==one || IsMe(cptr)) continue;
		if (IsPerson(cptr) && !forme) continue;
		if (IsServer(cptr) && !OPCanGNotice(from)) continue;
		sendto_prefix_one(cptr, from, pattern, p1, p2, p3, p4);
	}
}

/* sendto_ops_butone
**	Send message to all +w users.
** one - client not to send message to
** from- client which message is from *NEVER* NULL!!
*/
void	sendto_ops_butone(one, from, pattern, p1, p2)
aClient *one, *from;
char	*pattern, *p1, *p2;
{
	int i;
	aClient *cptr;
	
	for (i=0; i <= highest_fd; i++)
		if ((cptr = local[i]) && SendWallops(cptr))
			sendto_prefix_one(cptr, from, pattern, p1, p2);
}

/*
 * sendto_umode_i
 *
 *	Send to users with all specified umodes, used internally.
 */
void	sendto_umode_i(u_int umodes, char *ppat, char *pattern, va_list vl)
{
	aClient *cptr;
	int i;
	
	mysprintf(nbuf, ppat, me.name, me.name);
	myvsprintf(mbuf, pattern, vl);
	
	if ((umodes & UMODE_OPER))
	{
	    umodes &= ~UMODE_OPER;
	    for (i = 0; i <= highest_fd; i++)
	        if ((cptr=local[i])&&IsAnOper(cptr)&&!((~cptr->umodes)&umodes))
	            sendto_one(cptr, nbuf, cptr->name, mbuf);
	}
	else
	{
	    for (i = 0; i <= highest_fd; i++)
	        if ((cptr=local[i])&&!((~cptr->umodes)&umodes)&&IsPerson(cptr))
	            sendto_one(cptr, nbuf, cptr->name, mbuf);
	}
}

/*
 * sendto_ops
 *
 *	Send to *local* ops only.
 */
void	sendto_ops(char *pattern, ...)
{
	va_list vl;
	
	va_start(vl, pattern);
	sendto_umode_i(UMODE_SERVNOTICE, ":%s NOTICE %%s :*** Notice -- %%s",
		pattern, vl);
	va_end(vl);
}
 
/*
 * sendto_umode
 *
 *	Send to *local* ops who have all the specified umodes.
 */
void	sendto_umode(u_int umodes, char *pattern, ...)
{
	va_list vl;
	
	va_start(vl, pattern);
	sendto_umode_i(umodes, ":%s NOTICE %%s :%%s", pattern, vl);
	va_end(vl);
}

/*
 * sendto_realops
 *
 *	Send to *ALL* local opers, not including +s nonopers.
 */
void	sendto_realops(char *pattern, ...)
{
	va_list vl;
	
	va_start(vl, pattern);
	sendto_umode_i(UMODE_OPER, ":%s NOTICE %%s :*** Notice -- %%s",
		pattern, vl);
	va_end(vl);
}

/*
 * sendout_wallops
 *
 *	Send out an internally generated wallops message.
 */
void	sendout_wallops(char *pattern, ...)
{
	va_list vl;

	va_start(vl, pattern);
	myvsprintf(nbuf, pattern, vl);
	va_end(vl);
	sendto_ops_butone(&me, &me, "WALLOPS :%s", nbuf);
}

/*
 * sendout_globops
 *
 *	Send out an internally generated globops message.
 */
void	sendout_globops(char *pattern, ...)
{
	va_list vl;

	va_start(vl, pattern);
	myvsprintf(mbuf, pattern, vl);
	sendto_serv_butone(&me, ":%s GLOBOPS :%s", me.name, mbuf);
	sendto_umode_i(UMODE_OPER|UMODE_FAILOP,
		":%s NOTICE %%s :*** Global -- from %s: %%s", pattern, vl);
	va_end(vl);
}
