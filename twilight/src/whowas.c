/************************************************************************
 *   IRC - Internet Relay Chat, ircd/whowas.c
 *   Copyright (C) 1990 Markku Savela
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * --- avalon --- 6th April 1992
 * rewritten to scrap linked lists and use a table of structures which
 * is referenced like a circular loop. Should be faster and more efficient.
 */

#ifndef lint
static  char sccsid[] = "@(#)whowas.c	2.16 08 Nov 1993 (C) 1988 Markku Savela";
#endif

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "whowas.h"
#include "h.h"

static	aName	was[NICKNAMEHISTORYLENGTH];
static	int	ww_index = 0;

void	add_history(cptr)
aClient *cptr;
{
	aName *np;
	
	np = &was[ww_index];
	if (++ww_index >= NICKNAMEHISTORYLENGTH)
		ww_index = 0;
	np->ww_online = cptr;
	strcpy(np->ww_nick, cptr->name);
	strcpy(np->ww_info, cptr->info);
	strcpy(np->ww_user, cptr->username);
	strcpy(np->ww_dhost, cptr->u->dhost);
	strcpy(np->ww_rhost, cptr->u->rhost);
	np->ww_host = IsDisguise(cptr) ? np->ww_dhost : np->ww_rhost;
	np->ww_name = np->ww_nick;
	np->ww_serv = cptr->srvptr->name;
	np->ww_logout = nowtime;
	np->ww_last = cptr->u->whowas;
	cptr->u->whowas = np;
}

void	off_history(cptr)
aClient *cptr;
{
	aName *np = cptr->u->whowas;
	
	for (; np && np->ww_online == cptr; np = np->ww_last)
		np->ww_online = NULL;
	
	cptr->u->whowas = NULL;
}

/*
 *  Put server on history list.  Then, find all references to the server's
 * name and replace them with a pointer to the location in the whowas list.
 */
void	add_server_history(cptr)
aClient	*cptr;
{
	aName *np;
	char *name = cptr->name;
	int i;
	
	np = &was[ww_index];
	if (++ww_index >= NICKNAMEHISTORYLENGTH)
		ww_index = 0;
	np->ww_online = NULL;
	strcpy(np->ww_info, cptr->info);
	strcpy(np->ww_user, "<server>");	 
	strcpy(np->ww_rhost, cptr->name);
	strcpy(np->ww_dhost, cptr->name);
	np->ww_host = np->ww_rhost;
	np->ww_name = np->ww_host;
	if (MyConnect(cptr) || !cptr->srvptr || !cptr->srvptr->name)
		np->ww_serv = me.name;
	else
		np->ww_serv = cptr->srvptr->name;
	np->ww_logout = nowtime;
	
	/*  Replace references to cptr->name to ref's to the server's *
	 * name in the *history* list.                                */
	for (i = 0; i < NICKNAMEHISTORYLENGTH; i++)
	{
		if (was[i].ww_serv == name)
			was[i].ww_serv = np->ww_rhost;
	}
}

aClient *get_history(nick, timelimit)
char *nick;
time_t timelimit;
{
	int i = ww_index;
	time_t limit = nowtime - timelimit;
	
	do
	{
		if (--i < 0)
			i = NICKNAMEHISTORYLENGTH-1;
		if (was[i].ww_online && !mycmp(was[i].ww_name, nick))
			return was[i].ww_online;

	} while (was[i].ww_logout >= limit && i != ww_index);
	return NULL;
}

void	initwhowas()
{
	static char noserv[] = "noserv";
	Reg1	int	i;
	
	for (i = 0; i < NICKNAMEHISTORYLENGTH; i++)
	{
		bzero((char *)&was[i], sizeof(aName));
		was[i].ww_name = was[i].ww_nick;
		was[i].ww_serv = noserv;
	}
	ww_index = 0;
	return;
}

 /********************************************************************
  * m_whowas                                                         *
  ********************************************************************/
 
char *ww_help[] =
{
	"*** Usage for /whowas *** ",
	" /whowas <mask> [<max#>]",
	"This command lists a limited set of past users that match the",
	" mask given.  It lists up to <max#> entries if the field is",
	" specified, otherwise 32 is assumed.",
	"The mandatory mask field contains a mask in the format",
	" [nick](!)[[user@]host.name][/gcos]",
	"   examples:",
	"    /whowas CoderGuy [match nick only]",
	"    /whowas ppp123.blah.com [hostname only]",
	"    /whowas henryj@ [ident only]",
	"    /whowas henryj@*.blah.com [match ident and host]",
	"    /whowas *oder*!*hen*@*.blah.com [match nick, ident, and host]",
	"    /whowas /henry?jay [match realname only]",
	"    /whowas *code*!henryj@*.blah.com/henry?jay [match all fields]",
	"*** End of help ***",
	NULL
};

/* chk_mask(x)
 *  makes x null if string is either "", "*", or "?*"
 */
#define CHK_MASK(x) (x=(x&&*x&&(*x!='*'||x[1])&&(*x!='?'||x[1]!='*'||x[2]))?x:NULL)

/*
** m_whowas
**	parv[0] = sender prefix
**	parv[1] = nickname queried
*/
int	m_whowas(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	char buf[USERLEN+HOSTLEN+2];
	aName	*wp, *wp2 = NULL;
	int	max = 32, j;
	char	*p = parv[1], *s;
	int	oper = IsAnOper(sptr);
	char	*nick, *user, *host, *real;
	aClient	*acptr;

	if (!p || (!p[1] && *p=='?'))
 		return dumpit(cptr, RPL_LISTSYNTAX, ww_help);

	if (parc > 2)
	{
		max = atoi(parv[2]);
		if (parc > 3)
		{
			if (hunt_server(cptr,sptr,":%s WHOWAS %s %s :%s", 3,parc,parv))
				return 0;
			if (max > 2)
				max = 2;
		}
		else if (max > 64 && !oper)
			max = 64;
	}
	
	while ((s = strsplit(&p, ',')))
	{
		nick = NULL;
		user = NULL;
		host = NULL;
		real = NULL;
		
		if ((*s == '$') && (acptr = find_person(s+1)))
		{
			strcpy(buf, acptr->u->host); 
			s = buf;
		}
		collapse(s);
		if ((real = index(s, '/')))
			*real++ = '\0';
		if ((host = index(s, '@')))
		{
			*host++ = '\0';
			if ((user = index(s, '!')))
			{
				*user++ = '\0';
				nick = s;
			}
			else
				user = s;
		}
		else if (index(s, '.'))
			host = s;
		else
			nick = s;
		
		CHK_MASK(nick);
		CHK_MASK(user);
		CHK_MASK(host);
		CHK_MASK(real);
		
		wp = wp2 = &was[ww_index - 1];
		j = 0;
		do {
			if (wp < was)
				wp = &was[NICKNAMEHISTORYLENGTH - 1];

			if (nick && qmatch(nick, wp->ww_name))
				continue;
			if (user && qmatch(user, wp->ww_user))
				continue;
			if (real && qmatch(real, wp->ww_info))
				continue;
			if (host && (qmatch(host, wp->ww_rhost)
			   && ((wp->ww_host==wp->ww_rhost&&!oper)
			   || qmatch(host, wp->ww_dhost))))
				continue;
			
			send_num(sptr, RPL_WHOWASUSER, wp->ww_name,
				wp->ww_user, wp->ww_host, wp->ww_info);
			
			if (oper)
			{
			    if (wp->ww_host!=wp->ww_rhost)
			        send_num(sptr, RPL_WHOISRHOST,
			            wp->ww_name, wp->ww_rhost);
			    if (wp->ww_online)
			        send_num(sptr, RPL_WHOWASNOW, wp->ww_name,
			            wp->ww_online->name);
			}
			
			send_num(sptr, RPL_WHOISSERVER, wp->ww_name,
				wp->ww_serv, myctime(wp->ww_logout));
			
			if (++j >= max)
				break;
		} while (--wp != wp2);

		if (j == 0)
			send_num(sptr, ERR_WASNOSUCHNICK, nick?nick:"*",
				   user?user:"*", host?host:"*", real?real:"*");
		if (p)
			p[-1] = ',';
		send_num(sptr, RPL_ENDOFWHOWAS, nick?nick:"*", user?user:"*",
			host?host:"*", real?real:"*");
	}
	return 0;
}
