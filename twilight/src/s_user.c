/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_user.c (formerly ircd/s_msg.c)
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   See file AUTHORS in IRC package for additional names of
 *   the programmers. 
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef lint
static  char sccsid[] = "@(#)s_user.c	2.74 2/8/94 (C) 1988 University of Oulu, \
Computing Center and Jarkko Oikarinen";
#endif

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "msg.h"
#include "channel.h"
#include <sys/stat.h>
#ifndef _WIN32
#include <utmp.h>
#else
#include <io.h>
#endif
#include <fcntl.h>
#include "h.h"

void	send_umode_out PROTO((aClient*, aClient *, int));
void	send_svsmode_out PROTO((aClient*, aClient *, aClient *, int));
void	send_umode PROTO((aClient *, aClient *, int, int, char *));
static	is_silenced PROTO((aClient *, aClient *));

static char buf[BUFSIZE], buf2[BUFSIZE], buf3[BUFSIZE];

DA_MASTER_D(away_mast, AWAYLEN+1, 4072);

/*
** m_functions execute protocol messages on this server:
**
**	cptr	is always NON-NULL, pointing to a *LOCAL* client
**		structure (with an open socket connected!). This
**		identifies the physical socket where the message
**		originated (or which caused the m_function to be
**		executed--some m_functions may call others...).
**
**	sptr	is the source of the message, defined by the
**		prefix part of the message if present. If not
**		or prefix not found, then sptr==cptr.
**
**		(!IsServer(cptr)) => (cptr == sptr), because
**		prefixes are taken *only* from servers...
**
**		(IsServer(cptr))
**			(sptr == cptr) => the message didn't
**			have the prefix.
**
**			(sptr != cptr && IsServer(sptr) means
**			the prefix specified servername. (?)
**
**			(sptr != cptr && !IsServer(sptr) means
**			that message originated from a remote
**			user (not local).
**
**		combining
**
**		(!IsServer(sptr)) means that, sptr can safely
**		taken as defining the target structure of the
**		message in this server.
**
**	*Always* true (if 'parse' and others are working correct):
**
**	1)	sptr->from == cptr  (note: cptr->from == cptr)
**
**	2)	MyConnect(sptr) <=> sptr == cptr (e.g. sptr
**		*cannot* be a local connection, unless it's
**		actually cptr!). [MyConnect(x) should probably
**		be defined as (x == x->from) --msa ]
**
**	parc	number of variable parameter strings (if zero,
**		parv is allowed to be NULL)
**
**	parv	a NULL terminated list of parameter pointers,
**
**			parv[0], sender (prefix string), if not present
**				this points to an empty string.
**			parv[1]...parv[parc-1]
**				pointers to additional parameters
**			parv[parc] == NULL, *always*
**
**		note:	it is guaranteed that parv[0]..parv[parc-1] are all
**			non-NULL pointers.
*/


/*
** hunt_server
**
**	Do the basic thing in delivering the message (command)
**	across the relays to the specific server (server) for
**	actions.
**
**	Note:	The command is a format string and *MUST* be
**		of prefixed style (e.g. ":%s COMMAND %s ...").
**		Command can have only max 8 parameters.
**
**	server	parv[server] is the parameter identifying the
**		target server.
**
**	*WARNING*
**		parv[server] is replaced with the pointer to the
**		real servername from the matched client (I'm lazy
**		now --msa).
**
**      redone by binary, though works almost the same:
**		- check if there's the server parameter, if not assume me
**		- first check if there's a client with that name explicitly
**		or if it matches a server hash (first four characters)
**		- if all else fails, go through the server list and do match
**	
**	returns: (see #defines)
*/
int	hunt_server(aClient *cptr, aClient *sptr, char *command, int server,
		int parc, char *parv[])
{
	aClient *acptr;
	char *mask;

	/*
	** Assume it's me, if no server
	*/
	if (parc <= server || !(mask = parv[server]))
		return (HUNTED_ISME);
	/*
	 * find the client exactly or try matching the first 4 characters
	 * with server hashes; if we get a possible fake direction we quit
	 * with HUNTED_NOSUCH but don't send any error message.
	 */
	if (!(acptr = find_client(mask)) && !(acptr = hash_find_serv(mask)))
	{
		collapse(mask);
		for (acptr = firstserv; acptr; acptr = acptr->s->next)
			if (!qmatch(mask, acptr->name))
				break;
		if (!acptr)
		{
			send_num(sptr, ERR_NOSUCHSERVER, mask);
			return(HUNTED_NOSUCH);
		}
 	}
	if (IsMe(acptr) || MyClient(acptr))
		return HUNTED_ISME;
	if (acptr->from == cptr)
		return HUNTED_NOSUCH;
	if (IsServHash(acptr->from))
		parv[server] = (IsServer(acptr)?acptr:acptr->srvptr)->s->sndot;
	else
		parv[server] = acptr->name;
	sendto_one(acptr, command, parv[0], parv[1], parv[2], parv[3],
		parv[4], parv[5], parv[6], parv[7], parv[8]);
	return(HUNTED_PASS);
}


#ifdef NOSPOOF
/*
** start_nospoof
**
**  Start the nospoof process.
*/
void start_nospoof(aClient *sptr)
{
	/*
	 * Client setting NICK the first time.
	 */
	sptr->nospoof = myrand();
	if (sptr->nospoof==0) sptr->nospoof = 0xC001BEED;

	sendto_one(sptr, "NOTICE %s :*** If you are having problems"
		" connecting due to ping timeouts, please"
		" type /notice %X nospoof now.",
		sptr->name, sptr->nospoof, sptr->nospoof);
	sendto_one(sptr, "PING :%X", sptr->nospoof);

#ifdef CONTACT_EMAIL
	sendto_one(sptr, ":%s NOTICE %s :*** If you need assistance with a"
	    " connection problem, please email " CONTACT_EMAIL
	    " with the name and version of the client you are"
	    " using, and the server you tried to connect to: %s",
	    me.name, sptr->name, me.name);
#endif /* CONTACT_EMAIL */
#ifdef CONTACT_URL
	sendto_one(sptr, ":%s NOTICE %s :*** If you need assistance with"
	    " connecting to this server, %s, please refer to: "
	    CONTACT_URL, me.name, sptr->name, me.name);
#endif /* CONTACT_URL */	
}
#endif


/*
 * do_username
 *
 *   Set up the user's username, filter out offensive chars, etc.
 */
void do_username(aClient *cptr, char *username)
{
	char uname[USERLEN+1];
	char *u2, *u1 = cptr->username;
	
	if ((cptr->flags & FLAGS_GOTID))
		strncpyzt(uname, cptr->username, USERLEN+1);
	else 
	{
		strncpyzt(uname, username, USERLEN);
		if ((cptr->flags&FLAGS_DOID))
			*u1++ = '~'; /* put it in final nick */
	}

	for (u2 = uname; *u2; u2++)
		if (isallowed(*u2))
			*u1++ = *u2;
	*u1 = '\0';
	if (!*cptr->username) /* blank, or no chars accepted */
		strcpy(cptr->username, "unknown");
}


/*
** 'do_nick_name' ensures that the given parameter (nick) is
** really a proper string for a nickname (note, the 'nick'
** may be modified in the process...)
**
**	RETURNS the length of the final NICKNAME (0, if
**	nickname is illegal)
**
**  Nickname characters are in range
**	'A'..'}', '_', '-', '0'..'9'
**  anything outside the above set will terminate nickname.
**  In addition, the first character cannot be '-'
**  or a Digit.
**
**  Note:
**	'~'-character should be allowed, but
**	a change should be global, some confusion would
**	result if only few servers allowed it...
*/
int	do_nick_name(char *nick)
{
	char *ch;

	if (*nick == '-' || isdigit(*nick)) /* first character in [0..9-] */
		return 0;

	for (ch = nick; *ch && (ch - nick) < NICKLEN; ch++)
		if (!isvalid(*ch))
			break;

	*ch = '\0';

	return (ch - nick);
}


/*
** change_nick
**  - change user's nick, send necessary protocol messages, reset various
**  flags, update the hash table, do watch notification, add to whowas list...
*/
extern	void change_nick(aClient *cptr, aClient *sptr, char *nick, time_t ts)
{ 
	Member *mp;
	
	if (!IsPerson(sptr))
		return;

	add_history(sptr);
	
	sendto_serv_butone(cptr, ":%s NICK %s %d", sptr->name, nick, ts);
	sendto_common_channels(sptr, "NICK %s", nick);

	if (ts!=0)
		sptr->u->nickts = ts;
	
	sptr->umodes &= ~UMODE_REGNICK;
	for (mp = sptr->u->channel; mp; mp = mp->nextc)
		mp->flags &= ~CHFL_BANSET;

	hash_check_notify(sptr, RPL_LOGOFF);
	set_client_name(sptr, nick);
	hash_check_notify(sptr, RPL_LOGON);
}

/*
** m_svsnick
**	parv[1] = old nickname
**	parv[2] = new nickname
**	parv[3] = timestamp
*/
int	m_svsnick(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	time_t ts;
	aClient *acptr, *ac2ptr;

	if (!IsULine(sptr) || parc < 4)
		return 0;

	if (!(acptr = find_client(parv[1])))
		return 0;
	if (MyClient(acptr))
	{
		if (find_client(parv[2]) || strlen(parv[2]) > NICKLEN)
			return exit_client(NULL, acptr, sptr, "SVSNICK collision");
		ts = atoi(parv[3]);
		change_nick(NULL, acptr, parv[2], ts?ts:nowtime);
	}
	else if (acptr->from != cptr)
		sendto_one(acptr, ":%s SVSNICK %s %s :%s", parv[0],
			parv[1], parv[2], parv[3]);
	
	return 0; 
}

/*
** register_user
**	This function is called when both NICK and USER messages
**	have been accepted for the client, in whatever order.
*/
static int register_user(aClient *cptr, aClient *sptr, char *username)
{
	char	*parv[3], *tmpstr;
	anUser	*user = sptr->u;
	aClient *nsptr, *acptr;
	int	i;

	if (MyConnect(sptr))
	{
		sptr->ulast = nowtime;
		if ((i = check_client(sptr)))
		{
			sendto_umode(UMODE_OPER|UMODE_CLIENT,
				"*** Notice -- %s from %s.",
				i == -3 ? "Too many connections" :
				"Unauthorized connection",
				get_client_host(sptr));
			return exit_client(cptr, sptr, &me, i == -3 ?
			  "This server is full.  Please try irc.darkfire.net" :
			  "You are not authorized to connect to this server");
		}
		
		if (!BadPtr(sptr->confs->value.aconf->passwd))
		{
			if (!StrEq(sptr->passwd,sptr->confs->value.aconf->passwd))
			{
				send_num(sptr, ERR_PASSWDMISMATCH);
				return exit_client(cptr, sptr, &me, "Bad Password");
			}
			else *sptr->passwd = '\0';
		}
		
		do_username(sptr, username);
		
		if (sptr->hostp)
		{
			/* No control-chars or ip-like dns replies... I cheat :)
			   -- OnyxDragon */
			for (tmpstr = sptr->sockhost; *tmpstr > ' ' &&
				*tmpstr < 127; tmpstr++);
			if (*tmpstr || isdigit(*(tmpstr-1)))
				strncpyzt(sptr->u->rhost, (char *)inetntoa((char *)&sptr->ip), HOSTLEN+1); /* Fix the sockhost for debug jic */
			else
				strncpyzt(sptr->u->rhost, sptr->sockhost, HOSTLEN+1);
		}
		else
			strncpyzt(sptr->u->rhost, sptr->sockhost, HOSTLEN+1);
  
		if (find_kill(sptr))
			return exit_client(cptr, sptr, &me, "K-lined");
	}
	else
		strncpyzt(sptr->username, username, USERLEN+1);
	
	SetClient(sptr); /* set this client's status as a person */
	
	/* Disguise host AHEAD of time... */
	disguise(sptr->u->dhost, sptr->u->rhost);
	
	/* Try to shorten the NICK msg to upgraded servers...             *
	 * only the first 4 chars of the servname transmitted; tokens too */
	sendto_serv_butone_wproto(cptr, PROTO_SERVHASH,
	  TOK_NICK" %s 0 %d %s %s %s %d :%s", sptr->name,
	  sptr->u->nickts, sptr->username, sptr->u->host,
	  sptr->srvptr->s->shortn, sptr->u->sstamp, sptr->info);
	sendto_serv_butone_nproto(cptr, PROTO_SERVHASH,
	  MSG_NICK" %s %d %d %s %s %s %d :%s", sptr->name, sptr->hopcount+1,
	  sptr->u->nickts, sptr->username, sptr->u->host, sptr->srvptr->name,
	  sptr->u->sstamp, sptr->info);
	  
	hash_check_notify(sptr, RPL_LOGON);

	if (MyConnect(sptr))
	{
		parv[1] = parv[2] = NULL;
		send_umode_out(cptr, sptr, 0);
		if (sptr->passwd[0])
		{
			if ((nsptr = find_person(NickServ)))
				sendto_one(nsptr, ":%s PRIVMSG %s :SIDENTIFY %s",
				sptr->name, NickServ, sptr->passwd);
			bzero(sptr->passwd, sizeof(sptr->passwd));
		}

		send_num(sptr, RPL_WELCOME,
			sptr->name, sptr->username, sptr->u->host);
		send_num(sptr, RPL_YOURHOST, get_client_name(&me, FALSE),
			version);
		send_num(sptr, RPL_CREATED, creation);
		send_num(sptr, RPL_MYINFO, me.name, version);
		send_num(sptr, RPL_PROTOCTL, PROTOCTL_SUPPORTED);
		m_lusers(sptr, sptr, 1, parv);
		m_motd(sptr, sptr, 1, parv);
		sendto_umode(UMODE_OPER|UMODE_CLIENT,"*** Notice -- Client connecting on port %d: %s (%s@%s)",
			sptr->acpt->port, sptr->name, sptr->username,
			sptr->u->host);
	}
	else if (IsULine(sptr->srvptr))
		sptr->flags |= FLAGS_ULINE;

	return 0;
}


/*
** m_nick
**	parv[1] = nickname
**  if from new client  -taz
**	parv[2] = nick password
**  if from server:
**      parv[2] = now open, since hopcount is derived from server
**      parv[3] = timestamp
**      parv[4] = username
**      parv[5] = hostname
**      parv[6] = servername
**      parv[7] = sstamp
**	parv[8] = info
*/
int	m_nick(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aConfItem *aconf;
	aSqlineItem *asqline;
	aClient *acptr, *serv;
	char	nick[NICKLEN+2], *s, c;
	Member	*mp;
	time_t	nickts = (time_t)0;
	int	differ, welose;
	
	if (parc < 2)
	{
		send_num(sptr, ERR_NONICKNAMEGIVEN);
		return 0;
	}

	strncpyzt(nick, parv[1], NICKLEN+1);
	
	if (IsServer(cptr) && (parc > 7 && (!(serv = hash_find_serv(parv[6]))
		|| serv->from != cptr->from)))
	    return 0;	

	/* make sure nick name is valid */
	if (strlen(nick)>NICKLEN || (!IsServer(cptr) && do_nick_name(nick)==0))
	{
		send_num(sptr, ERR_ERRONEOUSNICKNAME, parv[1],
			"Illegal characters");
		if (IsServer(cptr))
		{
			sendto_umode(UMODE_JUNK, "Bad Nick: %s From: %s %s",
				   parv[1], sptr->name, get_client_name(cptr, FALSE));
			sendto_one(cptr, ":%s KILL %s :%s (Illegal Nick)",
				   me.name, parv[1], me.name);
			if (sptr != cptr)
			{ /* bad nick change */
				sendto_serv_butone(cptr,
					":%s KILL %s :%s (Bad Nick Change)",
					me.name, sptr->name, me.name);
				sptr->flags |= FLAGS_KILLED;
				return exit_client(cptr,sptr,&me,"Bad Nick");
			}
		}
		return 0;
	}

	if (IsPerson(cptr))
	{ /* local users -- qlines, sqlines, and flood prots */
		if (((aconf = find_conf_name(nick, CONF_QUARANTINED_NICK))
			|| (asqline = find_sqline_match(nick))))
		{
			if (!IsAnOper(sptr))
			{
				if(aconf)
				 send_num(sptr, ERR_ERRONEOUSNICKNAME, nick,
				   BadPtr(aconf->passwd) ? "forbidden" :
				   aconf->passwd);
	    			else if(asqline)
				 send_num(sptr, ERR_ERRONEOUSNICKNAME, nick,
				   BadPtr(asqline->reason) ? "forbidden" :
				   asqline->reason);
				return 0; /* NICK message ignored */
		  	}
			sendto_realops ("Q-lined nick %s from %s.", nick, sptr->name);
		}
		if (!IsOper(cptr))
			cptr->since += 3; /* Nick-flood prot. -Donwulff */
	}

	if ((acptr = find_client(nick)))
	{
		if (acptr == sptr)
		{ /* same user changing case - return if no case change */
			if (!strcmp(acptr->name, nick))
				return 0;
		}
		else if (IsUnknown(acptr) && MyConnect(acptr))
		{ /* override my user who has sent nick but no user */
			if (acptr == cptr)
				return 0;
			acptr->flags |= FLAGS_KILLED;
			exit_client(NULL, acptr, &me, "Overridden");
		}
		else if (IsPerson(cptr) || IsUnknown(cptr))
		{ /* my user */
			send_num(sptr, ERR_NICKNAMEINUSE, nick);
			return 0;
		}
		else if (cptr == acptr->from)
		{ /* users with same nick on same side of net - fix ghost */
			exit_client(cptr, acptr, &me, "Ghosted");
		}
		/*
		 * Nick collision.  If the user on our side of the network is
		 * wrong, we remove the user.  Otherwise, if it's a nick
		 * change, kill their user user under the old nick on our
		 * side and ignore the NICK, assuming they will acknowledge
		 * their loss.  Actively nuking their user will result in
		 * massive desyncing -- it's been tried before.
		 */
		else if (IsServer(sptr))
		{
			differ = 1;
			welose = 0;
			if (parc>3)
			{
				nickts = IsULine(sptr)?0:atoi(parv[3]);
				if (parc > 5)
				 differ = (mycmp(acptr->username, parv[4]) ||
					mycmp(acptr->u->host, parv[5]));
			}
			else
				nickts = acptr->u->nickts;
			if (nickts == acptr->u->nickts)
				welose = -1; /* both die */
			else if (differ==(acptr->u->nickts > nickts))
				welose = 1; /* mine dies */
			if (welose != 0)
			{
				sendto_serv_butone(cptr, ":%s KILL %s :%s (Nick Collision (=))",
					me.name, acptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(cptr, acptr, &me, "Nick Collision");
			}
			if (welose != 1) /* don't accept... */
				return 0; /* hope other server handles it */
		}
		else /* a NICK change has collided */
		{
			differ = 1;
			welose = 0;
			nickts = (parc >= 2) ? atoi(parv[2]) : acptr->u->nickts;
			differ = (mycmp(acptr->username, sptr->username) ||
				mycmp(acptr->u->rhost, sptr->u->rhost));
			if (nickts == acptr->u->nickts)
				welose = -1; /* both lose */
			else if (differ == (acptr->u->nickts > nickts))
				welose = 1; /* just ours loses */
			if (welose != 0)
			{
				/* our user lost - kill our user */
				sendto_serv_butone(cptr, ":%s KILL %s :%s (Nick Collision)",
					me.name, acptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(NULL, acptr, &me, "Nick Collision");
			}
			if (welose != 1)
			{
				/* their user lost - let my servers know */
				sendto_serv_butone(cptr, ":%s KILL %s :%s (Nick Collision)",
					me.name, sptr->name, me.name);
				acptr->flags |= FLAGS_KILLED;
				exit_client(NULL, sptr, &me, "Nick Collision");
				return 0;
	 		}
		}
	}

	/*
	** We are now going to accept the nick and handle it appropriately.
	*/
	if (IsUnknown(sptr))
	{ /* user on my server and choosing first nick */
		set_client_name(sptr, nick);
		sptr->u->nickts = nowtime;		
#ifdef NOSPOOF
		start_nospoof(sptr);
#endif /*NOSPOOF*/

		/* Copy password to the passwd field if it's given after NICK
		 * - originally by taz, modified by Wizzu
		 */
		if((parv[2]) && (strlen(parv[2]) < sizeof(sptr->passwd)))
			strcpy(sptr->passwd,parv[2]);

		if (GotUser(sptr) && IsNotSpoof(sptr))
			return register_user(cptr, sptr, sptr->username);
	}
	else if (IsPerson(sptr))
	{ /* this is only a nick change. */
		if (IsPerson(cptr))
		    for (mp = cptr->u->channel; mp; mp = mp->nextc)
			if (cannot_do_nick(mp, nick))
			    return 0;

		if (mycmp(sptr->name, nick))
			sptr->u->nickts = (IsPerson(cptr) || !parv[2]) ?
				nowtime:atoi(parv[2]);
		
		change_nick(cptr, sptr, nick, sptr->u->nickts);
	}
	else if (IsServer(sptr))
	{ /* a server introducing a new client, replace source w/ new user */
		sptr = make_client(cptr, serv);
		add_client_to_list(sptr);
		sptr->hopcount = serv->hopcount;
		if (parc > 3)
			sptr->u->nickts = atoi(parv[3]);
		else
			sptr->u->nickts = nowtime;
		set_client_name(sptr, nick);
		if (parc>7)
		{
			parv[3]=nick;
			return m_user(cptr, sptr, parc-3, &parv[3]);
		}
	}

	
	return 0;
}

/*
** m_user
**	parv[1] = username (login name, account)
**	parv[2] = client host name (used only from other servers)
**	parv[3] = server host name (used only from other servers)
**	parv[4] = users real name info
*/
int	m_user(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*host, *server;
	anUser	*user;
 
	if (!IsUnknown(sptr))
	{
		send_num(sptr, ERR_ALREADYREGISTRED);
		return 0;
	}

	if (parc > 2 && (host = (char *)index(parv[1],'@')))
		*host = '\0'; 
	if (parc <= 4)
		return check_params(sptr, "USER", 0, 0);
	
	sptr->flags |= FLAGS_GOTUSER;

	user = make_user(sptr);
	user->sstamp = (parc>=6&&IsServer(cptr))?atol(parv[4]):0;
	strncpyzt(user->rhost, parv[2], HOSTLEN+1);
	strncpyzt(sptr->info, parv[parc-1], sizeof(sptr->info));

	if (sptr->name[0] && (IsServer(cptr) ? 1 : IsNotSpoof(sptr)))
		return register_user(cptr, sptr, parv[1]);
	
	strncpyzt(sptr->username, parv[1], USERLEN+1);
	
	return 0;
}


/*
 * check_privmsg(sptr, acptr, text)
 *  - Checks to see if sptr is trying to send a dcc to acptr and returns -1
 *  and prints appropriate notices if sptr is not allowed to send it.
 *  - Expanded to do other things.
 */
int check_privmsg(aClient *sptr, aClient *acptr, char *text)
{
	Member *mp;
	
	if (!IsRegNick(sptr) && (acptr->umodes&UMODE_LIMITREG))
	{
		/* maybe in the future use a numeric? */
		sendto_one(sptr, ":%s NOTICE %s :Cannot send to %s (+L)",
			me.name, sptr->name, acptr->name);
		return -1;
	}
	
	/* This checks for mIRC-style CTCP's only --                *
	 * we assume the first char will be a ^A or it's not a DCC. */
	
	if (!MyClient(sptr)) return 0;
	
	if (*text == 1 && !myncmp(text+1, "DCC SEND", 8) && !IsAnOper(sptr))
	  for (mp = sptr->u->channel; mp; mp = mp->nextc)
	    if ((mp->chptr->mode.mode&MODE_NODCCS)&&!(mp->flags&CHFL_OVERLAP))
	    {
	      /* allow moderate DCC sending... stop if they try to send
	       * a third one in less than 30 secs, or a fourth in less than
	       * 150 secs, fifth in 270, etc... and let the penalties keep
	       * accumulating so infected users will never be able to send.
	       */
	      sptr->lastdcc += 120;
	      if (sptr->lastdcc<nowtime)
	      {
	        if (sptr->lastdcc < nowtime-210)
	          sptr->lastdcc = nowtime-210;
	      }
	      else
	      {
	        sendto_one(sptr, ":%s NOTICE %s :%s%s%s%s%s%d%s", me.name,
	          sptr->name, "Channel ", mp->chptr->chname,
	          " prohibits rapid DCC sending.  Please /part ",
	          mp->chptr->chname, " or wait ",
	          sptr->lastdcc-nowtime, " seconds to send.");
	        return -1;
	      }
	    }
	return 0;
}

static void *targetlist[16];
static int targetnum;

/*
 * use this instead of CPU-heavy canonize...
 */
int duplicate_target(void *target)
{
	int i;
	
	if (targetnum >= 16)
		return -1;
	
	for (i = targetnum; i != 0;)
		if (targetlist[--i] == target)
			return -1;
	
	targetlist[targetnum++] = target;
	
	return 0;
}


/*
** m_message (used in m_private() and m_notice())
** the general function to deliver MSG's between users/channels
**
**	parv[1] = receiver list
**	parv[2] = message text
**
** massive cleanup
** rev argv 6/91
**
*/
static	int	m_message(cptr, sptr, parc, parv, notice)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
int	notice;
{
	aClient *acptr;
	aChannel *chptr;
	char *nick, *server, *p, *s, send;
	char *cmd=msgptr->cmd, *tok=msgptr->token;
	int flags;
	
	targetnum = 0;

	if (parc < 3)
	{
		if (parc < 2)
			send_num(sptr, ERR_NORECIPIENT, cmd);
		else
			send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if (IsPerson(sptr) && sptr->u->hush)
		return 0; /* don't allow foreign hushed users to send msgs */
	
	for (p = parv[1]; nick = strsplit(&p, ',');)
	{
		/* channel? now must start with # */
		if ((*nick == '#'))
		{
			if (!(chptr = hash_find_channel(nick)))
				goto m_msg_nosuch;
			if (duplicate_target(chptr))
				continue;
			if ((send = can_send(sptr, chptr, parv[2], notice)) == 0)
				send_channel_msg(sptr, chptr, cmd, tok, parv[2]);
			else if (!notice && send != 1)
				send_num(sptr, ERR_CANNOTSENDTOCHAN, nick, send);
			continue;
		}
		
		/* @[+]#channel addressed? */
		if (nick[0] == '@')
		{
			flags = CHFL_CHANOP;
			nick++;
			if (*nick == '+')
			{
				flags |= CHFL_VOICE;
				nick++;
			}
			
			if (!(chptr = hash_find_channel(nick)))
				goto m_msg_nosuch;
			if (duplicate_target(chptr))
				continue;
			if ((send=can_send(sptr, chptr, parv[2], 1)) != 0)
			{
				if (send!=1)
					send_num(sptr, ERR_CANNOTSENDTOCHAN, nick, send);
				continue;
			}
			send_channel_msg_byflag(sptr, chptr,
				flags, cmd, tok, parv[2]);
			continue;
		}
		
		/* $servermask addressed? */
		if ((*nick == '$') && IsAnOper(sptr))
		{
			sendto_match_butone(IsServer(cptr) ? cptr : NULL, 
				sptr, nick + 1, "%s %s :%s",
				cmd, nick, parv[2]);
			continue;
		}
		

		/* nickname addressed? */		 
		if ((acptr = find_person(nick)))
		{
			if (IsPerson(cptr) && duplicate_target(acptr))
				continue;
			if (IsPerson(cptr) && !notice)
			{
			    if (check_privmsg(sptr, acptr, parv[2])<0)
 			        continue;
			    else if (acptr->u->hush && !IsAnOper(sptr))
				send_num(sptr, RPL_USERISHUSHED,
					acptr->name, acptr->u->hush->s);
			    else if (acptr->u->away)
				send_num(sptr, RPL_AWAY,
					acptr->name, acptr->u->away);
			}
			if (!is_silenced(sptr, acptr))
			{
				sendto_prefix_one(acptr, sptr, "%s %s :%s",
					IsToken(acptr->from) ?
					tok : cmd, nick, parv[2]);
			}
			continue;
		}

		/* user@server addressed? */
		if ((server = index(nick, '@')) &&
			(acptr = hash_find_serv(server+1)))
		{
			if (!IsMe(acptr))
			{
				sendto_one(acptr, ":%s %s %s :%s", sptr->name,
					cmd, nick, parv[2]);
				continue;
			}

			*server = '\0';
			acptr = hash_find_nickserver(nick, &me);
			if (acptr)
			{
				if (!duplicate_target(acptr))
					sendto_prefix_one(acptr, sptr,
						"%s %s :%s", cmd,
						acptr->name, parv[2]);
				continue;
			}
		}

m_msg_nosuch:
		send_num(sptr, ERR_NOSUCHNICK, nick);
	}
	return 0;
}


/*
 * m_private
 *	parv[1] = receiver list
 *	parv[2] = message text
 */
int	m_private(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (MyConnect(sptr))
		cptr->ulast = nowtime; /* update idle time */
	if (check_registered_user(sptr)) return 0;
	return m_message(cptr, sptr, parc, parv, 0);
}


/*
 * m_notice
 *	parv[1] = receiver list
 *	parv[2] = notice text
 */
int	m_notice(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
#ifdef NOSPOOF
	if (!IsRegistered(cptr) && (cptr->name[0]) && !IsNotSpoof(cptr))
	{
		if (BadPtr(parv[1])) return 0;
		if (strtoul(parv[1], NULL, 16) == cptr->nospoof)
		{
			sptr->nospoof = 0;
			if (GotUser(sptr))
				return register_user(cptr, sptr,
					sptr->username);
			return 0;
		}
	}
#endif /*NOSPOOF*/
	if (check_registered(sptr)) return 0;
	return m_message(cptr, sptr, parc, parv, 1);
}


 /******************************************************************
  * m_who segment                                                  *
  ******************************************************************/

static char *w_help[] =
{
	"*** Usage for /who *** ",
	" /who <mask> [<flags>]",
	"The mandatory mask field contains either:",
	" - channel name (ex. #channel), lists users in channel",
	" - general format [nick](!)[[user@][host.name]][:server][/gcos]",
	" lists all noninvisible users that match all specified fields",
	" ex: /who CoderGuy [match nick exact]",
	"     /who ppp123.blah.com [match host exact]",
	"     /who henryj@ [match ident exact]",
	"     /who henryj@*.blah.com [match ident and host]",
	"     /who Coder*!*henry*@*.blah.com [match nick, ident, and host]",
	"     /who *:server.darkfire.net [match server]",
	"     /who /henry?jay [match real name only]",
	"     /who Code*!henryj@*.blah.com:server.darkfire.net/henry?jay",
	"      [match all fields]",
	"The optional flags field can contain:",
	" - flags that combine and require all to be true for a match:",
	"     o - IRC operator       w - mode +w",
	"     A - server admin       a - services admin",
	"     G - /away              H - here",
	"     & - local to this server",
	" ex: /who * o lists all IRC operators online",
	" - flags that test channel status for /who #channel only",
	"     @ - channel operator   + - voice",
	"     (multiple chan flags require only one match, logical OR)",
	" ex: /who #darkfire @ lists all channel operators on #darkfire",
	"*** End of help ***",
	NULL
};

char *w_nick, *w_user, *w_host, *w_real;
char w_isoper;

static	int	who_match(aClient *acptr)
{
	if (w_nick && qmatch(w_nick, acptr->name))
		return 0;
	if (w_user && qmatch(w_user, acptr->username))
		return 0;
	if (w_host && (qmatch(w_host, acptr->u->rhost)
	   && ((!IsDisguise(acptr) && !w_isoper)
	    || qmatch(w_host, acptr->u->dhost))))
		return 0;
	if (w_real && qmatch(w_real, acptr->info))
		return 0;
	return 1;
}

static	void	do_who(aClient *sptr, aClient *acptr, Member *mp)
{
	static char status[10];
	char *s = status;

	*s++ = acptr->u->away ? 'G' : 'H';
	if (IsInvisible(acptr))
		*s++ = '%';
	if (IsAnOper(acptr))
	{
		*s++ = '*';
		if (IsSAdmin(acptr))
			*s++ = 'a';
		if (IsAdmin(acptr))
			*s++ = 'A';
	}
	if (IsHushed(acptr))
		*s++ = '-';
	if (mp && mp->flags&CHFL_OVERLAP)
		*s++ = (mp->flags&CHFL_CHANOP) ? '@' : '+';
	*s = '\0';
	send_num(sptr, RPL_WHOREPLY, (mp) ? (mp->chptr->chname) : "*",
		acptr->username, acptr->u->host, acptr->srvptr->s->sndot,
		acptr->name, status, acptr->hopcount, acptr->info);
}

Member *find_who_chan(aClient *acptr)
{
	Member *mp;
	
	for (mp = acptr->u->channel; mp; mp = mp->nextc)
		if (PubChannel(mp->chptr)) return mp;

	return (w_isoper)?acptr->u->channel:NULL; /* 1 channel for ircops */
}

#define MAX_WHOREPLIES	64
#define CHK_WHO_USER(x)	(						    \
	(((x)->umodes&userflags) != userflags)				    \
	|| (servptr && (x)->srvptr!=servptr)				    \
	|| (awaystat && ((((x)->u->away==NULL)?1:-1)!=awaystat))	    \
			)

#define CHK_MASK(x) (x=(x&&*x&&(*x!='*'||x[1])&&(*x!='?'||x[1]!='*'||x[2]))?x:NULL)

/*
** m_who
**	parv[1] = mask; 0 #channel nick!user@host:serv/real
**	parv[2] = additional selection flag, only 'o' for now.
**		Now more! hwi@+HG (+h, +w, +i, chanop, voice, here, gone)
**		even more! Aa& (admin, services admin, local client)
** Expanded by binary.  Not QUITE so extensive as hybrid's monster,
** but instead short, sweet, and much more intuitive.
*/
int	m_who(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
    aClient	*acptr, *servptr = NULL;
    Member	*mp;
    aChannel	*chptr = NULL;
    char	*mask = parv[1];
    char	*s;
    u_int32_t	userflags = 0, chanflags = 0;
    char	showp, awaystat = 0;
    int		num_whoreplies = 0;

    if (check_registered_user(sptr))
        return 0;
    
    if (!mask || (mask[1]==0 && *mask=='?'))
        return dumpit(cptr, RPL_LISTSYNTAX, w_help);

    if (parc > 2)
    {
        for (s = parv[2]; *s; s++)
        {
            switch(*s)
            {
                case 'o': userflags |= UMODE_OPER; break;
                case 'w': userflags |= UMODE_WALLOP; break;
                case 'i': userflags |= UMODE_INVISIBLE; break;
                case 'A': userflags |= UMODE_ADMIN; break;
                case 'a': userflags |= UMODE_SADMIN; break;
                case '&': servptr = &me; break;
                case 'H': awaystat = 1; break;
                case 'G': awaystat = -1; break;
                case '@': chanflags |= CHFL_CHANOP; break;
                case '+': chanflags |= CHFL_VOICE; break;
            }
        }
    }
    
    w_isoper = IsAnOper(sptr);

    if (mask&&IsChannelName(mask))
    {
        /* List all users on a given channel */
        if (!(chptr = hash_find_channel(mask)))
            goto done_with_who;
        showp = (IsMember(sptr, chptr) || w_isoper);
        if (!PubChannel(chptr) && !showp)
            goto done_with_who;
            
        for (mp = chptr->members; mp; mp = mp->nextu)
        {
            acptr = mp->cptr;
            if (!showp && IsInvisible(acptr))
                continue;
            if ((chanflags) && ((mp->flags&chanflags)==0))
                continue;
            if (CHK_WHO_USER(acptr))
                continue;
            do_who(sptr, acptr, mp);
        }
    }
    else
    {
        /* Mask Preprocessor */
        w_nick = NULL;
        w_user = NULL;
        w_host = NULL;
        w_real = NULL;
        if (mask && (*mask != '0' || mask[1]))
        {
            collapse(mask);
            if ((w_real = index(mask, '/')))
                *w_real++ = '\0';
            if ((s = index(mask, ':')))
            {
                *s++ = '\0';
                if (!(servptr = hash_find_serv(s)))
                    goto done_with_who;
            }
            if ((w_host = index(mask, '@')))
            {
                *w_host++ = '\0';
                if ((w_user = index(mask, '!')))
                {
                    *w_user++ = '\0';
                    w_nick = mask;
                }
                else
                    w_user = mask;
            }
            else if (index(mask, '.'))
                w_host = mask;
            else
                w_nick = mask;
            CHK_MASK(w_nick);
            CHK_MASK(w_user);
            CHK_MASK(w_host);
            CHK_MASK(w_real);
            /* so end of who comes out correctly... */
            mysprintf((mask = buf), "%s!%s@%s:%s/%s", w_nick?w_nick:"*",
                w_user?w_user:"*", w_host?w_host:"*",
                servptr?servptr->name:"*", w_real?w_real:"*");
            if (w_nick && !index(w_nick, '?') && !index(w_nick, '*'))
            {
                if ((acptr = find_person(w_nick)) && !CHK_WHO_USER(acptr) &&
                   who_match(acptr))
                    do_who(sptr, acptr, find_who_chan(acptr));
                goto done_with_who;
            }

        }
        else
            mask = NULL;
        
        /* THIS IS THE BIG LOOP! */
        
        for (acptr = client; acptr; acptr = acptr->next)
        {
            if ((IsInvisible(acptr)&&!w_isoper) || !IsPerson(acptr)
               || CHK_WHO_USER(acptr) || !who_match(acptr))
                continue;
             
            if (++num_whoreplies > MAX_WHOREPLIES && !IsAnOper(sptr))
            {
                send_num(sptr, ERR_MAXWHO, MAX_WHOREPLIES);
                break;
            }
            
            do_who(sptr, acptr, find_who_chan(acptr));
        }
    }
done_with_who:
    send_num(sptr, RPL_ENDOFWHO, mask?mask:"*");
    return 0;
}


 /******************************************************************
  * m_whois segment                                                *
  ******************************************************************/

/*
 * send_whois(cptr, acptr)
 *
 *    Output whois info on acptr to recipient cptr.
 */
void send_whois(cptr, acptr)
aClient  *cptr, *acptr;
{
	aChannel *chptr;
	aClient *srvptr;
	anUser *user;
	Member *mp;
	char *bp, *s, *name;
	int show;
	
	if (!IsPerson(acptr))
		return;
	
	name = acptr->name;
	srvptr = acptr->srvptr;
	
	send_num(cptr, RPL_WHOISUSER, name, acptr->username,
		acptr->u->host, acptr->info);
	
	if (IsDisguise(acptr) && (IsAnOper(cptr) || cptr==acptr))
	 	send_num(cptr, RPL_WHOISRHOST, name, acptr->u->rhost);
		
	if (IsARegNick(acptr))
		send_num(cptr, RPL_WHOISREGNICK, name);
	
	if ((mp = acptr->u->channel))
	{		
		s = bp = buf + mysprintf(buf, ":%s 319 %s %s :", me.name,
			cptr->name, name);
		for (; mp; mp = mp->nextc)
		{
			chptr = mp->chptr;
			if (!(ShowChannel(cptr, chptr)))
			{
				if (IsSAdmin(cptr))
					*bp++ = '%';
				else
					continue;
			}
			if (bp > buf+500-CHANNELLEN)
			{
				if (!IsAnOper(cptr))
				{	/* Truncate list; let user know. */
					bp = strput(bp, ". . . ");
					break;
				}
				send_str(cptr, buf, bp-buf-1);
				bp = s;
			}
			if (mp->flags&CHFL_OVERLAP)
				*bp++ = (mp->flags&CHFL_CHANOP)?'@':'+';
			bp = strput(bp, chptr->chname);
			*bp++ = ' ';
		}
		if (bp!=s)
			send_str(cptr, buf, bp-buf-1);
	}
	
	send_num(cptr, RPL_WHOISSERVER, name, srvptr->name, srvptr->info);

	if (acptr->u->away)
		send_num(cptr, RPL_AWAY, name, acptr->u->away);
			
	if (acptr->u->hush)
		send_num(cptr, RPL_USERISHUSHED, name, acptr->u->hush->s);

	if (IsAnOper(acptr))
	{
		if (IsAdmin(acptr))
			bp = "a Server Administrator";
		else
			bp = "an IRC Operator";
		if (IsSRA(acptr))
			s = "SRA";
		else if (IsCSop(acptr))
			s = "CSop";
		else if (IsSAdmin(acptr))
			s = "Services Administrator";
		else if (IsOper(acptr))
			s = "Global";
		else
			s = "Local";
		send_num(cptr, RPL_WHOISOPERATOR, name, bp, s);
	}
	
	if (MyConnect(acptr))
		send_num(cptr, RPL_WHOISIDLE, name,
			nowtime - acptr->ulast, acptr->firsttime);
}

/*
** m_whois
**	parv[1] = nickname masklist
*/
int	m_whois(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*nick, *tmp;
	int	found;
	int	total = 0;

	if (check_registered_user(sptr))
		return 0;

    	if (parc < 2)
	{
		send_num(sptr, ERR_NONICKNAMEGIVEN);
		return 0;
	}

	if (parc > 2)
	{
		if (hunt_server(cptr,sptr,":%s WHOIS %s :%s", 1,parc,parv) !=
		    HUNTED_ISME)
			return 0;
		parv[1] = parv[2];
	}

	tmp = parv[1];
	while (nick = strsplit(&tmp, ','))
	{
		if (!*nick)
			continue;

		found = 0;
		
		if (!index(nick, '?') && !index(nick, '*'))
		{
		    if ((acptr = find_client(nick)) && IsPerson(acptr))
		    {
		        found++;
		        total++;
		        send_whois(sptr, acptr);
		    }
		}
		else if (IsServer(cptr))
			continue;
		else
		{
		    collapse(nick);
		    for (acptr = client; acptr; acptr = acptr->next)
		    {
			if (!IsPerson(acptr))
			    continue;
			if (IsInvisible(acptr) && acptr!=sptr && !IsAnOper(sptr))
			    continue;
			if (qmatch(nick, acptr->name) != 0)
			    continue;

			send_whois(sptr, acptr);
			if (++found > 32 || ++total > 64)
			{
			    sendto_one(sptr, ":%s NOTICE %s :Exceeded maximum "
			        "number of /whois replies.  Truncating list.",
			        me.name, sptr->name, total);
			    break;
			}
		    }
		}
		if (!found)
			send_num(sptr, ERR_NOSUCHNICK, nick);
	}
	send_num(sptr, RPL_ENDOFWHOIS, parv[1]);

	return 0;
}


/*
** m_quit
**	parv[1] = comment
*/
int	m_quit(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	Member *mp, *mp2;
	char *comment;

	if (IsPerson(cptr))
	    for (mp = sptr->u->channel; mp; mp = mp2)
	    {
	        mp2 = mp->nextc;
	        if (can_member_send(mp)!=0)
	        {
	            sendto_serv_butone(NULL, ":%s PART %s :Quit",
	                sptr->name, mp->chptr->chname);
	            sendto_channel_butserv(mp->chptr, sptr, "PART %s :Quit",
	                mp->chptr->chname);
	            member_del(mp);
	        }
	    }
	
	if (!parv[1])
		comment = "Quit";
	else if (!IsPerson(cptr))
		comment = parv[1];
	else
	{
		if (strlen(parv[1]) > 290)
			parv[1][290] = '\0';
		mysprintf(buf, "Quit: %s", parv[1]);
		for (mp = sptr->u->channel; mp; mp = mp->nextc)
			if (mp->chptr->mode.mode&MODE_STRIPCOLOR)
				{ strip_colors(buf+6); break; }
		comment = buf;
	}
	return exit_client(cptr, sptr, sptr, comment);
}


/*
** m_fquit
**	parv[1] = kill vitim(s) - comma separated list
**	parv[2] = kill path/reason
**
**	Similar to m_kill, but this is less likely to cause desyncs.
**	Basically, this is a forced quit.  Ghosts won't be generated as often
**	when using this.  When an oper uses /kill, this will be called instead.
**
**	written by binary
*/
int	m_fquit(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*path = parv[2], *reason = NULL;
	char	*users = parv[1], *p = NULL, *nick = NULL;
	char	nickhost[NICKLEN+HOSTLEN+2];
	int	x, targets = 0, flush = 0, gwarn = 0;
	char	*bpos = buf;
	aClient	*acptr;
	
	reason = (char *)NULL;
	if (path)
	{
		if (IsPerson(cptr))
		{
			reason = path;
			mysprintf(nickhost, "%s!%s", sptr->u->host, sptr->name);
			path = nickhost;
		}
		else
		{
			if (!(reason = index(path, ' ')))
				reason = path;
			else
				*reason++ = '\0';
			if ((*reason == '('))
			{
				*reason++ = '\0';
				if ((p = rindex(reason, ')')))
					*p = '\0';
			}
		}
	}
	else return check_params(sptr, "FQUIT", -1, 0);
	
	if (check_privs(sptr, 0, 0))
		return 0;

	if (strlen(path) > TOPICLEN)
		path[TOPICLEN-1] = '\0';
	
	*buf = '\0';
	
	for (p = users; nick = strsplit(&p, ',');)
	{
		if ((targets > 10))
		{
			bpos[-1] = '\0';
			sendto_serv_butone(cptr, ":%s FQUIT %s :%s!%s %s",
				sptr->name, buf, me.s->shortn, path, reason);
			bpos = buf;
			*bpos = '\0';
			targets = 0;
		}
		if (!(acptr = find_client(nick)))
		{
			if (!(acptr = get_history(nick, (long)KILLCHASETIMELIMIT)))
			{
				send_num(sptr, ERR_NOSUCHNICK, nick);
				continue;
			}
			sendto_one(sptr,":%s NOTICE %s :KILL changed from %s to %s",
				me.name, sptr->name, nick, acptr->name);
		}
		if (!IsPerson(acptr))
			continue; /* Killing a server??? */
		if (IsPerson(cptr) &&
			((!MyConnect(acptr) && !OPCanGKill(sptr)) ||
			(MyConnect(acptr) && !OPCanLKill(sptr))))
		{
	    		if (!gwarn)
				send_num(sptr, ERR_NOPRIVILEGES);
			gwarn = 1;
			continue;
		}
		if (MyConnect(acptr))
		{
			mysprintf(buf3, IsPerson(cptr)
			    ?"Local kill by %s (%s)":"Killed (%s (%s))",
			    sptr->name, reason);
			buf3[sizeof(buf3)-1] = '\0';

			/* FORCE them to quit, but ONLY if it is OUR user. */
			if (!MyConnect(sptr)) /* not a local kill */
			{
				sendto_serv_butone(cptr,
					":%s FQUIT %s :%s!%s %s", sptr->name,
					acptr->name, me.s->shortn, path, reason);
				/* send fquit msg before user quits */
			}
			if (exit_client(NULL, acptr, &me, buf3)==FLUSH_BUFFER)
				return FLUSH_BUFFER;
		}
	 	else
			bpos += mysprintf(bpos, "%s,", acptr->name);
		if (IsPerson(sptr))
			sendto_ops("Received KILL message for %s!%s@%s from "
				"%s Path: %s!%s (%s)",
				acptr->name, acptr->username, acptr->u->host,
				sptr->name, me.s->shortn, path, reason);
		else
			sendto_umode(UMODE_KILLS, "*** Notice -- "
				"Received KILL message for %s!%s@%s"
				" from %s Path: %s!%s (%s)",
				acptr->name, acptr->username, acptr->u->host,
				sptr->name, me.s->shortn, path, reason);
		targets++;
	}
	if (*buf)
	{
		bpos[-1] = '\0'; /* murder last comma */
		sendto_serv_butone(cptr, ":%s FQUIT %s :%s!%s %s",
			sptr->name, buf, me.s->shortn, path, reason);
	}
	return flush;
}


/*
** m_kill
**	parv[1] = kill victim(s) - comma separated list
**	parv[2] = kill path
*/
int	m_kill(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	anUser 	*auser;
	char    inpath[HOSTLEN * 2 + USERLEN + 5];
	char	*oinpath = get_client_name(cptr,FALSE);
	char	*user, *path, *killer, *nick, *p, *s;
	int	chasing = 0, kcount = 0;

	if (check_registered(sptr) || check_params(sptr, "KILL", parc, 2))
		return 0;

	user = parv[1];
	path = parv[2]; /* Either defined or NULL (parc >= 2!!) */

	strcpy(inpath, oinpath);

	if (IsServer(cptr) && (s = (char *)index(inpath, '.')) != NULL)
		*s = '\0';	/* Truncate at first "." */

	if (check_privs(sptr, 0, 0))
		return 0;
	    
	if (IsAnOper(cptr) && MyConnect(sptr))
	{
		if (BadPtr(path))
			return check_params(sptr, "KILL", -1, 0);
		if (strlen(path) > TOPICLEN)
			path[TOPICLEN] = '\0';
		if (*user == '/')
			user++;
		else if (msgptr != &ms_rkill)
			return m_fquit(cptr, sptr, parc, parv);
	}
	
	/* canonize removed; assume opers know what they're doing! */
	
	for (p = user; (nick = strsplit(&p, ','));)
	{
		chasing = 0;

		if (!(acptr = find_client(nick)))
		{
			/*
			** If the user has recently changed nick, we automaticly
			** rewrite the KILL for this new nickname--this keeps
			** servers in synch when nick change and kill collide
			*/
			if (!(acptr = get_history(nick, (long)KILLCHASETIMELIMIT)))
		    	{
				send_num(sptr, ERR_NOSUCHNICK, nick);
				continue;
		    	}
			sendto_one(sptr,":%s NOTICE %s :KILL changed from %s to %s",
				   me.name, sptr->name, nick, acptr->name);
			chasing = 1;
		}
		if ((!MyConnect(acptr) && MyClient(cptr) && !OPCanGKill(cptr)) ||
		    (MyConnect(acptr) && MyClient(cptr) && !OPCanLKill(cptr)))
		{
			send_num(sptr, ERR_NOPRIVILEGES);
			continue;
		}
		if (!IsPerson(acptr))
			continue;
	
		/* From here on, the kill is probably going to be successful. */
	
		kcount++;
	
		if (!IsServer(sptr) && (kcount > MAXKILLS))
		{
		  sendto_one(sptr,":%s NOTICE %s :Too many targets, kill list was truncated. Maximum is %d.",me.name, sptr->name, MAXKILLS);
		  break;
		}
		if (!IsServer(cptr))
		{
			/*
			** The kill originates from this server, initialize path.
			** (In which case the 'path' may contain user suplied
			** explanation ...or some nasty comment, sigh... >;-)
			**
			**	...!operhost!oper
			**	...!operhost!oper (comment)
			*/
			strcpy(inpath, cptr->sockhost);
			if (kcount < 2) /* Only check the path the first time 
					   around, or it gets appended to itself. */
			if (!BadPtr(path))
			{
				mysprintf(buf, "%s%s (%s)",
					cptr->name, IsOper(sptr) ? "" : "(L)", path);
				path = buf;
			}
			else
				path = cptr->name;
		}
		else if (BadPtr(path))
			 path = "*no-path*"; /* Bogus server sending??? */
		/*
		** Notify all *local* opers about the KILL (this includes the one
		** originating the kill, if from this server--the special numeric
		** reply message is not generated anymore).
		**
		** Note: "acptr->name" is used instead of "user" because we may
		**	 have changed the target because of the nickname change.
		*/
	
		if (index(sptr->name, '.'))
			sendto_umode(UMODE_KILLS, "*** Notice -- Received KILL message for %s!%s@%s from %s Path: %s!%s",
				acptr->name, acptr->username, acptr->u->host,
				sptr->name, inpath, path);
		else
			sendto_ops("Received KILL message for %s!%s@%s from %s Path: %s!%s",
				acptr->name, acptr->username, acptr->u->host,
				sptr->name, inpath, path);
#if defined(USE_SYSLOG) && defined(SYSLOG_KILL)
		if (IsOper(sptr))
			syslog(LOG_DEBUG,"KILL From %s For %s Path %s!%s",
				sptr->name, acptr->name, inpath, path);
#endif
		/*
		** And pass on the message to other servers. Note, that if KILL
		** was changed, the message has to be sent to all links, also
		** back.
		** Suicide kills are NOT passed on --SRB
		*/
		if (!MyConnect(acptr) || !MyConnect(sptr) || !IsAnOper(sptr))
		    {
			sendto_serv_butone(cptr, ":%s KILL %s :%s!%s",
					   sptr->name, acptr->name, inpath, path);
			if (chasing && IsServer(cptr))
				sendto_one(cptr, ":%s KILL %s :%s!%s",
					   me.name, acptr->name, inpath, path);
			acptr->flags |= FLAGS_KILLED;
		    }
	
		/*
		** Tell the victim she/he has been zapped, but *only* if
		** the victim is on current server--no sense in sending the
		** notification chasing the above kill, it won't get far
		** anyway (as this user don't exist there any more either)
		*/
		if (MyConnect(acptr))
			sendto_prefix_one(acptr, sptr, "KILL %s :%s!%s",
					  acptr->name, inpath, path);
		/*
		** Set FLAGS_KILLED. This prevents exit_one_client from sending
		** the unnecessary QUIT for this. (This flag should never be
		** set in any other place)
		*/
		if (MyConnect(acptr) && MyConnect(sptr) && IsAnOper(sptr))
			mysprintf(buf2, "Local kill by %s (%s)", sptr->name,
				BadPtr(parv[2]) ? sptr->name : parv[2]);
		else
		    {
			if ((killer = index(path, ' ')))
			    {
				while (*killer && *killer != '!')
					killer--;
				if (!*killer)
					killer = path;
				else
					killer++;
			    }
			else
				killer = path;
			mysprintf(buf2, "Killed (%s)", killer);
		    }
		    if (exit_client(cptr, acptr, sptr, buf2) == FLUSH_BUFFER)
			return FLUSH_BUFFER;
	}
	return 0;
}

/* m_mkill() - kills all users with the host provided or with the same host
 *	as a nickname typed in.  Now allows masks; finds the first match
 *      for a hostmask and kills all clients with that specific host.
 */
int m_mkill(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int parc;
char *parv[];
{
	char *lookhost, *bpos;
	aClient	*acptr, *next = client;
	int i = 0;
	
	if (check_registered_user(sptr) || check_privs(sptr, 0, 0))
		return 0;
	if (parc < 3 || *parv[2] == '\0')
		return check_params(sptr, "MKILL", -1, 0);
	
	if (!index(parv[1], '.'))
	{
		if ((acptr = find_client(parv[1])))
			lookhost = acptr->u->rhost;
		else
		{
			send_num(sptr, ERR_NOSUCHNICK, parv[1]);
			return 0;
		}
	}
	else
	{
		lookhost = parv[1];
		if (index(lookhost, '?') || index(lookhost, '*'))
		{
			for (acptr = client; acptr; acptr = acptr->next)
				if (IsPerson(acptr) &&
					!match(lookhost, acptr->u->rhost))
				{
					lookhost = acptr->u->rhost;
					break;
				}
			if (!acptr)
				return 0;
		}
	}
	
	parv[1] = bpos = buf2;
	
	while ((acptr = next))
	{
		next = acptr->next;
		if (!IsPerson(acptr) || (!MyConnect(acptr) && !IsOper(sptr))
				|| IsAnOper(acptr))
			continue;
		if (!mycmp(acptr->u->rhost, lookhost))
		{
			bpos += mysprintf(bpos, "%s,", acptr->name);
			if (bpos >= buf+100 || ++i >= 10)
			{
				bpos[-1] = '\0';
				m_fquit(cptr, sptr, parc, parv);
				bpos = buf2;
				i = 0;
			}
		}
	}
	
	if (bpos != buf2)
	{
		bpos[-1] = '\0';
		m_fquit(cptr, sptr, parc, parv);
	}
	
	return 0;
}
		
	

/***********************************************************************
 * m_away() - Added 14 Dec 1988 by jto. 
 *            Not currently really working, I don't like this
 *            call at all...
 *
 *            ...trying to make it work. I don't like it either,
 *	      but perhaps it's worth the load it causes to net.
 *	      This requires flooding of the whole net like NICK,
 *	      USER, MODE, etc messages...  --msa
 ***********************************************************************/

/*
** m_away
**	parv[1] = away message
*/
int	m_away(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	if (check_registered_user(sptr))
		return 0;

	if (!parv[1])
	{
		if (MyConnect(sptr))
			send_num(sptr, RPL_UNAWAY);
		if (sptr->u->away)
		{
			da_free(&away_mast, sptr->u->away);
			sptr->u->away = NULL;
			sendto_serv_butone(cptr, ":%s AWAY", sptr->name);
		}

		return 0;
	}

	if (strlen(parv[1]) > AWAYLEN)
		parv[1][AWAYLEN] = '\0';
	sendto_serv_butone(cptr, ":%s AWAY :%s", sptr->name, parv[1]);

	if (!sptr->u->away)
		sptr->u->away = da_alloc(&away_mast);

	strcpy(sptr->u->away, parv[1]);
	if (MyConnect(sptr))
		send_num(sptr, RPL_NOWAWAY);
	return 0;
}

/*
** m_ping
**	parv[1] = origin
**	parv[2] = destination
*/
int	m_ping(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*origin, *destination;

        if (check_registered(sptr))
                return 0;
 
 	if (!parv[1])
	{
		send_num(sptr, ERR_NOORIGIN, sptr->name);
		return 0;
	}
	origin = parv[1];
	destination = parv[2];

	acptr = find_client(origin);
	if (!acptr)
		acptr = hash_find_serv(origin);
	if (acptr && acptr != sptr)
		origin = cptr->name;
	if (!BadPtr(destination))
	{
		if (!(acptr = hash_find_serv(destination)))
		{
			send_num(sptr, ERR_NOSUCHSERVER, destination);
			return 0;
		}
		else if (!IsMe(acptr))
		{
			sendto_one(acptr, ":%s PING %s :%s", sptr->name,
				   origin, acptr->name);
			return 0;
		}
	}
	sendto_one(sptr, ":%s PONG %s :%s", me.name,
		   (destination) ? destination : me.name, origin);
	return 0;
    }

#ifdef NOSPOOF
/*
** m_nospoof - allows clients to respond to no spoofing patch
**	sptr->name = prefix
**	parv[1] = code
*/
int	m_nospoof(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	unsigned long result;

	if (IsNotSpoof(cptr) || IsRegistered(cptr) || !*sptr->name) return 0;
	if (BadPtr(parv[1])) goto badspoof;
	result = strtoul(parv[1], NULL, 16);
	/* Accept code in second parameter (ircserv) */
	if (result != sptr->nospoof)
	{
		if (BadPtr(parv[2])) goto badspoof;
		result = strtoul(parv[2], NULL, 16);
		if (result != sptr->nospoof) goto badspoof;
	}
	sptr->nospoof = 0;
	if (GotUser(sptr))
		return register_user(cptr, sptr, sptr->username);
	return 0;
badspoof:
	/* Homer compatibility */
	sendto_one(cptr, ":%X!nospoof@%s PRIVMSG %s :%cVERSION%c",
		cptr->nospoof, me.name, cptr->name, 1, 1);
	return 0;
}
#endif /* NOSPOOF */

/*
** m_pong
**	parv[1] = origin
**	parv[2] = destination
*/
int	m_pong(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char	*origin, *destination;

#ifdef NOSPOOF
	if (!IsRegistered(cptr))
		return m_nospoof(cptr, sptr, parc, parv);
#endif

	if (parc < 2 || *parv[1] == '\0')
	{
		send_num(sptr, ERR_NOORIGIN);
		return 0;
	}

	origin = parv[1];
	destination = parv[2];
	cptr->flags &= ~FLAGS_PINGSENT;
	sptr->flags &= ~FLAGS_PINGSENT;

	if (!BadPtr(destination) && mycmp(destination, me.name) != 0)
	    {
		if ((acptr = find_client(destination)))
		{
			if (!IsServer(cptr) && !IsServer(acptr))
			{
				send_num(sptr, ERR_NOSUCHSERVER, destination);
				return 0;
			}
			else
			sendto_one(acptr,":%s PONG %s %s",
				   sptr->name, origin, destination);
		}
		else
		    {
			send_num(sptr, ERR_NOSUCHSERVER, destination);
			return 0;
		    }
	    }
#ifdef	DEBUGMODE
	else
		Debug((DEBUG_NOTICE, "PONG: %s %s", origin,
		      destination ? destination : "*"));
#endif
	return 0;
    }

/*
** m_oper
**	parv[1] = oper name
**	parv[2] = oper password
*/
int	m_oper(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
	aConfItem *aconf;
	char	*name, *password, *encr;
#ifdef CRYPT_OPER_PASSWORD
	char	salt[3];
	extern	char *crypt();
#endif /* CRYPT_OPER_PASSWORD */

	if (check_registered_user(sptr))
		return 0;

	name = parc > 1 ? parv[1] : NULL;
	password = parc > 2 ? parv[2] : NULL;

	if (!IsServer(cptr) && (BadPtr(name) || BadPtr(password)))
		return check_params(sptr, "OPER", -1, 0);
	
	/* if message arrived from server, trust it, and set to oper */
	    
	if ((IsServer(cptr) || IsMe(cptr)) && !IsOper(sptr))
	    {
		sptr->umodes |= UMODE_OPER;
		sendto_serv_butone(cptr, ":%s MODE %s :+o", sptr->name, sptr->name);
		if (IsMe(cptr))
			send_num(sptr, RPL_YOUREOPER);
		return 0;
	    }
	else if (IsOper(sptr))
	    {
		if (MyConnect(sptr))
			send_num(sptr, RPL_YOUREOPER);
		return 0;
	    }
	if (!(aconf = find_conf_exact(name, sptr->username, sptr->sockhost,
				      CONF_OPS)) &&
	    !(aconf = find_conf_exact(name, sptr->username,
				      inetntoa((char *)&cptr->ip), CONF_OPS)))
	    {
		send_num(sptr, ERR_NOOPERHOST);
                sendto_realops("Failed OPER attempt by %s (%s@%s)",
                  sptr->name, sptr->username, sptr->sockhost);
		sptr->since += 10;
		return 0;
	    }
#ifdef CRYPT_OPER_PASSWORD
        /* use first two chars of the password they send in as salt */

        /* passwd may be NULL. Head it off at the pass... */
        salt[0] = '\0';
        if (password && aconf->passwd && aconf->passwd[0] && aconf->passwd[1])
	    {
        	salt[0] = aconf->passwd[0];
		salt[1] = aconf->passwd[1];
		salt[2] = '\0';
		encr = crypt(password, salt);
	    }
	else
		encr = "";
#else
	encr = password;
#endif  /* CRYPT_OPER_PASSWORD */

	if (!aconf->passwd)
	{
		sendto_one(sptr, ":%s NOTICE %s :ERROR! NULL PASSWD!!!",
			me.name, sptr->name);
		return 0;
	}
	if ((aconf->status & CONF_OPS) && !(aconf->status & CONF_ILLEGAL) &&
	    StrEq(encr, aconf->passwd) && !attach_conf(sptr, aconf))
	    {
		int old = (sptr->umodes & ALL_UMODES);
		char *s;

		if (!(s = index(aconf->host, '@')))
		{
			sendto_one(sptr, ":%s NOTICE %s :INTERNAL ERROR!!!!",
				me.name, sptr->name);
			return 0;
		}
		*s++ = '\0';
		if (!(aconf->port & OFLAG_ISGLOBAL))
			SetLocOp(sptr);
		else
			SetOper(sptr);
		sptr->oflag = aconf->port;
		*--s =  '@';
		sendto_ops("%s (%s!%s@%s) is now operator (%c)", parv[1],
			   sptr->name, sptr->username,
			   sptr->u->host, IsOper(sptr) ? 'O' : 'o');
		sptr->umodes |=
			(UMODE_SERVNOTICE|UMODE_WALLOP|UMODE_FAILOP|
				UMODE_FLOOD|UMODE_HUSHNOTICE);
		send_umode_out(cptr, sptr, old);
 		send_num(sptr, RPL_YOUREOPER);
#if !defined(CRYPT_OPER_PASSWORD) && (defined(FNAME_OPERLOG) ||\
    (defined(USE_SYSLOG) && defined(SYSLOG_OPER)))
		encr = "";
#endif
#if defined(USE_SYSLOG) && defined(SYSLOG_OPER)
		syslog(LOG_INFO, "OPER (%s) (%s) by (%s!%s@%s)",
			name, encr,
			sptr->name, sptr->username, sptr->sockhost);
#endif
#ifdef FNAME_OPERLOG
	      {
                int     logfile;

                /*
                 * This conditional makes the logfile active only after
                 * it's been created - thus logging can be turned off by
                 * removing the file.
                 *
                 * stop NFS hangs...most systems should be able to open a
                 * file in 3 seconds. -avalon (curtesy of wumpus)
                 */
                if (IsPerson(sptr) &&
                    (logfile = open(FNAME_OPERLOG, O_WRONLY|O_APPEND)) != -1)
		{
                        mysprintf(buf, "%s OPER (%s) (%s) by (%s!%s@%s)\n",
				      myctime(nowtime), name, encr,
				      sptr->name, sptr->username,
				      sptr->sockhost);
		  write(logfile, buf, strlen(buf));
		  close(logfile);
		}
                /* Modification by pjg */
	      }
#endif
	    }
	else
	    {
		detach_conf(sptr, aconf);
		send_num(sptr, ERR_PASSWDMISMATCH);
#ifdef  FAILOPER_WARN
		sendto_one(sptr,":%s NOTICE :Your attempt has been logged.",me.name);
#endif
                sendout_globops("Failed OPER attempt by %s (%s@%s) using UID %s [Invalid Pass]",
                   sptr->name, sptr->username, sptr->sockhost, name);
		sptr->since += 7;
#ifdef FNAME_OPERLOG
              {
                int     logfile;

                /*
                 * This conditional makes the logfile active only after
                 * it's been created - thus logging can be turned off by
                 * removing the file.
                 *
                 * stop NFS hangs...most systems should be able to open a
                 * file in 3 seconds. -avalon (curtesy of wumpus)
                 */
                if (IsPerson(sptr) &&
                    (logfile = open(FNAME_OPERLOG, O_WRONLY|O_APPEND)) != -1)
                {
                        mysprintf(buf, "%s FAILED OPER (%s) (%s) by (%s!%s@%s)\n PASSWORD %s",
                                      myctime(nowtime), name, encr,
                                      sptr->name, sptr->username,
                                      sptr->sockhost, password);
                  write(logfile, buf, strlen(buf));
                  close(logfile);
                }
                /* Modification by pjg */
              }
#endif
	    }
	return 0;
    }

/***************************************************************************
 * m_pass() - Added Sat, 4 March 1989
 ***************************************************************************/

/*
** m_pass
**	parv[1] = password
*/
int	m_pass(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
    {
	char *password = parc > 1 ? parv[1] : NULL;

	if (BadPtr(password))
		return check_params(sptr, "PASS", -1, 0);
	if (!MyConnect(sptr) || (!IsUnknown(cptr) && !IsHandshake(cptr)))
	    {
		send_num(cptr, ERR_ALREADYREGISTRED);
		return 0;
	    }
	strncpyzt(cptr->passwd, password, sizeof(cptr->passwd));
	return 0;
    }

/*
 * m_userhost added by Darren Reed 13/8/91 to aid clients and reduce
 * the need for complicated requests like WHOIS. It returns user/host
 * information only (no spurious AWAY labels or channels).
 */
int	m_userhost(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	char	*p = NULL;
	aClient	*acptr;
	char	*s;
	char    *curpos, *head;
	int	i = 0;

	if (check_registered(sptr))
		return 0;

	if (parc < 2)
		return check_params(sptr, "USERHOST", -1, 0);

	/*
	 * use curpos to keep track of where we are in the output buffer,
	 * and use resid to keep track of the remaining space in the
	 * buffer
	 */
	head = buf+mysprintf(buf, ":%s 302 %s :", me.name, sptr->name);
	curpos = head;

	/*
	 * for each user found, print an entry if it fits.
	 */
	for (p = parv[1]; (s = strsplit(&p, ' '));)
	{
		if ((acptr = find_person(s)))
		{
			if (curpos > buf+BUFSIZE-NICKLEN-USERLEN-HOSTLEN-20)
			{
				send_str(sptr, buf, curpos-buf-1);
				curpos = head;
				i++;
			}
			curpos += mysprintf(curpos, "%s%s=%c%s@%s ",
				acptr->name, IsAnOper(acptr) ? "*" : "",
				acptr->u->away ? '-' : '+', acptr->username,
				sptr==acptr||IsAnOper(sptr)?acptr->u->rhost:
				acptr->u->host);
		}
	}

	if (!i || curpos != head)
	{
		if (curpos != head)
			curpos--;
		send_str(sptr, buf, curpos-buf);
	}
	return 0;
}


/*
 * m_ison added by Darren Reed 13/8/91 to act as an efficent user indicator
 * with respect to cpu/bandwidth used. Implemented for NOTIFY feature in
 * clients. Designed to reduce number of whois requests. Can process
 * nicknames in batches as long as the maximum buffer length.
 *
 * format:
 * ISON :nicklist
 *
 * cleaned by binary
 */
int     m_ison(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int     parc;
char    *parv[];
{
	aClient *acptr;
	char *bpos;
	char *p, *nick;
	char *user, *host;
	
	if (check_registered_user(sptr))
		return 0;
	if (check_params(sptr, "ISON", parc, 1))
		return 0;
	
	bpos = buf;
	*bpos = '\0';
	
	for (p = parv[1]; (nick = strsplit(&p, ' '));)
	{
		if (!*nick)
			continue;
		if ((user = index(nick, '!')))
			*user++ = '\0';
		if ((acptr = find_client(nick)) && IsPerson(acptr))
		{
			if (user && (host = index(user, '@')))
			{
				*host++ = '\0';
				if (  match(user, acptr->username)
				   || match(host, acptr->u->host)
				   )	continue;
			}
			bpos += mysprintf(bpos, "%s ", acptr->name);
			if (bpos-buf > BUFSIZE-NICKLEN-10)
				break;
		}
	}
	
	if (bpos != buf)
		bpos[-1] = '\0'; /* Murder the final space... */
		
	send_num(sptr, RPL_ISON, buf);
	
	return 0;
}



static int user_modes[] =
{
				UMODE_OPER, 'o',
				UMODE_LOCOP, 'O',
				UMODE_INVISIBLE, 'i',
				UMODE_WALLOP, 'w',
				UMODE_FAILOP, 'g',
				UMODE_SERVNOTICE, 's',
				UMODE_KILLS, 'k',
				UMODE_SADMIN, 'a',
				UMODE_ADMIN, 'A',
				UMODE_CLIENT, 'c',
				UMODE_FLOOD, 'f',
				UMODE_REGNICK, 'r',
				UMODE_JUNK, 'j',
				UMODE_DISGUISE, 'd',
				UMODE_LIMITREG, 'L',
				UMODE_SRA,	'R',
				UMODE_CSOP,	'C',
#ifdef CHATOPS
				UMODE_CHATOP, 'b',
#endif
				UMODE_HUSHNOTICE, 'u',
				0, 0
};


/*
 * m_umode() added 15/10/91 By Darren Reed.
 * parv[1] - username to change mode for
 * parv[2] - modes to change
 */
int	m_umode(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	Reg1	int	flag;
	Reg2	int	*s;
	Reg3	char	**p, *m;
	aClient	*acptr;
	int	setflags;
	char	sign = '+';

	if (check_registered_user(sptr))
		return 0;
	
	acptr = sptr; /* Assume user *INTENDS* to change his OWN mode... */

	if (parc < 3)
	{
		if (*parv[1] != '+' && *parv[1] != '-')
		{
			m = buf;
			*m++ = '+';
			for (s = user_modes; (flag = *s); s += 2)
				if ((sptr->umodes & flag))
					*m++ = (char)(*(s+1));
			*m = '\0';
			send_num(sptr, RPL_UMODEIS, buf);
			return 0;
		}
		parv[2] = parv[1]; /* allow /mode +modes for users */
	}

	/* find flags already set for user */
	setflags = 0;
	for (s = user_modes; (flag = *s); s += 2)
		if ((sptr->umodes & flag))
			setflags |= flag;

	/*
	 * parse mode change string(s)
	 */
	for (p = &parv[2]; p && *p; p++ )
	    for (m = *p; *m; m++)
		switch(*m)
		{
		  case '+' :
		  case '-' :
		    sign = *m;
		    break;
	  	  default :
		    for (s = user_modes; (flag = *s); s += 2)
		    if (*m == (char)(*(s+1)))
		    {
		        if (MyConnect(sptr)&&flag&(UMODE_CSOP|UMODE_SRA|UMODE_REGNICK))
		            break;
			if (sign=='+')
		            sptr->umodes |= flag;
		        else
			    sptr->umodes &= ~flag;
		        break;
		    }

				
		    if (flag == 0 && MyConnect(sptr))
		        send_num(sptr, ERR_UMODEUNKNOWNFLAG, *m);
		    break;
		}
	
	if (!IsServer(cptr))
	{	
		/*
		 * Let only operators set FloodF, ClientF; also
		 * remove those flags if they've gone -o/-O.
		 *  FloodF sends notices about possible flooding -Cabal95
		 *  ClientF sends notices about clients connecting or exiting
		 *  Admin is for server admins
		 *  SAdmin is for services admins (mode changers)
		 */
		sptr->umodes &= (setflags&UMODE_ANYOP)|(~UMODE_ANYOP);
		if (!IsAnOper(sptr))
		{
			sptr->umodes &= ~(UMODE_CLIENT|UMODE_ADMIN|UMODE_FAILOP
				|UMODE_SADMIN|UMODE_FLOOD|UMODE_HUSHNOTICE);
			/*
			 *  If opers had any one of these three modes when
			 * they went -o, simply allow them to retain them
			 * afterwards, and add them back if they really want.
			 */
			if ((setflags&(UMODE_CHATOP))==0)
				sptr->umodes &= ~(UMODE_CHATOP);
		}
		/*
		 * New oper access flags - Only let them set certian usermodes
		 * on themselves IF they have access to set that specific mode
		 * in their O:Line.
		 */
		else
		{
			if (IsAdmin(sptr) && !OPIsAdmin(sptr))
				ClearAdmin(sptr);
			if (IsSAdmin(sptr) && !OPIsSAdmin(sptr))
				ClearSAdmin(sptr);
		}

		if ((setflags & (UMODE_OPER|UMODE_LOCOP)) && !IsAnOper(sptr))
		{
			det_confs_butmask(sptr, CONF_CLIENT & ~CONF_OPS);
			sptr->oflag = 0;
		}
	}
		
	/* Disguise the user */
	if (!(setflags & UMODE_DISGUISE) && IsDisguise(sptr))
		sptr->u->host = sptr->u->dhost;
	else if ((setflags & UMODE_DISGUISE) && !IsDisguise(sptr))
	{
		if (IsForceDis(sptr) && cptr == sptr)
			sptr->umodes |= UMODE_DISGUISE;
		else
			sptr->u->host = sptr->u->rhost;
			/* set host back to normal */
	}
	/*
	 * compare new flags with old flags and send string which
	 * will cause servers to update correctly.
	 */
	send_umode_out(cptr, sptr, setflags);

	return 0;
}




/*
 * m_svsmode() added by taz
 * parv[1] - username to change mode for
 * parv[2] - modes to change
 * parv[3] - Service Stamp (if mode == d)
 */
int	m_svsmode(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	int	flag;
	int	*s;
	char	**p, *m;
	aClient	*acptr;
	int	setflags;
	char	sign = '+';

	if (!IsULine(sptr))
		return 0;

	if (parc < 3)
		return 0;

	if (!(acptr = find_person(parv[1])))
		return 0;

	setflags = 0;
	for (s = user_modes; (flag = *s); s += 2)
		if (acptr->umodes & flag)
			setflags |= flag;
	/*
	 * parse mode change string(s)
	 */
	for (p = &parv[2]; p && *p; p++ )
	    for (m = *p; *m; m++)
		switch(*m)
		{
		case '+' :
		case '-' :
			sign = *m;
			break;
		case 'l' :
			if(parv[3] && isdigit(*parv[3]))
				lu_mglobalu = atoi(parv[3]);
			break;
		case 'd' :
			if(parv[3] && isdigit(*parv[3]))
				acptr->u->sstamp = atol(parv[3]);
			break;
			
		case 'D' :
			if (sign=='+')
				acptr->umodes |= UMODE_FORCEDIS;
			else
				acptr->umodes &= ~UMODE_FORCEDIS;
		case 'X' :
			if (sign=='+')
			{
				acptr->umodes |= UMODE_DISGUISE;
				acptr->u->host = acptr->u->dhost;
			}
			else
			{
				acptr->umodes &= ~UMODE_DISGUISE;
				acptr->u->host = acptr->u->rhost;
			}
			if (MyConnect(acptr))
				sendto_prefix_one(acptr, acptr,
					"MODE %s %cd", acptr->name, sign);
			break;
		default :
			for (s = user_modes; (flag = *s); s += 2)
				if (*m == (char)(*(s+1)))
			    {
				if (sign=='+')
					acptr->umodes |= flag;
				else
					acptr->umodes &= ~flag;	
				break;
			    }
			break;
		}
	if(parc > 3)
		sendto_serv_butone(cptr, ":%s SVSMODE %s %s %s",
			sptr->name, parv[1], parv[2], parv[3]);
	else
		sendto_serv_butone(cptr, ":%s SVSMODE %s %s", sptr->name,
			parv[1], parv[2]);

	return 0;
}
	
/*
 * send the MODE string for user (user) to connection cptr
 * -avalon
 */
void	send_umode(cptr, sptr, old, sendmask, umode_buf)
aClient *cptr, *sptr;
int	old, sendmask;
char	*umode_buf;
{
	int	*s, flag;
	char	*m;
	char	sign = '\0';

	/*
	 * build a string in umode_buf to represent the change in the user's
	 * mode between the new (sptr->flag) and 'old'.
	 */
	m = umode_buf;
	*m = '\0';
	for (s = user_modes; (flag = *s); s += 2)
	    {
		if (MyConnect(sptr) && !(flag & sendmask))
			continue;
		if ((flag & old) && !(sptr->umodes & flag))
		    {
			if (sign=='-')
				*m++ = *(s+1);
			else
			    {
				*m++ = sign = '-';
				*m++ = *(s+1);
			    }
		    }
		else if (!(flag & old) && (sptr->umodes & flag))
		    {
			if (sign=='+')
				*m++ = *(s+1);
			else
			    {
				*m++ = sign = '+';
				*m++ = *(s+1);
			    }
		    }
	    }
	*m = '\0';
	if (*umode_buf && cptr)
	{
		if (IsServHash(cptr))
			sendto_one(cptr, ":%s "TOK_MODE" %s",
				sptr->name, umode_buf);
		else
			sendto_one(cptr, ":%s %s %s :%s", sptr->name,
				(IsToken(cptr)?TOK_MODE:MSG_MODE),
				IsServHash(cptr)?"*":sptr->name, umode_buf);
	}
}

/*
 * added Sat Jul 25 07:30:42 EST 1992
 */
void	send_umode_out(cptr, sptr, old)
aClient *cptr, *sptr;
int	old;
{
	Reg1    int     i;
	Reg2    aClient *acptr;

	send_umode(NULL, sptr, old, SEND_UMODES, buf);

	if (*buf)
	{	/* ServHash ==> lenient m_umode (:%s MODE +modes) */
		sendto_serv_butone_wproto(cptr, PROTO_SERVHASH,
			":%s "TOK_MODE" %s", sptr->name, buf);
		sendto_serv_butone_nproto(cptr, PROTO_SERVHASH,
			":%s MODE %s :%s", sptr->name, sptr->name, buf);
	}

	if (cptr && MyClient(cptr))
		send_umode(cptr, sptr, old, ALL_UMODES, buf);
}

/*
 * added by taz
 */
void	send_svsmode_out(cptr, sptr, bsptr, old)
aClient *cptr, *sptr, *bsptr;
int	old;
{
	send_umode(NULL, sptr, old, SEND_UMODES, buf);

	sendto_serv_butone(cptr, ":%s SVSMODE %s :%s",
				   bsptr->name, sptr->name, buf);

}

/***********************************************************************
 * m_silence() - Added 19 May 1994 by Run. 
 *
 ***********************************************************************/

/*
 * is_silenced : Does the actual check wether sptr is allowed
 *               to send a message to acptr.
 *               Both must be registered persons.
 * If sptr is silenced by acptr, his message should not be propagated,
 * but more over, if this is detected on a server not local to sptr
 * the SILENCE mask is sent upstream.
 */
static int is_silenced(sptr, acptr)
aClient *sptr;
aClient *acptr;
{ Reg1 Link *lp;
  static char sender[HOSTLEN+NICKLEN+USERLEN+5];

  if (!(lp = acptr->u->silence))
      return 0;
  mysprintf(sender,"%s!%s@%s",sptr->name,sptr->username,sptr->u->host);
  for (; lp; lp = lp->next)
  { if (!match(lp->value.cp, sender))
    { if (!MyConnect(sptr))
      { sendto_one(sptr->from, ":%s SILENCE %s :%s",acptr->name,
            sptr->name, lp->value.cp);
        lp->flags=1; }
      return 1; } }
  return 0;
}

int del_silence(sptr, mask)
aClient *sptr;
char *mask;
{ Reg1 Link **lp;
  Reg2 Link *tmp;

  for (lp = &(sptr->u->silence); *lp; lp = &((*lp)->next))
    if (mycmp(mask, (*lp)->value.cp)==0)
    { tmp = *lp;
      *lp = tmp->next;
      MyFree(tmp->value.cp);
      free_link(tmp);
      return 0; }
  return -1;
}

static int add_silence(sptr, mask)
aClient *sptr;
char *mask;
{ Reg1 Link *lp;
  Reg2 int cnt = 0, len = 0;

  for (lp = sptr->u->silence; lp; lp = lp->next)
  { len += strlen(lp->value.cp);
    if (MyConnect(sptr))
      if ((len > MAXSILELENGTH) || (++cnt >= MAXSILES))
      { send_num(sptr, ERR_SILELISTFULL, mask);
	return -1; }
      else
      { if (!match(lp->value.cp, mask))
	  return -1; }
    else if (!mycmp(lp->value.cp, mask))
      return -1;
  }
  lp = make_link();
  bzero((char *)lp, sizeof(Link));
  lp->next = sptr->u->silence;
  lp->value.cp = (char *)MyMalloc(strlen(mask)+1);
  strcpy(lp->value.cp, mask);
  sptr->u->silence = lp;
  return 0;
}

/*
** m_silence
** From local client:
**	parv[1] = mask (NULL sends the list)
** From remote client:
**	parv[1] = nick that must be silenced
**      parv[2] = mask
*/
int m_silence(cptr, sptr, parc, parv)
aClient	*cptr, *sptr;
int	parc;
char	*parv[];
{
  Link *lp;
  aClient *acptr;
  char c, *cp;

  if (check_registered_user(sptr)) return 0;

  if (IsPerson(cptr))
  {
    acptr = sptr;
    if (parc < 2 || *parv[1]=='\0')
    { if (!IsPerson(acptr)) return 0;
      for (lp = acptr->u->silence; lp; lp = lp->next)
	send_num(sptr, RPL_SILELIST, acptr->name, lp->value.cp);
      send_num(sptr, RPL_ENDOFSILELIST);
      return 0; }
    cp = parv[1];
    c = *cp;
    if (c=='-' || c=='+') cp++;
    else if (!(index(cp, '@') || index(cp, '.') ||
	index(cp, '!') || index(cp, '*')))
    { send_num(sptr, ERR_NOSUCHNICK, parv[1]);
      return -1; }
    else c = '+';
    cp = pretty_mask(cp);
    if ((c=='-' && !del_silence(sptr,cp)) ||
        (c!='-' && !add_silence(sptr,cp)))
    { sendto_prefix_one(sptr, sptr, "SILENCE %c%s", c, cp);
      if (c=='-')
	sendto_serv_butone(NULL, ":%s SILENCE * -%s", sptr->name, cp);
    }
  }
  else if (parc < 3 || *parv[2]=='\0')
  {
    return check_params(sptr, "SILENCE", -1, 0);
  }
  else if ((c = *parv[2])=='-' || (acptr = find_person(parv[1])))
  {
    if (c=='-')
    { if (!del_silence(sptr,parv[2]+1))
	sendto_serv_butone(cptr, ":%s SILENCE %s :%s",
	    sptr->name, parv[1], parv[2]); }
    else
    { add_silence(sptr,parv[2]);
      if (!MyClient(acptr))
        sendto_one(acptr, ":%s SILENCE %s :%s",
            sptr->name, parv[1], parv[2]); }
  }
  else
  {
    send_num(sptr, ERR_NOSUCHNICK, parv[1]);
    return -1;
  }
  return 0;
}


int m_hush(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	time_t	hush_time = 0;
		
	if (check_registered(sptr))
		return 0;
	if (parc < 4 || !parv[3] || !*parv[3])
		return check_params(sptr, "HUSH", -1, 0);
	if (!(acptr = find_person(parv[1])))
	{
		send_num(sptr, ERR_NOSUCHNICK, parv[1]);
		return 0;
	}
	if (MyConnect(sptr) &&
	    (  (!MyConnect(acptr) && !OPCanGKill(sptr)) ||
	       ( MyConnect(acptr) && !OPCanLKill(sptr)) ||
	       IsULine(acptr)				))
	{
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	}
	
	sendto_serv_butone(cptr, ":%s HUSH %s %s :%s", sptr->name,
		acptr->name, parv[2], parv[3]);
	
	if (acptr->u->hush)
		MyFree(acptr->u->hush);
	
	hush_time = atoi(parv[2]);
	
	acptr->u->hush = (Hush *)MyMalloc(sizeof(Hush) + strlen(parv[3]) +
		strlen(sptr->name) + 6);
	mysprintf(acptr->u->hush->s, "%s (%s)", sptr->name, parv[3]);
	acptr->u->hush->end = (hush_time == 0) ? 0 : nowtime + hush_time;
	
	if (MyConnect(acptr))
	{
		send_num(acptr, ERR_YOUREHUSHED, acptr->u->hush->s);
		sendto_one(acptr, ":%s NOTICE %s :*** - You have been hushed "
			"by %s (%s)", me.name, acptr->name, sptr->name, parv[3]);
		if (hush_time)
			sendto_one(acptr, ":%s NOTICE %s :*** - Your "
				"hush will expire in %d seconds.",
				me.name, acptr->name, hush_time);
	}
	
	if (hush_time)
		sendto_umode(UMODE_HUSHNOTICE,
			"*** Notice -- %s hushed %s for %d seconds (%s)",
			sptr->name, acptr->name, hush_time, parv[3]);
	else
		sendto_umode(UMODE_HUSHNOTICE,
			"*** Notice -- %s hushed %s indefinitely (%s)",
			sptr->name, acptr->name, parv[3]);				
	return 0;
}

void unhush(cptr, sptr)
aClient *cptr, *sptr;
{
	if (!cptr || !IsPerson(cptr) || !cptr->u->hush)
		return;
	
	free(cptr->u->hush);
	cptr->u->hush = (Hush *)NULL;

	if (MyConnect(cptr))
		sendto_one(cptr,":%s NOTICE %s :You are no longer hushed.",
			me.name, cptr->name);
	sendto_serv_butone(sptr, ":%s UNHUSH %s", sptr->name, cptr->name);
	sendto_umode(UMODE_HUSHNOTICE, "%s was unhushed %s%s", cptr->name,
		sptr ? "by " : "(time elapsed)", sptr ? sptr->name : "");
}

int m_unhush(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
		
	if (check_registered(sptr))
		return 0;
	if (parc < 2 || !parv[1] || parv[1] == '\0')
		return check_params(sptr, "UNHUSH", -1, 0);
	if (!(acptr = find_person(parv[1])))
	{
		send_num(sptr, ERR_NOSUCHNICK, parv[1]);
		return 0;
	}	
	if (IsPerson(cptr) && ((!MyConnect(acptr) && !OPCanGKill(sptr)) ||
	    (MyConnect(acptr) && !OPCanLKill(sptr))))
	{
		send_num(sptr, ERR_NOPRIVILEGES);
		return 0;
	}

	if (!acptr->u->hush)
	{
		sendto_one(sptr, ":%s NOTICE %s :User %s is not hushed.",
			me.name, sptr->name, acptr->name);
		return 0;
	}
	unhush(acptr, sptr);
	return 0;
}

int	m_chanserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(ChanServ)))
		sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
			ChanServ, parv[1]);
	else
		send_num(sptr, ERR_SERVICESDOWN, ChanServ);
}

int	m_memoserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(MemoServ)))
		sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
			MemoServ, parv[1]);
	else
		send_num(sptr, ERR_SERVICESDOWN, MemoServ);
}

int	m_nickserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(NickServ)))
		sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
			NickServ, parv[1]);
	else
		send_num(sptr, ERR_SERVICESDOWN, NickServ);
}

int	m_operserv(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((acptr = find_person(OperServ)))
		sendto_one(acptr,":%s PRIVMSG %s :%s", sptr->name, 
			OperServ, parv[1]);
	else
		send_num(sptr, ERR_SERVICESDOWN, OperServ);
}

/* 
 * Automatic NickServ/ChanServ direction for the identify command
 * -taz
 */
int	m_identify(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if (*parv[1]) {
		if((*parv[1] == '#') && ((char *)index(parv[1], ' '))) {
			if ((acptr = find_person(ChanServ)))
				sendto_one(acptr,":%s PRIVMSG %s :IDENTIFY %s", sptr->name, 
					ChanServ, parv[1]);
			else
				send_num(sptr, ERR_SERVICESDOWN, ChanServ);
		} else {
			if ((acptr = find_person(NickServ)))
				sendto_one(acptr,":%s PRIVMSG %s :IDENTIFY %s", sptr->name,
					NickServ, parv[1]);
			else
				send_num(sptr, ERR_SERVICESDOWN, NickServ);
		}
	}
}

/*
 * Automatic NickServ/ChanServ parsing. If the second word of parv[1]
 * starts with a '#' this message goes to ChanServ. If it starts with 
 * anything else, it goes to NickServ. If there is no second word in 
 * parv[1], the message defaultly goes to NickServ. If parv[1] == 'help'
 * the user in instructed to /cs, /ns or /ms HELP for the help they need.
 * -taz
 */

int	m_services(cptr, sptr, parc, parv)
aClient *cptr, *sptr;
int	parc;
char	*parv[];
{
	aClient *acptr;
	char *tmps;

	if (check_registered_user(sptr))
		return 0;

	if (parc < 2 || *parv[1] == '\0') {
		send_num(sptr, ERR_NOTEXTTOSEND);
		return -1;
	}

	if ((strlen(parv[1])>=4) && (!strncmp(parv[1], "help", 4))) {
		sendto_one(sptr, ":%s NOTICE %s :For ChanServ help use: /chanserv help", me.name, sptr->name);
		sendto_one(sptr, ":%s NOTICE %s :For NickServ help use: /nickserv help", me.name, sptr->name);
		sendto_one(sptr, ":%s NOTICE %s :For MemoServ help use: /memoserv help", me.name, sptr->name);
		return 0;
	}

	if ((tmps = (char *)index(parv[1], ' '))) {
		tmps++;
		if(*tmps == '#')
			return m_chanserv(cptr, sptr, parc, parv);
		else
			return m_nickserv(cptr, sptr, parc, parv);
	}

	return m_nickserv(cptr, sptr, parc, parv);

}
