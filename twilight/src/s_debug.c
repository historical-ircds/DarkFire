/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_debug.c
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef lint
static  char sccsid[] = "@(#)s_debug.c	2.30 1/3/94 (C) 1988 University of Oulu, \
Computing Center and Jarkko Oikarinen";
#endif

#include "struct.h"
/*
 * Option string.  Must be before #ifdef DEBUGMODE.
 */
char	serveropts[] = {
#ifdef	SENDQ_ALWAYS
'A',
#endif
#ifdef	CHROOTDIR
'c',
#endif
#ifdef	CMDLINE_CONFIG
'C',
#endif
#ifdef	DO_ID
'd',
#endif
#ifdef	DEBUGMODE
'D',
#endif
#ifdef	NOTE_FORWARDER
'f',
#endif
#ifdef	HUB
'H',
#endif
#ifdef	SHOW_INVISIBLE_LUSERS
'i',
#endif
#ifndef	NO_DEFAULT_INVISIBLE
'I',
#endif
#ifdef	LEAST_IDLE
'L',
#endif
#ifdef	M4_PREPROC
'm',
#endif
#ifdef	IDLE_FROM_MSG
'M',
#endif
#ifdef	CRYPT_OPER_PASSWORD
'p',
#endif
#ifdef	CRYPT_LINK_PASSWORD
'P',
#endif
#ifdef NOSPOOF
'n',
#endif
#ifdef	NPATH
'N',
#endif
#ifdef SCRIPTINIFIX
's',
#endif
#ifdef	ENABLE_SUMMON
'S',
#endif
#ifdef	IRCII_KLUDGE
'u',
#endif
#ifdef	ENABLE_USERS
'U',
#endif
#ifdef	VALLOC
'V',
#endif
#ifdef	_WIN32
'W',
#endif
#ifdef	UNIXPORT
'X',
#endif
#ifdef	USE_SYSLOG
'Y',
#endif
#ifdef	V28PlusOnly
'8',
#endif
#ifdef SCRIPTINIFIX
'S',
#endif
'\0'};

#include "numeric.h"
#include "common.h"
#include "sys.h"
#include "whowas.h"
#include "hash.h"
#ifndef _WIN32
#include <sys/file.h>
#endif
#ifdef HPUX
#include <fcntl.h>
#endif
#if !defined(ULTRIX) && !defined(SGI) && !defined(sequent) && \
    !defined(__convex__) && !defined(_WIN32)
# include <sys/param.h>
#endif
#ifdef HPUX
# include <sys/syscall.h>
# define getrusage(a,b) syscall(SYS_GETRUSAGE, a, b)
#endif
#ifdef GETRUSAGE_2
# ifdef SOL20
#  include <sys/time.h>
#  ifdef RUSAGEH
#   include <sys/rusage.h>
#  endif
# endif
# include <sys/resource.h>
#else
#  ifdef TIMES_2
#   include <sys/times.h>
#  endif
#endif
#ifdef PCS
# include <time.h>
#endif
#ifdef HPUX
#include <unistd.h>
#ifdef DYNIXPTX
#include <sys/types.h>
#include <time.h>
#endif
#endif
#include "h.h"

#ifndef ssize_t
#define ssize_t unsigned int
#endif

#ifdef DEBUGMODE
static	char	debugbuf[1024];

#ifndef	USE_VARARGS
/*VARARGS2*/
void	debug(level, form, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10)
int	level;
char	*form, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *p9, *p10;
{
# ifndef _WIN32
	int	err = errno;
# else
	int	err = WSAGetLastError();
# endif
#else
void	debug(level, form, va_alist)
int	level;
char	*form;
va_dcl
{
	va_list	vl;
# ifndef _WIN32
	int	err = errno;
# else
	int	err = WSAGetLastError();
# endif

	va_start(vl);
#endif

	if ((debuglevel >= 0) && (level <= debuglevel))
	    {
#ifndef	USE_VARARGS
		(void)sprintf(debugbuf, form,
				p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
#else
		(void)vsprintf(debugbuf, form, vl);
#endif
#ifndef _WIN32
		if (local[2])
		    {
			local[2]->sendM++;
			local[2]->sendB += strlen(debugbuf);
		    }
		(void)fprintf(stderr, "%s", debugbuf);
		(void)fputc('\n', stderr);
	    }
	errno = err;
#else
		strcat(debugbuf, "\r");
		Cio_Puts(hCio, debugbuf, strlen(debugbuf));
	    }
	WSASetLastError(err);
#endif
}

/*
 * This is part of the STATS replies. There is no offical numeric for this
 * since this isnt an official command, in much the same way as HASH isnt.
 * It is also possible that some systems wont support this call or have
 * different field names for "struct rusage".
 * -avalon
 */
void	send_usage(cptr, nick)
aClient *cptr;
char	*nick;
{

#ifdef GETRUSAGE_2
	struct	rusage	rus;
	time_t	secs, rup;
#ifdef	hz
# define hzz hz
#else
# ifdef HZ
#  define hzz HZ
# else
	int	hzz = 1;
#  ifdef HPUX
	hzz = (int)sysconf(_SC_CLK_TCK);
#  endif
# endif
#endif

	if (getrusage(RUSAGE_SELF, &rus) == -1)
	    {
#if !defined(__FreeBSD__) && !defined(__NetBSD__)
		extern char *sys_errlist[];
#endif
		sendto_one(cptr,":%s NOTICE %s :Getruseage error: %s.",
			   me.name, nick, sys_errlist[errno]);
		return;
	    }
	secs = rus.ru_utime.tv_sec + rus.ru_stime.tv_sec;
	rup = time(NULL) - me.since;
	if (secs == 0)
		secs = 1;

	sendto_one(cptr,
		   ":%s %d %s :CPU Secs %d:%d User %d:%d System %d:%d",
		   me.name, RPL_STATSDEBUG, nick, secs/60, secs%60,
		   rus.ru_utime.tv_sec/60, rus.ru_utime.tv_sec%60,
		   rus.ru_stime.tv_sec/60, rus.ru_stime.tv_sec%60);
	sendto_one(cptr, ":%s %d %s :RSS %d ShMem %d Data %d Stack %d",
		   me.name, RPL_STATSDEBUG, nick, rus.ru_maxrss,
		   rus.ru_ixrss / (rup * hzz), rus.ru_idrss / (rup * hzz),
		   rus.ru_isrss / (rup * hzz));
	sendto_one(cptr, ":%s %d %s :Swaps %d Reclaims %d Faults %d",
		   me.name, RPL_STATSDEBUG, nick, rus.ru_nswap,
		   rus.ru_minflt, rus.ru_majflt);
	sendto_one(cptr, ":%s %d %s :Block in %d out %d",
		   me.name, RPL_STATSDEBUG, nick, rus.ru_inblock,
		   rus.ru_oublock);
	sendto_one(cptr, ":%s %d %s :Msg Rcv %d Send %d",
		   me.name, RPL_STATSDEBUG, nick, rus.ru_msgrcv, rus.ru_msgsnd);
	sendto_one(cptr, ":%s %d %s :Signals %d Context Vol. %d Invol %d",
		   me.name, RPL_STATSDEBUG, nick, rus.ru_nsignals,
		   rus.ru_nvcsw, rus.ru_nivcsw);
#else
# ifdef TIMES_2
	struct	tms	tmsbuf;
	time_t	secs, mins;
	int	hzz = 1, ticpermin;
	int	umin, smin, usec, ssec;

#  ifdef HPUX
	hzz = sysconf(_SC_CLK_TCK);
#  endif
	ticpermin = hzz * 60;

	umin = tmsbuf.tms_utime / ticpermin;
	usec = (tmsbuf.tms_utime%ticpermin)/(float)hzz;
	smin = tmsbuf.tms_stime / ticpermin;
	ssec = (tmsbuf.tms_stime%ticpermin)/(float)hzz;
	secs = usec + ssec;
	mins = (secs/60) + umin + smin;
	secs %= hzz;

	if (times(&tmsbuf) == -1)
	    {
		sendto_one(cptr,":%s %d %s :times(2) error: %s.",
#  ifndef _WIN32
			   me.name, RPL_STATSDEBUG, nick, strerror(errno));
#  else
			   me.name, RPL_STATSDEBUG, nick,
				   strerror(WSAGetLastError()));
#  endif
		return;
	    }
	secs = tmsbuf.tms_utime + tmsbuf.tms_stime;

	sendto_one(cptr,
		   ":%s %d %s :CPU Secs %d:%d User %d:%d System %d:%d",
		   me.name, RPL_STATSDEBUG, nick, mins, secs, umin, usec,
		   smin, ssec);
# endif
#endif
	sendto_one(cptr, ":%s %d %s :Reads %d Writes %d",
		   me.name, RPL_STATSDEBUG, nick, readcalls, writecalls);
	sendto_one(cptr, ":%s %d %s :DBUF alloc %d blocks %d",
		   me.name, RPL_STATSDEBUG, nick, dbufalloc, dbufblocks);
	sendto_one(cptr,
		   ":%s %d %s :Writes:  <0 %d 0 %d <16 %d <32 %d <64 %d",
		   me.name, RPL_STATSDEBUG, nick,
		   writeb[0], writeb[1], writeb[2], writeb[3], writeb[4]);
	sendto_one(cptr,
		   ":%s %d %s :<128 %d <256 %d <512 %d <1024 %d >1024 %d",
		   me.name, RPL_STATSDEBUG, nick,
		   writeb[5], writeb[6], writeb[7], writeb[8], writeb[9]);
	return;
}
#endif

int send_za_stats(aClient *cptr, char *name, ZMast *master)
{
	int totalua = master->n_blocks*master->count;
	int ret = master->n_blocks*master->bsize;
	
	sendto_one(cptr, ":%s 249 %s :%s: %d/%d (%d/%d) block %d*(%d+%d)+%d=%d",
		me.name, cptr->name, name, master->n_items, totalua,
		master->n_items*master->size, ret, master->count,
		master->size, offsetof(ZItem, data), offsetof(ZBlock, items),
		master->bsize);
	
	return ret;
}

int send_da_stats(aClient *cptr, char *name, DMast *master)
{
	int totalua = master->blocks*master->count;
	int ret = totalua*master->size;
	
	sendto_one(cptr, ":%s 249 %s :%s: %d/%d (%d/%d) block %d*%d=%d",
		me.name, cptr->name, name, master->used, totalua,
		master->used*master->size, ret, master->count, master->size,
		master->count*master->size);

	return ret;
}

void	count_memory(cptr, nick)
aClient	*cptr;
char	*nick;
{
	static u_int rm = 0, banc = 0, banm = 0;
	static u_int watchc = 0, watchm = 0;
	u_int tot = 0, db, wwm, stat = 0, tot2;
	extern	aChannel	*channel;
	extern	ZMast	dbbmast;

	aClient *acptr;
	Ban *ban;
	Link *link;
	Member *mptr;
	aChannel *chptr;
	aConfItem *aconf;

	sendto_one(cptr, ":%s 249 %s :Rough memory usage (many sections are omitted, including codespace)",
		me.name, nick);
	sendto_one(cptr, ":%s 249 %s :TYPE: USED/ALLOC UNITS (BYTES) BLOCK BYTES",
		me.name, nick);
	tot += send_da_stats(cptr, "Client Local", &cloc_mast);
	tot += send_da_stats(cptr, "Client Remote", &crem_mast);
	tot += send_da_stats(cptr, "Chans", &chan_mast);
	tot += send_da_stats(cptr, "Members", &member_mast);
	tot += send_da_stats(cptr, "Links", &link_mast);
	tot += send_da_stats(cptr, "Aways", &away_mast);
	
	tot += send_za_stats(cptr, "DBufs", &dbbmast);
	
	if (IsAnOper(cptr)) /* only do the dirty work for IRCops */
	{
		banm = banc = 0;
		for (chptr = channel; chptr; chptr = chptr->nextch)
		{
			for (ban = chptr->banlist; ban; ban = ban->next)
			{
				banc++;
				banm += sizeof(Ban) + strlen(ban->banstr)
					+ strlen(ban->who) + 2;
			}
		}
		rm = cres_mem(cptr);
		count_watch_memory(&watchc, &watchm);
	}
	
	if (rm) /* if an ircop has used this already, use old info */
		sendto_one(cptr, ":%s 249 %s :Res memory %d", me.name, nick, rm);
	if (banc)
		sendto_one(cptr, ":%s 249 %s :Bans: %d(%d)", me.name, nick,
			banc, banm);
	if (watchc)
		sendto_one(cptr, ":%s 449 %s :Watch: %d(%d)", me.name, nick,
			watchc, watchm);
	tot += banm + rm + watchm;
	
#ifdef _WIN32
	tot2 = tot;
#else
	tot2 = (u_int)sbrk((size_t)0)-(u_int)sbrk0;
#endif
	sendto_one(cptr, ":%s 249 %s :Total dynamic mem accounted/total: %d/%d",
		me.name, nick, tot, tot2);
	
	stat += sizeof(void *) * CLIENTHASHSIZE;
	stat += sizeof(void *) * CHANNELHASHSIZE;
	stat += sizeof(void *) * SERVHASHSIZE;
	stat += sizeof(void *) * NOTIFYHASHSIZE;
	sendto_one(cptr, ":%s 249 %s :Hash: %d", me.name, nick, stat);
	wwm = sizeof(aName) * NICKNAMEHISTORYLENGTH;
	stat += wwm; 
	sendto_one(cptr, ":%s 249 %s :Whowas memory: %d (%d*%d)",
		me.name, nick, wwm, NICKNAMEHISTORYLENGTH, sizeof(aName));

	tot += stat;
	tot2 += stat;

	sendto_one(cptr, ":%s 249 %s :TOTAL: %dR/%dV (all approx.)",
		me.name, nick, tot, tot2);
	return;
}
