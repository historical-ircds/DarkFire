/************************************************************************
 *   IRC - Internet Relay Chat, ircd/s_misc.c (formerly ircd/date.c)
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   See file AUTHORS in IRC package for additional names of
 *   the programmers.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef lint
static  char sccsid[] = "@(#)s_misc.c	2.42 3/1/94 (C) 1988 University of Oulu, \
Computing Center and Jarkko Oikarinen";
#endif

#ifndef _WIN32
#include <sys/time.h>
#endif
#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include <sys/stat.h>
#include <fcntl.h>
#if !defined(ULTRIX) && !defined(SGI) && !defined(sequent) && \
    !defined(__convex__) && !defined(_WIN32)
# include <sys/param.h>
#endif
#if defined(PCS) || defined(AIX) || defined(SVR3)
# include <time.h>
#endif
#ifdef HPUX
#include <unistd.h>
#endif
#ifdef DYNIXPTX
#include <sys/types.h>
#include <time.h>
#endif
#ifdef _WIN32
# include <io.h>
#endif
#include "h.h"

#include "secret.h"

int	exit_server PROTO((aClient *, aClient *, aClient *, char *, char *));
static	void	exit_one_client PROTO((aClient *,aClient *,aClient *,char *,char *));

static	char	*months[] = {
	"January",	"February",	"March",	"April",
	"May",	        "June",	        "July",	        "August",
	"September",	"October",	"November",	"December"
};

static	char	*weekdays[] = {
	"Sunday",	"Monday",	"Tuesday",	"Wednesday",
	"Thursday",	"Friday",	"Saturday"
};

/*
 * stats stuff
 */

char	*date(clock) 
time_t	clock;
{
	static	char	buf[80], plus;
	Reg1	struct	tm *lt, *gm;
	struct	tm	gmbuf;
	int	minswest;

	if (!clock) 
		time(&clock);
	gm = gmtime(&clock);
	bcopy((char *)gm, (char *)&gmbuf, sizeof(gmbuf));
	gm = &gmbuf;
	lt = localtime(&clock);

	if (lt->tm_yday == gm->tm_yday)
		minswest = (gm->tm_hour - lt->tm_hour) * 60 +
			   (gm->tm_min - lt->tm_min);
	else if (lt->tm_yday > gm->tm_yday)
		minswest = (gm->tm_hour - (lt->tm_hour + 24)) * 60;
	else
		minswest = ((gm->tm_hour + 24) - lt->tm_hour) * 60;

	plus = (minswest > 0) ? '-' : '+';
	if (minswest < 0)
		minswest = -minswest;

	mysprintf(buf, "%s %s %d %04d -- %02d:%02d %c%02d:%02d",
		weekdays[lt->tm_wday], months[lt->tm_mon],lt->tm_mday,
		1900+lt->tm_year, lt->tm_hour, lt->tm_min,
		plus, minswest/60, minswest%60);

	return buf;
}

/*
** canonize
**
** reduce a string of duplicate list entries to contain only the unique
** items.  Unavoidably O(n^2).
**
** rewritten by binary
*/
char	*canonize(buffer)
char	*buffer;
{
	static	char	cbuf[512];
	char *cpos = cbuf;
	char *bpos, *b2pos;
	char *bnext=buffer, *bend, *b2next;
	
	while (1)
	{
		bpos = bend = bnext;
		while (*bend != ',')
		{
			if (!*bend) /* just breaking from 2 loops */
				goto canonize_done;
			bend++;
		}
		
		*bend = '\0';
		
		bnext = bend+1;
		while (*bnext == ',')
			bnext++; /* skip excess commas */
		if (!*bnext)
			break;
			
		b2pos = bnext;
		
		while (1)
		{
			if ((b2next = index(b2pos, ',')))
				*b2next = '\0';
			if (mycmp(bpos, b2pos) == 0)
				break;
			if (!(b2pos = b2next))
				break;
			*b2pos++ = ',';
		}
		*bend++ = ',';
		if (b2pos == NULL) /* we didn't quit early */
		{
			strncpy(cpos, bpos, bend-bpos);
			cpos += (bend-bpos);
		}
		else if (b2next)
			*b2next = ',';
	}
canonize_done:
	strcpy(cpos, bpos);
	
	return cbuf;
	
}

/*
 * strip_colors()
 *
 *	Strip often annoying mIRC colors out of specially designated
 * messages.  Along with bold/underline/etc.
 */
char *strip_colors(in)
char *in;
{
	char *s, *m;
	
	if (!in)
		return "";
		
	m = in;
	s = in;
	while (*s)
	{
		if (*s < 32)
		{
			if (*s == 3) /* CTRL-C */
			{
				if (isdigit(*++s))
					if (isdigit(*++s))
						s++;
 				if (*s == ',')
					if (isdigit(*++s))
						if (isdigit(*++s))
							s++;
				continue;
			}
			if (*s == 2 || *s == 31 || *s == 22 || *s == 15)
			{
				s++;
				continue;
			}
		}
		*m++ = *s++;
	}
	*m = '\0';
	return(in);
}

/*
 * messagestamp()
 *
 *	Stamps a message for a hashlike string semi-identity detection.
 */
u_int32_t messagestamp(str)
u_char *str;
{
	char *s;
	u_int32_t val = 0;
	
	for (s = str; *s; s++)
	{
		val += (val>>27) + (val<<5) + (u_char)*s;
	}
	
	return val;
}

#ifndef SECRET_DIS
/*
 * disguise() - do the process of masking a hostname but keeping the
 *    addy somewhat unique to their ip
 * NOTE: This is the default disguise algorithm.  A custom include/secret.h
 * file can override this.  Include in there an algorithm similar to this
 * and include a #define for SECRET_DIS
 */
char *disguise(char *dest, char *src)
#define	VALID_IP(x)	((unsigned int)x < 256)
#ifndef DIS_KEY
#define	DIS_KEY	0
#endif
#define cons (sizeof(conv)-1)
{
	static char conv[] = "d7eu9l3zp1bty6nz0br2wony4e5vk9gijhi1xv0sf5amd2ojq8cpmk4xu6hgw3atc8fsrl7q";
	static char *list[16];
	u_char *m, *n, *s;
	u_int32_t a, b, c, d, z;
	int dots = 0, i;
	
	for (s = src; *s; s++)
		if (*s == '.')
		{
			list[dots] = s;
			if (++dots > 15)
				break;
		}
	
	/* if more than 15 dots...					*
	 * less than 2 dots (ie localhost or darkfire.net)		*
	 * or something like "taco.net.mx" or "something.co.uk" 	*
	 * we will keep original.					*/
	if (dots < 2 || dots > 15 || (dots == 2 && strlen(list[0]) <= 7))
		return strcpy(dest, src); /* don't bother... */
	
	list[dots] = s;

	if ((sscanf(src, "%d.%d.%d.%d", &a, &b, &c, &d)==4) &&
		VALID_IP(a) && VALID_IP(b) && VALID_IP(c) && VALID_IP(d))
	{
		if (dots == 3)
		{
			s = dest + mysprintf(dest, "%d.%d.%d.", a, b, c);
			z = (a*7) + (b*5) + (c*3) + d;
			*s++ = 'a' + z%26;
			z /= 26;
			*s++ = 'a' + z%26;
			*s = '\0';
			return dest;
		}
		else
			i = 3; /* host like 24.33.14.99.stupidisp.com */
	}
	else
	{
		if (list[dots]-src < 10)
			return strcpy(dest, src);
		for (i = 0; list[i]-src < 4 && i < dots-2; i++)
			;
	}
	
	s = list[i];
	*s++ = '\0';
	a = b = c = d = *s+DIS_KEY;
	for (m = src; *m; m++)
	{
		a = (a * 3) / 2 + (*m%15) + (*m%7) + (*m%35);
		b <<= 1;
		b ^= *m+12;
		c ^= a + 97 + *m;
		d ^= a + b;
	}
	s[-1] = '.';
	mysprintf(dest, "%c%c%c%c.%s", conv[a%cons], conv[b%cons],
		conv[c%cons], conv[d%cons], s);
	dest[HOSTLEN] = '\0';

	return dest;
}
#undef VALID_IP
#endif /* SECRET_DIS */

/*
 *  Fixes a string so that the first white space found becomes an end of
 * string marker (`\0`).  returns the 'fixed' string or "*" if the string
 * was NULL length or a NULL pointer.
 */
char	*check_string(s)
Reg1	char *s;
{
	static	char	star[2] = "*";
	char	*str = s;

	if (BadPtr(s))
		return star;

	for ( ;*s; s++)
		if (isspace(*s))
		    {
			*s = '\0';
			break;
		    }

	return (BadPtr(str)) ? star : str;
}

/*
 * convert a given mask into a nice nick!user@host, with some guess work
 */
char *pretty_mask(mask)
char *mask;
{
	char *cp = mask;
	char *user;
	char *host;
	
	collapse(mask); /* shorten first -- mnuh() trunctates fields */
	if ((user = index(cp, '!')))
		*user++ = '\0';
	if ((host = rindex(user ? user : cp, '@')))
	{
		*host++ = '\0';
		if (!user)
			return make_nick_user_host(NULL, cp, host);
	}
	else if (!user && index(cp, '.'))
		return make_nick_user_host(NULL, NULL, cp);
	return make_nick_user_host(cp, user, host);
}

/*
 * put it; cut off at a null or space char, and make sure we don't put
 *  more than len chars.  returns the number of non-null chars copied.
 * PLUS -- if the incoming parameter is greater than the specified len
 * and it begins with a *, get rid of the leading * (to cope with some
 * scripts that put a * before idents and end up messing it up)
 */
int mnu_put(dest, src, len)
char *dest, *src;
int len;
{
	char c, *s = src, *d = dest;
	
	if (!s || !*s)
	{
		*d = '*';
		return 1;
	}
	
	if (*s == '*' && strlen(s) > len)
		s++;
	
	for (; (c = *s) && c != ' ' && d-dest < len; d++, s++)
		*d = c;
	
	return d-dest;
}

/*
 * create a string of form "foo!bar@fubar" given foo, bar and fubar
 * as the parameters.  If NULL, they become "*".
 */
char *make_nick_user_host(nick, user, host)
Reg1	char	*nick, *user, *host;
{
	static	char	namebuf[NICKLEN+USERLEN+HOSTLEN+6];
	Reg2	char	*s = namebuf;

	s += mnu_put(s, nick, NICKLEN);
	*s++ = '!';
	s += mnu_put(s, user, USERLEN);
	*s++ = '@';
	s += mnu_put(s, host, HOSTLEN);
	*s = '\0';

	return (namebuf);
}

/**
 ** myctime()
 **   This is like standard ctime()-function, but it zaps away
 **   the newline from the end of that string. Also, it takes
 **   the time value as parameter, instead of pointer to it.
 **   Note that it is necessary to copy the string to alternate
 **   buffer (who knows how ctime() implements it, maybe it statically
 **   has newline there and never 'refreshes' it -- zapping that
 **   might break things in other places...)
 **
 **/

char	*myctime(value)
time_t	value;
{
	static	char	buf[28];
	Reg1	char	*p;

	(void)strcpy(buf, ctime(&value));
	if ((p = (char *)index(buf, '\n')) != NULL)
		*p = '\0';

	return buf;
}

/*
** dump a NULL-terminated array of strings to user sptr using
** the numeric rplnum, and then return 0
*/
int dumpit(aClient *sptr, int rplnum, char **p)
{
	for (; *p != NULL; p++)
		send_num(sptr, rplnum, *p);
	if (MyClient(sptr))
		sptr->since += 8; /* let user take 8 seconds to read it! */
	return 0;
}

/*
** check_registered_user is used to cancel message, if the
** originator is a server or not registered yet. In other
** words, passing this test, *MUST* guarantee that the
** sptr->u is valid (not checked after this--let there
** be coredumps to catch bugs... this is intentional --msa ;)
**
** There is this nagging feeling... should this NOT_REGISTERED
** error really be sent to remote users? This happening means
** that remote servers have this user registered, althout this
** one has it not... Not really users fault... Perhaps this
** error message should be restricted to local clients and some
** other thing generated for remotes...
*/
int	check_registered_user(sptr)
aClient	*sptr;
{
	if (!IsRegisteredUser(sptr))
	    {
		send_num(sptr, ERR_NOTREGISTERED);
		return -1;
	    }
	return 0;
}

/*
** check_registered user cancels message, if 'x' is not
** registered (e.g. we don't know yet whether a server
** or user)
*/
int	check_registered(sptr)
aClient	*sptr;
{
	if (!IsRegistered(sptr))
	    {
		send_num(sptr, ERR_NOTREGISTERED);
		return -1;
	    }
	return 0;
}

/*
**  check_umode makes sure user is an IRC operator and has all specified
** umodes; return 0 if so, otherwise send "no privileges" error str and
** return nonzero
**  however, allows remote and servers to pass
**
** sample usage: (note - all the following do allow remote users and servers)
**  if (check_privs(sptr, 0, 0)) return 0;            // IRCops only
**  if (check_privs(sptr, UMODE_SADMIN, 0)) return 0; // SA's only
**  if (check_privs(sptr, 0, OFLAG_WALLOP)) return 0; // has wallop oflag
**  if (check_privs(sptr, -1, 0)) return 0;           // servers only
*/
int	check_privs(sptr, umodes, oflags)
aClient	*sptr;
u_int32_t umodes, oflags;
{
	if (IsServer(sptr->from))
		return 0;
	if (IsAnOper(sptr) && ((sptr->umodes&umodes) == umodes)
	   && ((sptr->oflag&oflags) == oflags))
		return 0;
	
	send_num(sptr, ERR_NOPRIVILEGES);
	return -1;
}

/*
** check_params is to check if the user entered enough params
**  - if not, an error is sent to the user and -1 is returned
**  - normal usage would be:
**	if (check_params(sptr, "MODE", parc, 1))
**		return 0;
**  - can be used to quit with a "not enough params" error message
**  in case there are more factors than JUST parc to consider, such
**  as modified strings being empty.  This can be safely used in a
**  return because check_params returns 0 in this particular case.
**  A sample usage would be:
**	if (parc < 3 || !reason)
**		return check_params(sptr, "KILL", -1, 0);
*/
int	check_params(sptr, command, parc, needed)
aClient *sptr;
int parc, needed;
char *command;
{
	if (parc <= needed)
	{
		send_num(sptr, ERR_NEEDMOREPARAMS, command);
		return needed;
	}
	return 0;
}

/*
** get_client_name
**      Return the name of the client for various tracking and
**      admin purposes. The main purpose of this function is to
**      return the "socket host" name of the client, if that
**	differs from the advertised name (other than case).
**	But, this can be used to any client structure.
**
**	Returns:
**	  "name[user@ip#.port]" if 'showip' is true;
**	  "name[sockethost]", if name and sockhost are different and
**	  showip is false; else
**	  "name".
**
** NOTE 1:
**	Watch out the allocation of "nbuf", if either sptr->name
**	or sptr->sockhost gets changed into pointers instead of
**	directly allocated within the structure...
**
** NOTE 2:
**	Function return either a pointer to the structure (sptr) or
**	to internal buffer (nbuf). *NEVER* use the returned pointer
**	to modify what it points!!!
**
*/
char	*get_client_name(sptr, showip)
aClient *sptr;
int	showip;
{
	static char nbuf[HOSTLEN * 2 + USERLEN + 5];

	if (MyConnect(sptr))
	{
		if (showip)
		{
			mysprintf(nbuf, "%s[%s@%s.%d]",
				sptr->name,
				sptr->username,
				IsDisguise(sptr) ? "255.255.255.255" :
				inetntoa((char *)&sptr->ip),
				(unsigned int)sptr->port);
			return nbuf;
		}
		else
		{
			mysprintf(nbuf, "%s[%s]",
				sptr->name, IsPerson(sptr) ?
				sptr->u->host : sptr->sockhost);
			return nbuf;
		}
	}
	return sptr->name;
}

char	*get_client_host(cptr)
aClient	*cptr;
{
	static char nbuf[HOSTLEN * 2 + USERLEN + 5];

	if (!MyConnect(cptr))
		return cptr->name;
	if (!cptr->hostp)
		return get_client_name(cptr, FALSE);
	(void)sprintf(nbuf, "%s[%-.*s@%-.*s]",
		cptr->name, USERLEN,
		(!(cptr->flags & FLAGS_GOTID)) ? "" : cptr->username,
		HOSTLEN, cptr->hostp->h_name);
	return nbuf;
}

/*
 * Form sockhost such that if the host is of form user@host, only the host
 * portion is copied.
 */
void	get_sockhost(cptr, host)
Reg1	aClient	*cptr;
Reg2	char	*host;
{
	Reg3	char	*s;
	if ((s = (char *)index(host, '@')))
		s++;
	else
		s = host;
	strncpyzt(cptr->sockhost, s, sizeof(cptr->sockhost));
}

/*
 * Return wildcard name of my server name according to given config entry
 * --Jto
 */
char	*my_name_for_link(name, aconf)
char	*name;
aConfItem *aconf;
{
	static	char	namebuf[HOSTLEN];
	register int	count = aconf->port;
	register char	*start = name;

	if (count <= 0 || count > 5)
		return start;

	while (count-- && name)
	{
		name++;
		name = (char *)index(name, '.');
	}
	if (!name)
		return start;

	namebuf[0] = '*';
	(void)strncpy(&namebuf[1], name, HOSTLEN - 1);
	namebuf[HOSTLEN - 1] = '\0';

	return namebuf;
}

aClient *top_server;

/*
** exit_client
**	This is old "m_bye". Name  changed, because this is not a
**	protocol function, but a general server utility function.
**
**	This function exits a client of *any* type (user, server, etc)
**	from this server. Also, this generates all necessary prototol
**	messages that this exit may cause.
**
**   1) If the client is a local client, then this implicitly
**	exits all other clients depending on this connection (e.g.
**	remote clients having 'from'-field that points to this.
**
**   2) If the client is a remote client, then only this is exited.
**
** For convenience, this function returns a suitable value for
** m_funtion return value:
**
**	FLUSH_BUFFER	if (cptr == sptr)
**	0		if (cptr != sptr)
*/
int	exit_client(cptr, sptr, from, comment)
aClient *cptr;	/*
		** The local client originating the exit or NULL, if this
		** exit is generated by this server for internal reasons.
		** This will not get any of the generated messages.
		*/
aClient *sptr;	/* Client exiting */
aClient *from;	/* Client firing off this Exit, never NULL! */
char	*comment;	/* Reason for the exit */
{
	aClient	*acptr;
	aClient	*next;
	
	if (MyConnect(sptr))
	{
		if (IsMe(sptr)) return 0; /* bad bad... */
		sptr->flags |= FLAGS_CLOSING;
		if (IsPerson(sptr))
			sendto_umode(UMODE_CLIENT,
				"*** Notice -- Client exiting: %s (%s@%s) [%s]", 
				sptr->name, sptr->username,
				sptr->u->rhost, comment);
		if (sptr->fd >= 0 && !IsConnecting(sptr))
		{
			if (cptr != NULL && sptr != cptr)
				sendto_one(sptr, "ERROR :Closing Link: %s %s (%s)",
					get_client_name(sptr,FALSE),
					cptr->name, comment);
			else
				sendto_one(sptr, "ERROR :Closing Link: %s (%s)",
					get_client_name(sptr,FALSE), comment);
		}
		close_connection(sptr);
	}

	if (IsServer(sptr))
	{
		static	char	splitcomment[64];
		
		mysprintf(splitcomment, "Split: %s <-> %s", sptr->s->shortn,
			sptr->srvptr->s->shortn);
		
		sendto_ops("netsplit: %s by %s (%s)", splitcomment+7,
			from->name, comment);
		
		top_server = sptr;
		return exit_server(cptr, sptr, from, splitcomment, comment);
	}

	exit_one_client(cptr, sptr, from, comment, NULL);
	return cptr == sptr ? FLUSH_BUFFER : 0;
}

/*
 * recursive function to squit servers
 * and this gets the right servers in the message.
 */
int exit_server(cptr, sptr, from, comment, rcomment)
aClient *cptr, *sptr, *from;
char *comment, *rcomment;
{
	aClient *next, *prev, *acptr;
	
	/* First, remove the clients on the server itself. */
	for (acptr = client; acptr; acptr=next)
	{
		next = acptr->next;
		if ((acptr->srvptr == sptr) && IsClient(acptr))
			exit_one_client(NULL, acptr, &me, comment, rcomment);
	}
	/*
	 * Now, go SQUIT off the servers which are down-stream of
	 * the one we just lost.
	 */
	for (acptr = sptr->s->down; acptr; acptr=next)
	{
		next = acptr->s->side;
		exit_server(sptr, acptr, sptr, comment, rcomment);
	}

	exit_one_client(cptr, sptr, from, comment, rcomment);
 	
	return cptr == sptr ? FLUSH_BUFFER : 0;
}

/*
** Exit one client, local or remote. Assuming all dependants have
** been already removed, and socket closed for local client.
*/
/* DANGER: Ugly hack follows. */
/* Yeah :/ */
/* not quite anymore, I guess.. */
static	void	exit_one_client(cptr, sptr, from, comment, rcomment)
aClient *sptr;
aClient *cptr;
aClient *from;
char	*comment;
char	*rcomment;
{
	aClient *acptr;
	Member	*mp, *mp2;
	int	i;
	Link	*lp;
	anUser	*user;

	/*
	**  For a server or user quitting, propagate the information to
	**  other servers (except to the one where is came from (cptr))
	*/
	if (IsMe(sptr))
	{
		sendto_ops("ERROR: tried to exit me! : %s", comment);
		return;	/* ...must *never* exit self!! */
	}
	else if (IsServer(sptr))
	{
		if (!rcomment || !*rcomment)
			rcomment = "Unspecified Reason";
		for (i = 0; i <= highest_fd; i++)
		{
			if (!(acptr = local[i]) || !IsServer(acptr) ||
					acptr == cptr || IsMe(acptr))
				continue;
			/* We are doing a cascade of servers.  To *
			 * get the right server names in the quit * 
			 * messages, we don't send leaf squits to *
			 * servers that have the NoQuit protoctl. */
			if (sptr!=top_server && DontSendQuit(acptr))
				continue;
			
			if (sptr->from == acptr)
				sendto_one(acptr, ":%s SQUIT %s :%s",
					from->name, sptr->name, rcomment);
			else
				sendto_one(acptr, "SQUIT %s :%s",
					sptr->name, rcomment);
		}
	}
	else if (sptr->name[0] && IsPerson(sptr))
		/* ...just clean all others with QUIT... */
	{
		/*
		** If this exit is generated from "m_kill", then there
		** is no sense in sending the QUIT--KILL's have been
		** sent instead.
		*/
		if (!(sptr->flags & FLAGS_KILLED))
		{
			if (rcomment) /* rcomment specified only for splits */
				sendto_serv_butone_nproto(cptr, PROTO_NOQUIT,
					":%s QUIT :%s", sptr->name, comment);
			else
				sendto_serv_butone(cptr, ":%s QUIT :%s",
					sptr->name, comment);
		}
				
		sendto_common_channels(sptr, "QUIT :%s", comment);
		
		if (sptr->fd == -2)
		{
			hash_del_notify_list(sptr);
			if (sptr->lopt)
			{
				free_str_list(sptr->lopt->yeslist);
				free_str_list(sptr->lopt->nolist);
				MyFree(sptr->lopt);
			}
		}
		
		if (sptr->u->away)
			da_free(&away_mast, sptr->u->away);
		if (sptr->u->hush)
			MyFree(sptr->u->hush);
			
		while (mp = sptr->u->channel)
			member_del(mp);
			
		/* Clean up invitefield */
		while ((lp = sptr->u->invited))
			del_invite(sptr, lp->value.chptr);
			
		/* Clean up silencefield */
		while ((lp = sptr->u->silence))
			del_silence(sptr, lp->value.cp);
		
		hash_check_notify(sptr, RPL_LOGOFF);
	}
	remove_client_from_list(sptr);
}

void	checklist()
{
	Reg1	aClient	*acptr;
	Reg2	int	i;

	if (!(bootopt & BOOT_AUTODIE))
		return;
	for (i = 0; i <= highest_fd; i++)
		if ((acptr = local[i]) && IsClient(acptr))
			return;
#ifdef	USE_SYSLOG
	syslog(LOG_WARNING,"ircd exiting: autodie");
#endif
	exit(0);
}
