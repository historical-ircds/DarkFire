/************************************************************************
 *   IRC - Internet Relay Chat, ircd/mode.c
 *   Copyright (C) 2001 Garry Boyer
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "struct.h"
#include "common.h"
#include "sys.h"
#include "numeric.h"
#include "channel.h"
#include "msg.h"	/* For TOK_*** and MSG_*** strings  */
#include "h.h"

#ifndef	lint
static	char sccsid[] = "@(#)mode.c	1.0 (C) 2001-02 Garry Boyer";
#endif

/*
 * mode.c - taken out of channel.c because the functions simply involved
 *  with reading and writing to mode buffers took up a large quantity of
 *  space in the midst of mostly unrelated functions
 *
 * Almost all of this is original code, Copyright (C) 2001-02 Garry Boyer
 *
 * NOTES: Heavily streamlined.  Heavily!  Very good performance now.....
 *  bans and o/v don't have to be put into separate buffers at separate
 *  times when doing mode syncs, due to improved modular programming. :)
 *  Now you'll get combinations like
 *     +bbbbbovo a!*@* b!*@* c!*@* d!*@* e!*@* op1 ov1 ov1
 *     +ooovovontl op2 op3 op4 voice1 ov2 ov2 op5 768
 *     +a 1
 *  on separate lines, containing 8 parameters whenever possible.  Note
 *  that +ov is now properly synced! (expanded to 10 parameters)
 *
 *						-- binary
 *
 * NEW: Now, whenever a user sends a command that *might* cause a buffer
 *  overflow, the buffer is flushed and cleared before this might happen.
 *  With mb.pvar stuff gone, one mode command is not necessarily limited
 *  to 12 mode parameters.  Thus, the channel mode +0 not only bzeroes the
 *  mode struct but also removes all channel bans.
 * Now on the input side a mode command can have unlimited parameters; the
 * parse function reads it all as one big param and the mode reader does
 * all the separation.
 */

extern	int	del_banid PROTO((aClient *, aChannel *, char *));
extern	int	find_banid PROTO((aChannel *, char *));
extern	int	add_banid PROTO((aClient *, aChannel *, char *));
extern	aClient	*find_chasing PROTO((aClient *, char *, int *));

static	void	mb_init PROTO((aClient *, aClient *, aChannel *, time_t, char));
static	void	mb_clear PROTO(());
inline	void	mb_finalize PROTO(());
static	void	mb_addp PROTO(());
static	void	mb_write PROTO((char *, char *, int));
static	void	mb_writep PROTO((char, char, char *));
inline	void	mb_writeflag PROTO((char, char));
static	void	mb_flush PROTO((int));

static	void	mb_sendmemberflags PROTO(());
static	void	mb_sendbans PROTO(());
static	void	mb_unban PROTO((aClient *, char *));
static	Ban	**mb_delban PROTO((Ban **));

static	void	mb_getmodes PROTO((int));
static	void	mb_readmode PROTO((Mode *, char *, char));
static	void	mb_sendall PROTO((Mode *, Mode *, int));
static	int	mb_readchar PROTO((Mode *, ModeFlag *, char, char *, char));
static	void	do_mode PROTO((aChannel *, aClient *, char *, time_t));
static	void	bounce_mode PROTO((aChannel *, aClient *, char *));

static	ModeBuf	mb; /* check channel.h for info about ModeBuf */


 /**** LIST OF MODE CHAR-FLAGS AND RESPECTIVE BIT-FLAGS ****/

/*
 * chan_modes[] - array containing definitions of mode chars that are
 *   not simple flags
 */
static ModeFlag chan_modes[] =
{
	{ MODE_MEMBER,		'o', CHFL_CHANOP		},
	{ MODE_MEMBER,		'v', CHFL_VOICE			},
	{ MODE_MEMBER,		'h', 0				},
	{ MODE_INTPARAM,	'a', offsetof(Mode, maxrept)	},
	{ MODE_INTPARAM,	'l', offsetof(Mode, limit)	},
	{ MODE_INTPARAM,	'f', offsetof(Mode, flood)	},
	{ MODE_STRPARAM,	'k', offsetof(Mode, key[0])	},
	{ MODE_BAN,		'b', 0				},
	{ MODE_CLEAR,		'0', 0				},
	{ 0, 0, 0 }
};

/*
 * chan_flags[] - array containing mode definitions for simple flags
 *   that are OR'd into chptr->mode.mode
 */
static ModeFlag chan_flags[] =
{
	{ MODE_NOPRIVMSGS,	'n', 0				},
	{ MODE_MODERATED,	'm', 0				},
	{ MODE_TOPICLIMIT,	't', 0				},
	{ MODE_STRIPCOLOR,	'c', 0				},
	{ MODE_SECRET,		's', 0				},
	{ MODE_PRIVATE,		'p', 0				},
	{ MODE_RGSTR,		'r', 0				},
	{ MODE_RGSTRONLY,	'R', 0				},
	{ MODE_INVITEONLY,	'i', 0				},
	{ MODE_PRIVMSGONLY,	'z', 0				},
	{ MODE_NODCCS,		'd', 0				},
	{ 0, 0, 0 }
};

static ModeFlag *chan_modemap[256];

/*
 * mode_init() -- init all the mode stuff that needs to be initialized
 *  at start of runtime
 *
 * Currently, initiliazes (for example) chan_modemap['n'] to be a pointer
 * to the ModeFlag struct for the specified channel mode 'n'
 */
void mode_init()
{
	int i;
	ModeFlag *mflag;
	
	for (i = 0; i < 256; i++)
		chan_modemap[i] = NULL;
	
	for (mflag = chan_modes; (i = (u_char)mflag->c); mflag++)
		chan_modemap[i] = mflag;
	for (mflag = chan_flags; (i = (u_char)mflag->c); mflag++)
		chan_modemap[i] = mflag;
}


 /**** MODEBUF FUNCTIONS (INIT/FINALIZE/WRITE/FLUSH) ****/


/*
 * mb_init()
 *  - initializes mode buffers and related variables; sets cptr as the
 *  destination client and chptr as the channel
 *  - if sign is non-zero, that character will be placed at the beginning
 *  - from is the client from whom the message came (never null when sending)
 *  - if not null, cptr is the client to send to; otherwise the message
 *  will be sent to all servers and channel members but 'from'
 */
void mb_init(aClient *from, aClient *cptr, aChannel *chptr,
	time_t sendts, char sign)
{
	mb.fsign = sign;
	mb.from = from;
	mb.dest = cptr;
	mb.chptr = chptr;
	mb.sendts = sendts;
	mb.sent = 0;
	mb_clear();
}

/*
 * mb_clear()
 *  - clear the buffers but leave the initial data alone
 */
void mb_clear()
{
	mb.mpos = mb.mbuf;
	if ((mb.sign = mb.fsign))
		*mb.mpos++ = mb.sign;
	*mb.mpos = '\0';
	mb.ppos = mb.pbuf;
	*mb.ppos = '\0';
	mb.params = 0;
}

/*
 * mb_finalize()
 *  - terminate the strings at the current positions, make mbuf "+" if empty
 */
void mb_finalize()
{
	if (mb.mpos == mb.mbuf)
		*mb.mpos++ = '+';
	*mb.mpos = '\0';
}

/*
 * mb_addp()
 *  - increase parameter and check param count and buffer size; send if
 *  we surpass max mode params or we encounter possible buffer overflow
 *  when reassembling the string before sending it
 */
static void mb_addp()
{
	if (mb.params >= 10 || mb.mpos-mb.mbuf+mb.ppos-mb.pbuf > MBSLEN)
		mb_flush(1); /* flush after 10 params or possible overflow */
	mb.params++;
}

/*
 * mb_write()
 *  - write to the modebuf; include string param if sparam is non-null,
 *  or integer param if iparam is non-zero; now with orphan control!
 */
void mb_write(char *change, char *sparam, int iparam)
{
	if (sparam || iparam)
		mb_addp(); /* make sure enough room for another param... */

	mb_writeflag(change[0], change[1]);

	if (sparam)
		mb.ppos += mysprintf(mb.ppos, " %s", sparam);
	else if (iparam)
		mb.ppos += mysprintf(mb.ppos, " %d", iparam);
}

/*
 * mb_writep()
 *  - write a mode specifically with a string parameter
 */
void mb_writep(char sign, char flag, char *param)
{
	mb_addp();
	mb_writeflag(sign, flag);
	*mb.ppos = ' ';
	mb.ppos = strput(mb.ppos+1, param);
}

/*
 * mb_writeflag()
 *  - write a simple mode (no parameters)
 */
inline void mb_writeflag(char sign, char flag)
{
	if (mb.sign != sign)
		*mb.mpos++ = mb.sign = sign;
	*mb.mpos++ = flag;
}

/*
 * mb_flush()
 *  - send all the data in the buffers, and wipe it out again
 *  - assumes that mb.dest and mb.chptr are already set to NON-NULL vals!
 *  - if sendempty == 0, data will be sent only if the mode bufs are not empty
 */
void mb_flush(int sendempty)
{
	mb_finalize();
	
	if (!sendempty && mb.mpos<mb.mbuf+2)
		return;
	
	if (mb.dest) /* we are sending to just one client */
	{
		if (!IsToken(mb.dest))
			sendto_one(mb.dest, ":%s MODE %s %s%s %d",
				mb.from->name, mb.chptr->chname, mb.mbuf,
				mb.pbuf, mb.sendts);
		else
			sendto_one(mb.dest, TOK_MODE" %s %s%s %d",
				mb.chptr->chname, mb.mbuf, mb.pbuf, mb.sendts);
	}
	else
	{
		if (IsServer(mb.from))
		{
			sendto_serv_butone_nproto(mb.from, PROTO_SERVHASH,
				":%s MODE %s %s%s %d", mb.from->name,
				mb.chptr->chname, mb.mbuf, mb.pbuf, mb.sendts);
			sendto_serv_butone_wproto(mb.from, PROTO_SERVHASH,
				":%s "TOK_MODE" %s %s%s %d", mb.from->s->sndot,
				mb.chptr->chname, mb.mbuf, mb.pbuf, mb.sendts);
		}
		else
			sendto_serv_butone(mb.from, ":%s MODE %s %s%s 0",
				mb.from->name, mb.chptr->chname, mb.mbuf,
				mb.pbuf);
		if (mb.mbuf[1]) /* don't send blanks to users */
			sendto_channel_butserv(mb.chptr, mb.from, "MODE %s %s%s",
				mb.chptr->chname, mb.mbuf, mb.pbuf);
		if (mb.sendts == TS_SAMODE)
			sendout_wallops("%s used SAMODE cmd on %s (%s%s)", mb.from->name, mb.chptr->chname, mb.mbuf, mb.pbuf);
	}
	
	mb_clear();
	mb.sent = 1;
}


 /**** FUNCTIONS THAT PUT TOGETHER MODE STRINGS OR SYNC MODES ****/


/*
 * remove a ban
 */
Ban **mb_delban(Ban **bpp)
{
	Ban *ban = *bpp;
	
	*bpp = ban->next;
	mb_write("-b", ban->banstr, 0);
	free_ban(ban);
	
	return bpp;
}

/*
 * mb_unban(cptr, mask)
 *  - if mask specified, clear all bans matching that mask; else if cptr
 *  specified, clear all bans affecting that user; otherwise, clear all bans
 *  - reset banflags accordingly; after this, no user is marked as banned
 */
void mb_unban(aClient *cptr, char *mask)
{
	Ban **bpp = &mb.chptr->banlist;
	Member *mp;

	if (mask)
	{
		while (*bpp)
			if (match(mask, (*bpp)->banstr)==0)
				mb_delban(bpp);
			else
				bpp = &(*bpp)->next;
	}
	else
		while (*(bpp = next_ban(cptr, bpp)))
			mb_delban(bpp);
	
	for (mp = mb.chptr->members; mp; mp = mp->nextu)
		mp->flags &= ~CHFL_BAN;
}

/*
 * mb_sendbans()
 *  - send channel bans to the already designated destinations
 *  (modebufs must be pre-initialized)
 *  - may be left over data at end
 */
void mb_sendbans()
{
	Ban *ban;

	for (ban = mb.chptr->banlist; ban; ban = ban->next)
		mb_write("+b", ban->banstr, 0);
}

/*
 * mb_sendmemberflags()
 *  - send appropriate member flags; modebufs must be already initialized
 */
void mb_sendmemberflags()
{
	Member *mp;
	char *name;
	
	for (mp = mb.chptr->members; mp; mp = mp->nextu)
	{
		if (mp->flags&CHFL_OVERLAP)
		{
			if (mp->flags&CHFL_VOICE)
				mb_write("+v", mp->cptr->name, 0);
			if (mp->flags&CHFL_CHANOP)
				mb_write("+o", mp->cptr->name, 0);
		}
	}
}

/*
 * this basically takes info for the selected mb.chptr and places a basic
 * channel mode string into mb.mbuf and mb.pbuf.  all the modebuf stuff
 * must be initialized first! (with the '+' sign)
 *  - send parameters only if sendp is nonzero
 */
void mb_getmodes(int sendp)
{
	ModeFlag *mflag;
	aChannel *chptr = mb.chptr;
	u_int32_t mode = chptr->mode.mode;
	
	for (mflag = chan_flags; mflag->flag; mflag++)
		if (mode & mflag->flag)
			*mb.mpos++ = mflag->c;
	
	if (*chptr->mode.key)
		mb_write("+k", sendp ? chptr->mode.key : NULL, 0);
	if (chptr->mode.limit)
		mb_write("+l", NULL, sendp ? chptr->mode.limit : 0);
	if (chptr->mode.maxrept)
		mb_write("+a", NULL, sendp ? chptr->mode.maxrept : 0);
	if (chptr->mode.flood)
		mb_write("+f", NULL, sendp ? chptr->mode.flood : 0);
	
	mb_finalize();
}

/*
 * send "cptr" a full list of the modes for channel chptr.
 */
void send_channel_modes(aClient *cptr, aChannel *chptr)
{
	mb_init(&me, cptr, chptr, chptr->creationtime, '+');
	mb_sendmemberflags();
	mb_sendbans();
	mb_getmodes(1);
	mb_flush(!mb.sent);
}

/* mb_sendall -- written by binary
 *	Constructs a string containing all the changes between two mode
 *  structures, and flushes at the end.  If the mode buffer is empty,
 *  it will only be sent if sendempty is true and no mode has been sent
 *  out already for this initialization of mb.
 */
void mb_sendall(Mode *oldm, Mode *newm, int sendempty)
{
	ModeFlag *mflag;
	u_int32_t x, diff;
	
	/* flag +<x> */
	if ((diff = (~oldm->mode)&newm->mode))
		for (mflag = chan_flags; (x = mflag->flag); mflag++)
			if ((diff&x))
				mb_writeflag('+', mflag->c);
	/* flag -<x> */
	if ((diff = (~newm->mode)&oldm->mode))
		for (mflag = chan_flags; (x = mflag->flag); mflag++)
			if ((diff&x))
				mb_writeflag('-', mflag->c);

	/* mode +k: channel key */
	if (strcmp(newm->key, oldm->key))
	{
		if (*newm->key)
			mb_write("+k", newm->key, 0);
		else
			mb_write("-k", oldm->key, 0);
	}

	/* mode +l: user limit */
	if (newm->limit != oldm->limit)
		mb_write(newm->limit ? "+l" : "-l", NULL, newm->limit);

	/* mode +a: anti repeat */
	if (newm->maxrept != oldm->maxrept)
		mb_write(newm->maxrept ? "+a" : "-a", NULL, newm->maxrept);
	
	if (newm->flood != oldm->flood)
		mb_write(newm->flood ? "+f" : "-f", NULL, newm->flood);
	
	mb_flush(sendempty&&!mb.sent);
}


 /**** FUNCTIONS TO READ A MODE COMMAND ****/


/* mb_readchar
 *  processes one mode character
 *  returns 1 if it ate up a param, otherwise 0
 *	written by binary
 */
int mb_readchar(Mode *mode, ModeFlag *mflag, char sign,
	char *param, char bounce)
{
	u_int32_t modetype = mflag->flag;
	aClient *who;
	Member *mp;
	u_int16_t x, *iptr;
	char add, *key;
	 
	/*
	 * Simple flag modes.
	 */
	if (modetype&MODE_SIMPLE)
	{
		if (modetype==MODE_RGSTR && MyConnect(mb.from))
			return 0;
				
		if (sign == '+')
			mode->mode |= modetype;
		else
			mode->mode &= ~modetype;
		
		return 0;
	}

	add = (sign=='+')?1:0;
	if (bounce==1) sign = add ? '-' : '+';

	/*
	 * Member Flags (ov)
	 * Any additional member flags shall be handled here.
	 */	
	if (modetype&MODE_MEMBER)
	{
		if (!param)
			return 0;
		if (!(who = find_chasing(mb.from, param, NULL)))
			return 1;
		if (!(mp = find_member(mb.chptr, who)))
		{
			send_num(mb.from, ERR_USERNOTINCHANNEL, who->name,
				mb.chptr->chname);
			return 1;
		}
		x = mp->flags;
		if (add)
			x |= mflag->extra;
		else
			x &= ~mflag->extra;
		if (x != mp->flags || IsULine(mb.from))
		{
			if (bounce != 1) mp->flags = x;
			mb_writep(sign, mflag->c, who->name);
		}
		return 1;
	}
	
	/*
	 * Channel Ban
	 */	
	if (modetype == MODE_BAN)
	{
		if (!param) return 0;
		param = pretty_mask(param);
		if (bounce != 1)
		{
			if ((!add && del_banid(mb.from, mb.chptr, param))
			   ||(add && add_banid(mb.from, mb.chptr, param)))
				if (!IsULine(mb.from))
					return 1;
		}
		else if (add == find_banid(mb.chptr, param))
			return 1;
		mb_writep(sign, 'b', param);
		return 1;
	}
	
	/*
	 * Modes utilizing integer parameters
	 */
	if (modetype&MODE_INTPARAM)
	{
		iptr = (u_int16_t *)((void *)mode+(size_t)mflag->extra);
		if (add)
		{
			if (param)
			{
				x = atoi(param);
				if ((bounce != BOUNCE_SYNC) || (x > *iptr))
					*iptr = x;
				return 1;
			}
			else if (!*iptr) /* set equal to 1 only if not set */
				*iptr = 1;
		}
		else
			*iptr = 0;
		return 0;
	}
		
	/*
	 * Modes utilizing string parameters of size KEYLEN
	 */
	if (modetype&MODE_STRPARAM)
	{
		if (!param)
			return 0;
		key = (char *)((void *)mode+(size_t)mflag->extra);
		if (!add)
			*key = '\0';
		else if (bounce != BOUNCE_SYNC || strcmp(param, key) > 0)
			strncpyzt(key, param, KEYLEN+1);
		return 1;
	}

	/*
	 * Clear all modes and bans
	 * Note: Mode +0 and -0 will do the same thing... never bounced.
	 */
	if (modetype == MODE_CLEAR && bounce != 1)
	{
		bzero(mode, sizeof(Mode));
		mb_unban(NULL, NULL);
		return 0;
	}

	return 0;
}

/* mb_readmode
 *	written by binary
 */
void mb_readmode(Mode *mode, char *p, char bounce)
{
	char c, *s = strsplit(&p, ' '), *para = strsplit(&p, ' ');
	char sign = '+';
	ModeFlag *mflag;

	for (; (c = *s); s++)
	{
		if (!(mflag = chan_modemap[(u_char)c]))
		{
			if (c == '+' || c == '-')
				sign = c;
			else if (MyClient(mb.from)) /* only yell at my users */
				send_num(mb.from, ERR_UNKNOWNMODE, c);
		}
		else if (mb_readchar(mode, mflag, sign, para, bounce))
			para = strsplit(&p, ' ');
	}
}

/* bounce_mode -- written by binary
 *	User or server is NOT authorized to change the mode.  This takes care
 * of making the bounce string and sending it out.
 */
void bounce_mode(aChannel *chptr, aClient *sptr, char *para)
{
	Mode	badmode;
	
	bcopy(&chptr->mode, &badmode, sizeof(Mode));
	mb_init(&me, sptr, chptr, chptr->creationtime, '\0');
	mb_readmode(&badmode, para, BOUNCE_YES);
	mb_sendall(&badmode, &chptr->mode, 1); /* must send for timestamp */
}

/* do_mode -- written by binary
 *	User or server is authorized to do the mode.  This takes care of
 * setting the mode and relaying it to other users and servers.
 */
void do_mode(aChannel *chptr, aClient *sptr, char *para, time_t sendts)
{
	char	sendempty = 0;
	char	bounce = BOUNCE_NO;
	Mode	oldmode;

	if (IsServer(sptr) && sendts != 0)
	{
		if (sendts == chptr->creationtime)
			bounce = BOUNCE_SYNC;
		else if (sendts < chptr->creationtime || !chptr->creationtime)
		{
			sendempty = 1; /* send in order to sync ts */
			chptr->creationtime = sendts;
		}
		else /* (sendts > chptr->creationtime) */
		{
			sendts = chptr->creationtime;
			sendto_one(sptr, "MODE %s + %d",
				chptr->chname, chptr->creationtime);
		}
	}
	
	bcopy(&chptr->mode, &oldmode, sizeof(Mode));
	mb_init(sptr, NULL, chptr, sendts, '\0');
	mb_readmode(&chptr->mode, para, bounce);
	mb_sendall(&oldmode, &chptr->mode, sendempty);
}



 /**** PROTOCOL FUNCTIONS FOR CHANNEL-MODE-RELATED COMMANDS ****/


/*
 * m_mode -- rewritten by binary (binary@darkfire.net)
 *  (should hopefully be more readable)
 *
 * parv[1] - channel
 */
int m_mode(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	time_t sendts = TS_NONE;
	Ban *ban;
	aChannel *chptr;
	Member *mp;
	char *s;

	if (check_registered(sptr) || check_params(sptr, "MODE", parc, 1))
		return 0;
	
	if (*parv[1] != '#')
		return m_umode(cptr, sptr, parc, parv);
	
	if (!(chptr = find_channel(parv[1], sptr)))
		return 0;

	if (parc < 3)
	{
		mb_init(NULL, sptr, chptr, 0, '+');
		mb_getmodes(IsMember(sptr, chptr)); /* send params if member */
		send_num(sptr, RPL_CHANNELMODEIS, chptr->chname, mb.mbuf, mb.pbuf);
		send_num(sptr, RPL_CREATIONTIME, chptr->chname, chptr->creationtime);
		return 0;
	}
	
	if (IsPerson(sptr))
	{
		if (parc<4 && ((*parv[2]=='b' && parv[2][1]=='\0')
			|| (parv[2][1]=='b' && parv[2][2] == '\0' &&
			*parv[2] == '+')))
		{
			for (ban=chptr->banlist; ban; ban=ban->next)
				send_num(sptr, RPL_BANLIST,
					chptr->chname, ban->banstr,
					ban->who, ban->when);
			send_num(sptr, RPL_ENDOFBANLIST, chptr->chname);
			return 0;
		}
		if (!IsULine(sptr) && (!(mp = find_member(chptr, sptr))
			|| !(mp->flags & CHFL_CHANOP))
			&& (cptr == sptr || !IsSAdmin(sptr)))
		{	
			if (cptr == sptr)
			{
				send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
				return 0;
			}
			if (mp)
				sendto_one(cptr, "MODE %s -o %s 0",
					chptr->chname, sptr->name);
			bounce_mode(chptr, cptr, parv[2]);
			return 0;
		}
	}
	else if (IsServer(sptr))
	{
		if (!IsULine(sptr) && (s = rindex(parv[2],' '))
			&& (sendts = atoi(s+1)) && chptr->creationtime
			&& sendts>chptr->creationtime)
		{
			bounce_mode(chptr, cptr, parv[2]);
			return 0;
		}
	}

	do_mode(chptr, sptr, parv[2], sendts);
        
	return 0;
}

/*
 * m_samode
 * parv[1] = channel
 * parv[2] = modes
 * -taz
 */
int m_samode(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	aChannel *chptr;

	if (check_registered(cptr) || check_privs(sptr, UMODE_SADMIN, 0))
		return 0;
	
	if (check_params(sptr, "SAMODE", parc, 2)
	   || !(chptr = find_channel(parv[1], sptr)))
		return 0;
	
	do_mode(chptr, sptr, parv[2], TS_SAMODE);

	return 0;
}

/*
** m_rmban - unban stuff
**  parv[1] = channel
**  parv[2] = mask (if none, self assumed)
**  parv[3] = reason (if used by sa)
**
**  by RedLightning and sedition
**  expanded/renamed/etc by binary
*/
int m_rmban(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	Member *mp;
	aChannel *chptr;
	aClient *acptr = sptr;
	char *mask = NULL; 

	
	if (check_registered_user(cptr) || check_params(sptr, "RMBAN", parc, 1))
		return 0;	
	if (!(chptr = find_channel(parv[1], sptr)))
		return 0;

	if (!((mp = find_member(chptr, sptr)) && (mp->flags&CHFL_CHANOP))
		&& !IsULine(sptr))
	{
		if (!IsSAdmin(sptr) || !parv[3])
		{
			send_num(sptr, ERR_CHANOPRIVSNEEDED, chptr->chname);
			return 0;
		}
		sendout_wallops("%s used unbanme %s %s (%s)", sptr->name,
			chptr->chname, parv[2]?parv[2]:"on self",
			parv[3]);
	}
	if (!MyClient(sptr))
		sptr = &me; /* allow services to use w/o fake direction */
	
	if (parv[2] && !(acptr = find_chasing(NULL, parv[2], NULL)))
		mask = parv[2];

	mb_init(sptr, NULL, chptr, TS_NONE, '\0');
	mb_unban(acptr, mask);
	mb_flush(0);

	sendto_one(sptr, ":%s NOTICE %s :Removed all bans on %s matching %s",
		me.name, sptr->name, parv[1], parv[2]?parv[2]:sptr->name);

	return 0;
}

/*
** m_unbanme
**
**  allows SA to unban self from a channel
*/
int m_unbanme(aClient *cptr, aClient *sptr, int parc, char *parv[])
{
	parv[3] = "UNBANME";
	return m_rmban(cptr, sptr, parc, parv);
}
