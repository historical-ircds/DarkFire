/************************************************************************
 *   IRC - Internet Relay Chat, ircd/channel.h
 *   Copyright (C) 1990 Jarkko Oikarinen
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__channel_include__
#define __channel_include__
#define CREATE 1	/* whether a channel should be
			   created or just tested for existance */
#define	BOUNCE_NO	0
#define	BOUNCE_YES	1
#define	BOUNCE_SYNC	2
#define	SAMODE_YES	1
#define	SAMODE_NO	0
#define	TS_NONE		(0)
#define	TS_SAMODE	(-2)


#define NullChn	((aChannel *)0)

#define ChannelExists(n)	(find_channel(n) != NullChn)

#define IsULine(sptr)	(sptr->flags & FLAGS_ULINE)

/* NOTE: Timestamps will be added to MODE-commands, so never make
 * RESYNCMODES and MODEPARAMS higher than MAXPARA-3. DALnet servers
 * before Dreamforge aren't safe with more than six. -Donwulff
 */
#include "msg.h"
#define	MAXMODEPARAMS	(12)		/* Maximum modes processed */
#define RESYNCMODES	8		/* Max modes per MODE in resync */

#define	MODEBUFLEN	(64)
#define	ONEPARALEN	(NICKLEN+USERLEN+HOSTLEN+4)
#define MBSLEN		(BUFSIZE-ONEPARALEN-40-NICKLEN-USERLEN-HOSTLEN-CHANNELLEN)
#define PARABUFLEN	(MBSLEN+ONEPARALEN)

typedef struct
{
	aChannel *chptr; /* channel targeted by the mode being handled */
	aClient *dest; /* destination client; NULL means to all in channel */
	aClient *from; /* client who is sending the mode message(s) */

	int sendts; /* timestamp which will be sent out for this mode */	
	int params; /* number of parameters currently in parabuf */
	int sent; /* has anything been sent since modebuf initialized? */
	char sign; /* mode sign; +, -, or \0 */
	char fsign; /* initial sign when modebuf was initialized */
	char mbuf[MODEBUFLEN], *mpos; /* mode buffer and position */
	char pbuf[PARABUFLEN], *ppos; /* param buffer and position */
} ModeBuf;


#endif
