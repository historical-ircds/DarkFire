/****************************************************************************
 *
 *  alloc.h - struct definitions for dalloc.c and zalloc.c
 *
 *					Copyright (C) 2001 Garry Boyer
 *					All rights reserved.
 *
 */

#ifndef __DALLOC_H__
#define __DALLOC_H__

typedef struct sDMast DMast;

struct sDMast
{
	size_t size;
	int count;
	void *free;
	int blocks, used;
};

extern void da_masterinit(DMast *master, size_t size, int count);
extern void *da_alloc(DMast *master);
extern void *da_calloc(DMast *master);
extern void da_free(DMast *master, void *s);

#define ROUNDUP(x,y) ((x+y-1)/y*y)
#define DA_ADJ_LONG(x) (ROUNDUP(x,sizeof(long)))

#define DA_MASTER(name,size,count) DMast name =		{		      \
		DA_ADJ_LONG(size),					      \
		count,							      \
		NULL,0,0						      \
							};
						
#define DA_MASTER_D(name,size,bsize) DMast name =	{	   	      \
		DA_ADJ_LONG(size),					      \
		(bsize)/DA_ADJ_LONG(size),				      \
		NULL,0,0						      \
						  	};


typedef struct sZBlock ZBlock;
typedef struct sZMast ZMast;
typedef struct sZItem ZItem;

struct sZMast
{
	size_t size; /* size of each item */
	int count;   /* items per block */
	int bsize;   /* bytes per block */

	ZBlock *empty;
	ZBlock *last;
	ZBlock *full;
	
	int n_blocks;
	int n_items;
};

struct sZItem
{
	ZBlock *block;
	char data[0];
	ZItem *next; /* next is only used with nonallocated items */
};

struct sZBlock
{
	ZBlock *next;
	ZBlock *prev;
	ZMast *master;
	int n_free;
	
	ZItem *free;
	ZItem items[0];
	char data[0];
};

#define ZA_GETITEM(x) ((ZItem *)((char *)x-offsetof(ZItem,data)))
#define ZB_INRANGE(i,b,m) ((void*)i>(void*)b&&((m=b->master))&&(char*)i<((char*)b+m->bsize))

extern void *zalloc(ZMast *master);
extern void zfree(void *s);
extern void zmast_init(ZMast *master, size_t size, int count);
extern int zmast_freemem(ZMast *master, int n);

#define ZA_MASTER(name, size, count) ZMast name =	{		\
	ROUNDUP((size),sizeof(long)),					\
	(count),							\
	offsetof(ZBlock,items)+						\
	(ROUNDUP((size),sizeof(long))+offsetof(ZItem,data))*(count),	\
	NULL, NULL, NULL, 0, 0						\
							};

#define ZA_MASTER_D(name,size,bsize)					\
	ZA_MASTER(name,size,						\
	(bsize-offsetof(ZBlock,items))/					\
	(ROUNDUP(size,sizeof(long))-offsetof(ZItem,data)))
		
		
			
		

#endif

