/************************************************************************
 *   IRC - Internet Relay Chat, include/msg.h
 *   Copyright (C) 1990 Jarkko Oikarinen and
 *                      University of Oulu, Computing Center
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 1, or (at your option)
 *   any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef	__msg_include__
#define __msg_include__

/*
 * The tokens are in the ascii character range of 33-127, and we start
 * from 33 and just move up.  It would be nice to match then up so they
 * are slightly related to their string counterpart, but that makes it
 * too confusing when we want to add another one and need to make sure
 * we're not using one already used. -Cabal95
 *
 * As long as the #defines are kept statically placed, it will be fine.
 * We don't care/worry about the msgtab[] since it can be dynamic, but
 * the tokens it uses will still be static according to the messages
 * they represent.  In other words leave the #defines in order, if you're
 * going to add something, PUT IT AT THE END.  Do not even look for an
 * open spot somewhere, as that may lead to one type of message being
 * sent by server A to server B, but server B thinks its something else.
 * Remember, skip the : since its got a special use, and I skip the \ too
 * since it _may_ cause problems, but not sure.  -Cabal95
 * I'm skipping A and a as well, because some clients and scripts use
 * these to test if the server has already processed whole queue.
 * Since the client could request this protocol withhout the script
 * knowing it, I'm considering that reserved, and TRACE/A is now 'b'.
 * The normal msgtab should probably process this as special. -Donwulff
 */

#define MSG_PRIVATE	"PRIVMSG"	/* PRIV */
#define TOK_PRIVATE	"!"		/* 33 */
#define MSG_WHO		"WHO"		/* WHO  -> WHOC */
#define TOK_WHO		"\""		/* 34 */
#define MSG_WHOIS	"WHOIS"		/* WHOI */
#define TOK_WHOIS	"#"		/* 35 */
#define MSG_WHOWAS	"WHOWAS"	/* WHOW */
#define TOK_WHOWAS	"$"		/* 36 */
#define MSG_USER	"USER"		/* USER */
#define TOK_USER	"%"		/* 37 */
#define MSG_NICK	"NICK"		/* NICK */
#define TOK_NICK	"&"		/* 38 */
#define MSG_SERVER	"SERVER"	/* SERV */
#define TOK_SERVER	"'"		/* 39 */
#define MSG_LIST	"LIST"		/* LIST */
#define TOK_LIST	"("		/* 40 */
#define MSG_TOPIC	"TOPIC"		/* TOPI */
#define TOK_TOPIC	")"		/* 41 */
#define MSG_INVITE	"INVITE"	/* INVI */
#define TOK_INVITE	"*"		/* 42 */
#define MSG_VERSION	"VERSION"	/* VERS */
#define TOK_VERSION	"+"		/* 43 */
#define MSG_QUIT	"QUIT"		/* QUIT */
#define TOK_QUIT	","		/* 44 */
#define MSG_SQUIT	"SQUIT"		/* SQUI */
#define TOK_SQUIT	"-"		/* 45 */
#define MSG_KILL	"KILL"		/* KILL */
#define TOK_KILL	"."		/* 46 */
#define MSG_INFO	"INFO"		/* INFO */
#define TOK_INFO	"/"		/* 47 */
#define MSG_LINKS	"LINKS"		/* LINK */
#define TOK_LINKS	"0"		/* 48 */
#define	MSG_MAP		"MAP"		/* MAP */
#define TOK_MAP		"1"		/* 49 */
#define MSG_STATS	"STATS"		/* STAT */
#define TOK_STATS	"2"		/* 50 */
#define MSG_RKILL	"RKILL"		/* RKILL */
#define	TOK_RKILL	"3"		/* 51 */
#define MSG_HELP	"HELP"		/* HELP */
#define TOK_HELP	"4"		/* 52 */
#define MSG_ERROR	"ERROR"		/* ERRO */
#define TOK_ERROR	"5"		/* 53 */
#define MSG_AWAY	"AWAY"		/* AWAY */
#define TOK_AWAY	"6"		/* 54 */
#define MSG_CONNECT	"CONNECT"	/* CONN */
#define TOK_CONNECT	"7"		/* 55 */
#define MSG_PING	"PING"		/* PING */
#define TOK_PING	"8"		/* 56 */
#define MSG_PONG	"PONG"		/* PONG */
#define TOK_PONG	"9"		/* 57 */
#define MSG_OPER	"OPER"		/* OPER */
#define TOK_OPER	";"		/* 59 */
#define MSG_PASS	"PASS"		/* PASS */
#define TOK_PASS	"<"		/* 60 */
#define MSG_WALLOPS	"WALLOPS"	/* WALL */
#define TOK_WALLOPS	"="		/* 61 */
#define MSG_TIME	"TIME"		/* TIME */
#define TOK_TIME	">"		/* 62 */
#define MSG_NAMES	"NAMES"		/* NAME */
#define TOK_NAMES	"?"		/* 63 */
#define MSG_ADMIN	"ADMIN"		/* ADMI */
#define TOK_ADMIN	"@"		/* 64 */
// "A" not available
#define MSG_NOTICE	"NOTICE"	/* NOTI */
#define TOK_NOTICE	"B"		/* 66 */
#define MSG_JOIN	"JOIN"		/* JOIN */
#define TOK_JOIN	"C"		/* 67 */
#define MSG_PART	"PART"		/* PART */
#define TOK_PART	"D"		/* 68 */
#define MSG_LUSERS	"LUSERS"	/* LUSE */
#define TOK_LUSERS	"E"		/* 69 */
#define MSG_MOTD	"MOTD"		/* MOTD */
#define TOK_MOTD	"F"		/* 70 */
#define MSG_MODE	"MODE"		/* MODE */
#define TOK_MODE	"G"		/* 71 */
#define MSG_KICK	"KICK"		/* KICK */
#define TOK_KICK	"H"		/* 72 */
#define MSG_USERHOST	"USERHOST"	/* USER -> USRH */
#define TOK_USERHOST	"J"		/* 74 */
#define MSG_ISON	"ISON"		/* ISON */
#define TOK_ISON	"K"		/* 75 */
#define MSG_HUSH	"HUSH"		/* HUSH */
#define	TOK_HUSH	"M"		/* 77 */
#define MSG_UNHUSH	"UNHUSH"	/* UNHU */
#define	TOK_UNHUSH	"N"		/* 78 */
#define	MSG_REHASH	"REHASH"	/* REHA */
#define TOK_REHASH	"O"		/* 79 */
#define	MSG_RESTART	"RESTART"	/* REST */
#define TOK_RESTART	"P"		/* 80 */
#define	MSG_CLOSE	"CLOSE"		/* CLOS */
#define TOK_CLOSE	"Q"		/* 81 */
#define	MSG_DIE		"DIE"		/* DIE */
#define TOK_DIE		"R"		/* 82 */
#define	MSG_HASH	"HASH"		/* HASH */
#define TOK_HASH	"S"		/* 83 */
#define	MSG_DNS		"DNS"		/* DNS  -> DNSS */
#define TOK_DNS		"T"		/* 84 */
#define MSG_SILENCE	"SILENCE"	/* SILE */
#define TOK_SILENCE	"U"		/* 85 */
#define MSG_AKILL	"AKILL"		/* AKILL */
#define TOK_AKILL	"V"		/* 86 */
#define MSG_KLINE	"KLINE"		/* KLINE */
#define TOK_KLINE	"W"		/* 87 */
#define MSG_UNKLINE	"UNKLINE"	/* UNKLINE */
#define TOK_UNKLINE	"X"		/* 88 */
#define MSG_RAKILL	"RAKILL"	/* RAKILL */
#define TOK_RAKILL	"Y"		/* 89 */
#define MSG_GNOTICE	"GNOTICE"	/* GNOTICE */
#define TOK_GNOTICE	"Z"		/* 90 */
#define MSG_GOPER	"GOPER"		/* GOPER */
#define TOK_GOPER	"["		/* 91 */
#define MSG_GLOBOPS	"GLOBOPS"	/* GLOBOPS */
#define TOK_GLOBOPS	"]"		/* 93 */
#define MSG_LOCOPS	"LOCOPS"	/* LOCOPS */
#define TOK_LOCOPS	"^"		/* 94 */
#define MSG_PROTOCTL	"PROTOCTL"	/* PROTOCTL */
#define TOK_PROTOCTL	"_"		/* 95 */
#define MSG_WATCH	"WATCH"		/* WATCH */
#define TOK_WATCH	"`"		/* 96 */
#define MSG_TRACE	"TRACE"		/* TRAC */
#define TOK_TRACE	"b"		/* 98 */
#define MSG_SQLINE	"SQLINE"	/* SQLINE */
#define TOK_SQLINE	"c"		/* 99 */
#define MSG_UNSQLINE	"UNSQLINE"	/* UNSQLINE */
#define TOK_UNSQLINE	"d"		/* 100 */
#define MSG_SVSNICK	"SVSNICK"	/* SVSNICK */
#define TOK_SVSNICK	"e"		/* 101 */
#define MSG_SVSNOOP	"SVSNOOP"	/* SVSNOOP */
#define TOK_SVSNOOP	"f"		/* 102 */
#define MSG_IDENTIFY	"IDENTIFY"	/* IDENTIFY */
#define TOK_IDENTIFY	"g"		/* 103 */
#define MSG_SVSKILL	"SVSKILL"	/* SVSKILL */
#define TOK_SVSKILL	"h"		/* 104 */
#define MSG_NICKSERV	"NICKSERV"	/* NICKSERV */
#define MSG_NS		"NS"
#define TOK_NICKSERV	"i"		/* 105 */
#define MSG_CHANSERV	"CHANSERV"	/* CHANSERV */
#define MSG_CS		"CS"
#define TOK_CHANSERV	"j"		/* 106 */
#define MSG_OPERSERV	"OPERSERV"	/* OPERSERV */
#define MSG_OS		"OS"
#define TOK_OPERSERV	"k"		/* 107 */
#define MSG_MEMOSERV	"MEMOSERV"	/* MEMOSERV */
#define MSG_MS		"MS"
#define TOK_MEMOSERV	"l"		/* 108 */
#define MSG_SERVICES	"SERVICES"	/* SERVICES */
#define TOK_SERVICES	"m"		/* 109 */
#define MSG_SVSMODE	"SVSMODE"	/* SVSMODE */
#define TOK_SVSMODE	"n"		/* 110 */
#define MSG_SAMODE	"SAMODE"	/* SAMODE */
#define TOK_SAMODE	"o"		/* 111 */
#define MSG_CHATOPS	"CHATOPS"	/* CHATOPS */
#define TOK_CHATOPS	"p"		/* 112 */
#define MSG_ZLINE    	"ZLINE"		/* ZLINE */
#define TOK_ZLINE	"q"		/* 113 */
#define MSG_UNZLINE  	"UNZLINE"	/* UNZLINE */
#define TOK_UNZLINE	"r"		/* 114 */
#define	MSG_UNBANME	"UNBANME"	/* UNBANME */
#define MSG_RMBAN	"RMBAN"		/* RMBAN */
#define	TOK_UNBANME	"u"		/* 117 */
#define	MSG_MKILL	"MKILL"		/* MKILL */
#define	TOK_MKILL	"v"		/* 118 */
#define	MSG_FQUIT	"FQUIT"		/* FQUIT */
#define	TOK_FQUIT	"w"		/* 119 */
#define	MSG_QOTD	"QOTD"		/* QOTD */
#define TOK_QOTD	"x"		/* 120 */

#define MAXPARA    15 

extern int m_private(), m_topic(), m_join(), m_part(), m_mode(), m_svsmode();
extern int m_ping(), m_pong(), m_wallops(), m_kick(), m_svsnick();
extern int m_nick(), m_error(), m_notice(), m_samode(), m_svsnoop();
extern int m_invite(), m_quit(), m_kill(), m_svskill(), m_identify();
extern int m_akill(), m_kline(), m_unkline(), m_rakill(), m_sqline();
extern int m_zline(), m_unzline();
extern int m_gnotice(), m_goper(), m_globops(), m_locops(), m_unsqline();
extern int m_chatops(), m_rmban();
extern int m_protoctl(), m_unbanme(), m_mkill(), m_fquit(), m_qotd();
extern int m_motd(), m_who(), m_whois(), m_user(), m_list();
extern int m_server(), m_info(), m_links(), m_summon(), m_stats();
extern int m_users(), m_version(), m_help(), m_map();
extern int m_nickserv(), m_operserv(), m_chanserv(), m_memoserv();
extern int m_services(), m_identify();
extern int m_squit(), m_away(), m_connect(), m_hush(), m_unhush();
extern int m_oper(), m_pass(), m_trace();
extern int m_time(), m_names(), m_admin();
extern int m_lusers(), m_umode(), m_close();
extern int m_motd(), m_whowas(), m_silence();
extern int m_userhost(), m_ison(), m_watch();
extern int m_servset(), m_servlist();
extern int m_rehash(), m_restart(), m_die(), m_dns(), m_hash();
extern int m_noshortn(),m_noshortc(),m_noshortm(),m_noshorto();

extern Msg ms_private, ms_notice, ms_mode, ms_ison, ms_join, ms_pong, ms_nick,
	ms_part, ms_quit, ms_whois, ms_ping, ms_away, ms_topic, ms_invite,
	ms_kick, ms_hush, ms_unhush, ms_protoctl, ms_watch, ms_server,
	ms_squit, ms_who, ms_whowas, ms_list, ms_names, ms_userhost,
	ms_trace, ms_pass, ms_lusers, ms_time, ms_oper, ms_connect,
	ms_version, ms_stats, ms_links, ms_map, ms_admin, ms_svsmode,
	ms_samode, ms_svskill, ms_svsnick, ms_svsnoop, ms_chanserv, ms_cs,
	ms_nickserv, ms_ns, ms_operserv, ms_os, ms_memoserv, ms_ms,
	ms_services, ms_identify, ms_help, ms_motd, ms_close, ms_silence,
	ms_akill, ms_sqline, ms_unsqline, ms_kline, ms_unkline, ms_zline,
	ms_unzline, ms_rakill, ms_gnotice, ms_goper, ms_globops, ms_chatops,
	ms_locops, ms_unbanme, ms_rmban, ms_mkill, ms_fquit, ms_kill,
	ms_qotd, ms_hash, ms_dns, ms_rehash, ms_restart, ms_die, ms_rkill;

#ifdef MSGTAB
struct Message *msgmap[256];

Msg ms_private   = { MSG_PRIVATE, m_private,  0, 2,       TOK_PRIVATE, 0L };
Msg ms_notice    = { MSG_NOTICE,  m_notice,   0, 2,       TOK_NOTICE,  0L };
Msg ms_mode      = { MSG_MODE,    m_mode,     0, 2,       TOK_MODE,    0L };
Msg ms_ison      = { MSG_ISON,    m_ison,     0, 1,       TOK_ISON,    0L };
Msg ms_join      = { MSG_JOIN,    m_join,     0, MAXPARA, TOK_JOIN,    0L };
Msg ms_pong      = { MSG_PONG,    m_pong,     0, MAXPARA, TOK_PONG,    0L };
Msg ms_nick      = { MSG_NICK,    m_nick,     0, MAXPARA, TOK_NICK,    0L };
Msg ms_part      = { MSG_PART,    m_part,     0, 2,       TOK_PART,    0L };
Msg ms_quit      = { MSG_QUIT,    m_quit,     0, 1,       TOK_QUIT,    0L };
Msg ms_whois     = { MSG_WHOIS,   m_whois,    0, MAXPARA, TOK_WHOIS,   0L };
Msg ms_ping      = { MSG_PING,    m_ping,     0, MAXPARA, TOK_PING,    0L };
Msg ms_user      = { MSG_USER,    m_user,     0, MAXPARA, TOK_USER,    0L };
Msg ms_away      = { MSG_AWAY,    m_away,     0, 1,       TOK_AWAY,    0L };
Msg ms_topic     = { MSG_TOPIC,   m_topic,    0, MAXPARA, TOK_TOPIC,   0L };
Msg ms_invite    = { MSG_INVITE,  m_invite,   0, 2,       TOK_INVITE,  0L };
Msg ms_kick      = { MSG_KICK,    m_kick,     0, 3,       TOK_KICK,    0L };
Msg ms_wallops   = { MSG_WALLOPS, m_wallops,  0, 1,       TOK_WALLOPS, 0L };
Msg ms_error     = { MSG_ERROR,   m_error,    0, 1,       TOK_ERROR,   0L };
Msg ms_kill      = { MSG_KILL,    m_kill,     0, MAXPARA, TOK_KILL,    0L };
Msg ms_rkill     = { MSG_RKILL,   m_kill,     0, 2,       TOK_RKILL,   0L };
Msg ms_hush      = { MSG_HUSH,    m_hush,     0, 3,       TOK_HUSH,    0L };  
Msg ms_unhush    = { MSG_UNHUSH,  m_unhush,   0, MAXPARA, TOK_UNHUSH,  0L };  
Msg ms_protoctl  = { MSG_PROTOCTL,m_protoctl, 0, MAXPARA, TOK_PROTOCTL,0L };
Msg ms_watch     = { MSG_WATCH,   m_watch,    0, 1,       TOK_WATCH,   0L }; 
Msg ms_server    = { MSG_SERVER,  m_server,   0, MAXPARA, TOK_SERVER,  0L };
Msg ms_squit     = { MSG_SQUIT,   m_squit,    0, MAXPARA, TOK_SQUIT,   0L };
Msg ms_who       = { MSG_WHO,     m_who,      0, MAXPARA, TOK_WHO,     0L };
Msg ms_whowas    = { MSG_WHOWAS,  m_whowas,   0, MAXPARA, TOK_WHOWAS,  0L };
Msg ms_list      = { MSG_LIST,    m_list,     0, MAXPARA, TOK_LIST,    0L };
Msg ms_names     = { MSG_NAMES,   m_names,    0, MAXPARA, TOK_NAMES,   0L };
Msg ms_userhost  = { MSG_USERHOST,m_userhost, 0, 1,       TOK_USERHOST,0L };
Msg ms_trace     = { MSG_TRACE,   m_trace,    0, MAXPARA, TOK_TRACE,   0L };
Msg ms_pass      = { MSG_PASS,    m_pass,     0, MAXPARA, TOK_PASS,    0L };
Msg ms_lusers    = { MSG_LUSERS,  m_lusers,   0, MAXPARA, TOK_LUSERS,  0L };
Msg ms_time      = { MSG_TIME,    m_time,     0, MAXPARA, TOK_TIME,    0L };
Msg ms_oper      = { MSG_OPER,    m_oper,     0, MAXPARA, TOK_OPER,    0L };
Msg ms_connect   = { MSG_CONNECT, m_connect,  0, MAXPARA, TOK_CONNECT, 0L };
Msg ms_version   = { MSG_VERSION, m_version,  0, MAXPARA, TOK_VERSION, 0L };
Msg ms_stats     = { MSG_STATS,   m_stats,    0, MAXPARA, TOK_STATS,   0L };
Msg ms_links     = { MSG_LINKS,   m_links,    0, MAXPARA, TOK_LINKS,   0L };
Msg ms_map       = { MSG_MAP,     m_map,      0, MAXPARA, TOK_MAP,     0L };  
Msg ms_admin     = { MSG_ADMIN,   m_admin,    0, MAXPARA, TOK_ADMIN,   0L };
Msg ms_svsmode   = { MSG_SVSMODE, m_svsmode,  0, MAXPARA, TOK_SVSMODE, 0L };
Msg ms_samode    = { MSG_SAMODE,  m_samode,   0, 2,       TOK_SAMODE,  0L };
Msg ms_svskill   = { MSG_SVSKILL, m_svskill,  0, MAXPARA, TOK_SVSKILL, 0L };
Msg ms_svsnick   = { MSG_SVSNICK, m_svsnick,  0, MAXPARA, TOK_SVSNICK, 0L };
Msg ms_svsnoop   = { MSG_SVSNOOP, m_svsnoop,  0, MAXPARA, TOK_SVSNOOP, 0L };
Msg ms_chanserv  = { MSG_CHANSERV,m_chanserv, 0, 1,       TOK_CHANSERV,0L };
Msg ms_cs        = { MSG_CS,	  m_chanserv, 0, 1,       TOK_CHANSERV,0L };
Msg ms_nickserv  = { MSG_NICKSERV,m_nickserv, 0, 1,       TOK_NICKSERV,0L };
Msg ms_ns        = { MSG_NS,	  m_nickserv, 0, 1,       TOK_NICKSERV,0L };
Msg ms_operserv  = { MSG_OPERSERV,m_operserv, 0, 1,       TOK_OPERSERV,0L };
Msg ms_os        = { MSG_OS,      m_operserv, 0, 1,       TOK_OPERSERV,0L };
Msg ms_memoserv  = { MSG_MEMOSERV,m_memoserv, 0, 1,       TOK_MEMOSERV,0L };
Msg ms_ms        = { MSG_MS,      m_memoserv, 0, 1,       TOK_MEMOSERV,0L };
Msg ms_services  = { MSG_SERVICES,m_services, 0, 1,       TOK_SERVICES,0L };
Msg ms_identify  = { MSG_IDENTIFY,m_identify, 0, 1,       TOK_IDENTIFY,0L };
Msg ms_help      = { MSG_HELP,    m_help,     0, 1,       TOK_HELP,    0L };
Msg ms_info      = { MSG_INFO,    m_info,     0, MAXPARA, TOK_INFO,    0L };
Msg ms_motd      = { MSG_MOTD,    m_motd,     0, MAXPARA, TOK_MOTD,    0L };
Msg ms_close     = { MSG_CLOSE,   m_close,    0, MAXPARA, TOK_CLOSE,   0L };
Msg ms_silence   = { MSG_SILENCE, m_silence,  0, MAXPARA, TOK_SILENCE, 0L };
Msg ms_akill     = { MSG_AKILL,   m_akill,    0, MAXPARA, TOK_AKILL,   0L };
Msg ms_sqline    = { MSG_SQLINE,  m_sqline,   0, MAXPARA, TOK_SQLINE,  0L };
Msg ms_unsqline  = { MSG_UNSQLINE,m_unsqline, 0, MAXPARA, TOK_UNSQLINE,0L };
Msg ms_kline     = { MSG_KLINE,   m_kline,    0, 2,       TOK_KLINE,   0L };
Msg ms_unkline   = { MSG_UNKLINE, m_unkline,  0, MAXPARA, TOK_UNKLINE, 0L };
Msg ms_zline     = { MSG_ZLINE,   m_zline,    0, MAXPARA, TOK_ZLINE,   0L };
Msg ms_unzline   = { MSG_UNZLINE, m_unzline,  0, MAXPARA, TOK_UNZLINE, 0L };
Msg ms_rakill    = { MSG_RAKILL,  m_rakill,   0, MAXPARA, TOK_RAKILL,  0L };
Msg ms_gnotice   = { MSG_GNOTICE, m_gnotice,  0, 1,       TOK_GNOTICE, 0L };
Msg ms_goper     = { MSG_GOPER,   m_goper,    0, 1,       TOK_GOPER,   0L };
Msg ms_globops   = { MSG_GLOBOPS, m_globops,  0, 1,       TOK_GLOBOPS, 0L };
Msg ms_chatops   = { MSG_CHATOPS, m_chatops,  0, 1,       TOK_CHATOPS, 0L };
Msg ms_locops    = { MSG_LOCOPS,  m_locops,   0, 1,       TOK_LOCOPS,  0L };
Msg ms_unbanme   = { MSG_UNBANME, m_unbanme,  0, MAXPARA, TOK_UNBANME, 0L };
Msg ms_rmban     = { MSG_RMBAN,   m_rmban,    0, MAXPARA, TOK_UNBANME, 0L };
Msg ms_mkill     = { MSG_MKILL,   m_mkill,    0, 2,       TOK_MKILL,   0L };
Msg ms_fquit     = { MSG_FQUIT,   m_fquit,    0, MAXPARA, TOK_FQUIT,   0L };  
Msg ms_qotd      = { MSG_QOTD,    m_qotd,     0, 1,       TOK_QOTD,    0L };
Msg ms_hash      = { MSG_HASH,    m_hash,     0, MAXPARA, TOK_HASH,    0L };
Msg ms_dns       = { MSG_DNS,     m_dns,      0, MAXPARA, TOK_DNS,     0L };
Msg ms_rehash    = { MSG_REHASH,  m_rehash,   0, MAXPARA, TOK_REHASH,  0L };
Msg ms_restart   = { MSG_RESTART, m_restart,  0, MAXPARA, TOK_RESTART, 0L };
Msg ms_die       = { MSG_DIE,     m_die,      0, MAXPARA, TOK_DIE,     0L };

struct Message *msgtab[] =
{
	&ms_private,
	&ms_notice,
	&ms_mode,
	&ms_join,
	&ms_nick,
	&ms_pong,
	&ms_ison,
	&ms_part,
	&ms_quit,
	&ms_svsmode,
	&ms_whois,
	&ms_user,
	&ms_topic,
	&ms_kick,
	&ms_away,
	&ms_ping,
	&ms_userhost,
	&ms_who,
	&ms_whowas,
	&ms_invite,
	&ms_hush,
	&ms_unhush,
	&ms_wallops,
	&ms_protoctl,
	&ms_watch,
	&ms_server,
	&ms_squit,
	&ms_list,
	&ms_names,
	&ms_trace,
	&ms_pass,
	&ms_lusers,
	&ms_time,
	&ms_oper,
	&ms_connect,
	&ms_version,
	&ms_stats,
	&ms_links,
	&ms_map,
	&ms_admin,
	&ms_samode,
	&ms_svskill,
	&ms_svsnick,
	&ms_svsnoop,
	&ms_chanserv,
	&ms_cs,
	&ms_nickserv,
	&ms_ns,
	&ms_operserv,
	&ms_os,
	&ms_memoserv,
	&ms_ms,
	&ms_services,
	&ms_identify,
	&ms_help,
	&ms_motd,
	&ms_close,
	&ms_silence,
	&ms_akill,
	&ms_sqline,
	&ms_unsqline,
	&ms_kline,
	&ms_unkline,
	&ms_zline,
	&ms_unzline,
	&ms_rakill,
	&ms_gnotice,
	&ms_goper,
	&ms_globops,
	&ms_chatops,
	&ms_locops,
	&ms_unbanme,
	&ms_rmban,
	&ms_info,
	&ms_mkill,
	&ms_fquit,
	&ms_kill,
	&ms_rkill,
	&ms_qotd,
	&ms_hash,
	&ms_dns,
	&ms_rehash,
	&ms_restart,
	&ms_die,
	NULL
};

#else
extern struct Message *msgtab[];
extern struct Message *msgmap[256];
#endif
#endif /* __msg_include__ */
